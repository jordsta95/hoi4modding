if(document.querySelector('.avatar')){	
	var consoleMsg1 = '%cGot experience with Javascript and/or Laravel/PHP?';
	var consoleMsg2 = '%cLooking for a project to help out on, and can add to your portfolio?';
	var consoleMsg3 = '%cWe\'re looking for someone like you!';
	var consoleMsg4 = '%cWhether is jQuery, Vue.js, or just plain old PHP, you could help speed up the HOI4 Modding development process, keeping the site up to date with the latest game updates, whilst also creating innovative tools to help the community create great mods.';
	var consoleMsg5 = 'Want to join? Email: development@hoi4modding.com along with your name/any relevant experience/language(s) you understand, code and communication for more info.';
	var lineSplit = '%c                                                      ';
	console.log(lineSplit, 'background: #1b93e1');
	console.log(consoleMsg1, 'font-size: 18px; color: #1b93e1; font-weight: 800; text-align: center; font-family: Arial, sans-serif;');
	console.log(consoleMsg2, 'font-size: 17px; color: #1675b3; text-align: center; font-family: Arial, sans-serif;');
	console.log(consoleMsg3, 'font-size: 16px; color: #333333; text-align: center; font-weight: 600; font-family: Arial, sans-serif;');
	console.log(consoleMsg4, 'font-family: Arial, sans-serif;');
	console.log(consoleMsg5);
	console.log(lineSplit, 'background: #1b93e1');
}
$('body').on('click', '.country-tab', function(){
	$(this).parent().find('.active').removeClass('active');
	$(this).addClass('active');
});

$('.country-leader-icon').on('click', function(){
	var image = $(this).attr('src');
	if($(this).closest('#choose-leader').attr('data-for') == 'leader'){
		$('.country-leader .picture').css('backgroundImage', 'url('+image+')');
		$('.country-leader .picture').attr('data-image', image);	
	}
	if($(this).closest('#choose-leader').attr('data-for') == 'military'){
		$('#country-military .image').attr('src', image);
		$('#choose-leader').attr('data-for', 'leader');
	}
	$(this).closest('.focus-panel').slideUp();
});

$('.country-idea').on('click', function(){
	$('.country-idea-add').attr('data-idea', $(this).attr('data-idea'));
	$('.country-idea-add').attr('data-needs-additional', $(this).attr('data-needs-additional'));
	$('.country-idea-add').attr('data-for', $(this).attr('data-for'));

	$('#existing-ideas').html('<i class="fa fa-spinner fa-spin"></i>');

	$.get('/country/edit/'+$('#country-id').attr('data-country-id')+'/get-ideas?type='+$(this).attr('data-idea'), function(data){
		$('#existing-ideas').html(data);
	});
});

$('#existing-ideas').on('click', '.existing-idea .fa-times', function(){
	var idea = $(this).closest('.existing-idea');
	idea.hide();
	$.post('/country/delete-idea/', {
		_token:$('[name="_token"]').val(),
		id:idea.attr('data-id')
	}, function(data){
		$('.country-idea[data-idea="'+$('.country-idea-add').attr('data-idea')+'"]').text((parseInt($('.country-idea[data-idea="'+$('.country-idea-add').attr('data-idea')+'"]').text()) - 1));
	});
});

$('.country-idea-add').on('click', function(){
	$('#choose-person-idea,#choose-non-person-idea,#choose-any-idea').attr('data-idea-to-add',$(this).attr('data-idea'));
	$('#choose-person-idea,#choose-non-person-idea,#choose-any-idea').attr('data-needs-additional',$(this).attr('data-needs-additional'));
	$('#'+$(this).attr('data-for')).slideDown();
	$('#edit-ideas').slideUp();

});

$('.country-idea-icon').on('click', function(){
	var parent = $(this).closest('.focus-panel');
	if(parent.attr('data-needs-additional') == 'yes'){
		$('#idea-additional-info').slideDown();
		$('#idea-additional-info').attr('data-idea-to-add',parent.attr('data-idea-to-add'));
		$('#idea-additional-info .left .image').css('backgroundImage', 'url('+$(this).attr('src')+')');
		$('#idea-additional-info .left [name="gfx"]').val($(this).attr('src'));
		$('#choose-person-idea,#choose-non-person-idea').slideUp();
	}else{
		$('[data-idea="'+parent.attr('data-idea-to-add')+'"]').css('backgroundImage', 'url('+$(this).attr('src')+')');
		$(this).closest('.focus-panel').slideUp();
	}
});

$('.country-ideas-info .right button').on('click', function(){
	var parent = $(this).closest('.focus-panel');

	$.post('/country/edit/'+$('#country-id').attr('data-country-id')+'/add-idea', {
		_token:$('[name="_token"]').val(),
		country_id:$('#country-id').attr('data-country-id'),
		name: value('name','.country-ideas-info'),
		short_desc: value('short_desc','.country-ideas-info'),
		long_desc: value('long_desc','.country-ideas-info'),
		effect: value('effect','.country-ideas-info'),
		gfx: value('gfx','.country-ideas-info'),
		national_spirit_type: parent.attr('data-idea-to-add'),
	}, function(data){
		var total = parseInt($('.country-idea[data-idea="'+parent.attr('data-idea-to-add')+'"]').eq(0).text()) + 1;
		$('.country-idea[data-idea="'+parent.attr('data-idea-to-add')+'"]').text(total);
	});
});

$('#country-state').on('click', '.searched_states' , function(){
	addState($(this).attr('id').replace('state_',''), $(this).text());
});

function addState(stateId, name, core, controls){
	var hasCore = false;
	var hasControl = false;
	if(core != null){
		hasCore = core;
	}
	if(controls != null){
		hasControl = controls;
	}
	var id = Math.round((new Date()).getTime() / 1000)+(Math.floor(Math.random() * 100) + 1);
	var countryBox = '<div class="grid with-gap" data-id="new" data-state-id="'+stateId+'">';
		countryBox += '<div class="grid_start_1 grid_span_1 removeState">x</div>';
		countryBox += '<div class="grid_start_2 grid_span_5">';
			countryBox += name;
			countryBox += '<input class="stateValue" name="states[new]['+id+'][state_name]" style="display:none;" value="'+name+'">';
			countryBox += '<input class="stateValue" name="states[new]['+id+'][state_id]" style="display:none;" value="'+stateId+'">';
		countryBox += '</div>';
		countryBox += '<div class="grid_start_7 grid_span_3"><input class="core stateValue" value="1" name="states[new]['+id+'][core]" '+(hasCore ? ' checked ' : '')+' type="checkbox"></div>';
		countryBox += '<div class="grid_start_10 grid_span_3"><input class="controls stateValue" value="1" name="states[new]['+id+'][controls]" '+(hasControl ? ' checked ' : '')+'type="checkbox"></div>';
	countryBox += '</div>';
	$('.country-states').append(countryBox);
}

function removeState(stateId){
	var row = $('.state-box').find('[data-state-id="'+stateId+'"]');
	if(row.length){
		if(row.attr('data-id') == 'new'){
			row.remove();
		}else{
			row.find('.removeState').trigger('click');
		}
	}
}

$('.country-extra-stuff').on('click', '.removeState', function(){
	if($(this).parent().attr('data-id') != 'new'){
		$(this).parent().find('.removeStateOption').prop('checked', true);
		$(this).parent().hide();
	}else{
		$(this).parent().remove();
	}
});

$('.leader-trait').on('click', function(){
	$('#idea-additional-info [name="effect"]').val($(this).attr('data-id'));
	$('#country-leader-traits').slideUp();
});

$('.country-save').on('click', function(){

	if($('.stateValue').length){
		var states = $('.stateValue').serialize();
	}else{
		var states = '';
	}

	if($('.generalJSON').length){
		var generals = $('.generalJSON').serialize();
	}else{
		var generals = '';
	}

	research = '';

	$('body').find('.research-item').each(function(){
		research += $(this).attr('data-id')+',';
	});


	$.post('/country/edit/'+$('#country-id').attr('data-country-id'), {
		_token: value('_token'),
		date_id: $('body').find('[data-tab-type="date"] .active').attr('id'),
		ideology_id: $('body').find('[data-tab-type="ideology"] .active').attr('id'),
		ideology_support: value('ideology_support'),
		country_name: value('country_name'),
		leader_name: value('country_leader'),
		leader_gfx: $('body').find('.country-leader .picture').attr('data-image'),
		states: states,
		generals: generals,
		research: research
	},
	function(data){
		GENERICreload();
	});
});

$('.country').on('click', '[data-tab-type="date"] .country-tab', function(){
	$('.country-leader-container').html('<div class="loader">Select ideology for selected date</div>');
	$('[data-tab-type="ideology"]').html('<div class="loader"><i class="fa fa-spinner fa-spin"></i></div>');
	$.post('/country/get-tabs', {
		_token: value('_token'),
		date_id: $(this).attr('id')
	}, function(data){
		$('[data-tab-type="ideology"]').html(data);
	});
	$('.state-box').html('<div class="loader"><i class="fa fa-spinner fa-spin"></i></div>');
	$.post('/country/get-states', {
		_token: value('_token'),
		date_id: $(this).attr('id')
	}, function(data){
		$('.state-box').html(data);
		setReadOnly();
		regenMapStates();
	});
});

$('.country').on('click', '[data-tab-type="ideology"] .country-tab', function(){
	$('.country-leader-container').html('<div class="loader"><i class="fa fa-spinner fa-spin"></i></div>');
	$.post('/country/get-leader', {
		_token: value('_token'),
		ideology_id: $(this).attr('id')
	}, function(data){
		$('.country-leader-container').html(data);
		setReadOnly();
	});
});

$('#country-military .submit').on('click', function(){
	var id = Math.round((new Date()).getTime() / 1000)+(Math.floor(Math.random() * 100) + 1);
	var json = {};
	$('#country-military input,#country-military textarea').each(function(){
		json[$(this).attr('name')] = $(this).val();
	});
	json['field_marshal'] = $('#country-military [name="field_marshal"]').prop('checked');
	json['gfx'] = $('#country-military .image').attr('src');
	json['type'] = $('#country-military').attr('data-type');
	if($('#country-military').attr('data-id') == 'new'){
		var countryBox = '<div class="grid with-gap" data-id="new">';
			countryBox += '<div class="grid_start_1 grid_span_1 removeState">x</div>';
			countryBox += '<div class="grid_start_2 grid_span_5">';
				countryBox += $('#country-military .name').val();
				countryBox += '<textarea class="generalJSON" name="general[new]['+id+']" style="display:none;">'+JSON.stringify(json)+'</textarea>';
			countryBox += '</div>';
			countryBox += '<div class="grid_start_7 grid_span_3"><img src="'+$('#country-military .image').attr('src')+'" class="general-icon"></div>';
			countryBox += '<div class="grid_start_10 grid_span_3">'+$('#country-military [name="level"]').val()+'</div>';
		countryBox += '</div>';
		$('.country-'+$('#country-military').attr('data-type')).append(countryBox);
	}else{
		var dataId = '[data-id="'+$('#country-military').attr('data-id')+'"]';
		$('.generalJSON'+dataId).val(JSON.stringify(json));
		$('.editGeneral .name'+dataId).text($('#country-military .name').val());
		$('.general-icon'+dataId).attr('src', $('#country-military .image').attr('src'));
		$('.generalLevel'+dataId).text($('#country-military [name="level"]').val());
	}
});

$('#search-traits').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.country-trait').show();
	$('.country-trait').each(function(){
		var hide = true
		if($(this).attr('data-id').toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('.name').text().toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('textarea').val() != null){
			if($(this).find('textarea').val().toLowerCase().includes(val)){
				hide = false;
			}
		}


		if(hide){
			$(this).hide();
		}
	});
});

$('#search-leader-traits').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.leader-trait').show();
	$('.leader-trait').each(function(){
		var hide = true
		if($(this).attr('data-id').toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('.name').text().toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('textarea').val() != null){
			if($(this).find('textarea').val().toLowerCase().includes(val)){
				hide = false;
			}
		}


		if(hide){
			$(this).hide();
		}
	});
});

$('#search-research').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.country-research').show();
	$('.country-research').each(function(){
		var hide = true
		if($(this).attr('data-id').toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('.name').text().toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('textarea').val() != null){
			if($(this).find('textarea').val().toLowerCase().includes(val)){
				hide = false;
			}
		}


		if(hide){
			$(this).hide();
		}
	});
});

$('.editGeneral').on('click', function(){
	var json = JSON.parse($(this).find('.generalJSON').val());
	var type = $(this).closest('.country-general-wrapper').attr('data-type');
	$('#country-military [name="field_marshal"]').prop('checked', false)

	$('#country-military').attr('data-id', $(this).closest('.grid').attr('data-id'));
	$('#country-military .image').attr('src', json.gfx);
	$('#country-military [name="name"]').val(json.name);
	$('#country-military [name="traits"]').val(json.traits);
	$('#country-military [name="level"]').val(json.level);
	$('#country-military [name="attack_stat"]').val(json.attack_stat);
	$('#country-military [name="defence_stat"]').val(json.defence_stat);
	$('#country-military [name="supply_stat"]').val(json.supply_stat);
	$('#country-military [name="planning_stat"]').val(json.planning_stat);
	if(json.is_field_marshal){
		$('#country-military [name="field_marshal"]').prop('checked', true);
	}

	$('#country-military').slideDown();
});

$('.country-trait').on('click', function(){
	$('[name="traits"]').val($('[name="traits"]').val()+' '+$(this).attr('data-id'));
	$(this).hide();
});

$('.country [for="country-military"]').on('click', function(){
	$('.country-trait').show();
	$('#country-military').attr('data-type', $(this).attr('data-type'));
	$('#country-military .image').attr('src', $('#country-military .image').attr('data-default'));
	$('#country-military input,#country-military textarea').each(function(){
		$(this).val('');
	});
	$('#country-military').attr('data-id', 'new');
	$('#country-military [name="field_marshal"]').val('1');
	$('#country-military [name="field_marshal"]').prop('checked', false);
});

$('#country-military .image').on('click', function(){
	$('#choose-leader').attr('data-for', 'military');
});

$('#country-military .close').on('click', function(){
	$('#choose-leader').attr('data-for', 'leader');
});

$('#search-country-leaders').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.country-leader-icon').show();
	$('.country-leader-icon').each(function(){
		if($(this).attr('id') !== null){
			if(!$(this).attr('id').toLowerCase().includes(val)){
				$(this).hide();
			}
		}
	});
});

$('.country-research').on('click', function(){
	var countryBox = '<div class="grid with-gap" data-id="new">';
		countryBox += '<div class="grid_start_1 grid_span_1 removeState">x</div>';
		countryBox += '<div class="grid_start_2 grid_span_11 research-item" data-id="'+$(this).attr('data-id')+'">'+$(this).find('.name').text()+'</div>';
	countryBox += '</div>';
	$('body').find('.country-research-items').append(countryBox);
	$(this).hide();
});

$('[for="country-research"]').on('click', function(){
	$('.country-research').show();
});
setReadOnly();
function setReadOnly(){
	$('.readOnly').find('input,textarea').each(function(){
		$(this).attr('disabled', 'disabled');
	});
}

function value(name, parent){
	if(parent){
		return $('body').find(parent).find('[name="'+name+'"]').val();
	}else{
		return $('body').find('[name="'+name+'"]').val();
	}
}

$('.map-wrapper .increase').on('click', function(){
	zoomInMap();
});

$('.map-wrapper .decrease').on('click', function(){
	zoomOutMap();
});

$('.map-wrapper').bind('mousewheel DOMMouseScroll', function(event){
	event.preventDefault();
    if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
        zoomInMap();
    }
    else {
    	zoomOutMap();
    }
});

function regenMapStates(){
	if($('.map-wrapper').length){
		$('.map-wrapper g').removeAttr('data-core-state');
		$('.map-wrapper g').removeAttr('data-control-state');

		$('body').find('.country-states .grid.with-gap').each(function(){
			var stateId = $(this).attr('data-state-id');
			if($(this).find('.core').prop('checked') && !$(this).find('.removeStateOption').prop('checked')){
				showCoreState(stateId);
			}
			if($(this).find('.controls').prop('checked') && !$(this).find('.removeStateOption').prop('checked')){
				showControlState(stateId);
			}
		});
	}
}

regenMapStates();
$('.state-box').on('click', '[for="mapWrapper"]', function(){
	regenMapStates();
});

var mX, mY,
    $element  = $('.map-wrapper svg').parent();

function calculateDistance(elem, mouseX, mouseY) {
    return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left+(elem.width()/2)), 2) + Math.pow(mouseY - (elem.offset().top+(elem.height()/2)), 2)));
}

$('.map-wrapper svg').parent().mousemove(function(e) {  
    mX = e.pageX;
    mY = e.pageY;
    distance = calculateDistance($element, mX, mY);        
});

function zoomInMap(){
	var f = parseFloat($('.map-wrapper').attr('data-zoom'));
	var zoomable = false;
	if(f < 16){
		zoomable = true;
		f = f * 2;
	}
	var sl = $('.map-wrapper svg').parent().scrollLeft();
	var st = $('.map-wrapper svg').parent().scrollTop();
	$('.map-wrapper').attr('data-zoom', f);
	$('.map-wrapper svg').css('height', (f * 100) + '%');

	if(zoomable){
		$('.map-wrapper svg').parent().scrollLeft((sl * 2) + mX);
		$('.map-wrapper svg').parent().scrollTop((st * 2) + mY);
	}

}

function zoomOutMap(){
	var f = parseFloat($('.map-wrapper').attr('data-zoom'));
	if(f > 0.5){
		f = f / 2;
	}
	var sl = $('.map-wrapper svg').parent().scrollLeft();
	var st = $('.map-wrapper svg').parent().scrollTop();
	$('.map-wrapper').attr('data-zoom', f);
	$('.map-wrapper svg').css('height', (f * 100) + '%');
	if(f > 0.5){
		$('.map-wrapper svg').parent().scrollLeft((sl / 2) - mX);
		$('.map-wrapper svg').parent().scrollTop((st / 2) - mY);
	}
}

var stateJson = '';
if($('.all-states').length){
	stateJson = JSON.parse($('.all-states').text());
}

$('.map-wrapper g[id*="state-"]').on('click', function(){
	$('.state-overview').hide();
	$('.state-overview .core .checkbox').removeClass('active');
	$('.state-overview .controls .checkbox').removeClass('active');
	if($(this).attr('data-state-id')){
		var tag = $(this).attr('data-controls');
		var stateId = $(this).attr('data-state-id');
		var state = '';
		$.each(stateJson, function(){
			if(this.id == stateId){
				state = this;
			}
		});
		$('.state-overview .state-name').text(state.name);
		$('.state-overview').attr('data-state-id', state.id);
		$('.state-overview #hide-tag').attr('data-tag', tag);

		if($('.state-box').find('[data-state-id="'+state.id+'"]').length){
			if($('.state-box').find('[data-state-id="'+state.id+'"] .core').prop('checked')){
				$('.state-overview .core .checkbox').addClass('active');
			}
			if($('.state-box').find('[data-state-id="'+state.id+'"] .controls').prop('checked')){
				$('.state-overview .controls .checkbox').addClass('active');
			}
		}
		$('.state-overview').slideDown();
	}
});

$('.state-overview .state-info .wrapper').on('click', function(){
	$(this).find('.checkbox').toggleClass('active');
});


$('.state-overview #hide-tag').on('click', function(){
	if($(this).attr('data-tag') != ''){
		$('.map-wrapper g[data-controls="'+$(this).attr('data-tag')+'"]').hide();
	}
});

$('.state-overview .update').on('click', function(){
	if($('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').length){
		var stateBox = $('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]');
		if($('.state-overview .core').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')){
			stateBox.find('removeStateOption').prop('checked', false);
			stateBox.find('core').prop('checked', $('.state-overview .core .checkbox').hasClass('active'));
			stateBox.find('controls').prop('checked', $('.state-overview .controls').hasClass('active'));
		}else{
			stateBox.find('removeStateOption').prop('checked', true);
		}
	}
	if(($('.state-overview .core .checkbox').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')) && !$('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').length){
		addState($('.state-overview').attr('data-state-id'), $('.state-overview .state-name').text(), $('.state-overview .core .checkbox').hasClass('active'), $('.state-overview .controls .checkbox').hasClass('active'));
	}

	$('.state-overview').slideUp();

	if($('.state-overview .core .checkbox').hasClass('active')){
		showCoreState($('.state-overview').attr('data-state-id'));
	}else{
		removeCoreState($('.state-overview').attr('data-state-id'));
	}

	if($('.state-overview .controls .checkbox').hasClass('active')){
		showControlState($('.state-overview').attr('data-state-id'));
	}else{
		removeControlState($('.state-overview').attr('data-state-id'));
	}
});

$('.state-overview .applyToAllStates').on('click', function(){
	var stateImage = $('g[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]');

	if($('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').length){
		var stateBox = $('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]');
		if($('.state-overview .core').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')){
			stateBox.find('removeStateOption').prop('checked', false);
			stateBox.find('core').prop('checked', $('.state-overview .core .checkbox').hasClass('active'));
			stateBox.find('controls').prop('checked', $('.state-overview .controls').hasClass('active'));
		}else{
			stateBox.find('removeStateOption').prop('checked', true);
		}
	}

	$('[data-controls="'+stateImage.attr('data-controls')+'"]').each(function(){
		var state = $(this).attr('data-state-id');

		var name = '';
		$.each(stateJson, function(){
			if(this.id == state){
				name = this.name;
			}
		});

		if(($('.state-overview .core .checkbox').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')) && !$('.state-box').find('[data-state-id="'+state+'"]').length){
			addState(state, name, $('.state-overview .core .checkbox').hasClass('active'), $('.state-overview .controls .checkbox').hasClass('active'));
		}

		if(!$('.state-overview .core .checkbox').hasClass('active') || !$('.state-overview .controls .checkbox').hasClass('active')){
			removeState(state);
		}

		$('.state-overview').slideUp();

		if($('.state-overview .core .checkbox').hasClass('active')){
			showCoreState(state);
		}else{
			removeCoreState(state);
		}

		if($('.state-overview .controls .checkbox').hasClass('active')){
			showControlState(state);
		}else{
			removeControlState(state);
		}
	});
});

$('.uploadCustomLeader').on('click', function(){
	var file = $(this).parent().find('input[type="file"]');
	var container = $(this).closest('.panel-body').find('.custom-images');

	var result = uploadImage(file, container);
	setTimeout(function(){
		if(result){
			$('body').find('#custom-image-'+result.id).remove();
			container.append('<img src="/images/find/'+result.id+'" id="custom-image-'+result.id+'" class="country-leader-icon">');
			container.find('#custom-image-'+result.id).trigger('click');
		}
	}, 5000);
});

function showCoreState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').attr('data-core-state', true);
}

function showControlState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').attr('data-control-state',true);
}

function removeCoreState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').removeAttr('data-core-state');
}

function removeControlState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').removeAttr('data-control-state');
}

$('.state-overview #hide-state').on('click', function(){
	$('.map-wrapper g[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').hide();
});

$('.map-wrapper #map-reshow').on('click', function(){
	$('.map-wrapper g').show();
});

$('.map-wrapper .map-hide').on('keyup', function(){
	$('.map-wrapper svg g').show();

	var vals = $(this).val().split('|');
	$.each(vals, function(){
		if(this.length > 0){
			$('.map-wrapper svg g[data-controls*="'+this.toUpperCase()+'|"]').hide();
		}
	});
});
$(document).ready(function(){
	if(!getCookie('CountryTags')){
		resetCountryTags();
	}

	if(!getCookie('BuilderResults')){
		resetBuilderResults();
	}


	if(!getCookie('StateResults')){
		resetStateResults();
	}

	function resetCountryTags(){
		$.getJSON("/tags.json", function(data) {
			window.localStorage.setItem('CountryTags', JSON.stringify(data));
			setCookie('CountryTags', 'Set', 5);
		});
	}

	function resetBuilderResults(){
		if($('[name="_token"]').length){
			$.post('/builder', {"_token":$('[name="_token"]').val(), "value":''}, function(data) {
				window.localStorage.setItem('BuilderResults', JSON.stringify(data));
				setCookie('BuilderResults', 'Set', 5);
			});
		}
	}

	function resetStateResults(){
		$.getJSON("/states.json", function(data) {
			window.localStorage.setItem('StateResults', JSON.stringify(data));
			setCookie('StateResults', 'Set', 5);
		});
	}

	$('.editFocusTreeDetails').on('click', function(){
		$('#editFocusTreeDetails').attr('action', $('#editFocusTreeDetails').attr('data-action')+$(this).attr('data-id'));
		$('#editFocusTreeDetails [name="country_tag"]').val($(this).attr('data-tag'));
		$('#editFocusTreeDetails [name="tree_id"]').val($(this).attr('data-tree-id'));
		$('#editFocusTreeDetails [name="lang"]').val($(this).attr('data-lang'));
	});

	$('.search_users').on('keyup', function(){
		if($(this).val().length > 0){
			var searchField = $(this).val();
			var regex = new RegExp(searchField, "i");
			var count = 1;
			var output = "";
			$.getJSON("/api/v1/users", function(data) {
				$.each(data, function(key, val){
					if ((val.username.search(regex) != -1)) {
						output += '<p>';
							output += '<a href="/api/v1/mod/add_role/'+$('.search_users').attr('data-mod-id')+'/'+val.id+'/'+$('.role').val()+'">'+val.username+'</a>';
						output += '</p>';
					}
				});
				$('.users_output').html(output);
			});
		}else{
			$('.users_output').html('');
		}
	});
	$('.toggle_menu').on('click', function(){
		$('.page').toggleClass('menu_toggled'); 
		if($('.page').hasClass('menu_toggled')){
			document.cookie = "menu=toggled; path=/";
		}else{
			document.cookie = "menu=default; path=/";
		}
	});

	$('.stepped .next').on('click', function(){
		var $stepped = $(this).closest('.stepped');
		var current = parseInt($stepped.attr('data-step'));
		var last = parseInt($stepped.attr('data-last'));
		var next = (current + 1)
		if(next == last){
			$stepped.attr('data-is-last', 1);
		}
		$stepped.find('.step_'+current).slideUp();
		$stepped.find('.step_'+next).slideDown();
		$stepped.attr('data-step', next);
	});
	$('.stepped .prev').on('click', function(){
		var $stepped = $(this).closest('.stepped');
		var current = parseInt($stepped.attr('data-step'));
		var prev = (current - 1)
		$stepped.attr('data-is-last', 0);
		$stepped.find('.step_'+current).slideUp();
		$stepped.find('.step_'+prev).slideDown();
		$stepped.attr('data-step', prev);
	});

	$(document).on('click', '.open', function(){
		var id = '#'+$(this).attr('for');
		$(id).slideDown();
	});

	$(document).on('click', '.clear-import', function(){
		$(this).parent().find('.can-be-cleared').html('');
	});

	$(document).on('click', '.close', function(){
		var id = '#'+$(this).attr('for');
		$(id).slideUp();
	});

	$(document).on('click', '.toggleActive', function(){
		var id = '#'+$(this).attr('for');
		$(id).toggleClass('active');
	});

	$(document).on('click', '.toggle', function(){
		var id = '#'+$(this).attr('for');
		$(id).slideToggle();
	});

	$(document).on('click', '.choose-country', function(e){
		e.stopPropagation();
		var id = $(this).attr('id');
		$('#focus-tag').attr('data-editing', id);
		$('#focus-tag').slideDown();
	});

	$(document).on('click', '.choose-state', function(e){
		e.stopPropagation();
		var id = $(this).attr('id');
		$('#focus-state').attr('data-editing', id);
		$('#focus-state').slideDown();
	});

	$(document).on('click', '.quickedit.textarea', function(){
		var content = $(this).text();
		$(this).replaceWith('<textarea>'+content+'</textarea><br><button class="submit_quickedit textarea">Submit</button>')
	});
	$(document).on('click', '.submit_quickedit.textarea', function(){
		$(this).prev().remove();
		var content = $(this).prev().val();
		$(this).prev().replaceWith('<div class="quickedit textarea">'+content+'</div>');
		$(this).remove();
	});

	$('body').on('click', '.tab', function(){
		$('.tabbed.active').removeClass('active');
		$('.tab.active').removeClass('active');
		var getClass = $(this).attr('class').replace('tab', '').trim();
		$(this).addClass('active');
		$('.tabbed.'+getClass).addClass('active');
	});

	$(document).on('click', '.loader-switch', function(){
		$(this).hide();
		$(this).parent().find('.loader').show();
	});

	$('body').on('click', '[for="focus-popup"],.edit-focus,.event-edit,[for="event-popup"],.idea-wrapper,[for="idea-popup"]', function(){
		resetLoader();
	});

	function resetLoader(){
		$('.loader').hide();
		$('.loader-switch').show();
	}

	$('.limit-children').on('keyup', function(){
		var search = $(this).val();
		search = search.toLowerCase();
		var limit = $(this).attr('data-limits');
		if(search.length !== 0){
			$(limit).children().each(function(){
				var text = $(this).text();
				text = text.toLowerCase();
				if(text.indexOf(search) !== -1){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
		}else{
			$(limit).children().show();
		}
	});

	setTimeout(function(){
		$('.popup_message').not('.clickToDismiss').remove();
	}, 15000);

	$('.clickToDismiss').on('click', function(){
		$(this).remove();
	});

	$('body').on('click', '.delete.genericMessage', function(e){
		e.preventDefault();
		var confirm = window.confirm('Are you sure you want to delete this? This action is irreversable.');
		if(confirm == true){
			window.location.href = $(this).attr('href');
		}
	});

	//Images to Base64
	File.prototype.convertToBase64 = function(callback){
		var reader = new FileReader();
		reader.onload = function(e) {
			 callback(e.target.result)
		};
		reader.onerror = function(e) {
			 callback(null, e);
		};        
		reader.readAsDataURL(this);
	};

	$(".to_base_64").on('change',function(){
		var alters = '#'+$(this).attr('data-alters');
		var customfield = '#'+$(this).attr('data-field');
		var selectedFile = this.files[0];
		selectedFile.convertToBase64(function(base64){
			$(alters).attr('src', base64);
			$(customfield).val('custom');
		});
	});

	$('form').on('change', '.colour-picker', function(){
		$(this).parent().find('.colour-viewer').css('background', $(this).val());
	});

	$('.addCookie').on('click', function(){
		var date = new Date();
		date.setTime(date.getTime() + (24*60*60*1000));
		var expires = "; expires=" + date.toUTCString();
		var name = $(this).attr('data-cake');
		var value = $(this).attr('data-cake-value');
		document.cookie = name + "=" + value  + expires + "; path=/";
	});

	$('#downloadMod').on('click', function(){
		var cookieCheck = setInterval(function(){
			if(document.cookie.indexOf("downloadedMod") >= 0){
				document.cookie = "downloadedMod=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
				clearInterval(cookieCheck);
				window.location.href = window.location.href;
			}
		}, 1000);
	});

	if($('.colour-picker').length){
		function refreshSwatch() {
			var red = $( "#red" ).slider( "value" );
			var green = $( "#green" ).slider( "value" );
			var blue = $( "#blue" ).slider( "value" );
			$( "#swatch" ).css( "background-color", "rgb(" + red + ',' + green + ',' + blue + ')' );
			$('body').find('[name="'+$( "#swatch" ).attr('data-name')+'"]').val(red + ',' + green + ',' + blue);
		}
	 
		$( "#red, #green, #blue" ).slider({
		  orientation: "horizontal",
		  range: "min",
		  max: 255,
		  value: 127,
		  slide: refreshSwatch,
		  change: refreshSwatch
		});
		$( "#red" ).slider( "value", 0 );
		$( "#green" ).slider( "value", 0 );
		$( "#blue" ).slider( "value", 0 );
	}

	$('.editMod').on('click', function(){
		$('.mod-edit').hide();
		$('button.mod-edit,input.mod-edit,textarea.mod-edit,.wrapper.mod-edit').show();
	});
});

function uploadImage(file, appendTo, type){
	var formdata = new FormData();
	formdata.append('_token', $('[name="_token"]').val());
	if(file.prop('files').length > 0){
        data = file.prop('files')[0];
        formdata.append("image", data);
    }else{
    	alert('Please select an image');
    	return;
    }
	$.ajax({
	    url: '/image/upload',
	    type: "POST",
	    data: formdata,
	    processData: false,
	    contentType: false,
	    success: function (result) {
	    	if(type){
	    		if(type == 'focus'){
					appendTo.append('<img src="/image/find/'+result.id+'/focus" id="custom-image-'+result.id+'" class="nficon">');
					appendTo.find('#custom-image-'+result.id).trigger('click');
					nfGFX('/image/find/'+result.id+'/focus');
	    		}
	    		if(type == 'event'){
					appendTo.append('<img src="/image/find/'+result.id+'/idea" id="custom-image-'+result.id+'" class="nficon">');
					appendTo.find('#custom-image-'+result.id).trigger('click');
					nfGFX('/image/find/'+result.id);
	    		}
	    		if(type == 'idea'){
					appendTo.append('<img src="/image/find/'+result.id+'/idea" id="custom-image-'+result.id+'" class="nficon">');
					appendTo.find('#custom-image-'+result.id).trigger('click');
					nfGFX('/image/find/'+result.id);
	    		}
	    	}else{
	    		appendTo.append('<img src="/image/find/'+result.id+'" id="custom-image-'+result.id+'">');
	    	}
	    	return result;
	    }
	});
}

function generateId(name){
	return name.replace(/\s+/g, '').replace(/[^a-zA-Z_0-9]/g, '').toLowerCase();
}

window.GENERICreload = function(){
	window.location.href = window.location.href;
}

window.GENERICdeleteNotFound = function(){
	alert('You have tried to delete an item which no longer exists');
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return false;
}

//Not too sure why, but the last window variable keeps getting called.
window.fix = '';
/**
 * @fileoverview dragscroll - scroll area by dragging
 * @version 0.0.8
 * 
 * @license MIT, see http://github.com/asvd/dragscroll
 * @copyright 2015 asvd <heliosframework@gmail.com> 
 */


(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['exports'], factory);
    } else if (typeof exports !== 'undefined') {
        factory(exports);
    } else {
        factory((root.dragscroll = {}));
    }
}(this, function (exports) {
    var _window = window;
    var _document = document;
    var mousemove = 'mousemove';
    var mouseup = 'mouseup';
    var mousedown = 'mousedown';
    var EventListener = 'EventListener';
    var addEventListener = 'add'+EventListener;
    var removeEventListener = 'remove'+EventListener;
    var newScrollX, newScrollY;

    var dragged = [];
    var reset = function(i, el) {
        for (i = 0; i < dragged.length;) {
            el = dragged[i++];
            el = el.container || el;
            el[removeEventListener](mousedown, el.md, 0);
            _window[removeEventListener](mouseup, el.mu, 0);
            _window[removeEventListener](mousemove, el.mm, 0);
        }

        // cloning into array since HTMLCollection is updated dynamically
        dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
        for (i = 0; i < dragged.length;) {
            (function(el, lastClientX, lastClientY, pushed, scroller, cont){
                (cont = el.container || el)[addEventListener](
                    mousedown,
                    cont.md = function(e) {
                        if (!el.hasAttribute('nochilddrag') ||
                            _document.elementFromPoint(
                                e.pageX, e.pageY
                            ) == cont
                        ) {
                            pushed = 1;
                            lastClientX = e.clientX;
                            lastClientY = e.clientY;

                            e.preventDefault();
                        }
                    }, 0
                );

                _window[addEventListener](
                    mouseup, cont.mu = function() {pushed = 0;}, 0
                );

                _window[addEventListener](
                    mousemove,
                    cont.mm = function(e) {
                        if (pushed) {
                            (scroller = el.scroller||el).scrollLeft -=
                                newScrollX = (- lastClientX + (lastClientX=e.clientX));
                            scroller.scrollTop -=
                                newScrollY = (- lastClientY + (lastClientY=e.clientY));
                            if (el == _document.body) {
                                (scroller = _document.documentElement).scrollLeft -= newScrollX;
                                scroller.scrollTop -= newScrollY;
                            }
                        }
                    }, 0
                );
             })(dragged[i++]);
        }
    }

      
    if (_document.readyState == 'complete') {
        reset();
    } else {
        _window[addEventListener]('load', reset, 0);
    }

    exports.reset = reset;
}));

$(document).ready(function(){
	$('[name="_token"]').attr('data-default', $('[name="_token"]').val());
	$('.chooseModForEvent').on('change', function(){
		var option = $(this).find('option[value="'+$(this).val()+'"]');
		if(option.attr('data-has-events-language') == 'true'){
			$('.goToEvents').show();
			$('.goToEvents a').attr('href', $('.goToEvents').attr('data-url')+$(this).val());
			$('.stepper').hide();
		}else{
			$('.goToEvents').hide();
			$('.stepper').show();
		}
	});

	$('[for="event-popup"]').on('click', function(){
		resetEventData();
		$('#display-gfx').attr('src', $('#chosen-gfx').val());
	});

	$('.event-outcome-add').on('click', function(){
		resetOutcomeData();
		resetEventData();
		$('#event-outcome form').attr('data-task', 'add');
		$('#event-outcome form').attr('action', $('#event-outcome form').attr('data-action')+'/'+$(this).closest('.event-wrapper').attr('data-event-id'));
		$('#event-outcome').slideDown();
	});

	$('body').on('click', '.event-outcome .event-edit', function(){
		resetOutcomeData();
		resetEventData();
		$('#event-outcome form').attr('data-task', 'edit');
		var json = JSON.parse($(this).parent().find('.outcome_json').text());
		$.each(json, function(key, value){
			$('#event-outcome form [name="'+key+'"]').val(value);
		});
		var eventUrl = $('#event-outcome form').attr('data-action');
		eventUrl = eventUrl.replace('/add', '/edit/'+$(this).parent().attr('data-outcome-id'));
		$('#event-outcome form').attr('action', eventUrl);
		$('#event-outcome').slideDown();

	});

	$('body').on('click', '.event-wrapper .event-edit.event', function(){
		resetOutcomeData();
		resetEventData();
		$('#event-popup form').attr('data-task', 'edit');
		var json = JSON.parse($(this).parent().find('.event-json').text());
		$.each(json, function(key, value){
			$('#event-popup form [name="'+key+'"]').val(value);
		});
		$('#event-popup form [name="chosen_gfx"]').val(json.event_gfx);
		$('#display-gfx').attr('src', json.event_gfx);
		var eventUrl = $('#event-popup form').attr('data-action');
		eventUrl = eventUrl.replace('/add', '/edit/'+$(this).parent().attr('data-event-id'));
		$('#event-popup form').attr('action', eventUrl);
		$('#event-popup').slideDown();

	});

	//Delete Outcome
	$('body').on('click', '.event-outcome .event-delete', function(){
		var confirm = window.confirm('Are you sure you want to delete '+$(this).parent().find('span').text()+'?');
		if(confirm == true){
			var eventid = $(this).closest('.event-wrapper').attr('data-event-id');
			var outcomeid = $(this).parent().attr('data-outcome-id');
			$(this).closest('.event-outcome').remove();
			$.get('/events/delete/'+eventid+'/'+outcomeid+'/');
		}
	});

	//Delete Event
	$('body').on('click', '.event-wrapper .event-delete.event', function(){
		var confirm = window.confirm('Are you sure you want to delete '+$(this).closest('.event-wrapper').find('.event-title').text()+' and all outcomes attached to it? The action is irreversable!');
		if(confirm == true){
			var eventid = $(this).closest('.event-wrapper').attr('data-event-id');
			$(this).closest('.event-wrapper').remove();
			$.get('/events/delete/'+eventid+'/');
		}
	});



	$('#event-outcome form button').on('click', function(){
		if($('#event-outcome form').attr('data-task') == 'edit'){
			

		}
		var inputs = {};
		$('#event-outcome').slideUp();
	});

	function resetOutcomeData(){
		$('#event-outcome form input, #event-outcome form textarea').each(function(){
			$(this).val($(this).attr('data-default'));
		});
	}

	function resetEventData(){
		$('#event-popup form').attr('action', $('#event-popup form').attr('data-action'));
		$('#event-popup form input, #event-popup form textarea').each(function(){
			$(this).val($(this).attr('data-default'));
		});
	}

});

window.EVENTuploadEvent = function(outcome, result, id){
	if(outcome == 'success'){
		var image = result;
		var title = $('input[name="event_title"]').val();
		var description = $('textarea[name="event_description"]').val();
		var eventType = $('select[name="event_type"]').val();
		var json = {};
		$('.can_json input, .can_json select, .can_json textarea').each(function(){
			var name = $(this).attr('name');
			var value = $(this).val();
			if(name.length > 1){
				json[name] = value;
			}
		});
		json['event_gfx'] = $('input[name="chosen_gfx"]').val();
		var event = '<div class="event-'+eventType+' event-wrapper">';
			event += '<div class="event-content">';
				event += '<div class="event-gfx"><img src="'+image+'"></div>';
				event += '<div class="event-title">'+title+'</div>';
				event += '<div class="event-description">'+description+'</div>';
				event += '<div class="event-json">'+JSON.stringify(json)+'</div>';
				event += '<div class="event-outcome-add">Add New Option</div>';
			event += '</div>';
		event += '</div>';

		$('.content.events').append(event);
		$('#focus-popup').slideUp();
	}else{
		alert('Error(s) occured: '+result);
	}


}

window.EVENTuploadOutcome = function(outcome, eventid, id){
	window.location.href = window.location.href;
}


$('.addFlagLayer').on('click', function(){
	var $form = $(this).closest('form');
	var num = parseInt($form.attr('data-number'));
	num++;
	var html = $('.copyable').html();
	$('.newLayers').append('<div id="addedlayer">'+html+'</div>');
	$('#addedlayer input,#addedlayer select').each(function(){
		$(this).attr('name', $(this).attr('data-name')+'['+num+']['+$(this).attr('data-type')+']');
	});
	$('#addedlayer').attr('id', '');
	$form.attr('data-number', num);	

});

$('.previewFlag').on('click', function(){
	$('.content').animate({
		scrollTop: ($("#preview-frame").offset().top + 200)
	}, 1000);
});

$('.flag-start_button').on('click', function(){
	if($('[name="tag"]').val().length){
		$('.flag-start').hide();
		$('.flag-choice').attr('style', 'display:flex;');
	}else{
		$('[name="tag"]').attr('style', 'border: 1px solid red;');
	}
});

$('.flag-choice .generic_button').on('click', function(){
	$('.'+$(this).attr('id')).show();
	$('.flag-preview').show();
	$('.flag-choice').hide();
});

$('#flag-randomize').on('click', function(){
	$('[name="randomize"]').prop('checked', true);
});

$('.flag-build').on('click', '.flag-options', function(){
	$(this).toggleClass('active');
});

$('.flag-build').on('click', '.flag-selector', function(e){
	e.stopPropagation();
	$(this).closest('.flag-options').removeClass('active');
});
$(document).ready(function(){
	$('#ideaGroup').on('change', function(){
		setIdeaOption($(this).val());
	});


	function setIdeaOption(val){
		$('.option-wrapper').hide();
		$('.option-wrapper *').addClass('inactive');
		$('.option-wrapper[data-type*="'+val+'|"]').show();
		$('.option-wrapper[data-type*="'+val+'|"] *').removeClass('inactive');
	}

	$('#idea-popup form').on('submit', function(e){
		e.preventDefault();
		var data = {};
		data["_token"] = $('[name="_token"]').val();
		data["prefix"] = $('#idea-popup form').attr('data-prefix');
		data["name"] = $('#idea-popup [name="name"]').val();
		data["group"] = $('#ideaGroup').val();
		data["options"] = {};
		$('#idea-popup [name*="option["]').each(function(){
			var spl = $(this).attr('name').split('[');
			var spli = spl[1].split(']')
			data["options"][spli[0]] = $(this).val();
		});
		data["options"] = data["options"];

		var id = $('#idea-popup form').attr('data-ideaid');
		var url = $('#idea-popup form').attr('action');


		$.post(url+id, data, function(returnData){
			appendIdeaData(returnData);
			$('#idea-popup').slideUp();
		});
		
	});

	$('.content.ideas').on('click', '.idea-wrapper .delete', function(e){
		e.stopPropagation();
		var deleteID = $(this).parent().attr('data-id');
		var confirm = window.confirm('Are you sure you want to delete '+$(this).parent().find('.name').text()+'? The action is irreversable!');
		if(confirm == true){
			$(this).parent().remove();
			$.post('/ideas/edit/'+$('.content.ideas').attr('data-id')+'/'+deleteID+'/delete', {
				_token: $('[name="_token"]').val()
			});
		}
	});

	$('.content.ideas').on('click', '.idea-wrapper', function(){
		var json = JSON.parse($(this).find('.json').text());

		$('#idea-popup [name="name"]').val($(this).find('.name').text());
		$('#idea-popup [name="group"]').val($(this).find('.group').text());
		$.each(json, function(key, value){
			$('#idea-popup [name="option['+key+']"').val(value);
			if(key == 'gfx'){
				$('#idea-popup #display-gfx').attr('src', value);
			}
		});
		$('#idea-popup .panel-head .action').text('Edit');
		$('#idea-popup form').attr('data-ideaid', $(this).attr('data-id'));
		$('#idea-popup').slideDown();
	});

	$('li[for="idea-popup"]').on('click', function(){
		$('[name="_token"]').attr('data-default', $('[name="_token"]').val());
		$('#idea-popup input,#idea-popup textarea, #idea-popup select').each(function(){
			$(this).val($(this).attr('data-default'));
		});
		$('#idea-popup form').attr('data-ideaid', 'new');
		$('#idea-popup #display-gfx').attr('src', '/media/get/hoi4/focus_icons/goal_unknown.png');
		$('#idea-popup .panel-head .action').text('Add');
	});

	$('.getIdeaValue').each(function(){
		$(this).text(getIdeaValue($(this).text()));
	});

	function getIdeaValue(text){
		return $('#ideaGroup option[value="'+text+'"]').text()
	}

	function appendIdeaData(data){
		var dataJson = JSON.parse(data.options);
		var text = '<div class="idea-wrapper" data-group="'+data.group+'" data-id="'+data.id+'">';
		text += '<span class="name">'+data.name+'</span>';
		text += '<span class="gfx"><img src="'+dataJson.gfx+'"></span>';
		text += '<span class="group">'+data.group+'</span>';
		text += '<span class="json">'+data.options+'</span>';
		text += '<span class="delete"><i class="fa fa-times"></i>';
		text += '</div>';

		var idExists = $('.content.ideas').find('[data-id="'+data.id+'"]');
		if(idExists.length){
			$(idExists).replaceWith(text);
		}else{
			var groupCheck = $('.content.ideas').find('[data-group="'+data.group+'"]');
			if(groupCheck.length){
				$(text).insertAfter(groupCheck);
			}else{
				$('.content.ideas').append('<h3>'+getIdeaValue(data.group)+'</h3>');
				$('.content.ideas').append(text);
			}
		}
	}
});
var nfGlobalWidth = 80;
var nfGlobalHeight = 170;

function calculateLocations(){
	$('.focus-content').not(".importing").each(function(){
		var x = $(this).attr('data-x');
		var y = $(this).attr('data-y');
		var location = {
			left: (parseInt(x) * nfGlobalWidth)+'px',
			top: (parseInt(y) * nfGlobalHeight)+'px'
		}
		$(this).css(location);
	});
	if($('.focus-content').length > 1){
		calculatePrerequisites();
	}
}

function calculatePrerequisites(){
	$('.focus-prerequisite').remove();
	$('.focus-content').each(function(){
		var thisid = $(this).attr('data-focus-id');
		var json = JSON.parse($(this).find('.focus-json').text());
		if(json.hasOwnProperty('focus_prerequisite') && json.focus_prerequisite !== null && json.focus_prerequisite !== ''){

			var preReq = json.focus_prerequisite;
			var hasAnd = false;
			var hasOr = false;

			if(preReq.indexOf('&&') > -1){
				hasAnd = true;
			}
			if(preReq.indexOf('||') > -1){
				hasOr = true;
			}

			if(hasAnd && hasOr){
				var orSplit = preReq.split('||');
				$.each(orSplit, function(){
					if(this.indexOf('&&') > -1){
						var andSplit = this.split('&&');
						$.each(andSplit, function(){
							doConnectors(thisid, this, null);
						});
					}else{
						doConnectors(thisid, this, 'or');
					}
				});		
			}else{
				if(hasAnd){
					var andSplit = preReq.split('&&');
					$.each(andSplit, function(){
						doConnectors(thisid, this, null);
					});
				}else{
					if(hasOr){
						var orSplit = preReq.split('||');
						$.each(orSplit, function(){
							doConnectors(thisid, this, 'or');
						});
					}else{
						doConnectors(thisid, preReq, null);
					}
				}
			}
		}
		if(json.hasOwnProperty('focus_mutually_exclusive') && json.focus_mutually_exclusive !== null && json.focus_mutually_exclusive !== ''){

			var preReq = json.focus_mutually_exclusive;
			var hasAnd = false;
			var hasOr = false;

			if(preReq.indexOf('&&') > -1){
				hasAnd = true;
			}
			if(preReq.indexOf('||') > -1){
				hasOr = true;
			}

			if(hasAnd && hasOr){
				var orSplit = preReq.split('||');
				$.each(orSplit, function(){
					if(this.indexOf('&&') > -1){
						var andSplit = this.split('&&');
						$.each(andSplit, function(){
							doConnectors(thisid, this, 'mutual');
						});
					}else{
						doConnectors(thisid, this, 'mutual');
					}
				});		
			}else{
				if(hasAnd){
					var andSplit = preReq.split('&&');
					$.each(andSplit, function(){
						doConnectors(thisid, this, 'mutual');
					});
				}else{
					if(hasOr){
						var orSplit = preReq.split('||');
						$.each(orSplit, function(){
							doConnectors(thisid, this, 'mutual');
						});
					}else{
						doConnectors(thisid, preReq, 'mutual');
					}
				}
			}
		}
	});
}

var hasBeenDeleting = false;
function doConnectors(first, second, extraclasses){
	var className = 'focus-prerequisite';
	if(extraclasses !== null){
		className += ' '+extraclasses;
	}
	
	var $this = $('[data-focus-id="'+first+'"]');
	var $prequisite = $('[data-focus-id="'+second+'"]');
	if($this.length && $prequisite.length){
		if(parseInt($this.css('top').replace('px', '')) >= parseInt($prequisite.css('top').replace('px', ''))){
			var height = (parseInt($this.css('top').replace('px', '')) - parseInt($prequisite.css('top').replace('px', ''))) - (10 + $prequisite.height());
			var top = parseInt($prequisite.css('top').replace('px', '')) + nfGlobalHeight;

		}else{
			var height = (parseInt($prequisite.css('top').replace('px', '')) - parseInt($this.css('top').replace('px', ''))) - (10 + $prequisite.height());
			var top = parseInt($this.css('top').replace('px', '')) + nfGlobalHeight;

		}


		if(parseInt($this.css('left').replace('px', '')) >= parseInt($prequisite.css('left').replace('px', ''))){
			var width = parseInt($this.css('left').replace('px', '')) - parseInt($prequisite.css('left').replace('px', ''));
			var left = (parseInt($this.css('left').replace('px', '')) + ($this.width() / 2 )) - 1;
			var styleFirst = 'top:'+(top - 100)+'px;left:'+(left - width)+'px;height:100px;';
			var styleSecond = 'top:'+top+'px;left:'+(left - width)+'px;width:'+width+'px;';
			var styleThird = 'top:'+top+'px;left:'+left+'px;height:'+height+'px;';
		}else{
			var width = parseInt($prequisite.css('left').replace('px', '')) - parseInt($this.css('left').replace('px', ''));
			var left = (parseInt($prequisite.css('left').replace('px', '')) + ($prequisite.width() / 2 )) - 1;
			var styleFirst = 'top:'+(top - 100)+'px;left:'+left+'px;height:100px;';
			var styleSecond = 'top:'+top+'px;left:'+(left - width)+'px;width:'+width+'px;';
			var styleThird = 'top:'+top+'px;left:'+(left - width)+'px;height:'+height+'px;';
		}
		if(extraclasses == 'mutual'){
			var styleFirst = 'top:'+(top - 0)+'px;left:'+left+'px;height:0;';
		}
	}else{
		if($('#delete-mode').prop('checked') == true){
			hasBeenDeleting = true;
		}else{
			if(hasBeenDeleting){
				//If the user has been deleting focuses, let's reload the page - save the hassle of pissing around with trying to delete stuff in jsons, and it cause errors with mutually exclusives.
				window.location.href = window.location.href;
			}else{
				if(second.length > 0){
					alert('An error has been found in your focus tree. '+$this.find('.focus-title').text()+' is referencing a focus which does not exist ('+second+') as a prerequisite or mutually exclusive focus.');
				}
			}
		}
	}

	


	var connector = '<div class="'+className+'" style="'+styleFirst+'"></div>';
		connector += '<div class="'+className+' horizontal" style="'+styleSecond+'"></div>';
		connector += '<div class="'+className+'" style="'+styleThird+'"></div>';
	$('.content.focus-tree').append(connector);
}
$(document).ready(function(){

	$('[name="mod_id"]').on('change', function(){
		if($(this).val() == 'createNewMod'){
			$('.new-mod').show();
			$('.toggleNewModStep').each(function(){
				$(this).attr('class', $(this).attr('data-new-mod-class'));
			});
			$('.create-focus .stepped').attr('data-last', '3');
		}else{
			$('.new-mod').hide();
			$('.country-wrapper').hide();
			$('.country-wrapper[data-id="'+$(this).val()+'"]').show();
			$('.toggleNewModStep').each(function(){
				$(this).attr('class', $(this).attr('data-existing-mod-class'));
			});
			$('.create-focus .stepped').attr('data-last', '2');
		}
	});
	
	$('li[for="focus-popup"]').on('click', function(){
		$('[name="_token"]').attr('data-default', $('[name="_token"]').val());
		$('#focus-popup input,#focus-popup textarea, #focus-popup select').each(function(){
			$(this).val($(this).attr('data-default'));
		});
		$('#focus-popup #display-gfx').attr('src', '/media/get/hoi4/focus_icons/goal_unknown.png');
		$('#focus-popup form').attr('action', '/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/add');
	});

	$('.content.focus-tree.edit').on('click', '.edit-focus', function(){
		$parent = $(this).parent().parent();
		var id = $parent.attr('data-focus-id');
		var treeid = $('#focus-popup form').attr('data-treeid');
		var newAction = '/focus-tree/edit/'+treeid+'/edit/'+id;
		$('#focus-popup form').attr('action', newAction);

		var img = $parent.find('.focus-image').attr('src');
		var title = $parent.find('.focus-title').text();
		var desc = $parent.find('.focus-description div').text();
		var json = JSON.parse($parent.find('.focus-json').text());

		$('#focus-popup [name="focus_title"]').val(title);
		$('#focus-popup [name="focus_description"]').val(desc);
		$('#focus-popup #chosen-gfx').val(img);
		$('#focus-popup #display-gfx').attr('src', img);

		$.each(json, function(name, value){
			$('#focus-popup [name="'+name+'"]').val(value);
		});

		$('#focus-popup').slideDown();
	});

	$('.nficon').on('click', function(){
		var src = $(this).attr('src');
		nfGFX(src);
	});

	$('.uses-builder').on('focus', function(){
		$('#focus-builder').attr('data-building', $(this).attr('name'));
		$('#focus-builder').slideDown();
	});

	$('#submit-build').on('click', function(){
		var input = $('#focus-builder').attr('data-building');
		$('[name="'+input+'"]').val($('.dragged.main').text().replace(/	/g, '').replace(/\n/g, '\r\n').replace(/}/g, '}\r\n'));
		$('.dragged.main').html('');
		$('#focus-builder').slideUp();
	});

	if($('.content.focus-tree').length){
		$( ".content.focus-tree.edit .focus-content" ).draggable({ 
			grid: [ nfGlobalWidth, nfGlobalHeight ],
			snap: ".content.focus-tree",
			stop: function(){
				var left = $(this).css('left');
				var top = $(this).css('top');
				left = parseInt(left.replace('px', ''));
				top = parseInt(top.replace('px', ''));
				if(0 > left){
					left = 0;
				}
				if(0 > top){
					top = 0;
				}
				$(this).attr('data-y', (top/nfGlobalHeight));
				$(this).attr('data-x', (left/nfGlobalWidth));
				$(this).css({
					left:left+'px',
					top:top+'px'
				})
				var json = $(this).find('.focus-json').text();
				json = JSON.parse(json);
				json.focus_x = (left/nfGlobalWidth);
				json.focus_y = (top/nfGlobalHeight);
				json = JSON.stringify(json);
				$(this).find('.focus-json').text(json);
				calculatePrerequisites();
				$.post('/focus-tree/quickedit/'+$('#focus-popup form').attr('data-treeid')+'/edit/'+$(this).attr('data-focus-id'),
					{
						_token: $('[name="_token"]').val(),
						action: "updateXY",
						x: (left/nfGlobalWidth),
						y: (top/nfGlobalHeight)
					}
				);
			}
		});
	}
	$('#old-tool-output-button').on('click', function(){
		

		$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/importFromOldTool', {
			_token: $('[name="_token"]').val(),
			password: $('#old-tool-output-form input[name="pw"]').val()
		}, function(data){
			$('#old-tool-output').html(data);
			$('#focus-import-old-popup .loader-switch').show();
			$('#focus-import-old-popup .loader').hide();
		});
	});

	$('body').on('click', '#importviajson', function(){
		$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/importViaJSON', {
			_token: $('[name="_token"]').val(),
			tree: $('#existing-focus-tree-output').val()
		}, function(data){
			window.location.href = window.location.href;
		});
	});

	$(document).on('click', 'button.imported-focus', function(){
		$(this).parent().parent().hide();
		$(this).parent().parent().next().hide();
	});

	var draggedNum = 0;
	var droppableOptions = {
        accept: '.builder-example',
        greedy: true, 
        hoverClass: "dragged-hover", /* highlight the current drop zone */
        drop: function (event, ui) {
        	$d = $(ui.draggable).clone();
        	var id = $d.attr('id').replace('_drag','');
        	var game_id = $('#'+$d.attr('id').replace('_drag','_id')).text()
	 		var outcome = $('#'+id+'_defaultoutcome').text();
            if(outcome == 'new-level'){
				var toAdd = '<div class="block">';
					toAdd += game_id.replace('TAG', '<span class="choose-country" id="country_'+id+draggedNum+'">CHOOSE COUNTRY</span>').replace('state_id', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>').replace('STATEID', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>')+' = {'
						toAdd += '<div class="new-level dragged">';
						toAdd += '</div> ';
					toAdd += '}'
				toAdd += '</div>';
			}else{
				var toAdd = '<div class="block">';
					toAdd += game_id.replace('TAG', '<span class="choose-country" id="country_'+id+draggedNum+'">CHOOSE COUNTRY</span>').replace('state_id', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>').replace('STATEID', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>');
						toAdd += '<div class="quickedit textarea">';
						toAdd += outcome.replace('TAG', '<span class="choose-country" id="country_'+id+draggedNum+'">CHOOSE COUNTRY</span>').replace('state_id', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>').replace('STATEID', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>');
						toAdd += '</div> '
					toAdd+= '</div> '
			}
			$d.html(toAdd);
			$d.removeClass('builder-example');
            $d.find('.dragged').droppable(droppableOptions);
            $(this).append($d);
            draggedNum++;
        }
    };

    if($('.content.focus-tree,.content.events,.content.ideas').length){
    	$(".dragged.main").droppable(droppableOptions);
    }

    $('#clear-build').on('click', function(){
    	$(".dragged.main").html('');
    });

    var builderTimer; 
    $('#search-builder').on('keydown', function () {
	  clearTimeout(builderTimer);
	});
	$('#search-builder').on('keyup', function(){
		searchBuilder();
	});
	function searchBuilder(){
		var searchField = $('#search-builder').val();
		var count = 1;
		var output = "";
		var data = JSON.parse(window.localStorage.getItem('BuilderResults'));
		$.each(data, function(key, val){
			if(val.game_id.indexOf(searchField) !== -1 || val.description.indexOf(searchField) !== -1){
				if(val.uses_tag){
					var tag = "yes";
				}else{
					var tag = "no";
				}
				if(val.uses_state){
					var state = "yes";
				}else{
					var state = "no";
				}
				output += '<div class="search-result">';
					output += '<p class="build-description" id="builder-'+val.id+'" tag="'+tag+'" state="'+state+'" >'+val.description+'</p>';
					output += '<div style="display:none;" id="builder-'+val.id+'_id">'+val.game_id+'</div>';
					output += '<div class="default-outcome" id="builder-'+val.id+'_defaultoutcome">'+val.default_outcome+'</div>';
					output += '<div class="builder-example" id="builder-'+val.id+'_drag">'+val.example+'</div>';
				output += '</div>';
			}
		});
		$('.builder-search-output').html(output);
		$('.builder-example').draggable({
	        helper: 'clone'
	    });
		
	}

	$('.builder-search-output').on('click', '.build-description', function(){
		$(this).parent().find('.builder-example').slideToggle();
	});

	if($(".tag-search-output").length){
		var data = JSON.parse(window.localStorage.getItem('CountryTags'));
		var output = '';
		$.each(data, function(key, val){
		 	output += '<p id="'+val.tag+'" class="focus_tree_tags">'+val.country+'</p>';
		});
		$(".tag-search-output").prepend(output);
	}

	$("#focus-tag input").keyup(function(){
		var searchField = $(this).val();
		var regex = new RegExp(searchField, "i");
		var count = 1;
		var output = "";
		if(searchField.length > 0){
			$('.focus_tree_tags').each(function(){
				if (($(this).text().search(regex) != -1) || ($(this).attr('id').search(regex) != -1)) {
			 		$(this).show();
				}else{
					$(this).hide();
				}
			});
		}else{
			$('.focus_tree_tags').show();
		}
	});
	$(document).on('click', ".focus_tree_tags", function() {
		var tagid = $(this).attr("id");
		var toedit = $('#focus-tag').attr('data-editing');
		$('#'+toedit).text(tagid);
		$('#'+toedit).val(tagid);
		$('#focus-tag').slideUp();
	});

	$('#search-states').keyup(function(){
		var searchField = $(this).val();
		var regex = new RegExp(searchField, "i");
		var count = 1;
		var output = "";
		var data = JSON.parse(window.localStorage.getItem('StateResults'));
		$.each(data, function(key, val){
			if ((val.id.search(regex) != -1) || (val.name.search(regex) != -1)) {
		 		output += '<p id="state_'+val.id+'" class="searched_states">'+val.name+'</p>';
			}
		});
		$('.state-search-output').html(output);
 
	});
	
	$(document).on('click', ".searched_states", function() {
		var tagid = $(this).attr("id").replace("state_","");
		var toedit = $('#focus-state').attr('data-editing');
		$('#'+toedit).text(tagid);
		$('#'+toedit).val(tagid);
		$("#focus-state").slideUp();
	});

	$(document).on('click', '.focus-content', function(){
		if($('#delete-mode').prop('checked') == true){
			var confirm = window.confirm('Are you sure you want to delete '+$(this).find('.focus-title').text()+'? The action is irreversable!');
			if(confirm == true){
				var id = $(this).attr('data-focus-id');
				$(this).remove();
				$('.focus-prerequisite').remove();
				calculateLocations();
				$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/delete/'+id+'/', {
					_token: $('[name="_token"]').val()
				});
			}
		}
	});
	
	$('#delete-all').on('click', function(){
		//Delete all!
		var confirm = window.confirm('Are you sure you want to delete everything? The action is irreversable!');
		if(confirm == true){
			$('.focus-content').remove();
			$('.focus-prerequisite').remove();
			$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/delete/all/', {
				_token: $('[name="_token"]').val()
			});
		}
	});

	$('[name="focus_mutually_exclusive"], [name="focus_prerequisite"]').on('focus', function(){
		$('#focus-chooser').attr('data-editing', $(this).attr('name'));
		$("#select-focus").html("");
		$('.focus-content').each(function () {
			var focusname = $(this).find('.focus-title').text();
			var focusimg = $(this).find('.focus-image').attr('src');
			var focusid = $(this).attr('data-focus-id');//generateId(focusname);
			var focus = '<p class="select-focus" id="'+focusid+'_sel">';
				focus += '<img src="'+focusimg+'" align="left" width="25px;">'+focusname+'</p>'
			$("#select-focus").append(focus);
		});
		$('#focus-chooser input').val('');
		$("#focus-chooser").slideDown();
	});

	$(document).on('click', ".select-focus", function() {
		var toedit = $('#focus-chooser').attr('data-editing');
		if($("#select-and").prop('checked') == true || $("#select-or").prop('checked') == true){
			if($("#select-and").prop('checked') == true){
				var connection = "&&";
			}
			if($("#select-or").prop('checked') == true){
				var connection = "||";
			}
			var msid = (this.id).replace("_sel","");
			
			$('[name="'+toedit+'"]').val($('[name="'+toedit+'"]').val() +  msid + connection);
			$(this).remove();
		}else{
			var msid = (this.id).replace("_sel","");

			$('[name="'+toedit+'"]').val($('[name="'+toedit+'"]').val() + msid);
			$("#select-focus").html("");
			$("#focus-chooser").slideUp();
		}
	});

	$('#multiMove').on('change', function(){
		if($(this).prop('checked')){
			$( ".focus-content" ).draggable( 'destroy' );
			$( ".content.focus-tree" ).selectable({
				stop: function( event, ui ) {
					$('.focus-content.ui-selected').each(function(){
						var elem = '<div class="addedFocus">'
							elem += '<input name="focus[]" value="'+$(this).attr('data-focus-id')+'">';
							elem += '<span>'+$(this).find('.focus-title').text()+' <i class="fa fa-times"></i></span>';
						elem += '</div>';
						$('#selectedFocuses').append(elem);
					});
					$('#focus-multi-move').slideDown();
				}
			});
		}else{
			window.location.href = window.location.href;
		}
	});

	$('body').on('click', '.addedFocus .fa-times', function(){
		$(this).closest('.addedFocus').remove();
	});

	$('.focus-tree-raw-toggle').on('click', function(){
		$('.focus-tree-raw').toggleClass('closed');
	});

	var focusTreeErrors = $('.focus-tree-raw .focus-error').length;
	if(focusTreeErrors > 0){
		$('.focus-tree-raw-toggle').append('<span class="errors">'+focusTreeErrors+'</span>');
	}

	$('.showFocusError').on('click', function(){
		$('.focus-tree-raw-preview').each(function(){
			if($(this).find('.focus-error').length){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
	});

	$('#focusTreeFile').on('change', function(){
		var reader = new FileReader();
    	reader.readAsText(document.getElementById('focusTreeFile').files[0], "UTF-8");
    	reader.onload = function (evt) {
	        $('#existing-focus-tree-output').val(JSON.stringify(toJSON(evt.target.result, 0)));
	    }
	});

	$("#treetojson").click(function(){

		formdata = new FormData();
		formdata.append('_token', $('[name="_token"]').val());
	    if($('#focusTreeFile').prop('files').length > 0)
	    {
	        file =$('#focusTreeFile').prop('files')[0];
	        formdata.append("focusTreeFile", file);
	    }
	    if($('#localisationFile').prop('files').length > 0)
	    {
	        file =$('#localisationFile').prop('files')[0];
	        formdata.append("localisationFile", file);
	    }
	    if($('#existing-focus-tree-output').val().length > 0){
	    	formdata.append('JSON', $('#existing-focus-tree-output').val());
	    }
		$.ajax({
		    url: '/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/importFromExistingFile',
		    type: "POST",
		    data: formdata,
		    processData: false,
		    contentType: false,
		    success: function (result) {
		  //   	$('#focus-import-existing-tree .loader').hide();
				// $('#focus-import-existing-tree .loader-switch').show();
				$('#focus-import-existing-tree .imported-textboxes').html(result);
		  //       var result = toJSON($("#existing-focus-tree").val().replace(/\t/g,""),0);
		  //       $('#import-result').html('Found '+result.focus_tree.focus.length+' focuses.<br>Press the button below to import them.<p><button class="generic_button" id="importviajson">Import Focus Tree</button></p>');
				// $("#existing-focus-tree-output").val(JSON.stringify(result));
		
		    }
		});

		
	});

});

$(window).load(function(){
	calculateLocations();
});

$('#customgfx').on('change', function(){
	var file = $(this);
	var container = $('#choosegfx .custom-images');
	var type = 'focus';
	if($('#event-popup').length){
		type = 'event';
	}
	if($('#idea-popup').length){
		type = 'idea';
	}
	var result = uploadImage(file, container, type);
});

window.NFunauthorisedUser = function(){
	alert('You are not authorised to edit this.')
}

window.NFsessionExpired = function(){
	alert('Your session has expired. Please relogin to continue using this tool.');
}

window.NFuploadOutcome = function(outcome, result, id){
	if(outcome == 'success'){
		var image = result;
		var title = $('input[name="focus_title"]').val();
		var description = $('textarea[name="focus_description"]').val();
		var json = {};
		$('.can_json input, .can_json select, .can_json textarea').each(function(){
			var name = $(this).attr('name');
			var value = $(this).val();
			if(name.length > 1){
				json[name] = value;
			}
		});

		var focus = '<div class="focus-content" data-x="'+$('input[name="focus_x"]').val()+'" data-y="'+$('input[name="focus_y"]').val()+'" data-focus-id="'+id+'">';
			focus += '<img src="'+image+'" class="focus-image">';
			focus += '<p class="focus-title">'+title+'</p>';
			focus += '<div class="focus-description"><p class="edit-focus">Edit</p><div>'+description+'</div></div>';
			focus += '<div class="focus-json">'+JSON.stringify(json)+'</div>';
		focus += '</div>';

		$('.content.focus-tree').append(focus);
		
		$( ".content.focus-tree.edit .focus-content" ).draggable({ 
			grid: [ nfGlobalWidth, nfGlobalHeight ],
			snap: ".content.focus-tree",
			stop: function(){
				var left = $(this).css('left');
				var top = $(this).css('top');
				left = parseInt(left.replace('px', ''));
				top = parseInt(top.replace('px', ''));
				$(this).attr('data-y', (top/nfGlobalHeight));
				$(this).attr('data-x', (left/nfGlobalWidth));
				calculatePrerequisites();
				$.post('/focus-tree/quickedit/'+$('#focus-popup form').attr('data-treeid')+'/edit/'+$(this).attr('data-focus-id'),
					{
						_token: $('[name="_token"]').val(),
						action: "updateXY",
						x: (left/nfGlobalWidth),
						y: (top/nfGlobalHeight)
					}
				);
			}
		});
		$('#focus-popup').slideUp();
		var sync = syncFocusConnections(id);
		if(sync){
			window.location.href = window.location.href;
		}
		calculateLocations();
	}else{
		alert('Error(s) occured: '+result);
	}
}

window.NFupdateOutcome = function(outcome, result, id){
	if(outcome == 'success'){
		$('[data-focus-id="'+id+'"]').remove();
		var image = result;
		var title = $('input[name="focus_title"]').val();
		var description = $('textarea[name="focus_description"]').val();
		var json = {};
		$('.can_json input, .can_json select, .can_json textarea').each(function(){
			var name = $(this).attr('name');
			var value = $(this).val();
			json[name] = value;
		});

		var focus = '<div class="focus-content" data-x="'+$('input[name="focus_x"]').val()+'" data-y="'+$('input[name="focus_y"]').val()+'" data-focus-id="'+id+'">';
			focus += '<div class="focus-description"><p class="edit-focus">Edit</p><div>'+description+'</div></div>';
			focus += '<img src="'+image+'" class="focus-image">';
			focus += '<p class="focus-title">'+title+'</p>';
			focus += '<div class="focus-json">'+JSON.stringify(json)+'</div>';
		focus += '</div>';

		$('.content.focus-tree').append(focus);
		
		$( ".content.focus-tree.edit .focus-content" ).draggable({ 
			grid: [ nfGlobalWidth, nfGlobalHeight ],
			snap: ".content.focus-tree",
			stop: function(){
				var left = $(this).css('left');
				var top = $(this).css('top');
				left = parseInt(left.replace('px', ''));
				top = parseInt(top.replace('px', ''));
				$(this).attr('data-y', (top/nfGlobalHeight));
				$(this).attr('data-x', (left/nfGlobalWidth));
				calculatePrerequisites();
				$.post('/focus-tree/quickedit/'+$('#focus-popup form').attr('data-treeid')+'/edit/'+$(this).attr('data-focus-id'),
					{
						_token: $('[name="_token"]').val(),
						action: "updateXY",
						x: (left/nfGlobalWidth),
						y: (top/nfGlobalHeight)
					}
				);
			}
		});
		$('#focus-popup').slideUp();
		var sync = syncFocusConnections(id);
		if(sync){
			window.location.href = window.location.href;
		}
		calculateLocations();
	}else{
		alert('Error(s) occured: '+result);
	}
}

function syncFocusConnections(focusToAdd){
	var focus = JSON.parse($('body').find('[data-focus-id="'+focusToAdd+'"] .focus-json').text());

	if(focus.hasOwnProperty('focus_mutually_exclusive')){
		//We only want to do this if it has one
		if(focus.focus_mutually_exclusive !== null && focus.focus_mutually_exclusive !== ''){
			var idsToSync = focus.focus_mutually_exclusive.replace(/&&/g, '||');
			idsToSync = idsToSync.split('||');
			var hasUpdated = false;
			$.each(idsToSync, function(key, value){
				var focusCheck = createFocusInfoToExistingJSONs(value, 'focus_mutually_exclusive', focusToAdd);
				if(focusCheck){
					hasUpdated = true;
				}
			});
			if(hasUpdated){
				return true;
			}
		}
	}

		
}

function createFocusInfoToExistingJSONs(id, property, focusToAdd){
	var json = JSON.parse($('[data-focus-id="'+id+'"]').find('.focus-json').text());
	if(json.hasOwnProperty(property)){
		//Check if we can check it's there or not.
		if(json[property] !== null && json[property] !== ''){
			if(json[property].indexOf(focusToAdd) > -1){
				//It's got it, so ignore it - kinda buggy with 1-10, but we don't care about them
				return false;
			}else{
				json[property] = json[property]+'&&'+focusToAdd;	
			}
		}else{
			json[property] = focusToAdd;
		}
	}else{
		if(json[property].length > 0){
			json[property] = json[property]+'&&'+focusToAdd;
		}else{
			json[property] = focusToAdd;
		}
	}
	$('[data-focus-id="'+id+'"]').find('.focus-json').text(JSON.stringify(json));
	return true;
}

function nfGFX(src){
	$('#display-gfx').attr('src', src);
	$('#chosen-gfx').val(src);
	$('#choosegfx').slideUp();
}


// $('#treetojson').on('click', function(){
// 	$('<p>This may take a minute to upload.</p><p><strong>Once upload is complete, your focus tree will be scheduled for import</strong>').insertAfter($(this));
// 	$(this).hide();
// });

$('.filterGfx').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.nficon').show();
	if(val.length === 0){
		return;
	}
	$('.nficon').each(function(){
		var id = $(this).attr('id');
		var regex = new RegExp(val, "i");
		if (id.search(regex) == -1) {
			$(this).hide();
		}
	});
});
function preprocess(s) {
	lines = s.split(/\r?\n/);
	result = "";
	for(var i = 0; i < lines.length; ++i) {
		var line = lines[i];
		var index = line.indexOf("#");
		if(index != -1) {
			line = line.substring(0, index);
		}
		result += line + "\n";
	}
	return result;
}

window.toJSON = function(s, level) {
	let maxLevel = 2;
	s = preprocess(s);
	var key = "";
	var value = "";
	var buildingKey = true;
	var braceCount = 0; 
	var hadBraces = false;
	var json = {};
	for(var i = 0; i < s.length; ++i) {
		let c = s.charAt(i);
		
		//If the parser is currently expecting a key
		if(buildingKey) {
			//Ignore these characters because they won't be part of a key
			if(c === "{" || c === "}") {
				continue;
			}
			
			//As long as we don't hit the = character, we are still building a key.
			//The assumption here is that keys can contain space characters
			if(c !== '=') {
				key += c;
			}
			else {
				buildingKey = false;
				key = key.trim();
			}
		}
		//The parser is expecting a value
		else {
			value += c;
			//if only whitespace
			//  continue
			if(c === "{") {
				++braceCount;
				hadBraces = true;
			}
			else if(c === "}") {
				--braceCount;
			}
			
			//If the braces are evenly matched,
			//and our value string is not just whitespace characters
			//and the next character to add is whitespace,
			//then we are done building the value string
			if(braceCount === 0 && /\S/.test(value) && /\s/.test(c)) {
				value = value.trim();
				
				//In the stupid format, the same key can appear multiple times.
				//If this happens, then what we really want is to treat that key
				//as an array
				if(key in json) {
					
					//Convert value stored at that key to
					//an array if it isn't already one
					if(!Array.isArray(json[key])) {
						var obj = json[key];
						json[key] = [obj];
					}

					//If the value had {} characters, then
					//it will consist of other key/value pairs.
					if(hadBraces && level < maxLevel) {                    
						json[key] = json[key].concat(toJSON(value, level+1));
						hadBraces = false;
					}
					else {
						json[key] = json[key].concat(value);
					} 
				}
				
				else {
					if(hadBraces && level < maxLevel) {                    
						json[key] = toJSON(value, level+1);
						hadBraces = false;
					}
					else {
						json[key] = value;
					} 
				}
				
				
				buildingKey = true;
				key = "";
				value = "";
			}
		}
	}

	return json;
	//$("#existing-focus-tree-output").val(JSON.stringify(json));
	//console.log(json);
	//$("#show-output").append(JSON.stringify(json));
	var obj = json; //JSON.parse($("#existing-focus-tree-output").val());

	var focus = obj.focus;
	for(var i in focus){

		if(focus[i].hasOwnProperty('id')){
			var id = focus[i].id;
			var name = "undefined";
			var desc = "undefined";
			var text = focus[i].id;
			if(focus[i].hasOwnProperty('text')){
				text = focus[i].text;
			}
			var localisation = $("#existing-localisation").val().split(/\n/);
			$.each(localisation,function(u, i) {
				if(i.indexOf(":") !== -1){
					var splitid = i.split(/:(.+)/);
					if(splitid[0].replace(/\s+/g, "") == text && name == 'undefined'){
						name = splitid[1].replace("0 ","").slice(1, -1);
					}
					if(splitid[0].replace(/\s+/g, "") == text+"_desc" || name !== 'undefined' && splitid[0].replace(/\s+/g, "") == text && splitid[0].replace(/\s+/g, "") !== name){
						desc = splitid[1].replace("0 ","").slice(1, -1);
					}
				}
			});
			if(focus[i].hasOwnProperty('mutually_exclusive')){
				var me = focus[i].mutually_exclusive;

				if(Array.isArray(me)){
					//It's an AND pr
					var mutually_exclusive = '';
					$.each(me, function(key, value){
						if(value.hasOwnProperty('focus')) {
							mutually_exclusive += value.focus+'&&';
						} else {
							var meVal = value.toLowerCase().replace(/\s/g, "");
							meVal = meVal.replace('{focus=', '');
							meVal = prVal.replace('}', '');
							//If it's an OR focus this'll replace those pesky ORs
							meVal = meVal.replace('focus=', '||');
							mutually_exclusive += meVal+'&&';
						}
					});
					mutually_exclusive = mutually_exclusive.toLowerCase();
				}else{
					if(me.hasOwnProperty('focus')){
						var mutually_exclusive = me.focus;
					}else{
						//It's an OR pr/solo
						var meVal = me.toLowerCase().replace(/\s/g, "");
						meVal = meVal.replace('{focus=', '');
						meVal = meVal.replace('}', '');
						//If it's an OR focus this'll replace those pesky ORs
						meVal = meVal.replace('focus=', '||');
						var mutually_exclusive = meVal.toLowerCase();
					}
					
				}

			}else{
				var mutually_exclusive = "";	
			}
			focus[i].mutually_exclusive = mutually_exclusive;
			if(focus[i].hasOwnProperty('prerequisite')){
				var pr = focus[i].prerequisite;
				
				if(Array.isArray(pr)){
					//It's an AND pr
					var prerequisite = '';
					$.each(pr, function(key, value){
						if(value.hasOwnProperty('focus')) {
							prerequisite += value.focus+'&&';
						} else {
							var prVal = value.toLowerCase().replace(/\s/g, "");
							prVal = prVal.replace('{focus=', '');
							prVal = prVal.replace('}', '');
							//If it's an OR focus this'll replace those pesky ORs
							prVal = prVal.replace('focus=', '||');
							prerequisite += prVal+'&&';
						}
							
						
					});
					prerequisite = prerequisite.toLowerCase();
				}else{
					if(pr.hasOwnProperty('focus')) {
						prerequisite = pr.focus;
					} else {
						//It's an OR pr/solo
						var prVal = pr.toLowerCase().replace(/\s/g, "");
						prVal = prVal.replace('{focus=', '');
						prVal = prVal.replace('}', '');
						//If it's an OR focus this'll replace those pesky ORs
						prVal = prVal.replace('focus=', '||');
						prerequisite = prVal.toLowerCase();
					}
				}
			}else{
				var prerequisite = "";	
			}
			focus[i].prerequisite = prerequisite;
			if(focus[i].hasOwnProperty('ai_will_do')){
				var ai = focus[i].ai_will_do;
				if(ai.hasOwnProperty('factor')){
					ai = ai.factor;
				}else{
					ai = ai.replace('factor', '').replace('=', '').replace(/ /g, '').replace('{', '').replace('}', '');
				}
				
			}else{
				var ai = "";	
			}
			focus[i].ai_will_do = ai;
			if(focus[i].hasOwnProperty('relative_position_id')){
				var relative_pos = focus[i].relative_position_id;
	
			}else{
				var relative_pos = '';
			}
			focus[i].relative_position_id = relative_pos;
			if(focus[i].hasOwnProperty('completion_reward')){
				var completion_reward = focus[i].completion_reward;
			}else{
				var completion_reward = "";	
			}
			focus[i].completion_reward = completion_reward;
			if(focus[i].hasOwnProperty('available')){
				var available = focus[i].available;
			}else{
				var available = "";	
			}
			focus[i].available = available;
			if(focus[i].hasOwnProperty('bypass')){
				var bypass = focus[i].bypass;
			}else{
				var bypass = "";	
			}
			focus[i].bypass = bypass;
			if(focus[i].hasOwnProperty('completion_tooltip')){
				var completion_tooltip = focus[i].completion_tooltip;
			}else{
				var completion_tooltip = "";	
			}
			focus[i].completion_tooltip = completion_tooltip;
			if(focus[i].hasOwnProperty('cost')){
				var cost = focus[i].cost;
			}else{
				var cost = "10";	
			}
			focus[i].cost = cost;

			if(focus[i].hasOwnProperty('icon')){
				var icon = focus[i].icon;
				icon = icon.replace('GFX_', '');
				icon = '/media/get/hoi4/focus_icons/'+icon+'.png';
			}else{
				var icon = "";	
			}

			focus[i].title = name;
			focus[i].description = desc;
			focus[i].icon = icon;
		}
	}
	return json;
	
}