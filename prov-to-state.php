<?php
if(!empty($_POST['json'])):
$json = $_POST['json'];
$history = $_POST['history'];
$vp = $_POST['vp'];
print_r($json);
echo '<br><hr><br>';
print_r($history);
echo '<br><hr><br>';
print_r($vp);die();

$state_name = $json['state']['name'];

$provinces = rtrim($json['state']['provinces']);
$provinces = ltrim($provinces);
$provinces = explode(' ', $provinces);
$total_provinces = count($provinces);

$vp = rtrim(str_replace('=', str_replace('{', str_replace('}', $json['state']['victory_points']))));
$vp = ltrim($vp);
$vp = explode(' ', $vp);

$manpower = ($json['manpower'] / $total_provinces);




else: 
$state_num = 1;
$directory = 'states';
$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
foreach ($scanned_directory as $value) {
	$state = file_get_contents('states/'.$value);
	echo '<div class="state" data-statenum="'.$state_num.'">'.$state.'</div>';
	$state_num++;
}
?>
<script src="/js/jquery-2.1.4.min.js?v=1"></script>
<style>
.state{
	height: 10px;
	overflow: hidden;
}
</style>
<div>
	<button id="convert">CONVERT</button>
</div>
<script>
$("#convert").click(function(){
	var statenum = 0;
	$('.state').each(function(){
		var val = $(this).text();
		var json = toJSON(val,0, 1);
		console.log(json);
		var history = toJSON(json.state.history, 0, 1);
		console.log(history);
		var vp = toJSON(val,0, 5);
		console.log(vp);
		$.post( "/prov-to-state.php",{ 
			json: json,
			history: history,
			vp: vp
		});
	});
});
</script>
<script>
function preprocess(s) {
		lines = s.split(/\r?\n/);
		result = "";
		for(var i = 0; i < lines.length; ++i) {
			var line = lines[i];
			var index = line.indexOf("#");
			if(index != -1) {
				line = line.substring(0, index);
			}
			result += line + "\n";
		}
		return result;
	}

	//Probably doesn't handle strings that contain {} or # characters
	function toJSON(s, level, max) {
		let maxLevel = max;
		s = preprocess(s);
		var key = "";
		var value = "";
		var buildingKey = true;
		var braceCount = 0; 
		var hadBraces = false;
		var json = {};
		for(var i = 0; i < s.length; ++i) {
			let c = s.charAt(i);
			
			//If the parser is currently expecting a key
			if(buildingKey) {
				//Ignore these characters because they won't be part of a key
				if(c === "{" || c === "}") {
					continue;
				}
				
				//As long as we don't hit the = character, we are still building a key.
				//The assumption here is that keys can contain space characters
				if(c !== '=') {
					key += c;
				}
				else {
					buildingKey = false;
					key = key.trim();
				}
			}
			//The parser is expecting a value
			else {
				value += c;
				//if only whitespace
				//  continue
				if(c === "{") {
					++braceCount;
					hadBraces = true;
				}
				else if(c === "}") {
					--braceCount;
				}
				
				//If the braces are evenly matched,
				//and our value string is not just whitespace characters
				//and the next character to add is whitespace,
				//then we are done building the value string
				if(braceCount === 0 && /\S/.test(value) && /\s/.test(c)) {
					value = value.trim();
					
					//In the stupid format, the same key can appear multiple times.
					//If this happens, then what we really want is to treat that key
					//as an array
					if(key in json) {
						
						//Convert value stored at that key to
						//an array if it isn't already one
						if(!Array.isArray(json[key])) {
							var obj = json[key];
							json[key] = [obj];
						}

						//If the value had {} characters, then
						//it will consist of other key/value pairs.
						if(hadBraces && level < maxLevel) {                    
							json[key] = json[key].concat(toJSON(value, level+1));
							hadBraces = false;
						}
						else {
							json[key] = json[key].concat(value);
						} 
					}
					
					else {
						if(hadBraces && level < maxLevel) {                    
							json[key] = toJSON(value, level+1);
							hadBraces = false;
						}
						else {
							json[key] = value;
						} 
					}
					
					
					buildingKey = true;
					key = "";
					value = "";
				}
			}
		}
		return json;
	}
</script>
<?php endif; ?>