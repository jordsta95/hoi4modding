<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventOutcomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_outcomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->string('outcome_name')->nullable();
            $table->text('outcome_ai_chance')->nullable();
            $table->text('outcome_trigger')->nullable();
            $table->text('outcome_reward')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_outcomes');
    }
}
