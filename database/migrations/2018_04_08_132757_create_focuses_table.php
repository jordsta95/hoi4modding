<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFocusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('focuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('focus_tree_id');
            $table->mediumText('focus_title');
            $table->mediumText('focus_description')->nullable();
            $table->string('focus_gfx')->nullable();
            $table->integer('focus_ai_will_do_factor')->nullable();
            $table->integer('focus_time_to_complete')->nullable();
            $table->integer('focus_x')->nullable();
            $table->integer('focus_y')->nullable();
            $table->mediumText('focus_bypass')->nullable();
            $table->mediumText('focus_available')->nullable();
            $table->mediumText('focus_reward')->nullable();
            $table->mediumText('focus_prerequisite')->nullable();
            $table->mediumText('focus_mutually_exclusive')->nullable();
            $table->mediumText('focus_complete_tooltip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('focuses');
    }
}
