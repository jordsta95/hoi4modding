<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mod_id');
            $table->string('event_type')->nullable(); //news/country event
            $table->string('event_title')->nullable();
            $table->text('event_description')->nullable();
            $table->string('event_gfx')->nullable();
            $table->boolean('event_is_triggered_only')->nullable();
            $table->boolean('event_fire_only_once')->nullable();
            $table->boolean('event_major')->nullable();
            $table->text('event_trigger')->nullable();
            $table->text('event_mean_time_to_happen')->nullable();
            $table->text('event_immediate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
