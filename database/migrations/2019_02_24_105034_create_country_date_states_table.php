<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryDateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_date_states', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_date_id');
            $table->integer('state_id');
            $table->string('state_name');
            $table->boolean('core')->default(0);
            $table->boolean('controls')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_date_states');
    }
}
