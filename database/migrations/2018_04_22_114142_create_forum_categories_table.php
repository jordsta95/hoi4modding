<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->boolean('public')->default(1);
            $table->integer('order')->default(0);
            $table->timestamps();
        });

        DB::table('forum_categories')->insert(
            array(
                array(
                    'title'=>'Staff Only',
                    'description'=>'Subforum specifically for moderators and admins of the forum.',
                    'public' => false,
                    'order' => 0
                ),
                array(
                    'title' => 'Site Talk',
                    'description' => 'Discussions about the site and its features; bug reports, tool suggestions, etc. go here.',
                    'public' => true,
                    'order' => 0
                ),
                array(
                    'title' => 'Modders Corner',
                    'description' => 'Whether it\'s issues with modding, looking for team members, or talking about new modding features, this is the place to go.',
                    'public' => true,
                    'order' => 1,
                ),
                array(
                    'title' => 'General Chat',
                    'description' => 'Talk which isn\'t related to modding, and is not a forum game, goes here.',
                    'public' => true,
                    'order' => 2,
                ),
                array(
                    'title' => 'Forum Games',
                    'description' => 'Last one to post wins, and how high can you count before an admin posts. You know the games, this is the place for them.',
                    'public' => true,
                    'order' => 3
                ),
                array(
                    'title' => 'International',
                    'description' => 'Français, Español, Deutsche, русский, and more. General chat for non-English threads.',
                    'public' => true,
                    'order' => 4
                )
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_categories');
    }
}
