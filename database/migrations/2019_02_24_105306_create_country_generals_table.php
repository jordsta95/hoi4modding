<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_generals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('name');
            $table->string('gfx');
            $table->text('traits');
            $table->integer('level')->default(1);
            $table->boolean('is_army')->default(0);
            $table->boolean('is_field_marshal')->default(0);
            $table->boolean('is_navy')->default(0);
            $table->integer('attack_stat')->default(1);
            $table->integer('defence_stat')->default(1);
            $table->integer('planning_stat')->default(1);
            $table->integer('supply_stat')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_generals');
    }
}
