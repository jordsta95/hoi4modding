<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->mediumText('description');
            $table->boolean('public')->default(true);
            $table->boolean('admin_only')->default(false);
            $table->integer('parent_id')->nullable();
            $table->timestamps();
        });

        DB::table('forums')->insert(
            array(
                array(
                    'slug' => 'bug-reports',
                    'name' => 'Bug Reports',
                    'description' => 'Found a bug with the site or one of the tools? Report it here.',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 2
                ),
                array(
                    'slug' => 'suggestions',
                    'name' => 'Site Suggestions',
                    'description' => 'Whether it\'s a suggestion for one of the tools, the forum, or the site in general, we would love to hear about it.',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 2
                ),
                array(
                    'slug' => 'admin-general-chat',
                    'name' => 'General Chat',
                    'description' => 'General chat away from normal users. Whether you want to talk about normal stuff, or forum stuff, anything goes',
                    'public' => false,
                    'admin_only' => true,
                    'parent_id' => 1
                ),
                array(
                    'slug' => 'general-chat',
                    'name' => 'General Chat',
                    'description' => 'This is a general discussion forum. Chat about anything and everything here.',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 4
                ),
                array(
                    'slug' => 'team-search',
                    'name' => 'Team Search',
                    'description' => 'Looking for team members, or teams to join?',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 3
                ),
                array(
                    'slug' => 'mod-discussion',
                    'name' => 'Mod Discussion',
                    'description' => 'Whether it\'s something you\'ve made or a popular mod you enjoy.',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 3
                ),
                array(
                    'slug' => 'forum-games',
                    'name' => 'Forum Games',
                    'description' => 'Participate in ongoing games, or create your own.',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 5
                ),
                array(
                    'slug' => 'multiligual',
                    'name' => 'Multiligual Chat',
                    'description' => 'Forum for conversations in any language.',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 6
                ),
                array(
                    'slug' => 'french',
                    'name' => 'Français',
                    'description' => 'Parle en Français',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 6
                ),
                array(
                    'slug' => 'spanish',
                    'name' => 'Español',
                    'description' => 'Hablo Español',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 6
                ),
                array(
                    'slug' => 'german',
                    'name' => 'Deutsche',
                    'description' => 'Spreche Deutsche',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 6
                ),
                array(
                    'slug' => 'portuguese',
                    'name' => 'Portuguese',
                    'description' => 'Fala Portuguese',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 6
                ),
                array(
                    'slug' => 'russian',
                    'name' => 'русский',
                    'description' => 'Говорить по-русски',
                    'public' => true,
                    'admin_only' => false,
                    'parent_id' => 6
                ),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forums');
    }
}
