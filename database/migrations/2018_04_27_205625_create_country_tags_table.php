<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
            $table->string('country');
            $table->string('country_fr')->nullable();
            $table->string('country_es')->nullable();
            $table->string('country_de')->nullable();
            $table->timestamps();
        });

        $json = json_decode(file_get_contents(env('APP_URL', 'http://local.hoi4modding.test').'/tags.json'));
        foreach($json as $row){
            DB::table('country_tags')->insert(
                [
                    'tag' => $row->tag,
                    'country' => $row->country
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_tags');
    }
}
