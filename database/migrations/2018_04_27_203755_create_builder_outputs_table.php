<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuilderOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builder_outputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('output_id');
            $table->text('example');
            $table->text('default_outcome');
            $table->text('description');
            $table->text('description_fr')->nullable();
            $table->text('description_es')->nullable();
            $table->text('description_de')->nullable();
            $table->timestamps();
        });

        $json = json_decode(file_get_contents(env('APP_URL', 'http://local.hoi4modding.test').'/output.json'));
        foreach($json as $row){
            DB::table('builder_outputs')->insert(
                [
                    'output_id' => $row->id,
                    'example' => $row->example,
                    'default_outcome' => $row->default_outcome,
                    'description' => $row->description,
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builder_outputs');
    }
}
