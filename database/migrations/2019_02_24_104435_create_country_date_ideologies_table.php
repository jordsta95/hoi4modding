<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryDateIdeologiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_date_ideologies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_date_id');
            $table->string('ideology_name');
            $table->string('leader_name');
            $table->string('leader_gfx');
            $table->decimal('popularity');
            $table->string('country_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_date_ideologies');
    }
}
