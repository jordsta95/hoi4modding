<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddListChecks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('mods', function (Blueprint $table) {
            $table->boolean('has_focus_tree')->default(0);
            $table->boolean('has_event')->default(0);
            $table->boolean('has_country')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('focuses', function (Blueprint $table) {
            $table->dropColumn('has_focus_tree');
            $table->dropColumn('has_event');
            $table->dropColumn('has_country');
        });
    }
}
