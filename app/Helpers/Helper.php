<?php
use App\ForumComments;
use App\ForumPosts;
use App\User;
use App\Media;

function isBeta(){
	return env('IS_BETA');
}

function getUnreadForumPosts($userId, $lastActive){
	$output = ForumPosts::selectRaw('forum_posts.id, forum_posts.slug AS pslug, forum_posts.title, forums.slug AS fslug')
	->join('forum_comments', 'forum_posts.id', '=', 'forum_comments.forum_post_id')
	->join('forums', 'forum_posts.forum_id', '=', 'forums.id')
	->whereRaw("forum_comments.user_id = '".$userId."' AND forum_posts.updated_at > '".$lastActive."'")
	->groupBy("forum_posts.slug")
	->get();
	return $output;
}

function storage($file, $prefix = null){
	$nf = str_replace('/public/storage', '', $file);
	if(\Storage::has('public/'.$nf)){
		return '/media/get/'.$nf;
	}
	return $file;
}

function storageFile($file, $prefix = null){
	$nf = str_replace('/public/storage', '', $file);
	if(!\Storage::has('public/'.$nf)){
		if(file_exists($prefix.'/'.$nf)){
			$f = file_get_contents($prefix.'/'.$nf);
			\Storage::write('public/'.$nf, $f);
			return \Storage::get('public/'.$nf);
		}
	}else{
		return \Storage::get('public/'.$nf);
	}
	return $file;
}

function storagePath($path = null){
	return _ROOT_.'/../public_html/storage/app'.(!empty($path) ? '/' : '').ltrim($path, '/');
}

function preprocessHOIFile($content){
	$content = str_replace(["\r\n", "\n\r", "\r",PHP_EOL], "\n", $content);
	$lines = explode("\n", $content);
	$noComments = "";
	foreach($lines as $line){
		$line = preg_replace('/[[:^print:]]/', '', $line);;
		if(strpos($line, '#') === false || substr(trim($line), 0, 1) !== '#'){
			$noComments .= $line . " \n";
		}
	}
	$noComments .= ' ';
	return $noComments;
}


function fileToJson($s, $level = 0, $maxLevel = 2, $children = 1){
	$s = preprocessHOIFile($s);
	$key = "";
	$value = "";
	$buildingKey = true;
	$braceCount = 0;
	$hadBraces = false;
	$json = [];

	$i = 0;
	$len = strlen($s);

	while($i < $len) {
		$c = $s[$i];
		$next = '';
		if(isset($s[$i + 1])){
			$next = $s[$i + 1];
		}

		if ($buildingKey) {
			if($c === "{" || $c === "}"){
				continue;
			}
			if($c !== "="){
				$key .= $c;
			}else{
				$buildingKey = false;
				$key = trim($key);
			}

		}else{
			$value .= $c;
			if($c === "{") {
				$braceCount++;
				$hadBraces = true;
			}elseif($c === "}"){
				$braceCount--;
			}

			if((!$hadBraces && $next  == PHP_EOL) || ($braceCount === $children && preg_match("/\S/", $value) && preg_match("/\s/", $c))) {
				\Log::info($value);
				//echo "Level ".$level.'/'.$maxLevel.' == <br>';
				$value = trim($value);
				if(array_key_exists($key, $json)) {
					if(!is_array($json[$key])){
						$json[$key] = [$json[$key]];
					}
					if($hadBraces && $level < $maxLevel) {
						$json[$key][] = fileToJson($value, ($level + 1));
						//set($json, $key, call_method(get($json, $key), "concat", call($toJSON, $value, _plus($level, 1.0))));
						$hadBraces = false;
					}else{
						$json[$key][] = $value;
						//set($json, $key, call_method(get($json, $key), "concat", $value));
					}

			  	}else{
					if($hadBraces && $level < $maxLevel) {
						$json[$key] = fileToJson($value, ($level + 1), $maxLevel, 0);
						$hadBraces = false;
					}else{
						$json[$key] = $value;

					}

				}

				$buildingKey = true;
				$key = "";
				$value = "";
			}
		}

		$i++;
	}


	return $json;

	/*
		if(isset($s[$i + 1])){
			$next = $s[$i + 1];
		}

		if($c == '='){
			$buildingKey = false;
		}

		if($next == '{'){
			$braceCount++;
			$hadBraces = true;
		}

		

		if($hadBraces && $braceCount == 0){
			$hadBraces = false;
		}

		if ($buildingKey) {
			if($c == '{' || $c == '}'){
				continue;
			}
			$key .= $c;
		}else{
			$value .= $c;

			if($next == "\n" && !$hadBraces){
				$key = trim($key);
				//Singluar value
				if(isset($json[$key])){
					if(!is_array($json[$key])){
						$json[$key] = [$json[$key]];
					}
					$json[$key][] = $value;
				}else{
					$json[$key] = $value;
				}
				$buildingKey = true;
				$key = "";
				$value = "";
			}else{
				$key = trim($key);

				if($next == '}' && ($braceCount - 1) == 0){
					$result = $value;
					if($level < $maxLevel){
						$result = fileToJson($value, ($level + 1));
					}
					if(isset($json[$key])){
						if(!is_array($json[$key])){
							$json[$key] = [$json[$key]];
						}
						$json[$key][] = $result;
					}else{
						$json[$key] = $result;
					}


					$buildingKey = true;
					$key = "";
					$value = "";
				}
			}
		}

		
		$i++;
	}


	return $json;
	*/
}


function patreons(){
	$emails = [
		'brushfirekhan@gmail.com',
		'Dacrazed@gmail.com',
		'obriami@yahoo.ca',
		'enaad501@gmail.com',
		'infiniryu@gmail.com',
		'killzone3fm@gmail.com',
		'put.422858@windowslive.com',
		'atomicsub927@gmail.com',
		'markutisrah@gmail.com',
		'dinoethan112@gmail.com',
	];

	$users = User::select('*');
	$i = 0;
	foreach($emails as $email){
		if($i == 0){
			$users = $users->where('email', 'LIKE', $email);
		}else{
			$users = $users->orWhere('email', 'LIKE', $email);
		}
		$i++;
	}

	return $users->get();
}

function getUserMedia(){
	if(\Auth::user()){
		return Media::where('user_id', '=', \Auth::user()->id)->get();
	}
	return [];
}

function bugUrl(){
	return '/forum/bug-reports/create?bug='.urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
}