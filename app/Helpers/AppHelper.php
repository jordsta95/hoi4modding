<?php
namespace App\Helpers;
use App\ForumComments;
use App\ForumPosts;
use App\User;
use Storage;
use stdClass;

class AppHelper{
	public function getUnreadForumPosts($userId, $lastActive){
		if(isset($_GET['markAllAsRead'])){
			$user = User::find($userId);
			$lastActive = date('Y-m-d H:i:s');
			$user->last_active = $lastActive;
			$user->save();
		}
		$output = ForumPosts::selectRaw('forum_posts.id, forum_posts.slug AS pslug, forum_posts.title, forums.slug AS fslug')
		->join('forum_comments', 'forum_posts.id', '=', 'forum_comments.forum_post_id')
		->join('forums', 'forum_posts.forum_id', '=', 'forums.id')
		->whereRaw("forum_comments.user_id = '".$userId."' AND forum_posts.updated_at > '".$lastActive."'")
		->groupBy("forum_posts.slug")
		->get();
		return $output;
	}

	public static function instance(){
		return new AppHelper();
	}

	public function getAllTraits($type = 'country'){
		$base = '/public/hoi4/traits/';
		$lang = Storage::get($base.'traits_l_english.yml');
		$traits = Storage::get($base.$type.'_leader/00_traits.txt');

		$lang = str_replace('l_english:', '', $lang);
		$langArray = [];
		$lang = explode("\n", $lang);
		foreach($lang as $line){
			$kv = explode(':0', $line);
			if(isset($kv[1])){
				$kv[1] = trim(trim($kv[1]), '"');
				$langArray[trim($kv[0])] = $kv[1];
			}
		}

		$lineHelper = explode("\n", $traits);
		foreach($lineHelper as $key => $line){
			if(substr($line, 0, 1) === '#'){
				unset($lineHelper[$key]);
			}
		}
		$traits = implode("\n", $lineHelper);
		$traits = trim(rtrim(ltrim(trim(ltrim(trim(ltrim(trim($traits), 'leader_traits')), '=')), '{'), '}'));
		$traitsArray = [];
		$t = explode("\n", $traits);
		$opening = 0;
		$key = null;
		foreach($t as $line){
			if($opening === 0){
				if(strpos($line, '{') !== false){
					$key = trim(rtrim(trim(rtrim(trim($line), '{')), '='));
					$traitsArray[$key] = '';
					$opening++;
				}
			}else{
				$traitsArray[$key] .= $line;
				if(strpos($line, '{') !== false){
					$opening++;
				}
				if(strpos($line, '}') !== false){
					$opening--;
					if($opening === 0){
						$traitsArray[$key] = rtrim($traitsArray[$key], '}');
					}
				}
			}
		}

		$obj = new stdClass();
		$obj->lang = $langArray;
		$obj->traits = $traitsArray;
		return $obj;

	}

	public function getAllResearch(){
		$base = '/public/hoi4/research/';
		$tabs = Storage::files($base.'tabs');

		$techs = [];
		$langArray = [];

		$langFile = Storage::get($base.'research_l_english.yml');
		$langFile = str_replace('l_english:', '', $langFile);
		$langArray = [];
		$langFile = explode("\n", $langFile);
		foreach($langFile as $line){
			$kv = explode(':0', $line);
			if(isset($kv[1])){
				$kv[1] = trim(trim($kv[1]), '"');
				$langArray[trim($kv[0])] = $kv[1];
			}
		}


		foreach($tabs as $tab){
			$file = Storage::get($tab);
			$lineHelper = explode("\n", $file);
			foreach($lineHelper as $key => $line){
				if(substr(trim($line), 0, 1) === '#'){
					unset($lineHelper[$key]);
				}
			}
			$file = implode("\n", $lineHelper);

			$file = trim(rtrim(ltrim(trim(ltrim(trim(ltrim(trim($file), 'technologies')), '=')), '{'), '}'));
			$traitsArray = [];
			$t = explode("\n", $file);
			$opening = 0;
			$key = null;
			foreach($t as $line){
				if($opening === 0){
					if(strpos($line, '{') !== false){
						$key = trim(rtrim(trim(rtrim(trim($line), '{')), '='));
						$techs[$key] = '';
						$opening++;
					}
				}else{
					$techs[$key] .= $line;
					if(strpos($line, '{') !== false){
						$opening++;
					}
					if(strpos($line, '}') !== false){
						$opening--;
						if($opening === 0){
							$techs[$key] = rtrim($techs[$key], '}');
						}
					}
				}
			}
			

			
		}
		$obj = new stdClass();
		$obj->lang = $langArray;
		$obj->tech = $techs;
		return $obj;
	}
}