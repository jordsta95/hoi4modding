<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model{
    public function mod(){
		return $this->belongsTo(Mod::class);
	}

	public function ideas(){
		return $this->hasMany(IdeaOptions::class);
	}

	public function ideasOrdered(){
		return $this->hasMany(IdeaOptions::class)->orderBy('group');
	}
}
