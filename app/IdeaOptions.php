<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdeaOptions extends Model
{
    public function group(){
    	return $this->belongsTo(Idea::class);
    }
}
