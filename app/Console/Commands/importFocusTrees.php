<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use Illuminate\Filesystem\Filesystem;
use App\FocusTree;
use App\Mod;
use App\User;
use App\UserMod;
use App\Focuses;
use App\Imports;
use Mail;
use Carbon\Carbon;


class importFocusTrees extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'focusTree:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Imports all unimported Focus Trees';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$toImport = Imports::where('type', '=', 'focus-tree')->get();
		$now = Carbon::now();

		$checkedTimeDiff = false;
		foreach($toImport as $row){
			if(!Storage::disk('local')->exists($row->filename) || !Storage::disk('local')->exists($row->localisation)){
				$row->delete();
				continue;
			}
			
			$file = Storage::disk('local')->get($row->filename);
			$localisation = Storage::disk('local')->get($row->localisation);

			$user = User::find($row->user_id);
			$tree = FocusTree::find($row->model_id);

			if(empty($tree)){
				$row->delete();
				continue;
			}

			if(!$checkedTimeDiff){
				
				$toCheck = new Carbon($row->created_at);
				$diff = $now->diffInSeconds($toCheck);
				//10 minute check
				if($diff > (11 * 60)){
					Mail::send('emails.imports.focustree.faulty-notification', ['tree' => $tree, 'user' => $user, 'row' => $row], function ($m) use ($user) {
						$m->to('jhoyland07@hotmail.co.uk')->subject('[HOI4 Modding]: Imports failed for more than 10 minutes');
					});
				}
				if($diff > (21 * 60)){
					$row->delete();
					Storage::disk('local')->delete($row->filename);
					Storage::disk('local')->delete($row->localisation);
				}
				$checkedTimeDiff = true;
				
			}

			if(json_decode($file)){
				$json = $file;

				$toImport = json_decode($json);
				$import = true;
				$imported = 0;
				if(isset($toImport->focus_tree) && isset($toImport->focus_tree->focus)){
					

					$lang = [];
					$langLines = explode("\n", $localisation);
					foreach($langLines as $line){
						$e = explode(":", $line);
						if(isset($e[1])){
							$lang[trim($e[0])] = rtrim(str_replace(['0 "', '1 "', '2 "', '3 "', '4 "'], "", $e[1]), '"');
						}
					}

					foreach($toImport->focus_tree->focus as $focus){
						if(isset($focus->id)){
							$focus_prerequisite = (isset($focus->prerequisite)) ? $this->toolifyValue($focus->prerequisite) : '';
							$focus_mutual = (isset($focus->mutually_exclusive)) ? $this->toolifyValue($focus->mutually_exclusive) : '';

							$insert = [
									'focus_tree_id' => $row->model_id,
									'external_id' => $focus->id,
									'focus_title' => isset($lang[$focus->id]) ? $lang[$focus->id] : 'undefined',
									'focus_description' => isset($lang[$focus->id.'_desc']) ? $lang[$focus->id.'_desc'] : 'undefined', 
									'focus_gfx' => isset($focus->icon) ? str_replace('GFX_', '/media/get/hoi4/focus_icons/', $focus->icon).'.png' : null, 
									'focus_ai_will_do_factor' => isset($focus->ai_will_do) ? $focus->ai_will_do : 1, 
									'focus_time_to_complete' => isset($focus->cost) ? $focus->cost : 1, 
									'focus_x' => isset($focus->x) ? $focus->x : 1,
									'focus_y' => isset($focus->y) ? $focus->y : 1,
									'focus_bypass' => isset($focus->bypass) ? rtrim(ltrim($focus->bypass, '{'), '}') : '',
									'focus_available' => (isset($focus->available) && is_string($focus->available)) ? rtrim(ltrim($focus->available, '{'), '}') : '',
									'focus_reward' => isset($focus->completion_reward) ? rtrim(ltrim($focus->completion_reward, '{'), '}') : '',
									'focus_prerequisite' => $focus_prerequisite,
									'focus_mutually_exclusive' => $focus_mutual,
									'focus_complete_tooltip' => isset($focus->complete_tooltip) ? rtrim(ltrim(isset($focus->complete_tooltip), '{'), '}') : '',
									'focus_relative_position' => (isset($focus->relative_position_id) && !empty($focus->relative_position_id)) ? $focus->relative_position_id : '',
									'created_at' => date('Y-m-d G:i:s'),
									'updated_at' => date('Y-m-d G:i:s'),
									'filter' => isset($focus->search_filters) ? is_array($focus->search_filters) ?  rtrim(ltrim($focus->search_filters[0], '{'), '}') : rtrim(ltrim($focus->search_filters, '{'), '}') : '',
								];
							Focuses::insert(
								$insert
							);
							$imported++;
						}
					}
					
					
				}
				if($imported === 0){
					$import = false;
				}

			}else{
				$file = fileToJson($file);

				if(empty($file)){
					$this->failedImport($tree, $user, $row);
					$row->delete();
					continue;
				}

				$focuses = [];
				$fuckedFocus = [];
				$toUnfuck = [];
				$unFucked = null;

				unset($file['focus_tree']);
				unset($file['id']);
				unset($file['country']);

				if(isset($file['focus'])){
					foreach($file['focus'] as $key => $value){
						if(is_numeric($key)){
							$focuses[] = $value;
						}else{
							$fuckedFocus[$key] = $value;
						}
					}

					$focuses[] = $fuckedFocus;
					foreach($focuses as $focus){
						$thisFocus = [];
						foreach($focus as $key => $value){
							if($key == 'id' && is_array($value)){
								foreach($value as $id => $val){
									$keys = ['x', 'y', 'cost', 'available_if_capitulated'];
									if(in_array($id, $keys)){
										$thisFocus[$id] = $val;
									}else{
										$ex = explode("icon", $id);
										$thisFocus['id'] = trim($ex[0]);
										if(is_array($val)){
											$val = 'undefined';
										}
										$thisFocus['icon'] = trim($val);
									}
								}
							}else{
								$thisFocus[$key] = $value;
							}
						}
						$toUnfuck[] = $thisFocus;
					}
				}
				$json = json_encode($toUnfuck);

				if(empty($json)){
					$this->failedImport($tree, $user, $row);
					$row->delete();
					continue;
				}
				
				$import = $this->importViaJSON($json, $localisation, $row->model_id);
			}

			
			if($import){
				echo "- Imported Focus tree \n";
				Mail::send('emails.imports.focustree.success', ['tree' => $tree, 'user' => $user, 'row' => $row], function ($m) use ($user) {
					$m->to($user->email)->subject('[HOI4 Modding]: Import Successful');
				});
				echo "- Sent success email\n";
				$row->delete();
				Storage::disk('local')->delete($row->filename);
				Storage::disk('local')->delete($row->localisation);
			}else{
				$toCheck = new Carbon($row->created_at);
				$diff = $now->diffInSeconds($toCheck);
				if($diff > (21 * 60)){
					$this->failedImport($tree, $user, $row);
					$row->delete();
					Storage::disk('local')->delete($row->filename);
					Storage::disk('local')->delete($row->localisation);
				}else{
					echo "- Faulty import? \n";
					Mail::send('emails.imports.focustree.faulty', ['tree' => $tree, 'user' => $user, 'row' => $row], function ($m) use ($user) {
						$m->to($user->email)->subject('[HOI4 Modding]: There was an issue with your import');
					});
					Mail::send('emails.imports.focustree.faulty-notification', ['tree' => $tree, 'user' => $user, 'row' => $row], function ($m) use ($user) {
						$m->to('jhoyland07@hotmail.co.uk')->subject('[HOI4 Modding]: Import issue');
					});
					echo "- Sent faulty email\n";
				}
			}
		}

		
		/*
		$folders = Storage::directories('/public/mods');
		$count = 0;
		foreach($folders as $f){
			$modid = str_replace('public/mods/', '', $f);
			echo 'Checking folders for mod id: '.$modid."\n";
			if(Storage::exists($f.'/export')){
				$exportFolders = Storage::directories($f.'/export');
				foreach($exportFolders as $e){
					$timestamp = (int) str_replace($f.'/export/', '', $e);
					$toDeleteFrom = (int) date('Ymdgis', strtotime('-1 days'));
					if($toDeleteFrom > $timestamp){
						Storage::deleteDirectory('/'.$e);
					}
				}
				if(count(Storage::directories($f.'/export')) === 0){
					Storage::deleteDirectory('/'.$f.'/export');
				}

				$files = Storage::files('/'.$f);
				if(count(Storage::directories($f)) === 0 && empty($files)){
					Storage::deleteDirectory('/'.$f);
					$count++;
				}
			}
		}

		echo $count.' focus trees imported';
		*/

	}

	public function importViaJSON($focusTree, $languageFile, $id){
		$return = false;

		$lang = [];
		$langLines = explode("\n", $languageFile);
		foreach($langLines as $line){
			$e = explode(":", $line);
			if(isset($e[1])){
				$lang[trim($e[0])] = rtrim(str_replace(['0 "', '1 "', '2 "', '3 "', '4 "'], "", $e[1]), '"');
			}
		}

		$json = json_decode($focusTree);

		foreach($json as $focus){
			$focus_prerequisite = (isset($focus->prerequisite)) ? $this->toolifyValue($focus->prerequisite) : '';
			$focus_mutual = (isset($focus->mutually_exclusive)) ? $this->toolifyValue($focus->mutually_exclusive) : '';
			$available = '';
			if(isset($focus->available) && !empty($focus->available)){
				foreach($focus->available as $key => $a){
					if(is_array($a) || is_object($a)){
						if(is_object($a)){
							foreach($a as $k => $val){
								if(is_array($val)){
									foreach($val as $va){
										$available .= ' '.$k.' = '.$va."\n";
									}
								}else{
									$available .= ' '.$k.' = '.$val."\n";
								}
							}
						}else{
							foreach($a as $v){
								$available .= ' '.$key.' = '.$v."\n";
							}
						}
					}else{
						$available .= ' '.$key.' = '.$a;
					}
				}	
			}

			$completion_reward = '';
			if(isset($focus->completion_reward) && !empty($focus->completion_reward)){
				foreach($focus->completion_reward as $key => $a){
					if(is_array($a) || is_object($a)){
						if(is_object($a)){
							foreach($a as $k => $val){
								if(is_array($val)){
									foreach($val as $va){
										$completion_reward .= ' '.$k.' = '.$va."\n";
									}
								}else{
									$completion_reward .= ' '.$k.' = '.$val."\n";
								}
							}
						}else{
							foreach($a as $v){
								$completion_reward .= ' '.$key.' = '.$v."\n";
							}
						}
					}else{
						$completion_reward .= ' '.$key.' = '.$a;
					}
				}	
			}

			$tooltip = '';
			if(isset($focus->completion_tooltip) && !empty($focus->completion_tooltip)){
				foreach($focus->completion_tooltip as $key => $a){
					if(is_array($a) || is_object($a)){
						if(is_object($a)){
							foreach($a as $k => $val){
								if(is_array($val)){
									foreach($val as $va){
										$tooltip .= ' '.$k.' = '.$va."\n";
									}
								}else{
									$tooltip .= ' '.$k.' = '.$val."\n";
								}
							}
						}else{
							foreach($a as $v){
								$tooltip .= ' '.$key.' = '.$v."\n";
							}
						}
					}else{
						$tooltip .= ' '.$key.' = '.$a;
					}
				}	
			}

			$bypass = '';
			if(isset($focus->bypass) && !empty($focus->bypass)){
				foreach($focus->bypass as $key => $a){
					if(is_array($a) || is_object($a)){
						if(is_object($a)){
							foreach($a as $k => $val){
								if(is_array($val)){
									foreach($val as $va){
										$bypass .= ' '.$k.' = '.$va."\n";
									}
								}else{
									$bypass .= ' '.$k.' = '.$val."\n";
								}
							}
						}else{
							foreach($a as $v){
								$bypass .= ' '.$key.' = '.$v."\n";
							}
						}
					}else{
						$bypass .= ' '.$key.' = '.$a;
					}
				}	
			}

			$factor = 1;
			if(isset($focus->ai_will_do)){
				if(isset($focus->ai_will_do->factor)){
					$factor = $focus->ai_will_do->factor;
				}
			}


			if(isset($focus->id)){
				$insert = [
						'focus_tree_id' => $id,
						'external_id' => $focus->id,
						'focus_title' => isset($lang[$focus->id]) ? $lang[$focus->id] : 'undefined',
						'focus_description' => isset($lang[$focus->id.'_desc']) ? $lang[$focus->id.'_desc'] : 'undefined', 
						'focus_gfx' => isset($focus->icon) ? str_replace('GFX_', '/media/get/hoi4/focus_icons/', $focus->icon).'.png' : null, 
						'focus_ai_will_do_factor' => $factor, 
						'focus_time_to_complete' => isset($focus->cost) ? $focus->cost : 1, 
						'focus_x' => isset($focus->x) ? $focus->x : 1,
						'focus_y' => isset($focus->y) ? $focus->y : 1,
						'focus_bypass' => (!empty($bypass)) ? rtrim(ltrim($bypass, '{'), '}') : '',
						'focus_available' => (!empty($available)) ? rtrim(ltrim($available, '{'), '}') : '',
						'focus_reward' => (!empty($completion_reward)) ? rtrim(ltrim($completion_reward, '{'), '}') : '',
						'focus_prerequisite' => $focus_prerequisite,
						'focus_mutually_exclusive' => $focus_mutual,
						'focus_complete_tooltip' => (!empty($tooltip)) ? rtrim(ltrim($tooltip, '{'), '}') : '',
						'focus_relative_position' => (isset($focus->relative_position_id) && !empty($focus->relative_position_id)) ? $focus->relative_position_id : '',
						'created_at' => date('Y-m-d G:i:s'),
						'updated_at' => date('Y-m-d G:i:s')
					];
				Focuses::insert(
					$insert
				);
			}
		}
		
		$return = true;
		return $return;
	}

	public function failedImport($tree, $user, $row){
		echo "- Unable to parse focus tree \n";
		Mail::send('emails.imports.focustree.fail', ['tree' => $tree, 'user' => $user, 'row' => $row], function ($m) use ($user) {
			$m->to($user->email)->subject('[HOI4 Modding]: Import Failed');
		});
		echo "- Sent failed email\n";
	}

	protected function toolifyValue($value){
		$string = '';
		if(is_object($value) && isset($value->focus)){
			if(is_array($value->focus)){
				foreach($value->focus as $v){
					$v = trim($v);
					$v = preg_replace('/\s+/', '', $v);
					$v = ltrim(rtrim($v, '}'), '{focus=');
					$v = str_replace("focus=", "||", $v);
					$string .= $v."&&";
				}
			}else{
				$string .= $value->focus."&&";
			}
		}

		//Imported via JSON
		if(empty($string) && !empty($value)){
			if(!isset($value->focus)){
				if(is_array($value)){
					foreach($value as $v){
						$v = trim($v);
						$v = str_replace(' ', '', $v);
						$v = ltrim(rtrim($v, '}'), '{focus=');
						$v = str_replace("focus=", "||", $v);
						$string .= $v."&&";
					}
				}else{
					$v = trim($value);
					$v = str_replace(' ', '', $v);
					$v = ltrim(rtrim($v, '}'), '{focus=');
					$v = str_replace("focus=", "||", $v);
					$string .= $v."&&";
				}
			}
		}

		if(is_array($value) && empty($string))
			foreach($value as $v){
				if(gettype($v) == 'string'){
					$v = trim($v);
					$v = preg_replace('/\s+/', '', $v);
					$v = ltrim(rtrim($v, '}'), '{focus=');
					$v = str_replace("focus=", "||", $v);
					$string .= $v."&&";
				}
			}

		return rtrim($string, '&&');
	}
}
