<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use Illuminate\Filesystem\Filesystem;

class clearExportFolders extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'files:clearExports';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clear export folders and empty folders';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$folders = Storage::directories('/public/mods');
		$count = 0;
		foreach($folders as $f){
			$modid = str_replace('public/mods/', '', $f);
			echo 'Checking folders for mod id: '.$modid."\n";
			if(Storage::exists($f.'/export')){
				$exportFolders = Storage::directories($f.'/export');
				foreach($exportFolders as $e){
					$timestamp = (int) str_replace($f.'/export/', '', $e);
					$toDeleteFrom = (int) date('Ymdgis', strtotime('-1 days'));
					if($toDeleteFrom > $timestamp){
						Storage::deleteDirectory('/'.$e);
					}
				}
				if(count(Storage::directories($f.'/export')) === 0){
					Storage::deleteDirectory('/'.$f.'/export');
				}

				$files = Storage::files('/'.$f);
				if(count(Storage::directories($f)) === 0 && empty($files)){
					Storage::deleteDirectory('/'.$f);
					$count++;
				}
			}
		}

		echo $count.' mod folders removed';

	}
}
