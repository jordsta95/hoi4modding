<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'can_post', 'steam','description', 'youtube_account', 'forum_strikes', 'last_active', 'email'
    ];

    public function mods(){
        return $this->belongsToMany(Mod::class,'user_mods', 'user_id', 'mod_id')->withPivot('role');
    }

    public function forumComments(){
        return $this->hasMany(ForumComments::class, 'user_id');
    }

    public function forumPosts(){
        return $this->hasMany(ForumPosts::class, 'user_id');
    }

    public function media(){
        return $this->hasMany(Media::class);
    }

}
