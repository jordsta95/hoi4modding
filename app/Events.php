<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    public function mod(){
    	return $this->belongsTo(Mod::class);
    }

    public function outcomes(){
    	return $this->hasMany(EventOutcomes::class, 'event_id');
    }

    //Fix stupid url issues
    public function getEventGfxAttribute($value)
    {

        return str_replace('/home/hoimoddi/beta.hoi4modding.com', '', $value);
    }
}
