<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumPosts extends Model
{
    public function comments(){
    	return $this->hasMany(ForumComments::class, 'forum_post_id');
    }

    public function user(){
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function forum(){
    	return $this->belongsTo(Forum::class);
    }
}
