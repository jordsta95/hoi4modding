<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public function mod(){
		return $this->belongsTo(Mod::class);
	}

	public function dates(){
		return $this->hasMany(CountryDate::class);
	}

	public function ideas(){
		return $this->hasMany(CountryIdea::class);
	}

	public function generals(){
		return $this->hasMany(CountryGeneral::class);
	}

	public function forDate($date){
		return $this->hasOne(CountryDate::class)->where('country_dates.id', '=', $date);
	}

	public function army(){
		return $this->hasMany(CountryGeneral::class)->where('is_army', '=', 1);
	}

	public function navy(){
		return $this->hasMany(CountryGeneral::class)->where('is_navy', '=', 1);
	}

	public function cabinet(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'cabinet');
	}

	public function tank(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'tank-designer');
	}

	public function ship(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'ship-designer');
	}

	public function air(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'air-designer');
	}

	public function gun(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'gun-designer');
	}

	public function industrial(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'industrial-designer');
	}

	public function doctrine(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'doctrine-advisor');
	}

	public function armygs(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'army-general-staff');
	}

	public function navygs(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'navy-general-staff');
	}

	public function airgs(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'air-general-staff');
	}

	public function military(){
		return $this->hasMany(CountryIdea::class)->where('national_spirit_type', '=', 'military-advisor');
	}


}
