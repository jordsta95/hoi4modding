<?php

namespace App\Http\Contracts\Admin;

use App\Models\Media;

class MediaManager{

	/**
	* Render the media manager's default view, without being inside a folder
	*
	*/
	public function renderMediaManager(){
		$folders = $this->getFolders();
		$images = $this->getImages();
		return view('admin.media-manager', ['folders' => $folders, 'images' => $images, 'path' => '']);
	}

	/**
	* Render the media manager with a path set
	*
	*/
	public function renderMediaManagerPath($path){
		$folders = $this->getFolders($path);
		$images = $this->getImages($path);
		return view('admin.media-manager', ['folders' => $folders, 'images' => $images, 'path' => $path]);
	}

	/**
	* Get all media paths, which are children of current path
	*
	*/
	protected function getFolders($path = null){
		$media = new Media();
		$folders = [];
		$newPath = '';

		if(!empty($path)){
			$newPath = str_replace('-', '/', $path);
			//Get only subdirectories of current path
			$media = $media->where('path', 'LIKE', '%/'.$newPath.'/%');

			//Get last folder
			$parts = explode('/', $newPath);
			$last = array_pop($parts);
		}

		$paths = $media->groupBy('path')->get();

		$topLevel = [];
		$values = [];

		//Kinda hacky way to get the next level's folders
		foreach($paths as $path){
			if(!empty($newPath)){
				$pathParts = explode('/'.$last, $path->path);
				$path->path = isset($pathParts[1]) ? $last.$pathParts[1] : null;
			}
			if(empty($path->path)){
				continue;
			}

			$directories = explode('/', $path->path);
			$added = 0;

			foreach($directories as $key => $dir){
				if(!empty($newPath) && empty($dir)){
					continue;
				}

				if($added == 0){
					if(!isset($folders[$dir])){
						$folders[$dir] = [];
					}
				}elseif($added == 1){
					if(!in_array($dir, $folders[$directories[$key - 1]])){
						$folders[$directories[$key - 1]][] = $dir;
					}
				}

				$added++;
			}
		}

		return $folders;
	}

	/**
	* Get all media in current directory
	*
	*/
	protected function getImages($path = null){
		$media = new Media();
		$folders = [];
		$newPath = '';
		if(!empty($path)){
			$newPath .= '%/';
		}
		$newPath .= str_replace('-', '/', $path);
		$media = $media->where('path', 'LIKE', $newPath);

		$media = $media->get();

		return $media;
	}

}