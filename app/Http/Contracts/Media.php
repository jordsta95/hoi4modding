<?php

namespace App\Http\Contracts;

use Imagick;
use Storage;
use App\Media as Model;
use stdClass;
use Auth;

class Media{
	protected $user;

	/**
	* Add/edit width/height for different sizes, and their names, for all image uploads to confine to (none square images fit to width)
	*
	*/
	public $sizes = [
		'focus' => ['width' => 95, 'height' => 85],
		'news-event' => ['width' => 397, 'height' => 153],
		'event' => ['width' => 210, 'height' => 176],
		'leader' => ['width' => 156, 'height' => 210],
		'idea' => ['width' => 65, 'height' => 67],
		'bug' => ['width' => 400, 'height' => 400],
		'national-spirit' => ['width' => 60, 'height' => 68]
	];

	/**
	* Generic upload function, nothing fancy here
	* ===
	* request - Post request 
	*
	*/
	public function upload($request){
		$this->setUser();

		if(empty($request->image)){
			return null;
		}

		$options = [];

		if(!empty($request->path)){
			$options['path'] = $request->path;
		}

		if(!empty($request->filename)){
			$options['name'] = $request->filename;
		}

		return $this->handleUpload($request->file('image'), $options);
	}

	public function mediaManagerUpload($request){
		$this->setUser();
		
		if(empty($request->image)){
			return null;
		}

		$options = [];

		if(!empty($request->path)){
			$options['path'] = $request->path;
		}

		if(!empty($request->filename)){
			$options['name'] = $request->filename;
		}

		if(!is_array($request->image)){
			return '<script> var json = '.$this->handleUpload($request->file('image'), $options).'; parent.mediaJson(json);</script>';
		}else{
			foreach($request->file('image') as $img){
				$json[] = $this->handleUpload($img, $options);
			}
			return '<script> var json = '.json_encode($json).'; parent.mediaJsonMultiple(json);</script>';
		}
	}

	public function setUser($id = null){
		if(empty($id) && !isset($id)){
			$this->user = Auth::user();
		}else{
			$thing = new stdClass();
			$thing->id = $id;
			$this->user = $thing;
		}
	}

	/**
	* Generic upload function, nothing fancy here
	* ===
	* request - Post request 
	*
	*/
	public function uploadAjax($request){
		$this->setUser();

		if(empty($request->image)){
			return null;
		}

		$options = [];

		if(!empty($request->path)){
			$options['path'] = $request->path;
		}

		if(!empty($request->filename)){
			$options['name'] = $request->filename;
		}

		return '<script> var json = '.$this->handleUpload($request->file('image'), $options).'; parent.mediaJson(json);</script>';
	}

	/**
	* Function to handle whether upload is an image, and should be treated as such, or whether it's another file type
	* ===
	* file - $request which is ->file('name');
	* options - array to override default data passed ['name' => 'name wanted', 'path' => 'file path']
	*
	*/
	public function handleUpload($file, $options = []){
		if(empty($this->user) && !isset($this->user)){
			return redirect()->back()->withErrors(['notLoggedIn', 'Must be logged in to upload files']);
		}

		$images = ['gif', 'png', 'jpg', 'jpeg'];

		if(in_array(strtolower($file->extension()), $images)){
			return $this->uploadImage($file, $options);
		}else{
			//Not an image. Should we be doing anything with this?
			return false;
		}
	}

	/**
	* Handle uploading and resizing of images
	* ===
	* file - $request which is ->file('name');
	* options - array to override default data passed ['name' => 'name wanted', 'path' => 'file path']
	*
	*/
	public function uploadImage($image, $options = []){
		if(empty($this->user) && !isset($this->user)){
			return redirect()->back()->withErrors(['notLoggedIn', 'Must be logged in to upload files']);
		}

		$name = $image->getClientOriginalName();
		$extension = $image->extension();
		$filesize = $image->getClientSize();
		$path = '/uploads/default';

		if(!empty($options['name'])){
			$name = $options['name'];
		}

		if(!empty($options['path'])){
			//Trim / off the start, if passed accross, and add if not
			$path = '/'.ltrim(str_replace('-','_',strtolower($options['path'])), '/');
		}

		$path = rtrim($path, '/');
		$path = str_replace(['-', ' '], '_', $path); //So - can be used for folder separation
		$name = rtrim($name, '.'.$extension);

		$filename = $this->getValidFileName($path.'/'.$name, $extension);

		Storage::write($filename, file_get_contents($image));

		$media = Model::create([
			'filename' => rtrim(str_replace($path.'/', '', $filename), '.'.$extension),
			'path' => $path,
			'extension' => $extension,
			'size' => $filesize,
			'user_id' => $this->user->id,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		foreach($this->sizes as $sizeName => $size){
			$imagick = new Imagick();
			$originalImage = $imagick->readimageblob(file_get_contents($image));
			$resizeImage = $imagick->resizeImage($size['width'], $size['height'], 1, 0, true);
	    	$resizedImage = $imagick->getImageBlob();
	    	$filename = $this->getValidFileName($path.'/'.$sizeName.'_'.$name, $extension);
	    	Storage::write($filename, $resizedImage);
		}

		return $media;
	}

	/**
	* Make sure the file being uploaded doesn't already exist
	* ===
	* filename - The desired filename
	* extension - extension of the file (because uploading untitled.png is fine if untitled.jpg exists)
	* num - Number of files already uploaded with that name
	*
	*/
	public function getValidFileName($filename, $extension, $num = 0){
		if($num > 0){
			$name = $filename.'_'.$num.'.'.$extension;
		}else{
			$name = $filename.'.'.$extension;
		}

		if(Storage::exists($name)){
			$name = $this->getValidFileName($filename, $extension, ($num + 1));
		}

		return $name;
	}

	/**
	* Get the file, and resized versions, based on the id
	* ===
	* id - id of the file in the database
	* size - desired size of the file
	*
	*/
	public function find($id, $size = ''){
		$media = Model::findOrFail($id);

		if(strtolower($media->extension) == 'gif'){
			//Resized gifs cannot animate - So we always want to pull out full size gif images
			$size = '';
		}

		$this->outputImage($media, $size);
	}

	/**
	* Get file from readable url; /media/path-to/media.png
	* ===
	* path - path to where it is stored on the server
	* filename - actual name of the file
	* extension - extension of the file
	*
	*/
	public function findByName($path, $filename, $extension){
		$media = new stdClass();
		$media->path = str_replace('-', '/', $path);
		$media->filename = $filename;
		$media->extension = $extension;
		$this->outputImage($media);
	}

	/**
	* Create image blob for browser to render image
	* ===
	* media - object containing media attributes
	* size - if looking for a specific resized variant
	*
	*/
	public function outputImage($media, $size = ''){
		$filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension;
		header("Content-Type: image/".$media->extension);
		echo Storage::get($filename);
    	die();
	}

	/**
	* Delete image
	*
	*/
	public function deleteImage($id){
		$media = Model::findOrFail($id);

		foreach($this->sizes as $size => $dimensions){
			Storage::delete($media->path.'/'.$size.'_'.$media->filename.'.'.$media->extension);
		}

		Storage::delete($media->path.'/'.$media->filename.'.'.$media->extension);
		$media->delete();
		return redirect()->back()->withSuccess('Image(s) deleted');
	}

	public function get($path = null){
		if($path){
			if(Storage::has('public/'.$path)){
				$f = Storage::get('public/'.$path);
				$parts = explode('.', $path);
				$last = array_pop($parts);
				header("Content-Type: image/".$last);
				echo $f;
		    	die();
			}
		}
		return null;
	}

	//Size is full path, including filename/extension, when copying core file.
	public static function copyImageToDirectory($id, $copyTo, $rename = null, $size = ''){
		if($id > 0){
			if(!empty($size)){
				$size = $size.'_';
			}
			$media = Model::find($id);
			if(empty($media)){
				return;
			}
			if(strtolower($media->extension) == 'gif'){
				//Resized gifs cannot animate - So we always want to pull out full size gif images
				$size = '';
			}
			$filename = $media->path.'/'.$size.$media->filename.'.'.$media->extension;
			$moveTo = rtrim($copyTo, '/');
			$extension = $media->extension;
			if(!empty($rename)){
				$moveTo .= '/'.$rename;
			}else{
				$moveTo .= '/'.$size.$media->filename;
			}
			if(!Storage::has($filename)){
				return;
			}
		}else{
			if(!Storage::has($size)){
				return;
			}
			$filename = $size;
			$file = Storage::get($filename);
			$path = explode('/', $filename);
			$last = end($path);
			$ex = explode('.', $last);
			$extension = end($ex);
			

			$moveTo = rtrim($copyTo, '/');
			if(!empty($rename)){
				$moveTo .= '/'.$rename;
			}else{
				$rename = str_replace('.'.$extension, '', $last);
				$moveTo .= '/'.$rename;
			}

		}

		$imagick = new Imagick();
		$originalImage = $imagick->readimageblob(Storage::get($filename));
		$reformatImage = $imagick->setformat('dds');
    	$reformattedImage = $imagick->getImageBlob();
    	
    	if(!Storage::has($moveTo.'.dds')){
	    	Storage::write($moveTo.'.dds', $reformattedImage);
			Storage::copy($filename, $moveTo.'.'.$extension);
		}
	}
}