<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mod;
use App\Country;
use App\CountryDate;
use App\CountryDateState;
use App\CountryDateIdeology;
use App\CountryIdea;
use App\CountryGeneral;
use App\User;
use App\UserMod;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Storage;

class CountryController extends Controller
{
    public function list(){
    	if(Auth::user()){
			$myMods = Auth::user()->mods;
		}else{
			$myMods = null;
		}

    	return view('countries.list', ['myMods' => $myMods]);
    }

    public function showCreate(){
    	if(Auth::user()){
			$id = Auth::user()->id;
			$mods = User::findOrFail($id)->mods;
		}else{
			$mods = [];
		}
    	return view('countries.create', ['mods' => $mods, 'can_edit' => true, 'is_editable' => false]);
    }

    public function delete($id){
    	$country = Country::findOrFail($id);
    	$name = $country->name;
    	$mod = $country->mod;
    	$check = UserMod::where('mod_id', '=', $mod->id)->where('user_id', '=', Auth::user()->id)->first();
    	if(strtolower($check->role) != 'creator'){
			return redirect()->back()->with('error', 'Only the creator of the parent mod can delete this');
		}
		foreach($country->dates as $date){
			foreach($date->states as $state){
				$state->delete();
			}
			foreach($date->ideologies as $i){
				$i->delete();
			}

			$date->delete();
		}

		foreach($country->ideas as $i){
			$i->delete();
		}

		foreach($country->generals as $g){
			$g->delete();
		}

		$country->delete();

		return redirect()->back()->with('success', $name.' successfully deleted');
    }

    public function create(Request $request){
    	if(Input::get('mod_id') == 'createNewMod'){
			//Create the mod
			$mod_logo_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('mod_name'))));
			if(!empty($request->file('mod_logo'))){
				$logo = $request->file('mod_logo');
				if ($request->file('mod_logo')->isValid()) {
					$path = $request->file('mod_logo')->move(public_path('/storage/mods'), $mod_logo_name.'.png');
				}
			}else{
				$mod_logo_name = 'default';
			}
			//$uploadedLogo = $logo->move(public_path('/storage/mods'))->getRealPath();
			$modid = \DB::table('mods')->insertGetId(
				[
					'mod_name' => Input::get('mod_name'), 
					'mod_description' => !empty(Input::get('mod_description')) ? Input::get('mod_description') : '',
					'mod_logo' => 'mods/'.$mod_logo_name.'.png',
					'created_at' => date('Y-m-d G:i:s'),
					'updated_at' => date('Y-m-d G:i:s')
				]
			);
			\DB::table('user_mods')->insert([
				'user_id' => Auth::user()->id,
				'mod_id' => $modid,
				'role' => 'Creator',
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]);
		}else{
			$modid = Input::get('mod_id');
		}

		$mod = Mod::find($modid);
		$mod->has_country = 1;
		$mod->save();

		$country = new Country();
		$country->name = $request->name;
		$country->colour = $request->colour;
		$country->capital = $request->capital;
		$country->war_support = $request->war_support;
		$country->culture = $request->culture;
		$country->tag = $request->tag;
		$country->language = $request->language;
		$country->stability = $request->stability;
		$country->mod_id = $modid;
		$country->save();

		switch($request->mod_type){
			case 'kaiserreich':
				$dates = [
					[
						'y' => 1936,
						'm' => 1,
						'd' => 1,
					]
				];
				$ideologies = [
					"totalist",
					"syndicalist",
					"radical_socialist",
					"social_democrat",
					"social_liberal",
					"market_liberal",
					"social_conservative",
					"authoritarian_democrat",
					"paternal_autocrat",
					"national_populist"
				];
				break;
			case 'endsieg':
				$dates = [
					[
						'y' => 1910,
						'm' => 1,
						'd' => 1,
					],
					[
						'y' => 1914,
						'm' => 1,
						'd' => 1,
					],
					[
						'y' => 1918,
						'm' => 3,
						'd' => 21,
					],
					[
						'y' => 1943,
						'm' => 7,
						'd' => 11,
					],
					[
						'y' => 1944,
						'm' => 6,
						'd' => 21,
					],
					[
						'y' => 1944,
						'm' => 12,
						'd' => 16,
					],
					[
						'y' => 1945,
						'm' => 3,
						'd' => 20,
					]
				];
				$ideologies = [
					"anarchist","fascism","left_wing_radical","leninist","market_liberal","national_socialist","paternal_autocrat"
					,"populist","social_conservative","social_democrat","social_liberal","stalinist"
				];
				break;
			default:
				$dates = [
					[
						'y' => 1936,
						'm' => 1,
						'd' => 1,
					],
					[
						'y' => 1939,
						'm' => 1,
						'd' => 1,
					]
				];
				$ideologies = [
					'democratic','fascism','communism','neutrality'
				];
			break;
		}

		foreach($dates as $date){
			$cd = new CountryDate();
			$cd->year = $date['y'];
			$cd->month = $date['m'];
			$cd->day = $date['d'];
			$cd->country_id = $country->id;
			$cd->created_at = date('Y-m-d H:i:s');
			$cd->updated_at = date('Y-m-d H:i:s');
			$cd->save();

			foreach($ideologies as $i){
				$cdi = new CountryDateIdeology();
				$cdi->country_date_id = $cd->id;
				$cdi->ideology_name = $i;
				$cdi->leader_name = 'TBC';
				$cdi->leader_gfx = '/media/get/hoi4/leaders/leader_unknown.png';
				$cdi->popularity = number_format(100 / count($ideologies), 2);
				$cdi->created_at = date('Y-m-d H:i:s');
				$cdi->updated_at = date('Y-m-d H:i:s');
				$cdi->save();
			}
		}

		return redirect('/country/edit/'.$country->id);

    }

    public function view($id){
    	$country = Country::find($id);
		$mod = $country->mod;
		$can_edit = $this->canEditCountry($country);
		return view('countries/view', ['country' => $country, 'mod' => $mod, 'can_edit' => $can_edit, 'is_editable' => false, 'research' => \AppHelper::instance()->getAllResearch()]);
    }

    public function edit($id){
    	$country = Country::find($id);
    	$mod = $country->mod;
		$can_edit = $this->canEditCountry($country);
		if(!$can_edit){
			return redirect('/country/')->with('error', 'You do not have access to edit '.$country->name);
		}
		
		$traits = \AppHelper::instance()->getAllTraits('unit');
		$leaderTraits = \AppHelper::instance()->getAllTraits('country');
		$research = \AppHelper::instance()->getAllResearch();

		return view('countries/edit', ['country' => $country, 'mod' => $mod, 'can_edit' => $can_edit, 'is_editable' => true, 'traits' => $traits, 'leaderTraits' => $leaderTraits, 'research' => $research]);
    }

    public function update(Request $request, $id){
    	if(empty($request->ideology_id)){
    		return json_encode(['success' => false, 'error' => 'Ideology not identified']);
    	}
    	if(empty($request->date_id)){
    		return json_encode(['success' => false, 'error' => 'Date not identified']);
    	}
    	$country = Country::find($id);
    	$dateIds = [];
    	foreach($country->dates as $d){
    		$dateIds[] = $d->id;
    	}
    	$date = CountryDate::find($request->date_id);
    	$ideology = CountryDateIdeology::find($request->ideology_id);

    	$ideology->leader_name = $request->leader_name;
    	$ideology->leader_gfx = $request->leader_gfx;
    	$ideology->popularity = $request->ideology_support;
    	$ideology->save();

    	$similarIdeologies = CountryDateIdeology::where('ideology_name', '=', $ideology->ideology_name)->whereIn('country_date_id', $dateIds)->get();
    	foreach($similarIdeologies as $s){
    		$s->country_name = $request->country_name;
    		$s->save();
    	}
    	
    	$date->research = $request->research;
    	$date->save();

    	parse_str($request->states, $states);
    	if(!empty($states)){
    		$states = $states['states'];
    		foreach($states as $stateId => $state){
	    		if($stateId != 'new'){
	    			$this->updateState($stateId, $state);
	    		}
	    	}

	    	if(isset($states['new'])){
	    		foreach($states['new'] as $stateId => $state){
	    			$this->addState($date->id, $state);
	    		}
	    	}
    	}

    	
    	parse_str($request->generals, $generals);
    	if(!empty($generals)){
	    	$generals = $generals['general'];

	    	foreach($generals as $generalId => $general){
	    		if($generalId != 'new'){
	    			$this->updateGeneral($generalId, $general);
	    		}
	    	}

	    	if(isset($generals['new'])){
	    		foreach($generals['new'] as $generalId => $general){
	    			$this->addGeneral($id, json_decode($general));
	    		}
	    	}
	    }

    }


    public function addIdea(Request $request, $id){
    	$idea = new CountryIdea();
    	$idea->country_id = $id;
    	$idea->name = $request->name;
    	$idea->short_desc = $request->short_desc;
    	$idea->long_desc = $request->long_desc;
    	$idea->gfx = $request->gfx;
    	$idea->effect = $request->effect;
    	$idea->national_spirit_type = $request->national_spirit_type;
    	$idea->created_at = date('Y-m-d H:i:s');
    	$idea->updated_at = date('Y-m-d H:i:s');
    	$idea->save();

    	return $idea;
    }

    public function updateState($dateStateId, $stateArray){
    	if(!empty($stateArray['state_id'])){
	    	$state = CountryDateState::find($dateStateId);
	    	if(isset($stateArray['remove'])){
	    		$state->delete();
	    		return;
	    	}
	    	$state->state_id = $stateArray['state_id'];
	    	$state->state_name = $stateArray['state_name'];
	    	$state->core = isset($stateArray['core']) ? 1 : 0;
	    	$state->controls = isset($stateArray['controls']) ? 1 : 0;
	    	$state->updated_at = date('Y-m-d H:i:s');
	    	$state->save();
	    }
    }

    public function addState($dateId, $stateArray){
    	if(!empty($stateArray['state_id'])){
	    	$state = new CountryDateState();
	    	$state->country_date_id = $dateId;
	    	$state->state_id = $stateArray['state_id'];
	    	$state->state_name = $stateArray['state_name'];
	    	$state->core = isset($stateArray['core']) ? 1 : 0;
	    	$state->controls = isset($stateArray['controls']) ? 1 : 0;
	    	$state->created_at = date('Y-m-d H:i:s');
	    	$state->updated_at = date('Y-m-d H:i:s');
	    	$state->save();
	    }
    }

    public function updateGeneral($id, $json){
    	$general = CountryGeneral::find($id);
    	if(isset($json['remove'])){
    		$general->delete();
    		return;
    	}
    	$json = json_decode($json['json']);
    	if(!empty($json)){
    		$general->name = $json->name;
    		$general->gfx = $json->gfx;
    		$general->traits = $json->traits;
    		$general->level = $json->level;
    		$general->is_army = ($json->type == 'ground') ? 1 : 0;
    		$general->is_field_marshal = $json->field_marshal;
    		$general->is_navy = ($json->type != 'ground') ? 1 : 0;
    		$general->attack_stat = $json->attack_stat;
    		$general->defence_stat = $json->defence_stat;
    		$general->planning_stat = $json->planning_stat;
    		$general->supply_stat = $json->supply_stat;
	    	$general->updated_at = date('Y-m-d H:i:s');
    		$general->save();
    	}
    }

    public function addGeneral($countryId, $json){
    	if(!empty($json)){
    		$general = new CountryGeneral();
    		$general->country_id = $countryId;
    		$general->name = $json->name;
    		$general->gfx = $json->gfx;
    		$general->traits = $json->traits;
    		$general->level = $json->level;
    		$general->is_army = ($json->type == 'ground') ? 1 : 0;
    		$general->is_field_marshal = $json->field_marshal;
    		$general->is_navy = ($json->type != 'ground') ? 1 : 0;
    		$general->attack_stat = $json->attack_stat;
    		$general->defence_stat = $json->defence_stat;
    		$general->planning_stat = $json->planning_stat;
    		$general->supply_stat = $json->supply_stat;
    		$general->created_at = date('Y-m-d H:i:s');
	    	$general->updated_at = date('Y-m-d H:i:s');
    		$general->save();
    	}
    }

    public function getLeader(Request $request){
    	$ideology = CountryDateIdeology::find($request->ideology_id);

    	return view('countries.partials.leader', ['ideology' => $ideology]);
    }

    public function getTabs(Request $request){
    	$date = CountryDate::find($request->date_id);

    	return view('countries.partials.ideology-tabs', ['ideologies' => $date->ideologies, 'active' => null]);
    }

    public function getStates(Request $request){
    	$date = CountryDate::find($request->date_id);

    	return view('countries.partials.states', ['date' => $date]);
    }

    public function getIdeas(Request $request, $id){
    	$ideas = CountryIdea::where('country_id', '=', $id)->where('national_spirit_type', '=', $request->type)->get();
    	return view('countries.partials.ideas', ['ideas' => $ideas]);
    }

    public function deleteIdea(Request $request){
    	$idea = CountryIdea::find($request->id);
    	$idea->delete();
    }

    public function canEditCountry($country){
    	if(Auth::user()){
			foreach($country->mod->users as $user){
				if($user->id == Auth::user()->id){
					return true;
				}
			}
			if(Auth::user()->role_id == 1){
				return true;
			}
		}
		return false;
    }
}
