<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Contracts\Media;

class UserController extends Controller
{
	/**
	 * Show the profile for the given user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function view($id)
	{
		$user = User::findOrFail($id);
		$videos = [];
		if(!empty($user->youtube_account)){
			if(strpos($user->youtube_account, '?')){
				$split = explode('?', $user->youtube_account);
				$user->youtube_account = $split[0];
			}
			if(strpos($user->youtube_account, '/')){
				$elems = explode('/', $user->youtube_account);
				$u = array_search('user', $elems);
				if(empty($u)){
					$u = array_search('channel', $elems);
				}
				if(!empty($u)){
					$id = $elems[$u+1];
					$api = 'AIzaSyBiGUy-cRI9m4njm3wI9U-AKpbknGPHcuM';
					$content = file_get_contents('https://www.googleapis.com/youtube/v3/search?key='.$api.'&channelId='.$id.'&part=snippet,id&order=date&maxResults=20&q=hoi4|hearts-of-iron4|hearts-of-iron-4');
					$youtube = json_decode($content);

					if(!empty($youtube->items)){
						foreach($youtube->items as $item){
							if(!empty($item->id->videoId)){
								$videos[] = $item->id->videoId;
							}
						}
					}
				}
			}
		}
		return view('account/view', ['user' => $user, 'videos' => $videos]);
	}

	public function notifications(){
		if(Auth::user()){
			$forums = \AppHelper::instance()->getUnreadForumPosts(Auth::user()->id, Auth::user()->last_active);
			if(count($forums) > 0){
				$notifications = $forums;
			}else{
				$notifications = '';
			}
			return view('account/notifications', ['notifications' => $notifications]);
		}else{
			return redirect('/login');
		}
	}

	public function list()
	{
		return view('account/list', ['users' => User::get()]);
	}

	public function account()
	{
		$user = User::findOrFail(Auth::user()->id);
		$videos = [];
		if(!empty($user->youtube_account)){
			if(strpos($user->youtube_account, '?')){
				$split = explode('?', $user->youtube_account);
				$user->youtube_account = $split[0];
			}
			$elems = explode('/', $user->youtube_account);
			$u = array_search('user', $elems);
			if(empty($u)){
				$u = array_search('channel', $elems);
			}
			$id = $elems[$u+1];
			$api = 'AIzaSyBiGUy-cRI9m4njm3wI9U-AKpbknGPHcuM';
			$content = file_get_contents('https://www.googleapis.com/youtube/v3/search?key='.$api.'&channelId='.$id.'&part=snippet,id&order=date&maxResults=20&q=hoi4|hearts-of-iron4|hearts-of-iron-4');
			$youtube = json_decode($content);
			foreach($youtube->items as $item){
				$videos[] = $item->id->videoId;
			}
		}

		return view('account/account', ['user' => $user, 'videos' => $videos]);
	}

	public function media(){
		$user = User::findOrFail(Auth::user()->id);
		return view('account.media', ['user' => $user]);
	}

	public function mediaAction(Request $request){
		if(isset($request->deleteMedia)){
			$m = new Media();
			foreach($request->deleteMedia as $id){
				$m->deleteImage($id);
			}
		}

		$user = User::findOrFail(Auth::user()->id);
		return view('account.media', ['user' => $user])->withSuccess('Successfully deleted selected custom GFX');
	}

	public function updateAccount(Request $request)
	{
		$user = User::findOrFail(Auth::user()->id);
		if(!empty($request->name)){
			$user->name = $request->name;
		}
		if(!empty($request->avatar)){
			$avatar_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',$user->username)));
			if ($request->file('avatar')->isValid()) {
				$path = $request->file('avatar')->move(public_path('/storage/users/'.$user->id), $avatar_name.'.png');
				$path = 'users/'.$user->id.'/'.$avatar_name.'.png';
			}else{
				$path = ''; //Link to default image
				$errors['File_Upload'] = 'The file you tried to upload was not valid';
			}
			$user->avatar = $path;
		}
		$user->youtube_account = $request->youtube;
		$user->description = $request->description;
		$user->save();
		return redirect('/users/'.Auth::user()->id);
	}

	public function api(){
		return User::get();
	}
}