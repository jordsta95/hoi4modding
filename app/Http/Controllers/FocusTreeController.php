<?php

namespace App\Http\Controllers;

use App\FocusTree;
use App\Mod;
use App\User;
use App\UserMod;
use App\Focuses;
use App\Imports;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use mysqli;
use Storage;
use App\Http\Controllers\ModController;

class FocusTreeController extends Controller
{
	private $focusTreeInternalIds = [];
	/**
	 * Show the profile for the given user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function view($id)
	{
		$focusTree = FocusTree::findOrFail($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($focusTree->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}

		$this->moveFocusesToCorrectPosition($focusTree->focuses);

		foreach($focusTree->focuses as $focus){
			if(!empty($focus->external_id)){
				$this->focusTreeInternalIds[$focus->external_id] = $focus;
			}
		}
		foreach($focusTree->focuses as $focus){


			$pr = $this->stripMultiRelations($focus->focus_prerequisite);
			$me = $this->stripMultiRelations($focus->focus_mutually_exclusive);

			if(!empty($pr)){
				//If it isn't using internal IDs only, then we cry
				if(((int) $pr === 0) && !empty($pr)){
					$focus->focus_prerequisite = $this->externalIdToInternalId($focus->focus_prerequisite);
				}
			}
			if(!empty($me) ){
				//If it isn't using internal IDs only, then we cry
				if(((int) $me === 0) && !empty($me)){
					$focus->focus_mutually_exclusive = $this->externalIdToInternalId($focus->focus_mutually_exclusive);
				}
			}

			$focus->save();

		}
		return view('focustree/view', ['tree' => $focusTree, 'can_edit' => $can_edit, 'is_editable' => false]);
	}

	public function update(Request $request, $id){
		$tree = FocusTree::findOrFail($id);
		if(!empty($request->country_tag)){
			$tree->country_tag = $request->country_tag;
		}
		if(!empty($request->tree_id)){
			$tree->tree_id = $request->tree_id;
		}
		if(!empty($request->lang)){
			$tree->lang = $request->lang;
		}
		$tree->save();
		return redirect()->back()->with('success', 'Successfully updated focus tree for '.$tree->country_tag);
	}

	public function delete($id)
	{
		$tree = FocusTree::findOrFail($id);
		$name = $tree->country_tag;
		$modid = $tree->mod->id;
		$can_edit = false;
		$check = UserMod::where('mod_id', '=', $modid)->where('user_id', '=', Auth::user()->id)->first();
		if(strtolower($check->role) != 'creator'){
			return redirect()->back()->with('error', 'Only the creator of the parent mod can delete this');
		}
		if(Auth::user()){
			foreach($tree->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}
		if($can_edit){
			foreach($tree->focuses as $focus){
				$focus->delete();
			}
			$tree->delete();
			\Session::flash('success', 'You have successfully deleted the focus tree for '.$name);
		}else{
			\Session::flash('error', 'You do not have permission to perform this action');
		}
		return redirect('/mod/'.$modid);
	}

	public function list()
	{
		
		if(Auth::user()){
			$myMods = Auth::user()->mods;
		}else{
			$myMods = null;
		}
		return view('focustree/list', ['myMods' => $myMods]);
	}

	public function showCreate(){
		if(Auth::user()){
			$id = Auth::user()->id;
			$mods = User::findOrFail($id)->mods;
		}else{
			$mods = [];
		}
		return view('focustree/create', ['mods' => $mods, 'can_edit' => true, 'is_editable' => false]);
	}

	public function createFocusTree(Request $request){
		if(Input::get('mod_id') == 'createNewMod'){
			$controller = new ModController();
			$modid = $controller->createMod($request);
		}else{
			$modid = Input::get('mod_id');
		}
		//Create the focus tree
		$id = \DB::table('focus_trees')->insertGetId(
			[
				'mod_id' => $modid,
				'country_tag' => Input::get('tree_country'),
				'tree_id' => Input::get('tree_id'),
				'lang' => Input::get('tree_lang'),
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]
		);
		$mod = Mod::find($modid);
		$mod->has_focus_tree = 1;
		$mod->save();
		return redirect('/focus-tree/edit/'.$id);
	}

	public function moveFocusesToCorrectPosition($focuses){
		$focusList = [];
		$total = $focuses->count();

		$fixed = 0;
		$attempts = 0;
		while($fixed < $total && $attempts < ($fixed * $fixed * $fixed * $fixed * $fixed * $fixed)){
			foreach($focuses as $focus){
				if(empty($focus->focus_relative_position)){
					if(!isset($focusList[$focus->external_id])){
						$focusList[$focus->external_id] = ['x' => $focus->focus_x, 'y' => $focus->focus_y];
						$fixed++;
					}
				}else{
					if(isset($focusList[$focus->focus_relative_position])){
						$rel = $focusList[$focus->focus_relative_position];
						$focus->focus_x = $focus->focus_x + $rel['x'];
						$focus->focus_y = $focus->focus_y + $rel['y'];
						$focus->focus_relative_position = '';
						$focus->save();
					}
				}
			}
			$attempts++;
		}
	}

	public function edit($id){
		if(Auth::user()){
			$focusTree = FocusTree::findOrFail($id);
			$canEdit = $this->canUserEdit($id);
			if($canEdit){

				$this->moveFocusesToCorrectPosition($focusTree->focuses);

				foreach($focusTree->focuses as $focus){
					if(!empty($focus->external_id)){
						$this->focusTreeInternalIds[$focus->external_id] = $focus;
					}
				}

				foreach($focusTree->focuses as $focus){


					$pr = $this->stripMultiRelations($focus->focus_prerequisite);
					$me = $this->stripMultiRelations($focus->focus_mutually_exclusive);

					if(!empty($pr)){
						//If it isn't using internal IDs only, then we cry
						if(((int) $pr === 0) && !empty($pr)){
							$focus->focus_prerequisite = $this->externalIdToInternalId($focus->focus_prerequisite);
						}
					}
					if(!empty($me) ){
						//If it isn't using internal IDs only, then we cry
						if(((int) $me === 0) && !empty($me)){
							$focus->focus_mutually_exclusive = $this->externalIdToInternalId($focus->focus_mutually_exclusive);
						}
					}

					$focus->save();

				}

				return view('focustree/edit', ['tree' => $focusTree, 'can_edit' => $canEdit, 'is_editable' => true, 'rawtext' => $this->getFocusTreeOutput($focusTree)]);
			}else{
				die("You can't edit this");
			}
		}else{
			return redirect('/login');
		}
	}

	public function getFocusRelativePosition($externalID, $x, $y, $count = 0){
		$relatedFocus = null;
		if(isset($this->focusTreeInternalIds[$externalID]) && $count < 999999){
			$relatedFocus = $this->focusTreeInternalIds[$externalID];
			if(!empty($relatedFocus->focus_relative_position)){
				$relationFocus = $this->getFocusRelativePosition($relatedFocus->focus_relative_position, $relatedFocus->focus_x, $relatedFocus->focus_y, ($count + 1));
				$newX = $relationFocus[0];
				$newY = $relationFocus[1];
			}else{
				$newX = ($relatedFocus->focus_x + $x);
				$newY = ($relatedFocus->focus_y + $y);
			}
		}else{
			$newX = $x;
			$newY = $y;
		}
		return array($newX, $newY);
	}

	//Replace all external IDs in the string with internal IDs
	public function externalIdToInternalId($string){
		$editingString = $string;
		$readyToSplit = str_replace('&&', '||', $string);
		$array = explode('||', $readyToSplit);

		foreach(array_filter($array) as $externalID){
			if(isset($this->focusTreeInternalIds[$externalID])){
				$editingString = str_replace($externalID, $this->focusTreeInternalIds[$externalID]->id, $editingString);
			}else{
				$editingString = str_replace([$externalID.'&&', $externalID.'||'], '', $editingString);
				$editingString = str_replace($externalID, '', $editingString);
			}
		}
		return $editingString;
	}

	public function stripMultiRelations($unformattedRelation){
		return str_replace(['&&', '||'], '', $unformattedRelation);
	}

	public function globalMove(Request $request, $treeid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($treeid);
			if($canEdit){
				$tree = FocusTree::find($treeid);
				$distance = $request->input('distance');
				$axis = $request->input('axis');
				foreach($tree->focuses as $focus){
					if($axis == 'x'){
						if(($focus->focus_x + $distance) >= 0){
							$focus->focus_x = ($focus->focus_x + $distance);
						}else{
							$focus->focus_x = 0;
						}
					}
					if($axis == 'y'){
						if(($focus->focus_y + $distance) >= 0){
							$focus->focus_y = ($focus->focus_y + $distance);
						}else{
							$focus->focus_y = 0;
						}
					}
					$focus->save();
				}
				return redirect(route('tree.edit', ['id' => $treeid]));
			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function multiMove(Request $request, $treeid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($treeid);
			if($canEdit){
				$focuses = Focuses::where('focus_tree_id', '=', $treeid)->whereIn('id', $request->focus)->get();
				$distance = $request->input('distance');
				$axis = $request->input('axis');
				foreach($focuses as $focus){
					if($axis == 'x'){
						if(($focus->focus_x + $distance) >= 0){
							$focus->focus_x = ($focus->focus_x + $distance);
						}else{
							$focus->focus_x = 0;
						}
					}
					if($axis == 'y'){
						if(($focus->focus_y + $distance) >= 0){
							$focus->focus_y = ($focus->focus_y + $distance);
						}else{
							$focus->focus_y = 0;
						}
					}
					$focus->save();
				}
				return redirect(route('tree.edit', ['id' => $treeid]));
			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function createNewFocus(Request $request, $treeid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($treeid);
			if($canEdit){
				$errors = array();
				if($request->input('chosen_gfx') == 'custom'){
					//Upload file, and add it
					$icon_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('focus_title'))));
					if ($request->file('customgfx')->isValid()) {
						Storage::putFileAs('public/focustree/'.$treeid, $request->file('customgfx'), $icon_name.'.png');
						$file = '/focustree/'.$treeid.'/'.$icon_name.'.png';
						//Storage::write('public'.$file, $request->file('customgfx'));
						$path = '/media/get'.$file;
					}else{
						$path = ''; //Link to default image
						$errors['File_Upload'] = 'The file you tried to upload was not valid';
					}
				}else{
					//Do default
					$path = $request->input('chosen_gfx');
				}
				$id = \DB::table('focuses')->insertGetId(
					[
						'focus_tree_id' => $treeid,
						'focus_title' => $request->input('focus_title'),
						'focus_description' => $request->input('focus_description'), 
						'focus_gfx' => $path, 
						'focus_ai_will_do_factor' => $request->input('focus_ai_will_do_factor'), 
						'focus_time_to_complete' => $request->input('focus_time_to_complete'), 
						'focus_x' => $request->input('focus_x'),
						'focus_y' => $request->input('focus_y'),
						'focus_bypass' => $request->input('focus_bypass'),
						'focus_available' => $request->input('focus_available'),
						'focus_reward' => $request->input('focus_reward'),
						'focus_prerequisite' => $request->input('focus_prerequisite'),
						'focus_mutually_exclusive' => $request->input('focus_mutually_exclusive'),
						'focus_complete_tooltip' => $request->input('focus_complete_tooltip'),
						'external_id' => $request->input('external_id'),
						'focus_relative_position' => $request->input('focus_relative_position'),
						'created_at' => date('Y-m-d G:i:s'),
						'updated_at' => date('Y-m-d G:i:s'),
						'filter' => $request->input('filter')
					]
				);


				if(!empty($request->input('focus_mutually_exclusive'))){
					$convertAnd = str_replace('&&', '||', $request->input('focus_mutually_exclusive'));
					$focuses = explode('||', $convertAnd);
					$this->addMutuallyExclusive($focuses, $id);
				}

				if($errors){
					die('<script>parent.NFuploadOutcome("error","'.json_encode($errors).'", "");</script>');
				}else{
					die('<script>parent.GENERICreload();</script>');
				}
			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function addMutuallyExclusive($focuses, $focus_id){
		foreach($focuses as $f){
			$focus = Focuses::findOrFail($f);
			if(strpos($focus->focus_mutually_exclusive, $focus_id) === false){
				if(!empty($focus->focus_mutually_exclusive)){
					$focus->focus_mutually_exclusive = $focus->focus_mutually_exclusive.'&&';
				}
				$focus->focus_mutually_exclusive = $focus->focus_mutually_exclusive.$focus_id;
				$focus->save();
			}
		}
	}

	public function deleteRelatedFocuses($focuses){
		foreach($focuses as $f){

			$focus = Focuses::findOrFail($f);

			$prop = $focus->focus_mutually_exclusive;

			//Replace it as an and/or focus first, then replace it as a standalone last - just to ensure it works
			$val = str_replace(['&&'.$f, '||'.$f], '', $prop);
			$val = str_replace($f, '', $val);
			
			$focus->focus_mutually_exclusive = $val;
			$focus->save();
		}
	}

	public function editFocus(Request $request, $treeid, $focusid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($treeid);
			if($canEdit){
				if($request->input('chosen_gfx') == 'custom'){
					//Upload file, and add it
					$icon_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('focus_title'))));
					if ($request->file('customgfx')->isValid()) {
						$path = Storage::putFileAs('public/focustree/'.$treeid, $request->file('customgfx'), $icon_name.'.png');
						$file = '/focustree/'.$treeid.'/'.$icon_name.'.png';
						//Storage::write('public'.$file, $request->file('customgfx'));
						$path = '/media/get'.$file;
					}else{
						$path = ''; //Link to default image
						$errors['File_Upload'] = 'The file you tried to upload was not valid';
					}
				}else{
					//Do default
					$path = $request->input('chosen_gfx');
				}

				$focus = Focuses::findOrFail($focusid);
				$focus->focus_title = $request->input('focus_title');
				$focus->focus_description = $request->input('focus_description');
				$focus->focus_gfx = $path;
				$focus->focus_ai_will_do_factor = $request->input('focus_ai_will_do_factor'); 
				$focus->focus_time_to_complete = $request->input('focus_time_to_complete');
				$focus->focus_x = $request->input('focus_x');
				$focus->focus_y = $request->input('focus_y');
				$focus->focus_bypass = $request->input('focus_bypass');
				$focus->focus_available = $request->input('focus_available');
				$focus->focus_reward = $request->input('focus_reward');
				$focus->focus_prerequisite = $request->input('focus_prerequisite');
				$focus->focus_mutually_exclusive = $request->input('focus_mutually_exclusive');
				$focus->focus_complete_tooltip = $request->input('focus_complete_tooltip');
				$focus->updated_at = date('Y-m-d G:i:s');
				$focus->filter = $request->input('filter');
				$focus->save();

				if(!empty($request->input('focus_mutually_exclusive'))){
					$convertAnd = str_replace('&&', '||', $request->input('focus_mutually_exclusive'));
					$focuses = explode('||', $convertAnd);
					foreach($focuses as $key => $f){
						if(empty($f)){
							unset($focuses[$key]);
						}
					}
					$this->addMutuallyExclusive($focuses, $focusid);
				}

				die('<script>parent.GENERICreload();</script>');

			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function quickEditFocus(Request $request, $treeid, $focusid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($treeid);
			if($canEdit){
				$action = $request->input('action');
				$focus = Focuses::findOrFail($focusid);
				switch ($action) {
					case 'updateXY':
						$focus->focus_x = $request->input('x');
						$focus->focus_y = $request->input('y');
						$focus->updated_at = date('Y-m-d G:i:s');
						break;
					
					default:
						# code...
						break;
				}
				$focus->save();
			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function deleteFocus(Request $request, $treeid, $focusid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($treeid);
			if($canEdit){
				if($focusid == 'all'){
					$focus = Focuses::where('focus_tree_id', '=', $treeid)->delete();
				}else{
					$focus = Focuses::findOrFail($focusid);
					if(!empty($focus->focus_mutually_exclusive)){
						$convertAnd = str_replace('&&', '||', $focus->focus_mutually_exclusive);
						$focuses = explode('||', $convertAnd);
						$this->deleteRelatedFocuses($focuses);
					}
					
					$reliantFocuses = Focuses::where('focus_prerequisite','LIKE','%'.$focusid.'%')->get();
					foreach($reliantFocuses as $f){
						$prop = $f->focus_prerequisite;

						//Replace it as an and/or focus first, then replace it as a standalone last - just to ensure it works
						$val = str_replace(['&&'.$f, '||'.$f], '', $prop);
						$val = str_replace($f, '', $val);
						
						$f->focus_prerequisite = $val;
						$f->save();
					}
					$focus->delete();
				}

			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function canUserEdit($treeid){
		if(Auth::user()){
			$focusTree = FocusTree::findOrFail($treeid);
			$users = $focusTree->mod->users;
			$canEdit = false;
			foreach($users as $user){
				if(Auth::user()->id == $user->id){
					$canEdit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$canEdit = true;
			}
			if($canEdit){
				return true;
			}else{
				return false;
			}
		}else{
			return redirect('/login');
		}
	}

	public function importFromExistingFile(Request $request, $id){
		$filename = '';
		$localisationFile = '';
		
		if(empty($request->JSON) && !empty($request->file('focusTreeFile'))){
			$filename = Storage::putFileAs('imports/focus-tree/'.$id.'/', $request->file('focusTreeFile'), $request->file('focusTreeFile')->getClientOriginalName());
		}else{
			if(!empty($request->JSON)){
				$filename = 'imports/focus-tree/'.$id.'/Import-'.time().'.json';
				Storage::put($filename, $request->JSON);
			}
		}
		
		if(!empty($request->file('localisationFile'))){
			$localisationFile = Storage::putFileAs('imports/focus-tree/'.$id.'/', $request->file('localisationFile'), $request->file('localisationFile')->getClientOriginalName());
		}

		$failString = 'The following file(s) are required: ';
		
		if(empty($filename)){
			$failString .= 'Focus tree file. ';
		}
		if(empty($localisationFile)){
			$failString .= 'Localisation file. ';
		}

		if(empty($filename) || empty($localisationFile)){
			die('<script>alert("'.$failString.'");document.getElementById("treetojson").style.display = "block";');
		}

		Imports::insert([
			[
				'type' => 'focus-tree',
				'model_id' => $id,
				'filename' => $filename,
				'localisation' => $localisationFile,
				'user_id' => Auth::user()->id,
				'created_at' => date('Y-m-d H:i:s')
			]
		]);

		die('<script>alert("Focus tree scheduled for upload.");document.getElementById("treetojson").style.display = "none";</script>');
		return;
	}

	protected function toolifyValue($value){
		$string = '';

		if(!is_array($value)){
			$value = [$value];
		}
		foreach($value as $v){
			$v = trim($v);
			$v = preg_replace('/\s+/', '', $v);
			$v = ltrim(rtrim($v, '}'), '{focus=');
			$v = str_replace("focus=", "||", $v);
			$string .= $v."&&";
		}

		return rtrim($string, '&&');
	}

	public function getFocusTreeOutput($tree){
		$controller = new ModController();
		$controller->generateFocusFileArray();
		$output = $controller->getFocusTreeTextForExport($tree);
		return $output;
	}
}