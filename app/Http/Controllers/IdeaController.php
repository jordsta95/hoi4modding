<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Imports;
use App\Idea;
use App\IdeaOptions;
use App\Mod;
use App\User;
use Auth;
use App\Http\Controllers\ModController;
use Illuminate\Support\Facades\Input;

class IdeaController extends Controller{
    public function list(){
    	if(Auth::user()){
			$myMods = Auth::user()->mods;
		}else{
			$myMods = null;
		}
    	return view('ideas.list', ['myMods' => $myMods]);
    }

    public function showCreate(){
    	if(Auth::user()){
			$id = Auth::user()->id;
			$mods = User::findOrFail($id)->mods;
		}else{
			$mods = [];
		}
    	return view('ideas.create', ['mods' => $mods, 'can_edit' => true, 'is_editable' => false]);
    }

    public function create(Request $request){
    	if(Input::get('mod_id') == 'createNewMod'){
			$controller = new ModController();
			$modid = $controller->createMod($request);
		}else{
			$modid = Input::get('mod_id');
		}
		//Create the focus tree
		$id = \DB::table('ideas')->insertGetId(
			[
				'mod_id' => $modid,
				'name' => Input::get('name'),
				'filename' => str_slug(Input::get('name')),
				'language' => Input::get('language'),
				'prefix' => Input::get('prefix'),
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]
		);
		return redirect('/ideas/edit/'.$id);
    }

    public function view($id){
    	$idea = Idea::find($id);
    	$can_edit = false;
		if(Auth::user()){
			foreach($idea->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}

		$traits = \AppHelper::instance()->getAllTraits();

    	return view('ideas.view', ['ideas' => $idea, 'can_edit' => $can_edit, 'is_editable' => true, 'traits' => $traits]);
    }

    public function edit($id){
    	$idea = Idea::find($id);
    	$can_edit = false;
		if(Auth::user()){
			foreach($idea->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}
		if(!$can_edit){
			die("You can't edit this");
		}

		$traits = \AppHelper::instance()->getAllTraits();

    	return view('ideas.edit', ['ideas' => $idea, 'can_edit' => $can_edit, 'is_editable' => true, 'traits' => $traits]);
    }

    public function addIdea(Request $request, $id){
    	$idea = new IdeaOptions();
    	$idea->idea_id = $id;
    	$idea = $this->setIdeaData($idea, $request);
    	$idea->save();
    	return $idea;
    }

    public function editIdea(Request $request, $ideaID, $id){
    	$idea = IdeaOptions::find($id);
    	$idea = $this->setIdeaData($idea, $request);
    	$idea->save();
    	return $idea;
    }

    public function setIdeaData($idea, $request){
    	$idea->name = $request->name;
    	$idea->group = $request->group;

    	

    	$data = [];
    	foreach($request->options as $name => $value){
    		if(!empty($value)){
    			$data[$name] = $value;
    		}
    	}

    	$idea->options = json_encode($data);

    	return $idea;
    }

    public function deleteIdea(Request $request, $id, $ideaID){
    	$idea = Idea::find($id);
    	$can_edit = false;
    	if(Auth::user()){
			foreach($idea->mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}

		if($can_edit){
			IdeaOptions::find($ideaID)->delete();
		}

    }

    public function importFromExistingFile(Request $request, $id){

		$filename = Storage::putFileAs('imports/ideas/'.$id.'/', $request->file('ideasFile'), $request->file('ideasFile')->getClientOriginalName());
		$localisationFile = Storage::putFileAs('imports/ideas/'.$id.'/', $request->file('ideasFile'), $request->file('ideasFile')->getClientOriginalName());

		Imports::insert([
			[
				'type' => 'ideas',
				'model_id' => $id,
				'filename' => $filename,
				'localisation' => $localisationFile,
				'user_id' => Auth::user()->id,
				'created_at' => date('Y-m-d H:i:s')
			]
		]);

		die('<script>alert("Ideas scheduled for upload.");</script>');
		return;
	}
}
