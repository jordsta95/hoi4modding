<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Forum;
use App\ForumCategories;
use App\ForumComments;
use App\ForumCommentLikes;
use App\ForumPosts;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;

use Mail;
use App\Http\Contracts\Media;

class ForumPostsController extends Controller
{
	public function create($slug){
		if(Auth::user()){
			if($slug == 'bug-reports'){				
				return view('forum/bugReport', ['parent' => Forum::where('slug','=',$slug)->first(), 'data' => $_GET]);
			}else{
				return view('forum/createPost', ['parent' => Forum::where('slug','=',$slug)->first()]);
			}
		}else{
			return redirect('/forum/'.$slug);
		}
	}

	public function createPost(Request $request, $slug){
		if(Auth::user()){
			if($slug == 'bug-reports'){
				$content = $this->buildBugReport($request);
			}else{
				$content = $request->wysiwyg;
			}
			$forum = Forum::where('slug','=',$slug)->first();
			$userId = Auth::user()->id;
			$slug = str_slug(Input::get('title'), '_');

			$postId = ForumPosts::insertGetId(
				[
					'title' => $request->title,
					'slug' => $slug,
					'content' => $content,
					'user_id' => $userId,
					'forum_id' => $forum->id,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]
			);

			$user = User::find($userId);
			$user->post_count = ($user->post_count + 1);
			$user->last_active = date('Y-m-d H:i:s');
			$user->save();

			$forum->updated_at = date('Y-m-d H:i:s');
			$forum->save();

			Mail::send('emails.forum.create', ['user' => Auth::user(), 'slug' => '/forum/'.$forum->slug.'/'.$postId.'-'.$slug, 'post' => ForumPosts::find($postId)], function ($m) use ($request) {
				$m->to('jhoyland07@hotmail.co.uk')->subject('HOI4 Modding Post Created: '.$request->title);
			});

			return redirect('/forum/'.$forum->slug.'/'.$postId.'-'.$slug);
		}else{
			return redirect('/forum/'.$slug);
		}
	}

	public function forumPostAction(Request $request, $slug, $id, $post_slug, $action, $action_id = null){
		$forum = Forum::where('slug','=',$slug)->first();
		$post = ForumPosts::findOrFail($id);
		switch ($action) {
			case 'close':
				if(Auth::user()->id == $post->user_id || Auth::user()->role_id == 1){
					$post->open = 0;
					$post->updated_at = date('Y-m-d G:i:s');
					$post->save();
				}
				return redirect('/forum/'.$forum->slug.'/'.$id.'-'.$post_slug);
			break;

			case 'reopen':
				if(Auth::user()->role_id == 1){
					$post->open = 1;
					$post->updated_at = date('Y-m-d G:i:s');
					$post->save();
				}
				return redirect('/forum/'.$forum->slug.'/'.$id.'-'.$post_slug);
			break;

			case 'ban':
				if(Auth::user()->role_id == 1){
					$post->open = 0;
					$post->public = 0;
					$post->updated_at = date('Y-m-d G:i:s');
					$post->save();

					$user = User::find($post->user_id);
					$user->post_count = ($user->post_count - 1);
					$user->forum_strikes = ($user->forum_strikes + 1);
					if($user->forum_strikes >= 5){
						$user->can_post = 0;
					}
					$user->save();
				}
				return redirect('/forum/'.$forum->slug);
			break;

			case 'remove':
				if(Auth::user()->role_id == 1){
					$post->open = 0;
					$post->public = 0;
					$post->updated_at = date('Y-m-d G:i:s');
					$post->delete();

					$user = User::find($post->user_id);
					$user->post_count = ($user->post_count - 1);
					$user->save();
				}
				return redirect('/forum/'.$forum->slug);
			break;

			case 'edit':
				if(!empty($action_id)){
					$comment = ForumComments::findorfail($action_id);
					if(Auth::user()->role_id == 1 || Auth::user()->id ==$comment->user_id){
						$comment->content = $request->wysiwyg;
						$comment->updated_at = date('Y-m-d G:i:s');
						$comment->save();
					} 
				}
				return redirect('/forum/'.$forum->slug.'/'.$id.'-'.$post_slug);
			break;

			case 'edit_post':
				if(Auth::user()->role_id == 1 || Auth::user()->id == $post->user_id){
					$post->slug = str_slug($request->title, '_');
					$post->title = $request->title;
					$post->content = $request->wysiwyg;
					$post->updated_at = date('Y-m-d G:i:s');
					$post->save();
				}
				return redirect('/forum/'.$forum->slug.'/'.$id.'-'.str_slug($request->title, '_'));
			break;

			case 'like':
				$urlAppend = '';
				if(Auth::user() && !empty($action_id)){
					ForumCommentLikes::insert(
						[
							'forum_comment_id' => $action_id,
							'user_id' => Auth::user()->id,
							'created_at' => date('Y-m-d G:i:s'),
							'updated_at' => date('Y-m-d G:i:s')
						]
					);
					$commentCheck = ForumComments::where('forum_post_id','=',$id)->paginate(10);
					if($commentCheck->lastPage() >= 2){
						$urlAppend = '?page='.$commentCheck->lastPage().'#'.$action_id;
					}else{
						$urlAppend = '#'.$action_id;
					}
				}
				
				return redirect('/forum/'.$slug.'/'.$id.'-'.$post_slug.$urlAppend);
			break;
			
			default:
				return redirect('/forum/'.$forum->slug.'/'.$id.'-'.$post_slug);
				break;
		}
	}

	private function buildBugReport($request){
		$content = '';
		$content .= '<h5>Description</h5> ';
		$content .= '<p>'.nl2br($request->content).'</p> ';
		$content .= '<h5>Link</h5> ';
		$content .= '<p><a href="'.$request->link.'" target="_blank">'.$request->link.'</a></p> ';
		$content .= '<h5>Time</h5> ';
		$content .= '<p>'.$request->time.'</p> ';
		$content .= '<h5>Browser</h5> ';
		$content .= '<p>'.$request->browser.'</p> ';

		if(isset($request->images) && !empty($request->images)){
			$content .= '<h5>Images</h5>';
			foreach($request->images as $loop => $img){
				$media = new Media();
				$media->setUser(0);
				$options = [];
				$options['path'] = '/uploads/bug-reports';
				$options['name'] = time().'-'.$loop;
				$upload = $media->handleUpload($img, $options);
				$content .= '<a href="/image/find/'.$upload->id.'" target="_blank"><img src="/image/find/'.$upload->id.'/bug"></a>';
			}
		}

		return $content;
	}
}
