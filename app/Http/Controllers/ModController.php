<?php

namespace App\Http\Controllers;

use App\Mod;
use App\UserMod;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use ZipArchive;
use Storage;
use Illuminate\Http\Request;
use App\FocusTree;
use App\Events;
use Imagick;
use App\Http\Contracts\Media;
use Illuminate\Support\Facades\Input;

class ModController extends Controller
{
	protected $focusFileArray;
	protected $idArrays = [];
	protected $errors = [];
	protected $stateFiles = [];
	protected $countryColours = '';
	protected $storageDir;

	public function createMod($request){
		//Create the mod
		$mod_logo_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('mod_name'))));
		if(!empty($request->file('mod_logo'))){
			$logo = $request->file('mod_logo');
			if ($request->file('mod_logo')->isValid()) {
				$path = Storage::putFileAs('public/mods/', $request->file('mod_logo'), $mod_logo_name.'.png');
				$file = '/mods/'.$mod_logo_name.'.png';
				//Storage::write('public'.$file, $request->file('customgfx'));
				$path = '/media/get'.$file;
			}
		}else{
			$mod_logo_name = 'default';
		}
		//$uploadedLogo = $logo->move(public_path('/storage/mods'))->getRealPath();
		$modid = \DB::table('mods')->insertGetId(
			[
				'mod_name' => Input::get('mod_name'), 
				'mod_description' => !empty(Input::get('mod_description')) ? Input::get('mod_description') : ' ',
				'mod_logo' => 'mods/'.$mod_logo_name.'.png',
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]
		);
		\DB::table('user_mods')->insert([
			'user_id' => Auth::user()->id,
			'mod_id' => $modid,
			'role' => 'Creator',
			'created_at' => date('Y-m-d G:i:s'),
			'updated_at' => date('Y-m-d G:i:s')
		]);

		return $modid;
	}

	public function editMod(Request $request, $id){
		$mod = Mod::find($id);
		$mod->mod_name = Input::get('mod_name');
		$mod->mod_description = !empty(Input::get('mod_description')) ? Input::get('mod_description') : ' ';

		$mod_logo_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('mod_name'))));
		if(!empty($request->file('mod_logo'))){
			$logo = $request->file('mod_logo');
			if ($request->file('mod_logo')->isValid()) {
				$path = Storage::putFileAs('public/mods/', $request->file('mod_logo'), $mod_logo_name.'.png');
				$file = '/mods/'.$mod_logo_name.'.png';
				//Storage::write('public'.$file, $request->file('customgfx'));
				$path = '/media/get'.$file;

				$mod->mod_logo = 'mods/'.$mod_logo_name.'.png';
			}
		}

		$mod->save();

		return $this->view($id);
	}

	public function view($id)
	{
		$mod = Mod::findOrFail($id);
		$can_edit = false;
		if(Auth::user()){
			foreach($mod->users as $user):
				if($user->id == Auth::user()->id){
				   $can_edit = true; 
				}
			endforeach;
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}
		return view('mod/view', ['mod' => $mod, 'can_edit' => $can_edit]);
	}

	public function delete(Request $request, $id)
	{
		$mod = Mod::find($id);
		$check = UserMod::where('mod_id', '=', $id)->where('user_id', '=', Auth::user()->id)->first();
		if(strtolower($check->role) != 'creator'){
			return redirect()->back()->with('error', 'Only the creator of a mod can delete it');
		}
		$name = $mod->mod_name;
		if($request->delete == "DELETE"){
			if(!empty($mod->events)){
				foreach($mod->events as $event){
					$event->delete();
				}
			}
			if(!empty($mod->eventLanguage)){
				$mod->eventLanguage->delete();
			}
			$mod->delete();
			$roles = UserMod::where('mod_id', '=', $id)->get();
			foreach($roles as $role){
				$role->delete();
			}
			$focuses = FocusTree::where('mod_id', '=', $id)->get();
			if(!empty($focuses)){
				foreach($focuses->focuses as $focus){
					$focus->delete();
				}
				foreach($focuses as $focus){
					$focus->delete();
				}
			}
			\Session::flash('success', $name.' successfully deleted');
			return redirect('/');
		}else{
			\Session::flash('error', 'Cannot delete '.$name.' without confirming you wish to DELETE');
			return redirect('/mod/'.$id);
		}
	}

	public function list()
	{
		return view('mod/list', ['mods' => Mod::get()]);
	}

	public function apiAddRole($modid, $userid, $role){
		UserMod::insert([
			'user_id' => $userid,
			'mod_id' => $modid,
			'role' => $role,
			'created_at' => date('Y-m-d G:i:s'),
			'updated_at' => date('Y-m-d G:i:s')
		]);
		$user = User::find($userid);
		\Session::flash('success', $user->username.' successfully given the role '.$role);
		return redirect('/mod/'.$modid);
	}

	public function apiRemoveRole($modid, $userid){
		UserMod::where('user_id', '=', $userid)->where('mod_id', '=', $modid)->delete();
		$user = User::find($userid);
		\Session::flash('error', $user->username.' successfully removed from mod');
		return redirect('/mod/'.$modid);
	}

	public function export($id)
	{
		ini_set('max_execution_time', '120');
		ini_set("memory_limit","512M");

		$mod = Mod::findOrFail($id);
		if(!empty($mod)){

			$errors = [];

			$this->generateFocusFileArray();

			$states = Storage::files('/public/hoi4/game_files/states');
			foreach($states as $filePath){
				$arr = explode('-', $filePath, 2);
				$this->stateFiles[trim(str_replace('public/hoi4/game_files/states/','',$arr[0]))] = $filePath;
			}
			$directory = storagePath('/public/mods/'.$mod->id.'/export/'.date('Ymdgis'));
		  // /  die($directory);
			$createDir = '/public/mods/'.$mod->id.'/export/'.date('Ymdgis');
			$this->storageDir = $createDir;
			//Create the files
			Storage::makeDirectory($createDir);
			Storage::makeDirectory($createDir.'/gfx');
			if($mod->events->count() > 0 || $mod->focusTrees->count() > 0){
				Storage::makeDirectory($createDir.'/gfx/interface');
			}
			Storage::makeDirectory($createDir.'/interface');
			Storage::makeDirectory($createDir.'/common');
			if($mod->focusTrees->count() > 0){
				Storage::makeDirectory($createDir.'/gfx/interface/goals');
				Storage::makeDirectory($createDir.'/common/national_focus');
			}
			if($mod->countries->count() > 0){
				Storage::makeDirectory($createDir.'/gfx/leaders');
				Storage::makeDirectory($createDir.'/common/countries');
				Storage::makeDirectory($createDir.'/common/country_tags');
				Storage::makeDirectory($createDir.'/history');
				Storage::makeDirectory($createDir.'/history/countries');
				Storage::makeDirectory($createDir.'/history/states');
			}

			Storage::makeDirectory($createDir.'/common/ideas');
			Storage::makeDirectory($createDir.'/localisation');
			if($mod->events->count() > 0){
				Storage::makeDirectory($createDir.'/gfx/interface/event_gfx');
				Storage::makeDirectory($createDir.'/events');
			}
			

			if($mod->focusTrees->count() > 0){
				$this->generateFocusTreeFiles($mod, $directory);
			}

			if($mod->ideas->count() > 0){
				$this->generateIdeasFiles($mod, $directory);
			}

			if(!empty($mod->eventLanguage) || $mod->events->count() > 0){
				$this->generateEventFiles($mod, $directory);
			}

			if($mod->countries->count() > 0){
				$this->generateCountryFiles($mod, $directory);
				$colorsFile = Storage::get('/public/hoi4/game_files/common/countries/colors.txt');
				if (file_put_contents($directory.'/common/countries/colors.txt', $colorsFile."\n#Custom colours\n".$this->countryColours) !== false) {
				} else {
					$this->errors[] = "Cannot create file (Colours file for countries)";
				}
			}

			//Download files
			$zip_file_name = $mod->mod_name.'.zip';
			$download_file = true;
			$folder = $directory;

		   // die($folder);
			$za = new FlxZipArchive;
			$res = $za->open($zip_file_name, ZipArchive::CREATE);
			if($res === TRUE) {
				$za->addDir($folder, basename($folder));
				$za->close();
			}
			else  { $this->errors[] = 'Could not create a zip archive';}

			if ($download_file){
				ob_get_clean();
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private", false);
				header("Content-Type: application/zip");
				header("Content-Disposition: attachment; filename=" . basename($zip_file_name) . ";" );
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: " . filesize($zip_file_name));
				setcookie('downloadedMod', true, time() + 15); 
				readfile($zip_file_name);
			}
			unlink($zip_file_name);
			Storage::deleteDirectory($createDir);
			//Delete files

		}else{
			return redirect('/mod/'.$id)->with('error', 'Mod has nothing to export');
		}
		if(!empty($this->errors)){
			return redirect('/mod/'.$id)->with('error', 'Mod files exported successfully with the following common errors found.:'."\r\n".implode("\r\n", $this->errors));
		}
		return redirect('/mod/'.$id)->with('success', 'Mod files exported successfully with no common errors found.');
	}

	public function generateFocusFileArray(){
		$this->focusFileArray = [];
		$focusGfx = storagePath('public/hoi4/game_files/gfx/goals.gfx');
		$focusGfx = file_get_contents($focusGfx);
		foreach(explode("SpriteType = {", $focusGfx) as $opt){
			$lines = explode("=", $opt);
			
			foreach($lines as $line){
				$this->focusFileArray[] = $line;
			}
		}
	}

	public function nameToId($string, $id = null, $tag = null){
		$return = strtolower(preg_replace("/[^A-Za-z0-9_]/", '', str_replace(' ', '_', $string)));
		$returnTag = strtolower(preg_replace("/[^A-Za-z0-9_]/", '', (!empty($tag) ? str_replace(' ', '_', $tag).'_' : '' ).str_replace(' ', '_', $string)));

		if(empty($return) || (in_array($return, $this->idArrays) && empty($tag)) || (in_array($returnTag, $this->idArrays) && !empty($tag))){

			if(!isset($this->idArrays[$id])){
				$randomString = $this->generateRandomString();
				$this->idArrays[$id] = $randomString;
			}

			$return = $this->idArrays[$id];
		}
		$this->idArrays[$id] = $return;
		return $return; 
	}

	public function generateRandomString(){
		$permitted_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$id = substr(str_shuffle($permitted_chars), 0, 8);
		if(in_array($id, $this->idArrays)){
			$id = $this->generateRandomString();
		}else{
			return $id;
		}

	}

	public function searchFocuses($focuses, $id){
		foreach($focuses as $focus){
			if($focus->id == $id){
				return $this->nameToId($focus->focus_title, 'f_'.$id);
			}
		}
	}

	public function getFocusGFX($focusImage){
		$array = $this->focusFileArray;
		foreach($array as $key => $value){
			//If this array value contains the image we are searching, let's do the logic, else continue
			if(strpos($value, 'goals/'.$focusImage) !== false){
				//Remove leftover texturefile, all remaining \r\n\t's 
				$icon = str_replace(['texturefile', '='], '', str_replace("\r", '', str_replace("\n", '', str_replace("\t", '', $array[$key - 1]))));
				return str_replace(" ", "", str_replace('"', '', $icon));
			}
		}
		return 'GFX_goal_unknown ##Unfortunately, the tool could not find your icon in the game\'s files';
	}

	public function addLine($content, $tabs = 0){
		return str_repeat("\t", $tabs).$content."\r\n";
	}

	public function generateCountryFiles($mod, $directory){
		$id = $this->nameToId(strtolower(preg_replace("/[^A-Za-z]/", '', $mod->mod_name)), 'c_'.$mod->id);
		$statesEdited = [];

		foreach($mod->countries as $country){
			$language = $country->language;
			$tag = $country->tag;

			$startDate = $country->dates[0];

			$historyFile = chr(239) . chr(187) . chr(191) .'';
			$localisationFile = chr(239) . chr(187) . chr(191) ."l_".$language.":\n";

			$historyFile .= $this->addLine('capital = '.$country->capital);
			$historyFile .= $this->addLine('set_research_slots = 3');
			$historyFile .= $this->addLine('set_stability = '.$country->stability);
			$historyFile .= $this->addLine('set_war_support = '.$country->war_support);
			$historyFile .= $this->addLine('#Starting tech');
			$historyFile .= $this->addLine('set_technology = {');
				foreach(explode(',', $startDate->research) as $r){
					if(empty(trim($r))){
						continue;
					}
					$historyFile .= $this->addLine($r.' = 1', 1);
				}
			$historyFile .= $this->addLine('}');

			$dateIdeologies = [];
			foreach($country->dates as $date){
				if($date->id === $startDate->id){
					continue;
				}
				$historyFile .= $this->addLine('');
				$historyFile .= $this->addLine('#For start date '.$date->year.'.'.$date->month.'.'.$date->day);
				$historyFile .= $this->addLine($date->year.'.'.$date->month.'.'.$date->day.' = {');
					$historyFile .= $this->addLine('add_political_power = 500', 1);
					$historyFile .= $this->addLine('');
					$historyFile .= $this->addLine('#complete_national_focus = YOUR_FOCUS_ID', 1);
					$historyFile .= $this->addLine('#complete_national_focus = YOUR_FOCUS_ID', 1);
					$historyFile .= $this->addLine('');
					$historyFile .= $this->addLine('set_technology = {', 1);
						foreach(explode(',', $date->research) as $r){
							if(empty(trim($r))){
								continue;
							}
							$historyFile .= $this->addLine($r.' = 1', 2);
						}
					$historyFile .= $this->addLine('}', 1);
					$historyFile .= $this->addLine('#set_convoys = 100', 1);
				$historyFile .= $this->addLine('}');

				$dateIdeologies[$date->year.'.'.$date->month.'.'.$date->day] = [];
				foreach($date->ideologies as $i){
					$dateIdeologies[$date->year.'.'.$date->month.'.'.$date->day][$i->ideology_name] = $i->popularity;
				}
			}

			$historyFile .= $this->addLine('');
			$historyFile .= $this->addLine('#Politics for '.$startDate->year.'.'.$startDate->month.'.'.$startDate->day);
			$historyFile .= $this->addLine('set_politics = {');
				$popularityTotal = 0;
				$highestNum = 0;
				$ruling = ''; 
				foreach($startDate->ideologies as $i){
					$pop = intval($i->popularity);
					$popularityTotal += $pop;
					if($pop > $highestNum){
						$highestNum = $pop;
						$ruling = $i->ideology_name;
					}
				}
				if($popularityTotal > 100){
					$this->errors[] = $country->name." has total party popularity greater than 100% for date: ".$startDate->year.'.'.$startDate->month.'.'.$startDate->day;
				}
				$historyFile .= $this->addLine('ruling_party = '.$ruling, 1);
				$historyFile .= $this->addLine('last_election = "'.($startDate->year - 1).'.'.$startDate->month.'.'.$startDate->day.'"', 1);
				$historyFile .= $this->addLine('election_frequency = 48', 1);
				$historyFile .= $this->addLine('elections_allowed = no', 1);
			$historyFile .= $this->addLine('}');

			$historyFile .= $this->addLine('set_popularities = {');
				foreach($startDate->ideologies as $i){
					$historyFile .= $this->addLine($i->ideology_name.' = '.intval($i->popularity), 1);
				}
			$historyFile .= $this->addLine('}');

			foreach($dateIdeologies as $dt => $di){
				if(!empty($di)){
					$historyFile .= $this->addLine('');
					$historyFile .= $this->addLine('#Politics for '.$dt);
					$historyFile .= $this->addLine($dt.' = {');
						$historyFile .= $this->addLine('set_politics = {', 1);
							$popularityTotal = 0;
							$highestNum = 0;
							$ruling = ''; 
							foreach($di as $n => $i){
								$pop = intval($i);
								$popularityTotal += $pop;
								if($pop > $highestNum){
									$highestNum = $pop;
									$ruling = $n;
								}
							}
							if($popularityTotal > 100){
								$this->errors[] = $country->name." has total party popularity greater than 100% for date: ".$dt;
							}
							$historyFile .= $this->addLine('ruling_party = '.$ruling, 2);
							$historyFile .= $this->addLine('last_election = "'.$startDate->year.'.'.($startDate->month + 1).'.'.$startDate->day.'"', 2);
							$historyFile .= $this->addLine('election_frequency = 48', 2);
							$historyFile .= $this->addLine('elections_allowed = no', 2);
						$historyFile .= $this->addLine('}', 1);
						$historyFile .= $this->addLine('set_popularities = {', 1);
							foreach($startDate->ideologies as $i){
								$historyFile .= $this->addLine($i->ideology_name.' = '.intval($i->popularity), 2);
							}
						$historyFile .= $this->addLine('}', 1);
					$historyFile .= $this->addLine('}');
				}
			}

			$ideologyNames = [];
			Storage::makeDirectory($this->storageDir.'/gfx/leaders/'.$country->tag);

			$subIdeologies = [
				'democratic' => 'conservatism',
				'communism' => 'marxism',
				'facism' => 'nazism',
				'neutrality' => 'centrism'
			];
			foreach($country->dates as $d){
				$activeLeader = null;
				$ideologies = [];
				$popularity = [];
				foreach($d->ideologies as $i){
					if($i->leader_name == 'TBC'){
						continue;
					}
					$pop = (float) $i->popularity;
					$ideologies[$i->id] = $i;
					$popularity[$i->id] = $pop;
				}

				arsort($popularity);

				$ideologyAlts = [
					'democratic' => 'liberalism',
					'neutrality' => 'moderatism',
					'fascism' => 'fascism_ideology',
					'communism' => 'anti_revisionism'
				];

				foreach($popularity as $iID => $p){
					$i = $ideologies[$iID];
					$historyFile .= $this->addLine('');
					$historyFile .= $this->addLine('#Create country leader - '.$i->leader_name);
					$historyFile .= $this->addLine('create_country_leader = {');
						$historyFile .= $this->addLine('name = "'.$i->leader_name.'"', 1);
						//$historyFile .= $this->addLine('desc = ""', 1);
						if(strpos($i->leader_gfx, 'hoi4/leaders') !== false){
							$ex = explode('/', $i->leader_gfx);
							$leader = str_replace(['media/get/hoi4/leaders', '/public/storage/hoi4/leaders/', '.png'], '', end($ex));
							$historyFile .= $this->addLine('picture = "'.$leader.'.dds"', 1);
							Media::copyImageToDirectory(0, $this->storageDir.'/gfx/leaders/'.$country->tag, $leader, '/public/hoi4/leaders/'.$leader.'.png');
						}else{
							$gfxID = null;
							$ex = explode('/', $i->leader_gfx);
							$fileName = '';
							foreach($ex as $line){
								if(!in_array($line, ['', 'image', 'find', 'leader'])){
									$gfxID = $line;
									$fileName = $this->nameToId($i->leader_name, 'country_leader_gfx_'.$i->id).'-'.$gfxID;
									Media::copyImageToDirectory($gfxID, $this->storageDir.'/gfx/leaders/'.$country->tag, $fileName, 'leader');
								}
							}
							$historyFile .= $this->addLine('#Custom image for '.$i->leader_name, 1);
							$historyFile .= $this->addLine('picture = "'.$fileName.'.dds"', 1);
						}
						$historyFile .= $this->addLine('expire = "1965.1.1"', 1);
						$historyFile .= $this->addLine('ideology = '.(isset($ideologyAlts[$i->ideology_name]) ? $ideologyAlts[$i->ideology_name] : $i->ideology_name), 1);
						$historyFile .= $this->addLine('traits = { }', 1);
					$historyFile .= $this->addLine('}');

					$ideologyNames[$i->ideology_name] = $i->country_name;
				}
			}

			$localisationFile .= $this->addLine($tag.':0 "'.$country->name.'"');
			$localisationFile .= $this->addLine($tag.'_DEF:0 "'.$country->name.'"');
			$localisationFile .= $this->addLine($tag.'_ADJ:0 "Citizen of '.$country->name.'"');
			foreach($ideologyNames as $ideology => $name){
				$localisationFile .= $this->addLine($tag.'_'.$ideology.':0 "'.$name.'"');
				$localisationFile .= $this->addLine($tag.'_'.$ideology.'_DEF:0 "'.$name.'"');
				$localisationFile .= $this->addLine($tag.'_'.$ideology.'_ADJ:0 "Citizen of '.$name.'"');
			}

			foreach($country->generals as $general){
				$historyFile .= $this->addLine('');
				$historyFile .= $this->addLine('#Create military leader - '.$general->name);

				$leaderId = '1'.$country->mod->id.$country->id.$general->id; //To try to ensure it won't conflict with anything in game or in mod/other mods
				if($general->is_army){
					if($general->is_field_marshal){
						$historyFile .= $this->addLine('create_field_marshal = {');
					}else{
						$historyFile .= $this->addLine('create_corps_commander = {');
					}
					$historyFile .= $this->addLine('name = "'.$general->name.'"', 1);

					if(strpos($general->gfx, 'hoi4/leaders') !== false){
						$ex = explode('/', $general->gfx);
						$leader = str_replace(['media/get/hoi4/leaders', '/public/storage/hoi4/leaders/', '.png'], '', end($ex));
						$historyFile .= $this->addLine('picture = "'.$leader.'.dds"', 1);
						Media::copyImageToDirectory(0, $this->storageDir.'/gfx/leaders/'.$country->tag, $leader, '/public/hoi4/leaders/'.$leader.'.png');
					}else{
						$gfxID = null;
						$ex = explode('/', $general->gfx);
						$fileName = '';
						foreach($ex as $line){
							if(!in_array($line, ['', 'image', 'find', 'leader'])){
								$gfxID = $line;
								$fileName = $this->nameToId($i->leader_name, 'country_leader_gfx_'.$general->id).'-'.$gfxID;
								Media::copyImageToDirectory($gfxID, $this->storageDir.'/gfx/leaders/'.$country->tag, $fileName, 'leader');
							}
						}
						$historyFile .= $this->addLine('#Custom image for '.$i->leader_name, 1);
						$historyFile .= $this->addLine('picture = "'.$fileName.'.dds"', 1);
					}
					$historyFile .= $this->addLine('traits = {'.$general->traits.' }', 1);
					$historyFile .= $this->addLine('skill = '.$general->level, 1);
					$historyFile .= $this->addLine('attack_skill = '.$general->attack_stat, 1);
					$historyFile .= $this->addLine('defence_skill = '.$general->defence_stat, 1);
					$historyFile .= $this->addLine('planning_skill = '.$general->planning_stat, 1);
					$historyFile .= $this->addLine('logistics_skill = '.$general->supply_stat, 1);
					$historyFile .= $this->addLine('id = '.$leaderId, 1);
				}

				if($general->is_navy){
					$historyFile .= $this->addLine('create_navy_leader = {');
					$historyFile .= $this->addLine('name = "'.$general->name.'"', 1);
					if(strpos($general->gfx, 'hoi4/leaders') !== false){
						$ex = explode('/', $general->gfx);
						$leader = str_replace(['media/get/hoi4/leaders', '/public/storage/hoi4/leaders/', '.png'], '', end($ex));
						$historyFile .= $this->addLine('picture = "'.$leader.'.dds"', 1);
						Media::copyImageToDirectory(0, $this->storageDir.'/gfx/leaders/'.$country->tag, $leader, '/public/hoi4/leaders/'.$leader.'.png');
					}else{
						$gfxID = null;
						$ex = explode('/', $general->gfx);
						$fileName = '';
						foreach($ex as $line){
							if(!in_array($line, ['', 'image', 'find', 'leader'])){
								$gfxID = $line;
								$fileName = $this->nameToId($i->leader_name, 'country_leader_gfx_'.$general->id).'-'.$gfxID;
								Media::copyImageToDirectory($gfxID, $this->storageDir.'/gfx/leaders/'.$country->tag, $fileName, 'leader');
							}
						}
						$historyFile .= $this->addLine('#Custom image for '.$i->leader_name, 1);
						$historyFile .= $this->addLine('picture = "'.$fileName.'.dds"', 1);
					}
					$historyFile .= $this->addLine('traits = {'.$general->traits.' }', 1);
					$historyFile .= $this->addLine('skill = '.$general->level, 1);
					$historyFile .= $this->addLine('attack_skill = '.$general->attack_stat, 1);
					$historyFile .= $this->addLine('defence_skill = '.$general->defence_stat, 1);
					$historyFile .= $this->addLine('maneuvering_skill = '.$general->planning_stat, 1);
					$historyFile .= $this->addLine('coordination_skill = '.$general->supply_stat, 1);
					$historyFile .= $this->addLine('id = '.$leaderId, 1);
				}

				$historyFile .= $this->addLine('}');
			}

			$cultures = [
				'Europe' => 'western_european',
				'African' => 'african',
				'Africa' => 'african',
				'Asia' => 'asian',
				'SouthAmerica' => 'southamerican'
			];

			$commonFile = '';
			$commonFile .= $this->addLine('graphical_culture = '.$cultures[$country->culture].'_gfx');
			$commonFile .= $this->addLine('graphical_culture_2d = '.$cultures[$country->culture].'_2d');
			$commonFile .= $this->addLine('color = { '.implode(' ',explode(',', $country->colour)).' }');

			$this->countryColours .= $this->addLine($tag.' = {');
			$this->countryColours .= $this->addLine('color = rgb { '.implode(' ',explode(',', $country->colour)).' }', 1);
			$this->countryColours .= $this->addLine('color_ui = rgb { '.implode(' ',explode(',', $country->colour)).' }', 1);
			$this->countryColours .= $this->addLine('}');

			foreach($country->dates as $d){
				foreach($d->states as $state){
					if(isset($statesEdited[$state->state_id])){
						$statesEdited[$state->state_id] = $this->editStateFileContents($statesEdited[$state->state_id], $tag, $d->year.'.'.$d->month.'.'.$d->day, $state->core, $state->controls, true);
					}else{
						if(isset($this->stateFiles[$state->state_id])){
							$file = Storage::get($this->stateFiles[$state->state_id]);
							$statesEdited[$state->state_id] = $this->editStateFileContents($file, $tag, $d->year.'.'.$d->month.'.'.$d->day, $state->core, $state->controls);
						}else{
							$this->errors[] = "Unable to find file for state id: ".$state->state_id." to assign to ".$country->name;
						}
					}
				}
			}


			$ideasFile = '';
			$ideasFile .= $this->addLine('ideas = {');
				$ideasFile .= $this->addLine('political_advisor = {', 1);
					foreach($country->cabinet as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);
				$ideasFile .= $this->addLine('');
				$ideasFile .= $this->addLine('#Military', 1);
				$ideasFile .= $this->addLine('army_chief = {', 1);
					foreach($country->armygs as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('navy_chief = {', 1);
					foreach($country->navygs as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('air_chief = {', 1);
					foreach($country->airgs as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('high_command = {', 1);
					foreach($country->military as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('#Manufacturers', 1);

				$ideasFile .= $this->addLine('tank_manufacturer = {', 1);
					foreach($country->tank as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('research_bonus = { armor = 0.10 }', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('naval_manufacturer = {', 1);
					foreach($country->ship as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('research_bonus = { naval_equipment = 0.10 }', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('aircraft_manufacturer = {', 1);
					foreach($country->air as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('research_bonus = { air_equipment = 0.10 }', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('materiel_manufacturer = {', 1);
					foreach($country->air as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

				$ideasFile .= $this->addLine('theorist = {', 1);
					foreach($country->doctrine as $idea){
						$name = $this->nameToId($idea->name, 'i_'.$id);
						$ideasFile .= $this->addLine($name.' = {', 2);
							$ideasFile .= $this->addLine('allowed = {', 3);
								$ideasFile .= $this->addLine('original_tag = '.$tag, 4);
							$ideasFile .= $this->addLine('}', 3);
							$ideasFile .= $this->addLine('traits = {'.$idea->effect.'}', 3);
							$ideasFile .= $this->addLine('picture = '.str_replace(['/media/get/hoi4/ideas/people','/media/get/hoi4/ideas/not_people','/public/storage/hoi4/ideas/people/', '/public/storage/hoi4/ideas/not_people/', '.png'], '', $idea->gfx), 3);
						$ideasFile .= $this->addLine('}', 2);

						$localisationFile .= $this->addLine($name.':0 "'.$idea->name.'"', 1);
					}
				$ideasFile .= $this->addLine('}', 1);

			$ideasFile .= $this->addLine('}');

			$countryNameId = $this->nameToId($country->name, 'country-name');
			if (file_put_contents($directory.'/common/countries/'.$country->name.'.txt', $commonFile) !== false) {
				//echo "File created (" . $country->name . " - common file)";
			} else {
				$this->errors[] = "Cannot create file (" . $country->name . " - common file)";
			}

			if (file_put_contents($directory.'/history/countries/'.$tag.' - '.$country->name.'.txt', $historyFile) !== false) {
				//echo "File created (" . $country->name . " - history file)";
			} else {
				$this->errors[] =  "Cannot create file (" . $country->name . " - history file)";
			}

			if (file_put_contents($directory.'/localisation/'.$tag.'_l_'.$language.'.yml', $localisationFile) !== false) {
				//echo "File created (" . $country->name . " - localisation file)";
			} else {
				$this->errors[] =  "Cannot create file (" . $country->name . " - localisation file)";
			}

			if (file_put_contents($directory.'/common/ideas/'.$countryNameId.'.txt', $ideasFile) !== false) {
				//echo "File created (" . $country->name . " - ideas file)";
			} else {
				$this->errors[] =  "Cannot create file (" . $country->name . " - ideas file)";
			}

		}

		foreach($statesEdited as $stateId => $contents){
			$filename = str_replace('public/hoi4/game_files/states/', '', $this->stateFiles[$stateId]);
			if (file_put_contents($directory.'/history/states/'.$filename, $contents) !== false) {
				//echo "File created (" . $filename . " - state file)";
			} else {
				$this->errors[] =  "Cannot create file (" . $filename . " - modified state file)";
			}
		}

		$countryTagsFile = '';
		foreach($mod->countries as $c){
			$countryTagsFile .= $this->addLine($c->tag.' = "countries/'.$country->name.'.txt"');
		}
		if (file_put_contents($directory.'/common/country_tags/'.$mod->id.'_countries.txt', $countryTagsFile) !== false) {
			//echo "File created ( country tags )";
		} else {
			$this->errors[] =  "Cannot create file (Country tags)";
		}
	}

	public function editStateFileContents($file, $tag, $date, $core = false, $controls = false, $alreadyEdited = false){
		if(!$alreadyEdited){
			$newFile = explode('provinces', $file);
			$newFile[0] = rtrim(rtrim(rtrim(rtrim($newFile[0])), '}'));
			$newFile[0] .= $this->addLine('');
			$newFile[0] .= $this->addLine('');
			$newFile[0] .= $this->addLine('##State edits start');

			if($controls){
				$newFile[0] = str_replace('controller', '#controller', $newFile[0]);
				$newFile[0] = str_replace('owner', '#owner', $newFile[0]);
			}
		}else{
			$newFile = explode("##State edits end", $file);
		}

		$newFile[0] .= $this->addLine('');
		$newFile[0] .= $this->addLine('##State edit for '.$tag.' after '.$date);
		$newFile[0] .= $this->addLine($date.' = {');
			if($core == 1){
				$newFile[0] .= $this->addLine('add_core_of = '.$tag, 1);
			}
			if($controls == 1){
				$newFile[0] .= $this->addLine('owner = '.$tag, 1);
				$newFile[0] .= $this->addLine('controller = '.$tag, 1);
			}
		$newFile[0] .= $this->addLine('}');
		$newFile[0] .= $this->addLine('');
		if(!$alreadyEdited){
			$newFile[0] .= $this->addLine('##State edits end');
			$file = implode("}\nprovinces", $newFile);
		}else{
			$file = implode("##State edits end\n", $newFile);
		}

		return $file;

	}

	public function generateIdeasFiles($mod, $directory){
		foreach($mod->ideas as $group){
			$prefix = $group->prefix;
			if(!empty($prefix)){
				$prefix .= '_';
			}
			$filename = $group->filename;
			$language = $filename.'_l_'.$group->language.'.yml';

			$file = 'ideas = {';
			$ideaType = '';

			$lang = 'l_'.$group->language.':';
			$customGfx = "spriteTypes = { \r\n";
			foreach($group->ideasOrdered as $idea){
				if($idea->group != $ideaType){
					$file .= $this->addLine('');
					if(!empty($ideaType)){
						$file .= $this->addLine('}', 1);
					}
					$ideaType = $idea->group;
					$file .= $this->addLine($idea->group.' = {', 1);
				}
				$name = $prefix.$this->nameToId(preg_replace("/[^A-Za-z0-9_]/", '', str_replace(' ', '_',$idea->name)), 'i_'.$group->id);
				$options = json_decode($idea->options);
				$noBraceKeyValues = ['picture', 'removal_cost', 'ledger', 'cost'];
				$file .= $this->addLine('');
				$file .= $this->addLine($name.' = {', 2);
					foreach($options as $key => $value){
						if($key == 'gfx'){
							$key = 'picture';
							if(strpos($value,"hoi4/ideas") !== false){
								$value = str_replace(['/public/storage', '/media/get/', '/media/get','/hoi4/ideas/people/', '/hoi4/ideas/not_people/', '.png'], '', $value);
								$value = ltrim($value, 'idea_');
							}else{
								if(strpos($value, 'image/find')){
									$gfxID = null;
									$ex = explode('/', $value);
									foreach($ex as $line){
										if(!in_array($line, ['', 'image', 'find', 'idea', 'national-spirit'])){
											$gfxID = $line;
											$iconName = $name.'-'.$gfxID;
											$type = 'idea';
											$value = $iconName;
											Media::copyImageToDirectory($gfxID, $this->storageDir.'/gfx/interface/ideas/', $iconName, $type);
											$customGfx .= $this->addLine('##Icon For: '.$idea->name, 1);
											$customGfx .= $this->addLine('SpriteType = { ', 1);
											$customGfx .= $this->addLine('name = "'.$iconName.'"', 2);
											$customGfx .= $this->addLine('texturefile = "gfx/interface/ideas/'.$iconName.'.dds"', 2);
											$customGfx .= $this->addLine('}', 1);
											$customGfx .= $this->addLine('');
										}
									}
								}else{
									$value = '#Image not found';
								}
							}
						}
						if(in_array($key, $noBraceKeyValues)){
							$file .= $this->addLine($key.' = '.$value, 3);
						}else{
							$file .= $this->addLine($key.' = {', 3);
							$file .= $this->addLine($value, 4);
							$file .= $this->addLine('}', 3);
						}
					}
				$file .= $this->addLine('}', 2);
				$lang .= $this->addLine($name.':0 "'.$idea->name);


				

			}
			$file .= $this->addLine('}', 1);
			$file .= '}';

			$ideasFile = $directory.'/common/ideas/'.$filename.'.txt';
			$ln = $directory.'/localisation/'.$language;

			if (file_put_contents($ln, $lang) !== false) {
				//echo "File created (" . basename($ln) . ")";
			} else {
				$this->errors[] = "Cannot create file (" . basename($ln) . ")";
			}

			if (file_put_contents($ideasFile, $file) !== false) {
				//echo "File created (" . basename($eventFile) . ")";
			} else {
				$this->errors[] = "Cannot create file (" . basename($ideasFile) . ")";
			}

			$customGfx .= " \r\n }";
			$customgfxfile = $directory.'/interface/'.$this->nameToId($group->name, 'ig_'.$mod->id).'_customicons.gfx';
			if (file_put_contents($customgfxfile, $customGfx) !== false) {
				echo "File created (" . basename($customgfxfile) . ")";
			} else {
				$this->errors[] = "Cannot create file (" . basename($customgfxfile) . ")";
			}

			if(substr_count($file, '{') !== substr_count($file, '}')){
				$this->errrors[] = "Unmatched brace count in ideas file: ".$group->name.". ".substr_count($file, '{')." '{' found, and ".substr_count($file, '}')." '}' found.";
			}
		}
	}

	public function generateEventFiles($mod, $directory){
		$id = $this->nameToId(strtolower(preg_replace("/[^A-Za-z]/", '', $mod->mod_name)), 'm_'.$mod->id);
		if(!empty($mod->eventLanguage)){
			$language = $mod->eventLanguage->language;
		}else{
			$language = 'english';
		}

		$eventsLang = $id.'_l_'.$language.'.yml';
		$eventsFile = $id.'_events.txt';

		$customGfx = "spriteTypes = { \r\n";

		$lang = chr(239) . chr(187) . chr(191) .'l_'.$language.": \r\n";
		$events = 'add_namespace = '.$id."\r\n"."\r\n";
		$eventNum = 1;

		foreach($mod->events as $event){
			$lang .= $id.'.'.$eventNum.'.title:0 "'.$event->event_title.'"'."\r\n";
			$lang .= $id.'.'.$eventNum.'.desc:0 "'.$event->event_description.'"'."\r\n";


			$e = '';
			$outcomeChar = 'a';
			$e .= $this->addLine('##Event: '.$event->event_title);
			$e .= $this->addLine($event->event_type.'_event = {');
				$e .= $this->addLine('id = '.$id.'.'.$eventNum, 1);
				$e .= $this->addLine('title = '.$id.'.'.$eventNum.".title", 1);
				$e .= $this->addLine('desc = '.$id.'.'.$eventNum.".desc", 1);

				if(strpos($event->event_gfx,"hoi4/event_gfx") !== false){
					$e .= $this->addLine('picture = GFX_'.str_replace(['/public/storage', '/media/get/', '/media/get','/hoi4/event_gfx/', '.png'], '', $event->event_gfx), 1);
				}else{
					if(strpos($event->event_gfx, 'image/find')){
						$gfxID = null;
						$ex = explode('/', $event->event_gfx);
						foreach($ex as $line){
							if(!in_array($line, ['', 'image', 'find', 'event', 'news'])){
								$gfxID = $line;
								$fileName = $this->nameToId($event->event_title, 'e_'.$event->id).'-'.$gfxID;
								$type = 'event';
								if($event->event_type == 'news'){
									$type = 'news-event';
								}
								Media::copyImageToDirectory($gfxID, $this->storageDir.'/gfx/interface/event_gfx/', $fileName, $type);
								$customGfx .= $this->addLine('##Icon For: '.$event->event_title, 1);
								$customGfx .= $this->addLine('SpriteType = { ', 1);
								$customGfx .= $this->addLine('name = "'.$fileName.'"', 2);
								$customGfx .= $this->addLine('texturefile = "gfx/interface/event_gfx/'.$fileName.'.dds"', 2);
								$customGfx .= $this->addLine('}', 1);
								$customGfx .= $this->addLine('');
							}
						}
					}else{
						// DEPRECATED
						$nameid = $this->nameToId($event->event_title, 'e_'.$event->id);
						if(strpos($event->event_gfx, 'media/get') !== false){
							$img = new Imagick(storagePath(ltrim($event->event_gfx, '/media/get')));
						}else{
							if(strpos($event->event_gfx, '/home/hoimoddi') !== false){
								$img = new Imagick($event->event_gfx); //Load the uploaded image
							}else{
								$img = new Imagick(__DIR__ .'/../../..'.$event->event_gfx);
							}
						}
						$img->setformat('tga'); //Set the format to tga
						$img->writeimage($directory.'/gfx/interface/event_gfx/GFX_'.$nameid.'.tga'); //Write/save the tga texture
						$icon = 'GFX_'.$nameid;
						$customGfx .= '##Icon For: '.$event->event_title."\r\n".' SpriteType = { '."\r\n".' name = "'.$icon.'" '."\r\n".' texturefile = "gfx/interface/event_gfx/'.$icon.'.tga" '."\r\n".' }';

						$e .= $this->addLine('picture = '.$icon, 1);
					}
				}

				if($event->event_is_triggered_only){
					$e .= $this->addLine('is_triggered_only = yes',1);
				}

				if($event->event_fire_only_once){
					$e .= $this->addLine('fire_only_once = yes', 1);
				}

				if($event->event_major){
					$e .= $this->addLine('major = yes',1);
				}

				if($event->event_trigger){
					$e .= $this->addLine('trigger = {', 1);
					$e .= $this->addLine($event->event_trigger, 2);
					$e .= $this->addLine('}', 1);
				}

				if($event->event_mean_time_to_happen){
					$e .= $this->addLine('mean_time_to_happen = {',1);
					$e .= $this->addLine('days = '.$event->event_mean_time_to_happen, 2);
					$e .= $this->addLine('}', 1);
				}

				if($event->event_immediate){
					$e .= $this->addLine('immediate = {', 1);
					$e .= $this->addLine($event->event_immediate, 2);
					$e .= $this->addLine("}", 1);
				}

				
				foreach($event->outcomes as $outcome){
					$lang .= $id.'.'.$eventNum.'.'.$outcomeChar.':0 "'.$outcome->outcome_name.'"'."\r\n";

					$e .= $this->addLine('option = {', 1);
						$e .= $this->addLine('name = '.$id.'.'.$eventNum.'.'.$outcomeChar, 2);
						$e .= $this->addLine('ai_chance = { factor = '.$outcome->outcome_ai_chance."}", 2);
						if($outcome->outcome_reward){
							$e .= $this->addLine($outcome->outcome_reward, 2);
						}
					$e .= $this->addLine('}', 1);

					$outcomeChar++;
				}
				
			$e .= '}'."\r\n";
			$events .= $e;
			$eventNum++;
		}
		if($eventNum === 1){
			return;
		}

		$eventFile = $directory.'/events/'.$eventsFile;
		$ln = $directory.'/localisation/'.$eventsLang;

		if (file_put_contents($ln, $lang) !== false) {
			//echo "File created (" . basename($ln) . ")";
		} else {
			$this->errors[] = "Cannot create file (" . basename($ln) . ")";
		}

		if (file_put_contents($eventFile, $events) !== false) {
			//echo "File created (" . basename($eventFile) . ")";
		} else {
			$this->errors[] = "Cannot create file (" . basename($eventFile) . ")";
		}

		$customGfx .= " \r\n }";
		$customgfxfile = $directory.'/interface/'.$id.'_customicons.gfx';
		if (file_put_contents($customgfxfile, $customGfx) !== false) {
			echo "File created (" . basename($customgfxfile) . ")";
		} else {
			$this->errors[] = "Cannot create file (" . basename($customgfxfile) . ")";
		}

		if(substr_count($events, '{') !== substr_count($events, '}')){
			$this->errrors[] = "Unmatched brace count in events. ".substr_count($events, '{')." '{' found, and ".substr_count($events, '}')." '}' found.";
		}
	}

	public function generateFocusTreeFiles($mod, $directory){
		foreach($mod->focusTrees as $tree){
			$this->getFocusTreeTextForExport($tree, true, $directory);	
		}
	}

	public function getFocusTreeTextForExport($tree, $export = false, $directory = ''){
		$tag = $tree->country_tag;
		$tree_file = $tag.'_'.$tree->tree_id.'.txt';
		$language_file = $tag.'_'.$tree->tree_id.'_l_'.$tree->lang.'.yml';

		$customGfx = "";

		$focusTree = $this->addLine("focus_tree = {");
			$focusTree .= $this->addLine("id = ".$tree->tree_id, 1);
			$focusTree .= $this->addLine("country = {", 1);
				$focusTree .= $this->addLine("factor = 0", 2);
				$focusTree .= $this->addLine("modifier = {", 2);
					$focusTree .= $this->addLine("add = 10", 3);
					$focusTree .= $this->addLine("tag = ".$tree->country_tag, 3);
				$focusTree .= $this->addLine("}", 2);
			$focusTree .= $this->addLine("}", 1);

		$lang = chr(239) . chr(187) . chr(191) .'l_'.$tree->lang.": \r\n";

		$focuses = $tree->focuses;

		


		foreach($focuses as $focus){
			$focusID = $tag.'_'.$this->nameToId($focus->focus_title, 'f_'.$focus->id);

			$lang .= $tag.'_'.$this->nameToId($focus->focus_title, 'f_'.$focus->id, $tag).':0 "'.$focus->focus_title.'"'."\r\n";
			$lang .= $tag.'_'.$this->nameToId($focus->focus_title, 'f_'.$focus->id, $tag).'_desc:0 "'.str_replace("\n", '\n', $focus->focus_description).'"'."\r\n";

			$fortree = '';
			if(!$export){
				$fortree .= '<div class="focus-tree-raw-preview" data-id="'.$focus->id.'"><div class="preview-inner">';
			}
			$idMessage = empty(strtolower(preg_replace("/[^A-Za-z0-9]/", '', $focus->focus_title))) ? ' - focus ID randomly generated due to title being in non-latin characters' : '';
			$fortree .= $this->addLine('#Focus for '.$focus->focus_title. $idMessage, 1);
			$fortree .= $this->addLine("focus = {", 1);

			if(strpos($focus->focus_gfx,"hoi4/focus_icons") !== false){
				$icon = str_replace(['/public/storage/hoi4/focus_icons/','/media/get/', '/hoi4/focus_icons/'], '', $focus->focus_gfx);
				$rem_png = str_replace(".png","",$icon);
				$icon = $this->getFocusGFX($rem_png);
			}else{
				$nameid = $this->nameToId($focus->focus_title, 'f_'.$focus->id);
				//Focus Icon
				if(strpos($focus->focus_gfx, 'image/find')){
					$gfxID = null;
					$ex = explode('/', $focus->focus_gfx);
					foreach($ex as $line){
						if(!in_array($line, ['', 'image', 'find', 'focus'])){
							$gfxID = $line;
							$fileName = "GFX_".$focusID.'-'.$gfxID;
							$icon = $fileName;
							if($export){
								Media::copyImageToDirectory($gfxID, $this->storageDir.'/gfx/interface/goals/', $fileName, 'focus');
								$customGfx .= $this->addLine('##Icon For: '.$focus->focus_title, 1);
								$customGfx .= $this->addLine('SpriteType = { ', 1);
								$customGfx .= $this->addLine('name = "'.$fileName.'"', 2);
								$customGfx .= $this->addLine('texturefile = "gfx/interface/goals/'.$fileName.'.dds"', 2);
								$customGfx .= $this->addLine('}', 1);
								$customGfx .= $this->addLine('');
								$customGfx .= $this->addLine('##Icon Shine For: '.$focus->focus_title, 1);
								$customGfx .= $this->addLine('SpriteType = { ', 1);
								$customGfx .= $this->addLine('name = "'.$fileName.'_shine"', 2);
								$customGfx .= $this->addLine('texturefile = "gfx/interface/goals/'.$fileName.'.dds"', 2);
								$customGfx .= $this->addLine('effectFile = "gfx/FX/buttonstate.lua"', 2);
								//Animation 1
								$customGfx .= $this->addLine('animation = {', 2);
								$customGfx .= $this->addLine('animationmaskfile = "gfx/interface/goals/'.$fileName.'.dds"', 3);
								$customGfx .= $this->addLine('animationtexturefile = "gfx/interface/goals/shine_overlay.dds"', 3);
								$customGfx .= $this->addLine('animationrotation = -90.0', 3);
								$customGfx .= $this->addLine('animationlooping = no', 3);
								$customGfx .= $this->addLine('animationtime = 0.75', 3);
								$customGfx .= $this->addLine('animationdelay = 0', 3);
								$customGfx .= $this->addLine('animationblendmode = "add"', 3);
								$customGfx .= $this->addLine('animationtype = "scrolling"', 3);
								$customGfx .= $this->addLine('animationrotationoffset = { x = 0.0 y = 0.0 }', 3);
								$customGfx .= $this->addLine('animationtexturescale = { x = 1.0 y = 1.0 } ', 3);
								$customGfx .= $this->addLine('}', 2);
								//Animation 2
								//Animation 1
								$customGfx .= $this->addLine('animation = {', 2);
								$customGfx .= $this->addLine('animationmaskfile = "gfx/interface/goals/'.$fileName.'.dds"', 3);
								$customGfx .= $this->addLine('animationtexturefile = "gfx/interface/goals/shine_overlay.dds"', 3);
								$customGfx .= $this->addLine('animationrotation = 90.0', 3);
								$customGfx .= $this->addLine('animationlooping = no', 3);
								$customGfx .= $this->addLine('animationtime = 0.75', 3);
								$customGfx .= $this->addLine('animationdelay = 0', 3);
								$customGfx .= $this->addLine('animationblendmode = "add"', 3);
								$customGfx .= $this->addLine('animationtype = "scrolling"', 3);
								$customGfx .= $this->addLine('animationrotationoffset = { x = 0.0 y = 0.0 }', 3);
								$customGfx .= $this->addLine('animationtexturescale = { x = 1.0 y = 1.0 } ', 3);
								$customGfx .= $this->addLine('}', 2);
								$customGfx .= $this->addLine('legacy_lazy_load = no', 2);
								$customGfx .= $this->addLine('}', 1);
								$customGfx .= $this->addLine('');
							}
						}
					}
				}else{
					// DEPRECATED!

					if(strpos($focus->focus_gfx, '/home/hoimoddi') !== false){
						$icon = "GFX_goal_unknown";
					}else{
						if(strpos($focus->focus_gfx, 'base64') !== false){
							$icon = 'GFX_goal_unknown';
						}else{
							$local = __DIR__ .'/../../..'.$focus->focus_gfx;
							$i = false;
							// if(file_exists($local)){
							// 	$img = new Imagick($local);
							// 	$i = true;
							//}else
							if(Storage::has('public/'.ltrim($focus->focus_gfx, '/media/get/'))){
								$img = storagePath(ltrim($focus->focus_gfx, '/media/get/'));
							}

							if($i){
								if($export){
									$img->setformat('tga'); //Set the format to tga
									$img->writeimage($directory.'/gfx/interface/goals/GFX_'.$nameid.'.tga'); //Write/save the tga texture
								}
								$icon = 'GFX_'.$nameid;
								$customGfx .= '##Icon For: '.$focus->id."\r\n".' SpriteType = { '."\r\n".' name = "'.$icon.'" '."\r\n".' texturefile = "gfx/interface/goals/'.$icon.'.tga" '."\r\n".' }';
							}else{
								$icon = "GFX_goal_unknown";
							}
						}
					}
				}
				
			}
			$fortree .= $this->addLine('id = '.$focusID, 2);
			$fortree .= $this->addLine('icon = '.$icon, 2);
			$fortree .= $this->addLine('x = '.$focus->focus_x, 2);
			$fortree .= $this->addLine('y = '.$focus->focus_y, 2);
			$fortree .= $this->addLine('cost = '.$focus->focus_time_to_complete, 2);
			$fortree .= $this->addLine("available_if_capitulated = yes", 2);
			if(!empty($focus->focus_prerequisite)){
				if(strpos($focus->focus_prerequisite, '&&') !== false || strpos($focus->focus_prerequisite, '||') !== false){
					if(strpos($focus->focus_prerequisite, '&&') !== false && strpos($focus->focus_prerequisite, '||') !== false){
						$explode = explode('||', $focus->focus_prerequisite);
						foreach($explode as $and){
							$fortree .= $this->addLine("prerequisite = { ", 2);
								$getAnds = explode('&&', $and);
								foreach($getAnds as $focusId){
									$fortree .= $this->addLine('focus = '.$tag.'_'.$this->searchFocuses($focuses, $focusId), 3);
								}
							$fortree .= $this->addLine("}", 2);
						}
					}else{
						if(strpos($focus->focus_prerequisite, '||') !== false){
							$fortree .= $this->addLine("prerequisite = { ", 2);
								$getAnds = explode('||', $focus->focus_prerequisite);
								foreach($getAnds as $focusId){
									$fortree .= $this->addLine('focus = '.$tag.'_'.$this->searchFocuses($focuses, $focusId).' ', 3);
								}
							$fortree .= $this->addLine("}", 2);
						}else{
							$explode = explode('&&', $focus->focus_prerequisite);
							foreach($explode as $focusId){
								$fortree .= $this->addLine("prerequisite = { ", 2);
									$fortree .= $this->addLine('focus = '.$tag.'_'.$this->searchFocuses($focuses, $focusId).' ', 3);
								$fortree .= $this->addLine("}", 2);
							}
						}
					}
				}else{
					$fortree .= $this->addLine("prerequisite = { focus = ".$tag.'_'.$this->searchFocuses($focuses, $focus->focus_prerequisite)." }", 2);
				}
			}

			if(!empty($focus->focus_mutually_exclusive)){
				if(strpos($focus->focus_mutually_exclusive, '&&') !== false || strpos($focus->focus_mutually_exclusive, '||') !== false){
					if(strpos($focus->focus_mutually_exclusive, '&&') !== false && strpos($focus->focus_mutually_exclusive, '||') !== false){
						$explode = explode('||', $focus->focus_mutually_exclusive);
						foreach($explode as $and){
							$fortree .= $this->addLine("mutually_exclusive = { ", 2);
								$getAnds = explode('&&', $and);
								foreach($getAnds as $focusId){
									$fortree .= $this->addLine('focus = '.$tag.'_'.$this->searchFocuses($focuses, $focusId).' ', 3);
								}
							$fortree .= $this->addLine("}", 2);
						}
					}else{
						if(strpos($focus->focus_mutually_exclusive, '||') !== false){
							$fortree .= $this->addLine("mutually_exclusive = { ", 2);
								$getAnds = explode('||', $focus->focus_mutually_exclusive);
								foreach($getAnds as $focusId){
									$fortree .= $this->addLine('focus = '.$tag.'_'.$this->searchFocuses($focuses, $focusId).' ', 3);
								}
							$fortree .= $this->addLine("}", 2);
						}else{
							$explode = explode('&&', $focus->focus_mutually_exclusive);
							foreach($explode as $focusId){
								$fortree .= $this->addLine("mutually_exclusive = { ", 2);
									$fortree .= $this->addLine('focus = '.$tag.'_'.$this->searchFocuses($focuses, $focusId).' ', 3);
								$fortree .= $this->addLine("}", 2);
							}
						}
					}
				}else{
					$fortree .= $this->addLine("mutually_exclusive = { focus = ".$tag.'_'.$this->searchFocuses($focuses, $focus->focus_mutually_exclusive)." }", 2);
				}
			}

			if(isset($focus->filter) && !empty($focus->filter)){
				$fortree .= $this->addLine('search_filters = { '.$focus->filter.' } ', 2);
			}

			if(!empty($focus->focus_ai_will_do_factor)){
				$fortree .= $this->addLine("ai_will_do = {", 2);
				$fortree .= $this->addLine("factor = ".$focus->focus_ai_will_do_factor, 3);
				$fortree .= $this->addLine('}', 2);
			}
			if(!empty($focus->focus_bypass)){
				$fortree .= $this->addLine("bypass = {", 2);
				$fortree .= $this->addLine($focus->focus_bypass, 3);
				$fortree .= $this->addLine('}', 2);
			}
			if(!empty($focus->focus_available)){
				$fortree .= $this->addLine("available = {", 2);
				$fortree .= $this->addLine($focus->focus_available, 3);
				$fortree .= $this->addLine('}', 2);
			}
			if(!empty($focus->focus_complete_tooltip)){
				$fortree .= $this->addLine("complete_tooltip = {", 2);
				$fortree .= $this->addLine($focus->focus_complete_tooltip, 3);
				$fortree .= $this->addLine('}', 2);
			}
			if(!empty($focus->focus_reward)){
				$fortree .= $this->addLine("completion_reward = {", 2);
				$fortree .= $this->addLine($focus->focus_reward, 3);
				$fortree .= $this->addLine('}', 2);
			}
			$fortree .= $this->addLine('}', 1);
			if(!$export){
				$fortree .= '</div>';
			}

			$countDiff = (substr_count($fortree, '{') - (substr_count($fortree, '}')));
			if($countDiff === 1){
				if(!$export){
					$fortree .= '<span class="focus-error-end">';
				}
				$fortree .= "} #This may not be in the right place. If your focus tree is having issues, check the above focus, as this closing brace was added to close the focus.\r\n";
				if(!$export){
					$fortree .= '</span>';
				}
			}
			if($countDiff > 1 && !$export){
				$fortree .= '<span class="focus-error" data-id="'.$focus->id.'"><[ Error in focus. '.substr_count($fortree, '{').' of { compared to '.(substr_count($fortree, '}') - 1).' of } ]></span>';
				$fortree .='<span class="focus-error-end">---END ERROR---</span>';
			}
			$focusTree .= $fortree."\r\n";
			if(!$export){
				$focusTree .= '</div>';
			}
			
		}
		$focusTree .= "#End of focuses \r\n }";

		//Save files
		$treeFile = $directory.'/common/national_focus/'.$tree_file;
		$ln = $directory.'/localisation/'.$language_file;

		if(!$export){
			return $focusTree;
		}

		if (file_put_contents($ln, $lang) !== false) {
			//echo "File created (" . basename($ln) . ")";
		} else {
			$this->errors[] = "Cannot create file (" . basename($ln) . ")";
		}

		if (file_put_contents($treeFile, $focusTree) !== false) {
			// "File created (" . basename($treeFile) . ")";
		} else {
			$this->errors[] = "Cannot create file (" . basename($treeFile) . ")";
		}

		if(!empty($customGfx)){
			$customGfx = "spriteTypes = { \r\n".$customGfx." \r\n }";
			$customgfxfile = $directory.'/interface/'.$tag.'_customicons.gfx';
			if (file_put_contents($customgfxfile, $customGfx) !== false) {
				// "File created (" . basename($customgfxfile) . ")";
			} else {
				$this->errors[] = "Cannot create file (" . basename($customgfxfile) . ")";
			}
		}
		if(substr_count($focusTree, '{') !== substr_count($focusTree, '}')){
			$this->errors[] = "Unmatched brace count in focus tree for ".$tree->tree_id.' ('.$tree->country_tag."). ".substr_count($focusTree, '{')." '{' found, and ".substr_count($focusTree, '}')." '}' found.";
		}
   
	}

}

//$delete_file_after_download= true; doesnt work!!
class FlxZipArchive extends ZipArchive{
	/** Add a Dir with Files and Subdirs to the archive;;;;; @param string $location Real Location;;;;  @param string $name Name in Archive;;; @author Nicolas Heimann;;;; @access private  **/
	public function addDir($location, $name){
		$this->addEmptyDir($name);
		$this->addDirDo($location, $name);
	 } // EO addDir;
	/**  Add Files & Dirs to archive;;;; @param string $location Real Location;  @param string $name Name in Archive;;;;;; @author Nicolas Heimann
	 * @access private   **/
	private function addDirDo($location, $name){
		$name .= '/';
		$location .= '/';
		// Read all Files in Dir
		$dir = opendir ($location);
		while ($file = readdir($dir))
		{
			if ($file == '.' || $file == '..') continue;
			// Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
			$do = (filetype( $location . $file) == 'dir') ? 'addDir' : 'addFile';
			$this->$do($location . $file, $name . $file);
		}
	} // EO addDirDo();
}