<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\User;
use App\Mod;
use App\Events;
use App\EventOutcomes;
use Storage;

class EventsController extends Controller
{
    public function api($modid){
    	$events = Events::where('mod_id', '=', $modid)->get();
    	return $events;
    }

    public function list(){

    	if(Auth::user()){
			$myMods = Auth::user()->mods;
		}else{
			$myMods = null;
		}
		return view('events/list', ['myMods' => $myMods]);
    }

    public function view($modid)
	{
		$events = Events::where('mod_id', '=', $modid)->get();
		$mod = Mod::findOrFail($modid);
		$can_edit = false;
		if(Auth::user()){
			foreach($mod->users as $user){
				if($user->id == Auth::user()->id){
					$can_edit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$can_edit = true;
			}
		}
		return view('events/view', ['events' => $events, 'mod' => $mod, 'can_edit' => $can_edit, 'is_editable' => false]);
	}

	public function edit($modid){
		if(Auth::user()){
			$events = Events::with('outcomes')->where('mod_id', '=', $modid)->get();
			
			$mod = Mod::findOrFail($modid);
			$can_edit = $this->canUserEdit($modid);
			if($can_edit){
				return view('events/edit', ['events' => $events, 'mod' => $mod, 'can_edit' => $can_edit, 'is_editable' => false]);
			}else{
				die("You can't edit this");
			}
		}else{
			return redirect('/login');
		}
	}

	public function canUserEdit($modid){
		if(Auth::user()){
			$mod = Mod::findOrFail($modid);
			$users = $mod->users;
			$canEdit = false;
			foreach($users as $user){
				if(Auth::user()->id == $user->id){
					$canEdit = true;
				}
			}
			if(Auth::user()->role_id == 1){
				$canEdit = true;
			}
			if($canEdit){
				return true;
			}else{
				return false;
			}
		}else{
			return redirect('/login');
		}
	}

	public function showCreate(){
		if(Auth::user()){
			$id = Auth::user()->id;
			$mods = User::findOrFail($id)->mods;
		}else{
			$mods = [];
		}
		return view('events/create', ['mods' => $mods, 'can_edit' => true, 'is_editable' => false]);
	}

	public function createEvents(Request $request){
		if(Input::get('mod_id') == 'createNewMod'){
			//Create the mod
			$mod_logo_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('mod_name'))));
			if(!empty($request->file('mod_logo'))){
				$logo = $request->file('mod_logo');
				if ($request->file('mod_logo')->isValid()) {
					Storage::putFileAs('public/mods/', $request->file('mod_logo'), $mod_logo_name.'.png');
					$file = '/mods/'.$mod_logo_name.'.png';
					//Storage::write('public'.$file, $request->file('customgfx'));
					$path = '/media/get'.$file;
				}
			}else{
				$mod_logo_name = 'default';
			}
			//$uploadedLogo = $logo->move(public_path('/storage/mods'))->getRealPath();
			$modid = \DB::table('mods')->insertGetId(
				[
					'mod_name' => Input::get('mod_name'), 
					'mod_description' => !empty(Input::get('mod_description')) ? Input::get('mod_description') : '',
					'mod_logo' => 'mods/'.$mod_logo_name.'.png',
					'created_at' => date('Y-m-d G:i:s'),
					'updated_at' => date('Y-m-d G:i:s')
				]
			);
			\DB::table('user_mods')->insert([
				'user_id' => Auth::user()->id,
				'mod_id' => $modid,
				'role' => 'Creator',
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]);
		}else{
			$modid = Input::get('mod_id');
		}
		//Create the events language
		$id = \DB::table('events_languages')->insertGetId(
			[
				'mod_id' => $modid,
				'language' => Input::get('tree_lang'),
				'created_at' => date('Y-m-d G:i:s'),
				'updated_at' => date('Y-m-d G:i:s')
			]
		);
		$mod = Mod::find($modid);
		$mod->has_event = 1;
		$mod->save();
		return redirect('/events/edit/'.$modid);
	}

	public function addEvent(Request $request, $modid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($modid);
			if($canEdit){
				$errors = array();
				if($request->input('chosen_gfx') == 'custom'){
					//Upload file, and add it
					$icon_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('event_title'))));
					if ($request->file('customgfx')->isValid()) {
						Storage::putFileAs('public/events/'.$modid, $request->file('customgfx'), $icon_name.'.png');
						$file = '/mods/'.$modid.'/'.$icon_name.'.png';
						$path = '/media/get'.$file;
					}else{
						$path = ''; //Link to default image
						$errors['File_Upload'] = 'The file you tried to upload was not valid';
					}
				}else{
					//Do default
					$path = $request->input('chosen_gfx');
				}
				$id = \DB::table('events')->insertGetId(
					[
						'mod_id' => $modid,
						'event_type' => $request->input('event_type'), 
						'event_title' => $request->input('event_title'),
						'event_description' => $request->input('event_description'), 
						'event_gfx' => $path, 
						'event_is_triggered_only' => $request->input('event_is_triggered_only'), 
						'event_fire_only_once' => $request->input('event_fire_only_once'),
						'event_major' => $request->input('event_major'),
						'event_trigger' => $request->input('event_trigger'),
						'event_mean_time_to_happen' => $request->input('event_mean_time_to_happen'),
						'event_immediate' => $request->input('event_immediate'),
						'created_at' => date('Y-m-d G:i:s'),
						'updated_at' => date('Y-m-d G:i:s')
					]
				);
				if($errors){
					die('<script>parent.EVENTuploadEvent("error","'.json_encode($errors).'", "");</script>');
				}else{
					die('<script>parent.GENERICreload();</script>');
				}
			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function editEvent(Request $request, $modid, $eventid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($modid);
			if($canEdit){
				if($request->input('chosen_gfx') == 'custom'){
					//Upload file, and add it
					$icon_name = date('Ymdgis').'--'.str_replace(' ', '_', strtolower(preg_replace("/[^a-zA-Z_0-9 ]/", '',Input::get('event_title'))));
					if ($request->file('customgfx')->isValid()) {
						Storage::putFileAs('public/events/'.$modid, $request->file('customgfx'), $icon_name.'.png');
						$file = '/mods/'.$modid.'/'.$icon_name.'.png';
						$path = '/media/get'.$file;
					}else{
						$path = ''; //Link to default image
						$errors['File_Upload'] = 'The file you tried to upload was not valid';
					}
				}else{
					//Do default
					$path = $request->input('chosen_gfx');
				}

				$event = Events::find($eventid);
				$event->event_type = $request->input('event_type');
				$event->event_title = $request->input('event_title');
				$event->event_description = $request->input('event_description');
				$event->event_gfx = $path;
				$event->event_is_triggered_only = $request->input('event_is_triggered_only'); 
				$event->event_fire_only_once = $request->input('event_fire_only_once');
				$event->event_major = $request->input('event_major');
				$event->event_trigger = $request->input('event_trigger');
				$event->event_mean_time_to_happen = $request->input('event_mean_time_to_happen');
				$event->event_immediate = $request->input('event_immediate');
				$event->updated_at = date('Y-m-d G:i:s');
				$event->save();
				die('<script>parent.GENERICreload();</script>');
			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}


	public function addOutcome(Request $request, $modid, $eventid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($modid);
			if($canEdit){

				$id = \DB::table('event_outcomes')->insertGetId(
					[
						'event_id' => $eventid,
						'outcome_name' => $request->input('outcome_name'), 
						'outcome_ai_chance' => $request->input('outcome_ai_chance'),
						'outcome_trigger' => $request->input('outcome_trigger'), 
						'outcome_reward' => $request->input('outcome_reward'), 
						'created_at' => date('Y-m-d G:i:s'),
						'updated_at' => date('Y-m-d G:i:s')
					]
				);
					
				die('<script>parent.EVENTuploadOutcome("success", '.$eventid.', '.$id.');</script>');
			}else{
				die('<script>parent.NFunauthorisedUser();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function editOutcome(Request $request, $modid, $outcomeid){
		if(Auth::user()){
			$canEdit = $this->canUserEdit($modid);
			if($canEdit){
				$outcome = EventOutcomes::find($outcomeid);
				$outcome->outcome_name = $request->input('outcome_name');
				$outcome->outcome_ai_chance = $request->input('outcome_ai_chance');
				$outcome->outcome_reward = $request->input('outcome_reward');
				$outcome->updated_at = date('Y-m-d G:i:s');
				$outcome->save();
				die('<script>parent.GENERICreload();</script>');
			}else{
				die('<script>parent.GENERICreload();</script>');
			}
		}else{
			die('<script>parent.NFsessionExpired();</script>');
		}
	}

	public function deleteEventOutcome($eventid, $outcomeid){
		$event = Events::find($eventid);
		$canEdit = $this->canUserEdit($event->mod_id);
		if($canEdit){
			$id = EventOutcomes::find($outcomeid);
			if(!empty($id)){
				$id->delete();
				die('<script>parent.GENERICreload();</script>');
			}else{
				die('<script>parent.GENERICdeleteNotFound();</script>');
			}		
		}else{
			die('<script>parent.NFunauthorisedUser();</script>');
		}
	}

	public function deleteEvent($eventid){
		$event = Events::find($eventid);
		$canEdit = $this->canUserEdit($event->mod_id);
		if($canEdit){
			$outcomes = EventOutcomes::where('event_id', '=', $eventid)->get();
			if(!empty($outcomes)){
				foreach($outcomes as $outcome){
					$outcome->delete();
				}
				$event->delete();
			}
			die('<script>parent.GENERICreload();</script>');		
		}else{
			die('<script>parent.NFunauthorisedUser();</script>');
		}
	}
}
