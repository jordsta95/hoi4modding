<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Forum;
use App\ForumCategories;
use App\ForumPosts;
use App\ForumComments;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;

class ForumCommentsController extends Controller
{
    public function commentPost(Request $request, $slug, $id, $post_slug){
    	if(Auth::user()){
    		$userId = Auth::user()->id;

    		$commentId = ForumComments::insertGetId(
				[
					'content' => $request->wysiwyg,
					'user_id' => $userId,
					'forum_post_id' => $id,
					'created_at' => date('Y-m-d G:i:s'),
					'updated_at' => date('Y-m-d G:i:s')
				]
			);

    		$user = User::find($userId);
    		$user->post_count = ($user->post_count + 1);
    		$user->last_active = date('Y-m-d G:i:s');
    		$user->save();

    		$post = ForumPosts::find($id);
    		$post->updated_at = date('Y-m-d G:i:s');
    		$post->save();

    		$forum = Forum::where('slug','=',$slug)->first();
    		$forum->updated_at = date('Y-m-d G:i:s');
    		$forum->save();

    		$commentCheck = ForumComments::where('forum_post_id','=',$id)->paginate(10);
    		if($commentCheck->lastPage() >= 2){
    			$urlAppend = '?page='.$commentCheck->lastPage().'#'.$commentId;
    		}else{
    			$urlAppend = '#'.$commentId;
    		}
			return redirect('/forum/'.$slug.'/'.$id.'-'.$post_slug.$urlAppend);
    	}else{
    		return redirect('/forum/'.$slug.'/'.$id.'-'.$post_slug);
    	}
    }
}
