<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
* Contacts
*/
use App\Http\Contracts\Media;
use App\Http\Contracts\Admin\MediaManager;

class MediaController extends Controller
{
    protected $mediaContract;
    protected $mediaManager;

    public function __construct(){
        $this->mediaContract = new Media();
        $this->mediaManager = new MediaManager();
    }

    public function upload(Request $request){
        return method_exists($this->mediaContract, 'upload') ? $this->mediaContract->upload($request) : null;
    }

    public function find($id){
        return method_exists($this->mediaContract, 'find') ? $this->mediaContract->find($id) : null;
    }

    public function findSized($id, $size){
        return method_exists($this->mediaContract, 'find') ? $this->mediaContract->find($id, $size.'_') : null;
    }

    public function findByName($path, $filename, $extension){
        return method_exists($this->mediaContract, 'findByName') ? $this->mediaContract->findByName($path, $filename, $extension) : null;
    }

    public function renderMediaManager(){
        return method_exists($this->mediaManager, 'renderMediaManager') ? $this->mediaManager->renderMediaManager() : null;
    }

    public function renderMediaManagerPath($path){
        return method_exists($this->mediaManager, 'renderMediaManagerPath') ? $this->mediaManager->renderMediaManagerPath($path) : null;
    }

    public function deleteImage($id){
        return method_exists($this->mediaContract, 'deleteImage') ? $this->mediaContract->deleteImage($id) : null;
    }

    public function mediaManagerUpload(Request $request){
        return method_exists($this->mediaContract, 'mediaManagerUpload') ? $this->mediaContract->mediaManagerUpload($request) : null;
    }

    public function get($path){
        return method_exists($this->mediaContract, 'get') ? $this->mediaContract->get($path) : null;
    }

}
