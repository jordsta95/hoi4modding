<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Forum;
use App\ForumCategories;
use App\ForumPosts;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;

class ForumController extends Controller
{
    //
    public function index(){
    	if(Auth::user()){
    		if(Auth::user()->role_id == 1){
    			return view('forum/index', ['categories' => ForumCategories::orderBy('order', 'ASC')->get()]);
    		}else{
    			return view('forum/index', ['categories' => ForumCategories::orderBy('order', 'ASC')->where('public','=','1')->get()]);
    		}
    	}else{
    		return view('forum/index', ['categories' => ForumCategories::orderBy('order', 'ASC')->where('public','=','1')->get()]);
    	}
    }

    public function forum($slug){
    	if(Auth::user()){
    		return view('forum/forum', ['forum' => Forum::where('slug','=',$slug)->first()]);
    	}else{
    		$forum = Forum::where('slug','=',$slug)->where('public','=',1)->first();
    		if(!empty($forum)){
    			return view('forum/forum', ['forum' => $forum]);
    		}else{
    			return redirect('/forum');
    		}
    	}
    }

    public function forumPost($slug, $id, $post_slug){
    	$post = ForumPosts::findOrFail($id);

    	return view('forum/forumposts', [
    		'parent' => Forum::where('slug','=',$slug)->first(),
    		'post' => $post,
    		'comments' => $post->comments()->where('public','=',1)->paginate(10)
    	]);
    }
}
