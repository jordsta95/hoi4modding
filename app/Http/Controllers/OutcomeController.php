<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Outcome;

class OutcomeController extends Controller
{
    public function search(Request $request){
    	$val = $request->value;
    	$outcomes = Outcome::where('game_id', 'LIKE', '%'.$val.'%')->orWhere('description', 'LIKE', '%'.$val.'%')->get();
    	return $outcomes;
    }
}
