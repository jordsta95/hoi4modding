<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryDate extends Model
{

    public function ideologies(){
    	return $this->hasMany(CountryDateIdeology::class);
    }

    public function states(){
    	return $this->hasMany(CountryDateState::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function forIdeology($ideology){
    	return $this->hasOne(CountryDateIdeology::class)->where('country_date_ideologies.id', '=', $ideology);
    }

    public function getParsedResearchAttribute(){
    	return explode(',',$this->research);
    }

}
