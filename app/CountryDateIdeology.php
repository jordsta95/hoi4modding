<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryDateIdeology extends Model
{
    public function date(){
    	return $this->belongsTo(CountryDate::class, 'country_date_id');
    }
}
