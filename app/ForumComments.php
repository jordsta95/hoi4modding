<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumComments extends Model
{
    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function post(){
    	return $this->belongsTo(ForumPosts::class, 'forum_post_id');
    }

    public function likes(){
    	return $this->hasMany(ForumCommentLikes::class, 'forum_comment_id');
    }
}
