<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMod extends Model
{
    //
    public function mods(){
    	return $this->belongsToMany(Mod::class,'mod_id');
    }
}
