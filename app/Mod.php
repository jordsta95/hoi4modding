<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mod extends Model
{
    //
    public function users(){
    	return $this->belongsToMany(User::class, 'user_mods', 'mod_id', 'user_id')->withPivot('role');
    }

    public function focusTrees(){
    	return $this->hasMany(FocusTree::class);
    }

    public function events(){
    	return $this->hasMany(Events::class);
    }

    public function countries(){
        return $this->hasMany(Country::class);
    }

    public function eventLanguage(){
        return $this->hasOne(EventsLanguage::class);
    }

    public function ideas(){
        return $this->hasMany(Idea::class);
    }

}
