<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Focuses extends Model
{
	//Fix stupid url issues
    public function getFocusGfxAttribute($value)
    {

        return str_replace('/home/hoimoddi/beta.hoi4modding.com', '', $value);
    }
}
