<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FocusTree extends Model
{
    public function focuses(){
    	return $this->hasMany(Focuses::class);
    }

    public function mod(){
    	return $this->belongsTo(Mod::class);
    }

}
