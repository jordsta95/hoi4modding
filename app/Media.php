<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'filename', 'path', 'extension', 'size', 'user_id', 'created_at', 'updated_at'
    ];

    /**
    * Make a path attribute which can be used for media/<path> route & media manager
    *
    */
    public function getUrlFriendlyPathAttribute(){
    	return str_replace('/', '-', ltrim($this->path, '/'));
    }

    /**
    * media->url to get a nice-name url instead of /media/find/id
    *
    */
    public function getUrlAttribute(){
        $path = str_replace('/', '-', ltrim($this->path, '/'));
        return '/media/'.(!empty($path) ? $path : 'root').'/'.$this->filename.'.'.$this->extension;
    }
}
