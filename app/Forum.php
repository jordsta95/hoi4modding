<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    public function posts(){
    	return $this->hasMany(ForumPosts::class);
    }

    public function comments(){
    	return $this->hasManyThrough(ForumComments::class, ForumPosts::class, 'forum_id', 'forum_post_id');
    }
}
