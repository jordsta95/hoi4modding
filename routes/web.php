<?php
use App\CountryTags;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/tags.json', function(){
	return CountryTags::get();
});

Route::get('importTest', 'ModController@test');

//User Related
Route::get('/users', 'UserController@list');
Route::get('/users/{id}', 'UserController@view');
Route::get('/account', 'UserController@account');
Route::post('/account', 'UserController@updateAccount');
Route::get('/account/media', 'UserController@media');
Route::post('/account/media', 'UserController@mediaAction');
Route::get('/notifications', 'UserController@notifications');

//Mod Related
Route::get('/mod', 'ModController@list');
Route::get('/mod/{id}', 'ModController@view');
Route::post('/mod/{id}', 'ModController@editMod');
Route::get('/mod/{id}/export', 'ModController@export');
Route::post('/mod/{id}/delete', 'ModController@delete');

//Misc Tool Related
Route::get('/flag/', 'FlagController@viewCreate');
Route::post('/flag/create', ['as' => 'flag.create', 'uses' => 'FlagController@create']);
Route::post('/flag/download/{size}', ['as' => 'flag.download', 'uses' => 'FlagController@download']);

//Builder Related
Route::post('/builder', ['as' => 'builder.search', 'uses' => 'OutcomeController@search']);

//Focus Tree Related
Route::get('/focus-tree','FocusTreeController@list');
Route::get('/focus-tree/view/{id}','FocusTreeController@view');
Route::get('/focus-tree/delete/{id}','FocusTreeController@delete');
Route::get('/focus-tree/edit/{id}',['as' => 'tree.edit', 'uses' => 'FocusTreeController@edit']);
Route::get('/focus-tree/create','FocusTreeController@showCreate');
Route::post('/focus-tree/create','FocusTreeController@createFocusTree');
Route::post('/focus-tree/edit/{id}/add','FocusTreeController@createNewFocus');
Route::post('/focus-tree/edit/{id}/edit/{focus}','FocusTreeController@editFocus');
Route::post('/focus-tree/quickedit/{id}/edit/{focus}','FocusTreeController@quickEditFocus');
Route::post('/focus-tree/edit/{id}/delete/{focusid}','FocusTreeController@deleteFocus');
Route::post('/focus-tree/edit/{id}/importFromOldTool','FocusTreeController@importFromOldTool');
Route::post('/focus-tree/edit/{id}/importFromExistingFile','FocusTreeController@importFromExistingFile');
Route::post('/focus-tree/edit/{id}/importViaJSON','FocusTreeController@importViaJSON');
Route::post('/focus-tree/edit/{id}/globalMove','FocusTreeController@globalMove');
Route::post('/focus-tree/update/{id}','FocusTreeController@update');
Route::post('/focus-tree/edit/{id}/multiMove','FocusTreeController@multiMove');


//Event Related
Route::get('/events','EventsController@list');
Route::get('/events/view/{modid}','EventsController@view');
Route::get('/events/edit/{modid}','EventsController@edit');
Route::post('/events/edit/{modid}/add','EventsController@addEvent');
Route::post('/events/edit/{modid}/edit/{eventid}','EventsController@editEvent');
Route::get('/events/create','EventsController@showCreate');
Route::post('/events/create','EventsController@createEvents');
Route::post('/events/quickedit/{eventid}','EventsController@quickEditEvent');
Route::post('/events/quickedit/{eventid}/{outcomeid}','EventsController@quickEditEventOutcome');
Route::get('/events/delete/{eventid}/','EventsController@deleteEvent');
Route::get('/events/delete/{eventid}/{outcomeid}','EventsController@deleteEventOutcome');
Route::post('/events/edit/{modid}/outcome/add/{eventid}','EventsController@addOutcome');
Route::post('/events/edit/{modid}/outcome/edit/{outcomeid}','EventsController@editOutcome');

//Country Related
Route::group(['prefix' => 'country'], function () {
	Route::get('/', 'CountryController@list');
	Route::get('/view/{id}', 'CountryController@view');
	Route::get('/delete/{id}', 'CountryController@delete');
	Route::get('/edit/{id}', 'CountryController@edit');
	Route::get('/create', 'CountryController@showCreate');

	Route::post('/create', 'CountryController@create');
	Route::post('/edit/{id}', 'CountryController@update');
	Route::post('/edit/{id}/add-idea', 'CountryController@addIdea');
	Route::get('/edit/{id}/get-ideas', 'CountryController@getIdeas');
	Route::post('/get-leader', 'CountryController@getLeader');
	Route::post('/get-tabs', 'CountryController@getTabs');
	Route::post('/get-states', 'CountryController@getStates');
	Route::post('/delete-idea', 'CountryController@deleteIdea');
});

if(isBeta()){
	Route::group(['prefix' => 'ideas'], function () {
		Route::get('/', 'IdeaController@list');
		Route::get('/view/{id}', 'IdeaController@view');
		Route::get('/delete/{id}', 'IdeaController@delete');
		Route::get('/edit/{id}', 'IdeaController@edit');
		Route::get('/create', 'IdeaController@showCreate');

		Route::post('/create', 'IdeaController@create');
		Route::post('/edit/{id}/new/', 'IdeaController@addIdea');
		Route::post('/edit/{id}/import', 'IdeaController@import');
		Route::post('/edit/{id}/{ideaid}', 'IdeaController@editIdea');
		Route::post('/edit/{id}/{ideaid}/delete', 'IdeaController@deleteIdea');
	});
}

//Forum Related
Route::get('/forum', 'ForumController@index');
Route::get('/forum/{slug}', 'ForumController@forum');
Route::get('/forum/{slug}/{id}-{post_slug}', 'ForumController@forumPost');
Route::get('/forum/{slug}/{id}-{post_slug}/{action}/', 'ForumPostsController@forumPostAction');
Route::post('/forum/{slug}/{id}-{post_slug}/{action}/p/{action_id?}', 'ForumPostsController@forumPostAction');
Route::get('/forum/{slug}/{id}-{post_slug}/{action}/g/{action_id?}', 'ForumPostsController@forumPostAction');

Route::post('/forum/{slug}/{id}-{post_slug}', 'ForumCommentsController@commentPost');

Route::get('/forum/{slug}/create', 'ForumPostsController@create');
Route::post('/forum/{slug}/create', 'ForumPostsController@createPost');

//Api Stuff
Route::get('/api/v1/users', 'UserController@api');
Route::get('/api/v1/mod/add_role/{modid}/{userid}/{role}', 'ModController@apiAddRole');
Route::get('/api/v1/mod/remove_role/{modid}/{userid}', 'ModController@apiRemoveRole');
Route::get('/api/v1/events/{modid}', 'EventsController@api');


//Media
Route::get('media/get/{slashData?}', 'MediaController@get')->where('slashData', '(.*)');

/* ======
Images 
====== */

Route::group(['prefix' => 'image'], function(){
    // Route::get('/manager', ['as' => 'mediamanager.render', 'uses' => 'MediaController@renderMediaManager']);
    // Route::get('/manager/{path}', ['as' => 'mediamanager.renderPath', 'uses' => 'MediaController@renderMediaManagerPath']);
    Route::post('/delete/{id}', ['as' => 'media.delete', 'uses' => 'MediaController@deleteImage']);

    Route::post('/upload', ['as' => 'media.upload', 'uses' => 'MediaController@upload']);
    // Route::post('/upload-media-manager', ['as' => 'media.upload-media-manager', 'uses' => 'MediaController@mediaManagerUpload']);
    Route::get('/find/{id}', ['as' => 'media.find', 'uses' => 'MediaController@find']);
    Route::get('/find/{id}/{size}', ['as' => 'media.findSized', 'uses' => 'MediaController@findSized']);
    Route::get('/{path}/{filename}.{extension}', ['as' => 'media.findByName', 'uses' => 'MediaController@findByName']);
});

//Auth Related
Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

//Voyager Related
// Route::group(['prefix' => 'admin'], function () {
//     Voyager::routes();
// });
