var gulp = require ( 'gulp');
var sass = require ('gulp-sass');
var concat = require('gulp-concat');
var wait = require('gulp-wait');

gulp.task('styles', function () {
    gulp.src('resources/assets/scss/**/master.scss')
    .pipe(wait(500))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('css/'));
});

gulp.task('scripts', function() {
  gulp.src('resources/assets/js/files/*.js')
    .pipe(concat('master.js'))
    .pipe(gulp.dest('js'))
});

//Watch task
gulp.task('default',function() {
    gulp.watch('resources/assets/scss/**/*.scss',['styles']);
    gulp.watch('resources/assets/js/files/*.js',['scripts']);
});