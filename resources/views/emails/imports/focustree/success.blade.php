@extends('emails.template')

@section('title')
	<h1 style="margin: 0;line-height: 1;">Import result for {{ $tree->tree_id }}</h1>
@endsection

@section('content')
	<h3 style="text-align: center;">Import Success</h3>
	<p>Hi {{ $user->username }},<br>
	Your import, scheduled at {{ date("h:ia", strtotime($row->created_at)) }} on {{ date("d/M", strtotime($row->created_at)) }}, has been imported successfully.
	</p>
	<p>If you still have your focus tree open you will need to refresh the page.</p>
@endsection