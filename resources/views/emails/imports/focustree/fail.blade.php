@extends('emails.template')

@section('title')
	<h1 style="margin: 0;line-height: 1;">Import result for {{ $tree->tree_id }}</h1>
@endsection

@section('content')
	<h3 style="text-align: center;">Import failed</h3>
	<p>Hi {{ $user->username }},<br>
	Unfortunately, the import you attempted at {{ date("h:ia", strtotime($row->created_at)) }} on {{ date("d/M", strtotime($row->created_at)) }} has failed to import.
	</p>
	<p>This is because the site was unable to read your focus tree file. This may have happened because the focus tree file wasn't a valid file, or there was an error in the focus tree's syntax.</p>
	<p>If you are still getting this error after multiple import attempts, please use a service such as <a href="https://pastebin.com/">Pastebin</a> to upload your focus tree, and file a <a href="https://hoi4modding.com/forum/bug-reports">bug report</a> linking the focus tree in your report.</p>
@endsection