@extends('emails.template')

@section('title')
	<h1 style="margin: 0;line-height: 1;">Import result for {{ $tree->tree_id }}</h1>
@endsection

@section('content')
	<h3 style="text-align: center;">Import failed but shouldn't have</h3>
	<p>
	Import from {{ date("h:ia", strtotime($row->created_at)) }} on {{ date("d/M", strtotime($row->created_at)) }} has failed to import.
	</p>
	<p>Keep an eye out for bug reports from {{ $user->username }}</p>
@endsection