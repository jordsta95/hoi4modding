<p>{{$user->username}} just created a post on the HOI4 Modding Forums</p>
<p><a href="{{env('APP_URL').$slug}}" style="display: inline-block; padding: 10px 20px; border: 1px solid black;">See post here</a></p>
<p>Post content:</p>
{!! $post->content !!}