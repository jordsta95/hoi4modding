<table align="center" border="0" cellpadding="0" cellspacing="0" style="max-width: 640px; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',sans-serif">
	<tr>
		<td style="background: #1b93e1; padding: 20px; color: #ffffff; text-align: center;">
			@yield('title')
		</td>
	</tr>
	<tr>
		<td style="padding: 40px 20px;">
			<p style="font-size: 12px; text-align: center;">All times shown are in UTC</p>
			@yield('content')
		</td>
	</tr>
	<tr>
		<td style="background: #333333; font-size: 14px; text-align: center; color: #FFFFFF; padding: 10px 20px;">
			This email was generated on {{ date('d/M/Y') }} at {{ date("h:ia") }}, by HOI4modding.com.
		</td>
	</tr>
</table>