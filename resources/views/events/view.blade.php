@extends('templates.tool')

@section('title')
{{$mod->mod_name}}'s Events - HOI4 Modding
@endsection


@section('description')
{{$mod->mod_description}}
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@foreach($events as $event)
		<div class="event-{{$event->event_type}}">
			<div class="event-gfx">
				<img src="{{ storage( $event->event_gfx, 'public/storage') }}">
			</div>
			<div class="event-content">
				<div class="event-title">
					{{$event->event_title}}
				</div>
				<div class="event-description">
					{{$event->event_description}}
				</div>
				<div class="event-outcomes">
					@foreach($event->outcomes as $outcome)
						<div class="event-outcome">
							{{$outcome->outcome_name}}
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endforeach
	@if($can_edit)
		<a href="/events/edit/{{$mod->id}}" class="focus-user-edit">Edit This Mods' Events</a>
	@endif
@endsection

