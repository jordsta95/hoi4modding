@extends('templates.tool-focustree')

@section('title')
Create events - HOI4 Modding
@endsection


@section('description')
Create events for HOI IV
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@if(Auth::user())
		<div class="add_user_popup active create-focus" id="addUser">
			<div class="add_user_box stepped" data-step="1" data-last="3" data-is-last="0">
				<form method="POST" action="" enctype="multipart/form-data">
					<h3>Create New Events</h3>
					<div class="step step_1">
						{{ csrf_field() }}
						<h3>Select Mod Or Create A New One</h3>
						<p>
							<select name="mod_id" class="chooseModForEvent">
								<option value="createNewMod" data-has-events-language="false">Create New Mod</option>
								@if(!empty($mods))
									@foreach($mods as $mod)
										<?php
											$hasEventLangSet = 'false';
											if(!empty($mod->eventLanguage)){
												$hasEventLangSet = 'true';
											}
										?>
										<option value="{{$mod->id}}" data-has-events-language="{{$hasEventLangSet}}">{{$mod->mod_name}}</option>
									@endforeach
								@endif
							</select>
						</p>
					</div>
					<div class="step step_2 toggleNewModStep" data-new-mod-class="step step_2 toggleNewModStep" data-existing-mod-class="hidden_step toggleNewModStep">
						<div class="new-mod">
							<h3>New Mod Details</h3>
							<p><label>Mod Name</label></p>
							<p><input name="mod_name"></p>
							<p><label>Mod Description</label></p>
							<p><textarea name="mod_description"></textarea></p>
							<p><label>Mod Logo</label></p>
							<p><input name="mod_logo" type="file" accept="image/x-png"></p>
						</div>
						
					</div>
					<div class="step step_3 toggleNewModStep" data-new-mod-class="step step_3 toggleNewModStep" data-existing-mod-class="step step_2 toggleNewModStep">
						<div class="new-focus-tree">
							<p><label>Events Language</label></p>
							<select name="tree_lang">
								<option value="english">English</option>
								<option value="french">French</option>
								<option value="german">German</option>
								<option value="polish">Polish</option>
								<option value="braz_por">Portuguese</option>
								<option value="russian">Russian</option>
								<option value="spanish">Spanish</option>
							</select>
							<p><button class="generic_button">Submit</button></p>
						</div>
					</div>
					<div class="goToEvents" data-url="/events/edit/" style="display: none;">
						<a href="#" class="generic_button">Edit Events For This Mod</a>
					</div>
					<div class="stepper">
						<div class="prev">Back</div>
						<div class="next">Next</div>
					</div>
				</form>
			</div>
		</div>
	@else
		<div class="focus-panel create-focus">
			<div class="panel-head">
				Create Your Focus Tree
			</div>
			<div class="panel-body">
				<p>Logging in allows for automatic saving to the server, keeping multiple mods organised with ease, use of other elements of your mod (e.g. events), and much more.</p>
				<p>If you don't have an account, you can create one using the link below</p>
				<a href="/register" class="btn">Register</a>
				<p>Or Log In</p>
				 <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Login
                            </button>

                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                        </div>
                    </div>
                </form>
			</div>
		</div>
			@endif
@endsection

