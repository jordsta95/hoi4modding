@extends('templates.tool-events')

@section('title')
{{$mod->mod_name}}'s Events - HOI4 Modding
@endsection


@section('description')
{{$mod->mod_description}}
@endsection

@section('menu')
	@include('menus/tool-events')
@endsection

@section('content')
<?php $eventNum = 1; $eventid = strtolower(preg_replace("/[^A-Za-z]/", '', $mod->mod_name)); ?>
	@foreach($events as $event)
		<div class="event-{{$event->event_type}} event-wrapper" data-event-id="{{$event->id}}">
			<div class="event-edit event">
				Edit
			</div>
			<div class="event-delete event">
				X
			</div>
			
			<div class="event-gfx">
				<img src="{{ storage( $event->event_gfx, 'public/storage') }}">
			</div>
			<div class="event-content">
				<div class="event-title">{{$event->event_title}}</div>
				<div class="event-description">
					{{$event->event_description}}
				</div>
				<div class="event-json"><?php
						$outcomes = $event->outcomes;
						$data = $event;
						unset($data['id']);
						unset($data['mod_id']);
						unset($data['outcomes']);
						echo $data;
					?></div>
				<div class="event-outcomes">
					@foreach($outcomes as $outcome)
						<div class="event-outcome" data-outcome-id="{{$outcome->id}}">
							<div class="event-edit">
								Edit
							</div>
							<div class="event-delete">
								X
							</div> 
							<span>{{$outcome->outcome_name}}</span>
							<div class="outcome_json">
								<?php 
									unset($outcome['id']);
									unset($outcome['event_id']);
									unset($outcome['created_at']);
									unset($outcome['updated_at']);
									echo $outcome; 
								?>
							</div>
						</div>
					@endforeach
					<div class="event-outcome-add">
						Add New Option
					</div>
				</div>
				<p><strong>Event ID:</strong> <em>{{$eventid.'.'.$eventNum}}</em></p>
			</div>
		</div>
		<?php $eventNum++; ?>
	@endforeach
@endsection

