@extends('templates.tool')

@section('title')
Events List - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	


	<p style="text-align: right"><a href="/events/create" class="generic_button">Create Events</a></p>
	@if($myMods)
		<h3>My Mods With Events</h3>
		<div class="modlist">
			@foreach($myMods as $mod)
				<div class="mod">
					<div class="image">
						<img src="{{ storage($mod->mod_logo, '/public/storage/') }}">
					</div>
					<div class="info">
						<a href="/mod/{{$mod->id}}"><h3>{{$mod->mod_name}}</h3></a>
						<div class="focuses">
							<div class="focustree">
								<a href="/events/view/{{$mod->id}}">{{$mod->mod_name}} - {{count($mod->events)}} events</a>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif
@endsection

