<nav>
	<ul>
		<li for="focus-popup" class="open"><i class="fa fa-plus"></i><span> Add Focus</span></li>
		<li><i class="fa fa-trash"></i><span> Delete</span>
			<ul>
				<li><label><input id="delete-mode" type="checkbox"> Delete mode?</label></li>
				<li><button id="delete-all">Delete All</button></li>
			</ul>
		</li>
		<li for="focus-import-existing-tree" class="open"><i class="fa fa-download"></i><span> Import</span>
		</li>
		<li><i class="fa fa-arrows"></i> <span>Multi-move</span>
			<ul>
				<li for="focus-global-move" class="open">Global Move</li>
				<li><label>Enable multi-select move <input id="multiMove" type="checkbox"></label>
			</ul>
		</li>
		<a href="/mod/{{$tree->mod->id}}"><li><i class="fa fa-clipboard"></i> <span>View Mod</span></li></a>
		<a href="{{ bugUrl() }}"><li><i class="fa fa-bug"></i> <span>Report A Bug</span></li></a>
		<a href="/"><li><i class="fa fa-home"></i><span> Home</span></li></a>
		@if(Auth::user())	
			<a href="/account"><li><div class="avatar"><img src="{{ storage(Auth::user()->avatar, '/public/storage/') }}"></div><span>{{ Auth::user()->username }}</span></li></a>
			<a href="/account/media"><li>Manage Custom GFX</li></a>
		@else
			<li><i class="fa fa-user"></i><span> Account</span>
				<ul>
					<a href="/login"><li>Login</li></a>
					<a href="/register"><li>Register</li></a>
				</ul>
			</li>
		@endif
	</ul>
</nav>