<nav>
	<ul>
		<a href="/mod/{{$mod->id}}"><li><i class="fa fa-clipboard"></i> <span>View Mod</span></li></a>
		<a href="{{ bugUrl() }}"><li><i class="fa fa-bug"></i> <span>Report A Bug</span></li></a>
		<a href="/"><li><i class="fa fa-home"></i><span> Home</span></li></a>
		@if(Auth::user())	
			<a href="/account"><li><div class="avatar"><img src="{{ storage(Auth::user()->avatar, '/public/storage/') }}"></div><span>{{ Auth::user()->username }}</span></li></a>
			<a href="/account/media"><li>Manage Custom GFX</li></a>
		@else
			<li><i class="fa fa-user"></i><span> Account</span>
				<ul>
					<a href="/login"><li>Login</li></a>
					<a href="/register"><li>Register</li></a>
				</ul>
			</li>
		@endif
	</ul>
</nav>