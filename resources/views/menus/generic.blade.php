<nav>
	<ul>
		<a href="//hoi4modding.com/"><li><i class="fa fa-home"></i><span> Home</span></li></a>
		@if(Auth::user())
		<?php $posts = count(getUnreadForumPosts(Auth::user()->id, Auth::user()->last_active)); ?>
		@if($posts > 0)
			<li><div class="avatar"><img src="{{ storage(Auth::user()->avatar, '/public/storage') }}"></div><span>{{ Auth::user()->username }} <span class="notifications">{{$posts}}</span></span>
				<ul>
					<a href="/users/{{Auth::user()->id}}"><li>Account</li></a>
					<a href="/notifications"><li>Notifications</li></a>
					<a href="/account/media"><li>Manage Custom GFX</li></a>
					<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><li>Logout<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
					</form></li></a>
				</ul>
			</li>
		@else
			<li><div class="avatar"><img src="{{ storage(Auth::user()->avatar, '/public/storage') }}"></div><span>{{ Auth::user()->username }}</span>
				<ul>
					<a href="/users/{{Auth::user()->id}}"><li>Account</li></a>
					<a href="/account/media"><li>Manage Custom GFX</li></a>
					<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><li>Logout<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
					</form></li></a>
				</ul>
			</li>
			
		@endif
		@else
			<li><i class="fa fa-user"></i><span> Account</span>
				<ul>
					<a href="/login"><li>Login</li></a>
					<a href="/register"><li>Register</li></a>
				</ul>
			</li>
		@endif
		<a href="/forum"><li><i class="fa fa-comments"></i><span> Forum</span></li></a>
		<li><i class="fa fa-code"></i><span> Tools</span>
			<ul>
				<a href="/focus-tree/"><li>Focus Tree</li></a>
				<a href="/events/"><li>Event</li></a>
				<a href="/flag/"><li>Flag Creator</li></a>
				<a href="/country/"><li>Country Creator</li></a>
				@if(isBeta())
					<a href="/ideas"><li>Ideas</li></a>
				@endif
			</ul>
		</li>
		<a href="https://www.patreon.com/hoi4moddingtools"><li><span>Patreon</span></li></a>
	</ul>
</nav>