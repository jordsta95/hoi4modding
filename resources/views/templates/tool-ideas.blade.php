<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<!-- Bootstrap Core CSS -->
<!--<link href="css/bootstrap.min.css?v=1" rel='stylesheet' type='text/css' />-->
<!-- Custom CSS -->
<link href="/css/master.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="/css/font-awesome.css?v=1" rel="stylesheet"> 
<!-- jQuery -->
<script src="/js/jquery-2.1.4.min.js?v=1"></script>
<script src="/js/jquery-ui.min.js?v=1"></script>
<link href="/css/jquery-ui.min.css?v=1" rel="stylesheet"> 
<!-- //jQuery -->
<script src="/js/master.js?v={{ filemtime('js/master.js') }}" defer='defer'></script>

<!-- //lined-icons -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74118050-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-74118050-3');
</script>

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head> 
<body>
<div class="page @if(!empty($_COOKIE['menu'])) @if($_COOKIE['menu'] == 'toggled') menu_toggled @endif @endif" >
	<div class="sidebar-menu">
		<div class="toggle_menu">
			<i class="fa fa-bars"></i>
		</div>
		@yield('menu')
	</div>
	<div class="focus-popup-container">
		
		<div class="focus-panel" id="focus-import-existing-tree">
			<div class="panel-head">
				<i class="fa fa-times close" for="focus-import-existing-tree"></i>
				Import From Existing File
			</div>
			<div class="panel-body">
				<p>Upon importing your ideas, your files will be scheduled for import; This should take no longer than 10 minutes</p>
				<p>This is to ensure the whole file gets imported. <strong>You will recieve an email when the import is complete</strong></p>
				<hr>
				<p>Select the files which you wish to import below, then press the button below to select which focuses you wish to import.</p>
				<hr>
				<div>
					<div>
						Ideas File:<br>
						<input type="file" name="ideasFile" id="focusTreeFile">
					</div>
					<div>
						Localisation File:<br>
						<input type="file" name="localisation" id="localisationFile">
					</div>
					<div class="imported-textboxes" style="display: none;"></div>
					<textarea id="existing-focus-tree-output" style="display:none;"></textarea>
					<button id="treetojson" class="generic_button">Import ideas</button>
				</div>
				<div id="import-result">

				</div>
			</div>
		</div>
		@if(!empty($ideas))
			<div class="focus-panel" id="idea-popup">
				<div class="panel-head">
					<i class="fa fa-times close" for="idea-popup"></i>
					<span class="action">Add</span> Idea
				</div>
				<div class="panel-body">
					<iframe class="response_frame" id="post_iframe" name="post_iframe"></iframe>
					<form data-ideaid="new" method="POST" data-prefix="{{$ideas->prefix}}" target="post_iframe" action="/ideas/edit/{{$ideas->id}}/" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div>Name:<br>
							<input name="name" required="required" data-default="">
						</div>
						<div>Group:<br>
							<select name="group" id="ideaGroup">
								<option value="economy">Economy</option>
								<option value="mobilization_laws">Mobilization Laws</option>

								<option value="political_advisor">Political Advisor</option>
								<option value="theorist">Theorist</option>
								<option value="high_command">High Command</option>
								<option value="army_chief">Army Chief</option>
								<option value="air_chief">Air Chief</option>
								<option value="navy_chief">Navy Chief</option>

								<option value="country">Country</option>
							</select>
						</div>
						<div class="specific-options">
							<div class="option-wrapper" data-type="high_command|">
								Branch of High Command:<br>
								<select name="option[ledger]" class="inactive">
									<option value="army">Army</option>
									<option value="navy">Navy</option>
									<option value="air">Air Force</option>
								</select>
							</div>
							<div class="option-wrapper" data-type="political_advisor|theorist|high_command|army_chief|air_chief|navy_chief|">
								<p>Traits:<br>
								<input type="text" name="option[traits]" data-default="" class="search-traits"></p>
								<p>Cost:<br>
								<input type="text" name="option[cost]" data-default="" placeholder="150">
								</p>
								<p>Removal Cost:<br>
								<input type="text" name="option[removal_cost" data-default="" placeholder="0">
								</p>
							</div>
						</div>
						<div>GFX:<br>
							<div>
								<button type="button" class="toggle" for="choosegfx">Choose GFX</button>
								<input id="chosen-gfx" name="option[gfx]" value="{{ storage('/hoi4/focus_icons/goal_unknown.png', '/public/storage/') }}" data-default="/public/storage/hoi4/focus_icons/goal_unknown.png" style="display: none;" />
								<img src="{{ storage('/hoi4/focus_icons/goal_unknown.png', '/public/storage/') }}" id="display-gfx" />
								<div class="custom-upload-wrapper">
									<label for="customgfx">Or choose your own</label><br /> <input name="customgfx" data-alters="display-gfx" data-field="chosen-gfx" id="customgfx" type="file" class="" accept="image/x-png" />
									<p><strong>Previously uploaded images will show at bottom of GFX list - resized for focus dimensions</strong></p>
									<p>Custom focus GFX needs to be in a PNG format with a recommended Width: 95px Height: 85px</p>
								</div>
							</div>
							<div id="choosegfx">
								<div class="filter-gfx">
									<input type="text" class="filterGfx" placeholder="Filter GFX - Search 'custom' for previous uploads">
								</div>
								<?php
								$directory = storagePath('public/hoi4/ideas/people');

								$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
								foreach ($scanned_directory as $value) {
									$file = storage('/hoi4/ideas/people/'.$value, '/public/storage/');
									$id = str_replace(".png","",$value);
									echo '<img id="'.$id.'" class="idea-icon nficon" src="'.$file.'" loading="lazy" />';
								}

								$directory = storagePath('public/hoi4/ideas/not_people');

								$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
								foreach ($scanned_directory as $value) {
									$file = storage('/hoi4/ideas/not_people/'.$value, '/public/storage/');
									$id = str_replace(".png","",$value);
									echo '<img id="'.$id.'" class="idea-icon nficon" src="'.$file.'" loading="lazy" />';
								}
								?>
								<div class="custom-image-divider"></div>
								<div class="custom-images">
									@php 
									$gfx = getUserMedia();
									@endphp
									@foreach($gfx as $media)
										<img id="custom-image-{{ $media->id }}" class="idea-icon nficon" src="/image/find/{{ $media->id }}/idea" loading="lazy">
									@endforeach
									@if(count($gfx) === 0)
										<strong>GFX system has been re-worked, please re-upload custom GFX to ensure files export correctly</strong>
									@endif
								</div>
							</div>
						</div>
						<div>
							<p>Allowed</p>
							<textarea id="allowed" name="option[allowed]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>Allowed During Civil War</p>
							<textarea id="allowed_civil_war" name="option[allowed_civil_war]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>Available</p>
							<textarea id="available" name="option[available]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>Visible</p>
							<textarea id="visible" name="option[visible]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>Cancel</p>
							<textarea id="cancel" name="option[cancel]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>Equipment Bonus</p>
							<textarea id="modifier" name="option[modifier]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>Modifiers</p>
							<textarea id="equipment_bonus" name="option[equipment_bonus]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>Research Bonus</p>
							<textarea id="research_bonus" name="option[research_bonus]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<p>On Add</p>
							<textarea id="on_add" name="option[on_add]" class="uses-builder" data-default=""></textarea>
						</div>
						<div>
							<button class="generic_button loader-switch" id="submit-idea">Submit</button>
							<p style="display: none;" class="loader"><i class="fa fa-spinner fa-spin"></i></p>
						</div>
					</form>
				</div>
			</div>
			@endif
			<div class="focus-panel" id="focus-builder" data-building="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-builder"></i>
					Builder
				</div>
				<div class="panel-body">
					<div class="builder-output">
						Drag output here:
						<div class="dragged main">
						</div>
						<div class="build">
							<button id="submit-build">Submit</button> <button id="clear-build">Clear</button>
						</div>
					</div>
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-builder">
						</div>
						<div class="builder-search-output">

						</div>
					</div>
				</div>
			</div>

			<div class="focus-panel" id="focus-tag" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-tag"></i>
					Choose Country
				</div>
				<div class="panel-body">
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-tags">
						</div>
						<div class="tag-search-output">
							@if(isset($ideas))
								@if($ideas->mod->countries)
									@foreach($ideas->mod->countries as $country)
										<p id="{{ $country->tag }}" class="focus_tree_tags">{{ $country->name }}</p>
									@endforeach
								@endif
							@endif
						</div>
						
					</div>
				</div>
			</div>

			<div class="focus-panel" id="focus-state" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-state"></i>
					Choose State
				</div>
				<div class="panel-body">
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-states"  autocomplete="disabled">
						</div>
						<div class="state-search-output">

						</div>
					</div>
				</div>
			</div>

			<div class="focus-panel" id="country-traits">
				<div class="panel-head">
					<i class="fa fa-times close" for="country-traits"></i>
					Choose Traits
				</div>
				<div class="panel-body">
					<div class="search-traits">
						Search<br>
						<input placeholder="Search traits by name or effect" id="search-traits">
					</div> 
					@foreach($traits->traits as $id => $effect)
						<div class="country-trait" data-id="{{$id}}">
							@if(isset($traits->lang[$id]))
								<div class="name">
									{{$traits->lang[$id]}}
								</div>
								<div class="effect">
									<textarea disabled >{{$effect}}</textarea>
								</div>
							@endif
						</div>
					@endforeach
				</div>
			</div>
	</div>
	<div class="content ideas parent-mod-id @if($can_edit && $is_editable) edit @endif" data-id="{{ $ideas->id }}" data-mod-id="{{ $ideas->mod->id }}">
		@yield('content')
	</div>
</div>
</body>
</html>