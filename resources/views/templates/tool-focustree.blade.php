<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<!-- Bootstrap Core CSS -->
<!--<link href="css/bootstrap.min.css?v=1" rel='stylesheet' type='text/css' />-->
<!-- Custom CSS -->
<link href="/css/master.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="/css/font-awesome.css?v=1" rel="stylesheet"> 
<!-- jQuery -->
<script src="/js/jquery-2.1.4.min.js?v=1"></script>
<script src="/js/jquery-ui.min.js?v=1"></script>
<link href="/css/jquery-ui.min.css?v=1" rel="stylesheet"> 
<!-- //jQuery -->
<script src="/js/master.js?v={{ filemtime('js/master.js') }}" defer='defer'></script>

<!-- //lined-icons -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74118050-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-74118050-3');
</script>

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head> 
<body>
<div class="page @if(!empty($_COOKIE['menu'])) @if($_COOKIE['menu'] == 'toggled') menu_toggled @endif @endif">
	<div class="sidebar-menu">
		<div class="toggle_menu">
			<i class="fa fa-bars"></i>
		</div>
		@yield('menu')
	</div>
	<div class="focus-popup-container">
		
		<div class="focus-panel" id="focus-import-existing-tree">
			<div class="panel-head">
				<i class="fa fa-times close" for="focus-import-existing-tree"></i>
				Import From Existing Tree
			</div>
			<div class="panel-body">
				<p>Upon importing your focus tree, your files will be scheduled for import; This should take no longer than 10 minutes</p>
				<p>This is to ensure the whole focus tree gets imported, especially with larger focus trees. <strong>You will recieve an email when the import is complete</strong></p>
				<hr>
				<p>Select the files which you wish to import below, then press the button below to select which focuses you wish to import.</p>
				<hr>
				<div>
					<div>
						Focus Tree File:<br>
						<input type="file" name="focusTree" id="focusTreeFile">
					</div>
					<div>
						Localisation File:<br>
						<input type="file" name="localisation" id="localisationFile">
					</div>
					<div class="imported-textboxes" style="display: none;"></div>
					<textarea id="existing-focus-tree-output" style="display:none;"></textarea>
					<button id="treetojson" class="generic_button">Import focus tree</button>
				</div>
				<div id="import-result">

				</div>
			</div>
		</div>
		@if(!empty($tree))
			<div class="focus-panel" id="focus-global-move">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-global-move"></i>
					Global Move
				</div>
				<div class="panel-body">
					<p>Any focuses which are below 0 on the axis you are moving along, will be set to 0</p>
					<form action="/focus-tree/edit/{{$tree->id}}/globalMove" method="POST">
						{{csrf_field()}}
						<p>Axis to move along:<br>
							<select name="axis">
								<option value="x">X Axis</option>
								<option value="y">Y Axis</option>
							</select>
						</p>
						<p>
							Amount to move<br>
							<input type="number" step="1" value="0" name="distance">
						</p>
						<div>
							<button class="generic_button">Submit</button>
						</div>
					</form>
				</div>
			</div>

			<div class="focus-panel" id="focus-multi-move">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-multi-move"></i>
					Multi Move
				</div>
				<div class="panel-body">
					<p>Any focuses which are below 0 on the axis you are moving along, will be set to 0</p>
					<p>The focuses listed below will be moved</p>
					<form action="/focus-tree/edit/{{$tree->id}}/multiMove" method="POST">
						{{csrf_field()}}
						<div id="selectedFocuses">
						</div>
						<p>Axis to move along:<br>
							<select name="axis">
								<option value="x">X Axis</option>
								<option value="y">Y Axis</option>
							</select>
						</p>
						<p>
							Amount to move<br>
							<input type="number" step="1" value="0" name="distance">
						</p>
						<div>
							<button class="generic_button">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<div class="focus-panel" id="focus-popup">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-popup"></i>
					Add Focus
				</div>
				<div class="panel-body">
					<iframe class="response_frame" id="post_iframe" name="post_iframe"></iframe>
					<form data-treeid="{{$tree->id}}" method="POST" data-tag="{{$tree->country_tag}}" target="post_iframe" action="/focus-tree/edit/{{$tree->id}}/add" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div>Name:<br>
							<input name="focus_title" required="required" data-default="">
						</div>
						<div>Description:<br>
							<textarea name="focus_description" required="required" data-default=""></textarea>
						</div>
						<div>GFX:<br>
							<div>
								<button type="button" class="toggle" for="choosegfx">Choose GFX</button>
								<input id="chosen-gfx" name="chosen_gfx" value="{{ storage('/hoi4/focus_icons/goal_unknown.png', '/public/storage/') }}" data-default="/public/storage/hoi4/focus_icons/goal_unknown.png" style="display: none;" />
								<img src="{{ storage('/hoi4/focus_icons/goal_unknown.png', '/public/storage/') }}" id="display-gfx" />
								<div class="custom-upload-wrapper">
									<label for="customgfx">Or choose your own</label><br /> <input name="customgfx" data-alters="display-gfx" data-field="chosen-gfx" id="customgfx" type="file" class="" accept="image/x-png" />
									<p><strong>Previously uploaded images will show at bottom of GFX list - resized for focus dimensions</strong></p>
									<p>Custom focus GFX needs to be in a PNG format with a recommended Width: 95px Height: 85px</p>
								</div>
							</div>
							<div id="choosegfx">
								<div class="filter-gfx">
									<input type="text" class="filterGfx" placeholder="Filter GFX - Search 'custom' for previous uploads">
								</div>
								<?php
								$directory = storagePath('public/hoi4/focus_icons');

								$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
								foreach ($scanned_directory as $value) {
									$file = storage('/hoi4/focus_icons/'.$value, '/public/storage/hoi4/focus_icons/');
									$id = str_replace(".png","",$value);
									echo '<img id="'.$id.'" class="nficon" src="'.$file.'" loading="lazy" />';
								}
								?>
								<div class="custom-image-divider"></div>
								<div class="custom-images">
									@php 
									$gfx = getUserMedia();
									@endphp
									@foreach($gfx as $media)
										<img id="custom-image-{{ $media->id }}" class="nficon" src="/image/find/{{ $media->id }}/focus" loading="lazy">
									@endforeach
									@if(count($gfx) === 0)
										<strong>GFX system has been re-worked, please re-upload custom GFX to ensure files export correctly</strong>
									@endif
								</div>
							</div>
						</div>
						<div class="can_json">
							<div>
								<p>Bypass</p>
								<textarea id="bypass" name="focus_bypass" class="uses-builder" data-default=""></textarea>
							</div>
							<div>
								<p>Available</p>
								<textarea id="available" name="focus_available" class="uses-builder" data-default=""></textarea>
							</div>
							<div>
								<p>Mutually Exclusive</p>
								<input id="mutual" name="focus_mutually_exclusive" data-default="" />
							</div>
							<div>
								<p>Prerequisite</p>
								<input id="prefocus" class="trim" name="focus_prerequisite" data-default=""/>
							</div>
							<div>
								<p>AI Will Do Factor</p>
								<input id="ai" type="number" class="trim" name="focus_ai_will_do_factor" data-default="1"/>
							</div>
							<div>
								<p>Time to Complete</p>
								<select id="time" name="focus_time_to_complete" data-default="10">
									<option value="1">1 Week</option>
									<option value="2">2 Weeks</option>
									<option value="3">3 Weeks</option>
									<option value="4">4 Weeks</option>
									<option value="5">5 Weeks</option>
									<option value="6">6 Weeks</option>
									<option value="7">7 Weeks</option>
									<option value="8">8 Weeks</option>
									<option value="9">9 Weeks</option>
									<option value="10" selected>10 Weeks</option>
									<option value="11">11 Weeks</option>
									<option value="12">12 Weeks</option>
									<option value="13">13 Weeks</option>
									<option value="14">14 Weeks</option>
									<option value="15">15 Weeks</option>
								</select>
							</div>
							<div>
								<p>Location - X|Y</p>
								<input id="x" type="number" step="1" name="focus_x" required="required" value="0" data-default="0" min="0" />|<input id="y" type="number" step="1" name="focus_y" required="required" value="0" data-default="0" min="0"/>
							</div>
							<div>
								<p>Complete Tooltip</p>
								<textarea id="tooltip" name="focus_complete_tooltip" data-default=""></textarea>
							</div>
							<div>
								<p>Reward</p>
								<div class="reward_wrapper">
									<textarea id="reward" name="focus_reward" class="uses-builder" data-default=""></textarea>
								</div>
							</div>
							<div>
								<p>Reward filter</p>
								<select name="filter" id="reward" data-default="">
									<option value="" selected>No category</option>
									<option value="FOCUS_FILTER_POLITICAL">Political</option>
									<option value="FOCUS_FILTER_RESEARCH">Research</option>
									<option value="FOCUS_FILTER_INDUSTRY">Industry</option>
									<option value="FOCUS_FILTER_STABILITY">Stability</option>
									<option value="FOCUS_FILTER_WAR_SUPPORT">War Support</option>
									<option value="FOCUS_FILTER_MANPOWER">Manpower</option>
									<option value="FOCUS_FILTER_FRA_POLITICAL_VIOLENCE">Political Violence</option>
									<option value="FOCUS_FILTER_FRA_OCCUPATION_COST">Occupation Costs</option>
									<option value="FOCUS_FILTER_ANNEXATION">Territorial Expansion</option>
									<option value="FOCUS_FILTER_CHI_INFLATION">Inflation</option>
									<option value="FOCUS_FILTER_USA_CONGRESS">Congress</option>
									<option value="FOCUS_FILTER_TFV_AUTONOMY">Autonomy</option>
									<option value="FOCUS_FILTER_MEX_CHURCH_AUTHORITY">Church Authority</option>
									<option value="FOCUS_FILTER_MEX_CAUDILLO_REBELLION">Caudillo Rebellion</option>
									<option value="FOCUS_FILTER_SPA_CIVIL_WAR">Spanish Civil War</option>
									<option value="FOCUS_FILTER_SPA_CARLIST_UPRISING">Carlist Uprising</option>
								</select>
							</div>
						</div>
						<div>
							<button class="generic_button loader-switch" id="submit-focus" onclick="event.preventDefault(); var $this = $(this); $('.trim').each(function(){var x = $(this).val().slice(-2); if(x == '&&' || x == '||'){$(this).val($(this).val().slice(0, -2))} }); $this.closest('form').submit();">Submit</button>
							<p style="display: none;" class="loader"><i class="fa fa-spinner fa-spin"></i></p>
						</div>
					</form>
				</div>
			</div>
			@endif
			<div class="focus-panel" id="focus-builder" data-building="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-builder"></i>
					Builder
				</div>
				<div class="panel-body">
					<div class="builder-output">
						Drag output here:
						<div class="dragged main">
						</div>
						<div class="build">
							<button id="submit-build">Submit</button> <button id="clear-build">Clear</button>
						</div>
					</div>
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-builder">
						</div>
						<div class="builder-search-output">

						</div>
					</div>
				</div>
			</div>

			<div class="focus-panel" id="focus-tag" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-tag"></i>
					Choose Country
				</div>
				<div class="panel-body">
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-tags">
						</div>
						<div class="tag-search-output">
							@if(isset($tree))
								@if($tree->mod->countries)
									@foreach($tree->mod->countries as $country)
										<p id="{{ $country->tag }}" class="focus_tree_tags">{{ $country->name }}</p>
									@endforeach
								@endif
							@endif
							@if(isset($mods))
								<div class="mod-countries">
									@if(!empty($mods))
										@foreach($mods as $mod)
											<div class="country-wrapper" data-id="{{$mod->id}}">
												@if(!empty($mod->countries))
												<p class="focus_tree_tags"></p>
													@foreach($mod->countries as $country)
														<p class="focus_tree_tags" id="{{ $country->tag }}">{{ $country->name }}</p>
													@endforeach
												@endif
											</div>
										@endforeach
									@endif
								</div>
							@endif
						</div>
						
					</div>
				</div>
			</div>

			<div class="focus-panel" id="focus-state" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-state"></i>
					Choose State
				</div>
				<div class="panel-body">
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-states"  autocomplete="disabled">
						</div>
						<div class="state-search-output">

						</div>
					</div>
				</div>
			</div>

			<div class="focus-panel" id="focus-chooser" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-chooser"></i>
					Focus Chooser
				</div>
				<div class="panel-body">
					<div class="builder-search">
						<p>Want to select multiple? Choose whether you want all to be required, or any to be required</p>
						<p><label>ALL - <input type="radio" id="select-and" name="selectors" style="width:1rem;"></label> | <label>ANY - <input type="radio" id="select-or" name="selectors" style="width:1rem;"></label> | <label>NEITHER - <input type="radio" id="select-neither" name="selectors" style="width:1rem;"></label></p>
						<p><input class="limit-children" data-limits="#select-focus" placeholder="Find focus"></p>
						<div id="select-focus">
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="content focus-tree @if($can_edit && $is_editable) edit @endif">
		@yield('content')
	</div>
</div>
</body>
</html>