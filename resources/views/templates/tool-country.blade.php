<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<!-- Bootstrap Core CSS -->
<!--<link href="css/bootstrap.min.css?v=1" rel='stylesheet' type='text/css' />-->
<!-- Custom CSS -->
<link href="/css/master.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="/css/font-awesome.css?v=1" rel="stylesheet"> 
<!-- jQuery -->
<script src="/js/jquery-2.1.4.min.js?v=1"></script>
<script src="/js/jquery-ui.min.js?v=1"></script>
<link href="/css/jquery-ui.min.css?v=1" rel="stylesheet"> 
<!-- //jQuery -->
<script src="/js/master.js?v={{ filemtime('js/master.js') }}" defer='defer'></script>

<!-- //lined-icons -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74118050-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-74118050-3');
</script>

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head> 
<body>
<div class="page @if(!empty($_COOKIE['menu'])) @if($_COOKIE['menu'] == 'toggled') menu_toggled @endif @endif">
	<div class="sidebar-menu">
		<div class="toggle_menu">
			<i class="fa fa-bars"></i>
		</div>
		@yield('menu')
	</div>
	<div class="focus-popup-container">

		<div class="focus-panel" id="country-military" data-type="ground">
			<div class="panel-head">
				<i class="fa fa-times close" for="country-military"></i>
				Military Personel
			</div>
			<div class="panel-body">
				<div>	
					<img class="image open" for="choose-leader" data-default="{{ storage('/hoi4/leaders/leader_unknown.png', '/public/storage/') }}">
				</div>
				<div>
					Name<br>
					<input type="text" name="name" class="name" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false">
				</div>
				<div>
					Traits<br>
					<textarea name="traits" class="traits open" for="country-traits"></textarea>
				</div>
				<div>
					Overal Level<br>
					<input name="level" type="number" min="1" step="1" max="10">
				</div>
				<div>
					Attack Level<br>
					<input name="attack_stat" type="number" min="1" step="1" max="10">
					Defence Level<br>
					<input name="defence_stat" type="number" min="1" step="1" max="10">
					<span class="navy-only">Coordination Level</span><span>Logistics Level</span><br>
					<input name="supply_stat" type="number" min="1" step="1" max="10">
					<span class="navy-only">Maneuvering Level</span><span>Planning Level</span><br>
					<input name="planning_stat" type="number" min="1" step="1" max="10">
				</div>
				<div class="field-marshal">
					Is field marshal?<br>
					<input type="checkbox" value="1" name="field_marshal">
				</div>
				<div class="generic_button close submit" for="country-military">Submit</div>
			</div>
		</div>


		<div class="focus-panel" id="choose-leader" data-for="leader">
			<div class="panel-head">
				<i class="fa fa-times close" for="choose-leader"></i>
				Select Leader
			</div>
			<div class="panel-body">
				<div class="country-leader-search">
					<p>Search<br>
					<input id="search-country-leaders" autocomplete="disabled"><br>&nbsp;</p>
				</div>
				<div class="country-leaders">
					<div class="custom-leader-gfx custom-upload-wrapper">
						<h3>Custom leader</h3>
						<p>Scroll to bottom for previously uploaded images, or search "custom"</p>
						<input type="file" accept="image/x-png">
						<span class="generic_button uploadCustomLeader">Upload custom leader GFX</span>
					</div>
					<?php
					$directory = storagePath('public/hoi4/leaders');
					$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
					foreach ($scanned_directory as $value) {
						$file = storage('/hoi4/leaders/' . $value, '/public/storage/');
						$id = str_replace(".png","",$value);
						echo '<img id="'.$id.'" class="country-leader-icon" src="'.$file.'" loading="lazy" />';
					}
					?>
					<div class="custom-image-divider"></div>
					<div class="custom-images">
						@foreach(getUserMedia() as $media)
							<img id="custom-image-{{ $media->id }}" class="country-leader-icon" src="/image/find/{{ $media->id }}/leader">
						@endforeach
					</div>
				</div>
			</div>
		</div>

		<div class="focus-panel" id="edit-ideas">
			<div class="panel-head">
				<i class="fa fa-times close" for="edit-ideas"></i>
				Edit Ideas
			</div>
			<div class="panel-body">
				<div id="existing-ideas">

				</div>
				<button class="generic_button country-idea-add">Add New</button>
			</div>
		</div>


		<div class="focus-panel" id="choose-non-person-idea">
			<div class="panel-head">
				<i class="fa fa-times close" for="choose-non-person-idea"></i>
				Select Ideas (Not People)
			</div>
			<div class="panel-body">
				<div class="country-ideas-select">
					<?php
					$directory = storagePath('public/hoi4/ideas/not_people');
					$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
					foreach ($scanned_directory as $value) {
						$file = storage('/hoi4/ideas/not_people/' . $value, '/public/storage/');
						$id = str_replace(".png","",$value);
						echo '<img id="'.$id.'" class="country-idea-icon" src="'.$file.'" loading="lazy" />';
					}
					?>
				</div>
			</div>
		</div>

		<div class="focus-panel" id="choose-person-idea">
			<div class="panel-head">
				<i class="fa fa-times close" for="choose-person-idea"></i>
				Select Ideas (People)
			</div>
			<div class="panel-body">
				<div class="country-ideas-select">
					<?php
					$directory = storagePath('public/hoi4/ideas/people');
					$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
					foreach ($scanned_directory as $value) {
						$file = storage('/hoi4/ideas/people/' . $value, '/public/storage/');
						$id = str_replace(".png","",$value);
						echo '<img id="'.$id.'" class="country-idea-icon" src="'.$file.'" loading="lazy" />';
					}
					?>
				</div>
			</div>
		</div>

		<div class="focus-panel" id="choose-person-idea">
			<div class="panel-head">
				<i class="fa fa-times close" for="choose-person-idea"></i>
				Select Ideas
			</div>
			<div class="panel-body">
				<div class="country-ideas-select">
					<?php
					$directory = storagePath('public/hoi4/ideas/people');
					$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
					foreach ($scanned_directory as $value) {
						$file = storage('/hoi4/ideas/people/' . $value, '/public/storage/');
						$id = str_replace(".png","",$value);
						echo '<img id="'.$id.'" class="country-idea-icon" src="'.$file.'" loading="lazy" />';
					}
					?>
					<?php
					$directory = storagePath('public/hoi4/ideas/not_people');
					$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
					foreach ($scanned_directory as $value) {
						$file = storage('/hoi4/ideas/not_people/' . $value, '/public/storage/');
						$id = str_replace(".png","",$value);
						echo '<img id="'.$id.'" class="country-idea-icon" src="'.$file.'" loading="lazy" />';
					}
					?>
				</div>
			</div>
		</div>


		<div class="focus-panel" id="idea-additional-info">
			<div class="panel-head">
				<i class="fa fa-times close" for="idea-additional-info"></i>
				Additional Info
			</div>
			<div class="panel-body">
				<div class="country-ideas-info">
					<div class="left">
						<div class="image">
						</div>
						<input style="display: none;" id="idea-gfx" name="gfx">
					</div>
					<div class="right">
						{{csrf_field()}}
						<div>
							<label>Name:</label><br>
							<input name="name" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false">
						</div>
						<div>
							<label>Short Description:</label><br>
							<textarea name="short_desc"></textarea>
						</div>
						<div>
							<label>Long Description:</label><br>
							<textarea name="long_desc"></textarea>
						</div>
						<div>
							<label>Trait:</label><br>
							<input name="effect" class="open" for="country-leader-traits">
						</div>
						<div>
							<button class="generic_button close" for="idea-additional-info">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="focus-panel" id="country-state">
			<div class="panel-head">
				<i class="fa fa-times close" for="country-state"></i>
				Choose State
			</div>
			<div class="panel-body">
				<div class="builder-search">
					<div class="input">
						Search<br>
						<input id="search-states" autocomplete="disabled">
					</div>
					<div class="state-search-output">

					</div>
				</div>
			</div>
		</div>

		

		@yield('popups')
	</div>

	

	<div class="content country @if($can_edit && $is_editable) edit @endif">
		@yield('content')

		@if($can_edit && !$is_editable)
			<a href="/country/edit/{{$country->id}}" class="country-edit">Edit</a>
		@else
			<div class="country-save">Save</div>
		@endif
	</div>
</div>
</body>
</html>