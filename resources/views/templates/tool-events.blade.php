<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<!-- Bootstrap Core CSS -->
<!--<link href="css/bootstrap.min.css?v=1" rel='stylesheet' type='text/css' />-->
<!-- Custom CSS -->
<link href="/css/master.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="/css/font-awesome.css?v=1" rel="stylesheet"> 
<!-- jQuery -->
<script src="/js/jquery-2.1.4.min.js?v=1"></script>
<script src="/js/jquery-ui.min.js?v=1"></script>
<link href="/css/jquery-ui.min.css?v=1" rel="stylesheet"> 
<!-- //jQuery -->
<script src="/js/master.js?v={{ filemtime('js/master.js') }}" defer='defer'></script>

<!-- //lined-icons -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74118050-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-74118050-3');
</script>

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head> 
<body>
<div class="page @if(!empty($_COOKIE['menu'])) @if($_COOKIE['menu'] == 'toggled') menu_toggled @endif @endif">
	<div class="sidebar-menu">
		<div class="toggle_menu">
			<i class="fa fa-bars"></i>
		</div>
		@yield('menu')
	</div>
	<div class="focus-popup-container">
		@if(!empty($mod))
			<div class="focus-panel" id="event-popup">
				<div class="panel-head">
					<i class="fa fa-times close" for="event-popup"></i>
					Add Event
				</div>
				<div class="panel-body">
					<iframe class="response_frame" id="post_iframe" name="post_iframe"></iframe>
					<form data-modid="{{$mod->id}}" method="POST" target="post_iframe" action="/events/edit/{{$mod->id}}/add" data-action="/events/edit/{{$mod->id}}/add" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div>Event Type:<br>
							<select name="event_type">
								<option value="country">Country</option>
								<option value="news">News</option>
							</select>
						</div>
						<div>Name:<br>
							<input name="event_title" required="required" data-default="">
						</div>
						<div>Description:<br>
							<textarea name="event_description" required="required" data-default=""></textarea>
						</div>
						<div>GFX:<br>
							<div>
								<button type="button" class="toggle generic_button" for="choosegfx">Choose GFX</button>
								<input id="chosen-gfx" name="chosen_gfx" value="{{ storage('/hoi4/event_gfx/event_test.png', '/public/storage/') }}" data-default="{{ storage('/hoi4/event_gfx/event_test.png', '/public/storage/') }}" style="display: none;" />
								<img src="/public/storage/hoi4/event_gfx/event_test.png" id="display-gfx" style="max-width: 100%;" />
								<div class="custom-upload-wrapper">
									<label for="customgfx">Or choose your own</label><br /> <input name="customgfx" data-alters="display-gfx" data-field="chosen-gfx" id="customgfx" type="file" class="" accept="image/x-png" />
									<p><strong>Previously uploaded images will show at bottom of GFX list - resized for event dimensions</strong></p>
								</div>
							</div>
							<div id="choosegfx">
								<div class="filter-gfx">
									<input type="text" class="filterGfx" placeholder="Filter GFX - Search 'custom' for previous uploads">
								</div>
								<?php
								$directory = storagePath('public/hoi4/event_gfx');
								$scanned_directory = array_diff(scandir($directory, 1), array('..', '.'));
								foreach ($scanned_directory as $value) {
									$file = storage('/hoi4/event_gfx/' . $value, '/public/storage/');
									$id = str_replace(".png","",$value);
									echo '<img id="'.$id.'" class="nficon" src="'.$file.'" loading="lazy" />';
								}
								?>
								<div class="custom-image-divider"></div>
								<div class="custom-images">
									@php 
									$gfx = getUserMedia();
									@endphp
									@foreach($gfx as $media)
										<img id="custom-image-{{ $media->id }}" class="nficon" src="/image/find/{{ $media->id }}" loading="lazy">
									@endforeach
									@if(count($gfx) === 0)
										<strong>GFX system has been re-worked, please re-upload custom GFX to ensure files export correctly</strong>
									@endif
								</div>
							</div>
						</div>
						<div class="can_json">
							<div>Is Triggered Only:<br>
								<select name="event_is_triggered_only" data-default="1">
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
							</div>
							<div>Fire Only Once:<br>
								<select name="event_fire_only_once" data-default="1">
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
							</div>
							<div>Major Event:<br>
								<select name="event_major" data-default="1">
									<option value="0">No</option>
									<option value="1">Yes</option>
								</select>
							</div>
							<div>Mean Time To Happen: (in days)<br>
								<input type="number" step="1" data-default="0" min="0" name="event_mean_time_to_happen">
							</div>
							<div>
								Trigger<br>
								<textarea name="event_trigger" class="uses-builder" data-default=""></textarea>
							</div>
						</div>
						<div>
							<p>Create outcomes after submitting this</p>
							<button class="generic_button loader-switch">Submit</button>
							<p style="display: none;" class="loader"><i class="fa fa-spinner fa-spin"></i></p>
						</div>
					</form>
				</div>
			</div>

			<div class="focus-panel" id="event-outcome" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="event-outcome"></i>
					Event Outcome
				</div>
				<div class="panel-body">
					<form data-modid="{{$mod->id}}" data-action="/events/edit/{{$mod->id}}/outcome/add" method="POST" target="post_iframe" action="/events/edit/{{$mod->id}}/outcome/add" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div>Name:<br>
							<input name="outcome_name" required="required" data-default="">
						</div>
						<div>AI Chance:<br>
							<input name="outcome_ai_chance" type="number" value="1" step="1" required="required" data-default="1">
						</div>
						<div>Extras:<br>
							<textarea data-default="" name="outcome_reward" class="uses-builder"></textarea>
						</div>
						<button class="generic_button loader-switch">Submit</button>
						<p style="display: none;" class="loader"><i class="fa fa-spinner fa-spin"></i></p>
					</form>
				</div>
			</div>
			@endif
			<div class="focus-panel" id="focus-builder" data-building="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-builder"></i>
					Builder
				</div>
				<div class="panel-body">
					<div class="builder-output">
						Drag output here:
						<div class="dragged main">
						</div>
						<div class="build">
							<button id="submit-build">Submit</button> <button id="clear-build">Clear</button>
						</div>
					</div>
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-builder">
						</div>
						<div class="builder-search-output">

						</div>
					</div>
				</div>
			</div>

			<div class="focus-panel" id="focus-tag" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-tag"></i>
					Choose Country
				</div>
				<div class="panel-body">
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-tags">
						</div>
						<div class="tag-search-output">
							@if($mod->countries)
								@foreach($mod->countries as $country)
									<p id="{{ $country->tag }}" class="focus_tree_tags">{{ $country->name }}</p>
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="focus-panel" id="focus-state" data-editing="">
				<div class="panel-head">
					<i class="fa fa-times close" for="focus-state"></i>
					Choose State
				</div>
				<div class="panel-body">
					<div class="builder-search">
						<div class="input">
							Search<br>
							<input id="search-states">
						</div>
						<div class="state-search-output">

						</div>
					</div>
				</div>
			</div>


	</div>
	<div class="content events @if($can_edit && $is_editable) edit @endif">
		@yield('content')
	</div>
</div>
</body>
</html>