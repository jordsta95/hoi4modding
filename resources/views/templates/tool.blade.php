<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="description" content="@yield('description')">
<!-- Bootstrap Core CSS -->
<!--<link href="css/bootstrap.min.css?v=1" rel='stylesheet' type='text/css' />-->
<!-- Custom CSS -->
<link href="/css/master.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="/css/font-awesome.css?v=1" rel="stylesheet"> 
<!-- jQuery -->
<script src="/js/jquery-2.1.4.min.js?v=1"></script>
<!-- //jQuery -->
<script src="/js/master.js?v={{ filemtime('js/master.js') }}" defer='defer'></script>

<!-- //lined-icons -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74118050-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-74118050-3');
</script>

<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
@yield('extra-meta')
</head> 
<body>
<div class="page @if(!empty($_COOKIE['menu'])) @if($_COOKIE['menu'] == 'toggled') menu_toggled @endif @endif">
	@if(\Session::has('success'))
		<div class="success popup_message">{{ \Session::get('success') }}</div>
	@endif
	@if(\Session::has('error'))
		<div class="error popup_message">{{ \Session::get('error') }}</div>
	@endif
	{{-- @if(!isset($_COOKIE['golive']))
		<div class="popup_message info clickToDismiss addCookie" data-cake="golive" data-cake-value="dismissed">The site in its current form will be moving out of beta on 02/02/2019. All data will be transfered to the live site, and the beta site will only be accessible for users which wish to have access, and potentially break things.<br><small>Click to dismiss</small></div>
	@endif --}}
	<div class="sidebar-menu">
		<div class="toggle_menu">
			<i class="fa fa-bars"></i>
		</div>
		@yield('menu')
	</div>
	<div class="content">
		<div class="container">
			@yield('content')
		</div>
	</div>
</div>
</body>
</html>