@extends('templates.tool')

@section('title')
{{$post->title}} - HOI4 Modding
@endsection

@section('extra-meta')
<link href="/css/trumbowyg.min.css" rel='stylesheet' type='text/css'>
<script src="/js/trumbowyg.min.js"></script>
<script>$(window).load(function(){$('.wysiwyg').trumbowyg({
    btns: [
        ['viewHTML'],
        ['undo', 'redo'],
        ['formatting'],
        ['strong', 'em', 'del'],
        ['link'],
        ['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ],
    autogrow: true
});
$('.trumbowyg-textarea').attr('name', 'wysiwyg');
});</script>
@endsection

@section('description')
{{substr(strip_tags($post->content), 0, 140)}}...
@endsection

@section('menu')
    @include('menus/generic')
@endsection

@section('content')
    <div class="forum"> 
        <div class="breadcrumb">
            <a href="/forum">Forum</a> -> <a href="/forum/{{$parent->slug}}">{{$parent->name}}</a> -> <a href="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}">{{$post->title}}</a>
        </div> 
        @if($comments->currentPage() == 1)
            <div class="forum-post parent">
                <div class="user-info">
                    <a href="/users/{{$post->user->id}}"><p>{{$post->user->username}}@if($post->user->can_post == 0) <em class="banned">(banned)</em> @endif</p>
                    <img src="{{ storage($post->user->avatar, '/public/storage/') }}" class="post-avatar"></a>
                    <p>{{$post->user->role->display_name}}</p>
                    <p>{{$post->user->post_count}} Posts</p>
                </div>
                <div class="post-info"> 
                    <div class="post-title">
                        <h1>{{$post->title}}</h1>
                        @if(Auth::user())
                            @if(Auth::user()->role_id == 1 || Auth::user()->id == $post->user->id)
                            <div class="post-options">
                                <a href="#" class="edit-post edit">Edit Thread</a>
                                <a href="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}/close">Close Thread</a>
                                <a href="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}/remove">Delete Thread</a>
                                @if(Auth::user()->role_id == 1)
                                    <a href="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}/reopen">Reopen Thread</a>
                                    <a href="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}/ban">Close &amp; Hide Thread</a>
                                @endif
                            </div>
                            @endif
                        @endif
                    </div>
                    <div class="post-content">{!! strip_tags($post->content, '<p><br><a><i><em><b><strong><h1><h2><h3><h4><h5><h6><img><font><span><hr><ul><li><ol><table><thead><tbody><tr><td><th>') !!}</div>
                </div>
            </div>
        @else
            <div class="forum-post-title">
                <h1>{{$post->title}}</h1>
            </div>
        @endif
        @foreach($comments as $comment)
        <div class="forum-post" id="{{$comment->id}}">
            <div class="user-info">
                <a href="/users/{{$comment->user->id}}"><p>{{$comment->user->username}}@if($comment->user->can_post == 0) <em class="banned">(banned)</em> @endif</p>
                <img src="{{ storage($comment->user->avatar, '/public/storage/') }}" class="post-avatar"></a>
                <p>{{$comment->user->role->display_name}}</p>
                <p>{{$comment->user->post_count}} Posts</p>
            </div>
            <div class="post-info"> 
                <div class="post-content">{!! strip_tags($comment->content, '<p><br><a><i><em><b><strong><h1><h2><h3><h4><h5><h6><img><font><span><hr><ul><li><ol><table><thead><tbody><tr><td><th>') !!}</div>
                @if(Auth::user())
                <p class="edit-post-button">
                    <?php 
                    $likers = '';
                    if(count($comment->likes) >= 1){
                        if(count($comment->likes) > 3){
                            $i = 1;
                            foreach($comment->likes as $like){
                                $likers .= '<a href="/users/'.$like->user->id.'">'.$like->user->username.'</a>, ';
                                if ($i++ == 3) break; 
                            }
                            $likers .= ' +'.(count($comment->likes) - 3).' other';
                            if((count($comment->likes) - 3) > 1){ $likers .= 's'; }
                        }else{
                            foreach($comment->likes as $like){
                                $likers .= '<a href="/users/'.$like->user->id.'">'.$like->user->username.'</a>, ';
                            }
                        }
                        $likers = rtrim($likers, ', ');
                        echo $likers.' liked this.';
                    }
                    ?>
                    <br>
                    @if(Auth::user())
                        @if(Auth::user()->id !== $comment->user->id && !$comment->likes->contains('user_id', Auth::user()->id))
                            <a href="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}/like/g/{{$comment->id}}" class="edit-post">Like This Post</a> 
                        @endif
                        @if(Auth::user()->role_id == 1 || Auth::user()->id == $comment->user->id)
                           <a href="#" class="edit-post edit">Edit Post</a>
                        @endif
                    @endif
                </p>
                @endif
            </div>
        </div>
        @endforeach
        <div class="post-pagination">
            @if($comments->lastPage() >= 2)
                <a href="?page=1" class="text">First Page</a>
                    <a href="?page=1" class="number first-page-num @if($comments->currentPage() == 1) active @endif">1</a>
                    @if($comments->currentPage() == 1 || $comments->currentPage() == 2)
                        <a href="?page=2" class="number second-page-num @if($comments->currentPage() == 2) active @endif">2</a>
                    @endif
                    @if($comments->currentPage() == 2 && $comments->lastPage() >= 3)
                        <a href="?page=3" class="number @if($comments->currentPage() == 3) active @endif">3</a>          
                    @endif
                    @if($comments->lastPage() > 2 && $comments->currentPage() > 2)
                    ...
                    @endif
                    @if(($comments->currentPage() - 1) >= 3 && $comments->currentPage() !== 3)
                        <a href="?page={{($comments->currentPage() - 1)}}" class="number">{{($comments->currentPage() - 1)}}</a>
                    @endif
                    @if($comments->currentPage() > 2)
                        <a href="?page={{$comments->currentPage()}}" class="number active">{{$comments->currentPage()}}</a>
                    @endif
                    @if($comments->lastPage() >= ($comments->currentPage() + 1) && $comments->currentPage() >= 3 && $comments->lastPage() > 3)
                        <a href="?page={{($comments->currentPage() + 1)}}" class="number">{{($comments->currentPage() + 1)}}</a>
                    @endif
                    @if($comments->lastPage() > ($comments->currentPage() + 1) && ($comments->currentPage() !== 2 || $comments->lastPage() > 4) && $comments->lastPage() > ($comments->currentPage() + 2))
                        ...
                    @elseif($comments->lastPage() > 3 && ($comments->lastPage() - 1) > ($comments->currentPage() + 1))
                        <a href="?page={{($comments->lastPage() - 1)}}" class="second-last-page-num number">{{($comments->lastPage() - 1)}}</a>
                    @endif
                    @if($comments->lastPage() > 2 && $comments->lastPage() > ($comments->currentPage() + 1))
                        <a href="?page={{$comments->lastPage()}}" class="last-page-num number">{{$comments->lastPage()}}</a>
                    @endif
                <a href="?page={{$comments->lastPage()}}" class="text">Last Page</a>
            @endif
        </div>
        @if(Auth::user() && 5 >= Auth::user()->forum_strikes && $post->open == 1)
        <h4>Post a comment</h4>
        <div class="post-comment">
            <form method="POST">
                {{ csrf_field() }}
                <div class="wysiwyg" placeholder="Reply to this post..."></div>
                <button class="submit-post">Post Reply</button>
            </form>
        </div>
        @else
            @if($post->open == 1)
            <div class="post-comment">
                You must be logged in to comment
            </div>
            @else
            <div class="post-comment">
                This post is closed for further comments
            </div>
            @endif
        @endif
    </div>
<script>
$('.edit-post.edit').on('click', function(){
    $(this).hide();
    var $content = $(this).parent().parent().parent().find('.post-content');
    if($(this).parent().parent().parent().parent().hasClass('parent')){
        var action = 'edit_post/p/';
        var $parent = $content.parent(); 
        $parent.find('.post-title h1').replaceWith('<input name="title" value="'+ $('.parent .post-title h1').text() +'">');
        $parent.find('.post-content').html('<div class="post-content"><div class="post-comment"><div class="editor">'+ $content.html()+'</div><button class="submit-post">Edit Thread</button></div></div>');
        var $content = $parent.html('<form method="POST" action="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}/'+action+'">{{ csrf_field() }}'+$parent.html()+'</form>');
    }else{
        var action = 'edit/p/'+$(this).parent().parent().parent().attr('id');
        $content.html('<div class="post-content"><form method="POST" action="/forum/{{$parent->slug}}/{{$post->id}}-{{$post->slug}}/'+action+'">{{ csrf_field() }}<div class="post-comment"><div class="editor">'+ $content.html()+'</div><button class="submit-post">Edit Post</button></div></form></div>');
    }
    
    $content.find('.editor').trumbowyg({
    btns: [
        ['viewHTML'],
        ['undo', 'redo'],
        ['formatting'],
        ['strong', 'em', 'del'],
        ['link'],
        ['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ],
    autogrow: true
});
$('.trumbowyg-textarea').attr('name', 'wysiwyg');
});
</script>
@endsection