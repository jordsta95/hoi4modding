@extends('templates.tool')

@section('title')
Forum - HOI4 Modding
@endsection


@section('description')
HOI4 Modding Forum
@endsection

@section('menu')
    @include('menus/generic')
@endsection

@section('content')
    <div class="forum">
        <div class="breadcrumb">
            <a href="/forum">Forum</a>
        </div> 


        @foreach($categories as $category)
            <div class="forum-category">
                <h3 class="forum-category-title">{{$category->title}}</h3>
                <div class="forum-category-description">{{$category->description}}</div>
                <div class="forum-category-headers">
                    <div class="grid">
                        <div class="grid_span_5">
                            Forum
                        </div>
                        <div class="grid_span_1 grid_start_6">
                            Threads
                        </div>
                        <div class="grid_span_1 grid_start_7">
                            Posts
                        </div>
                        <div class="grid_span_5 grid_start_8">
                            Last Post
                        </div>
                    </div>
                </div>
                <div class="forum-category-forums">
                    @foreach($category->forums()->orderBy('name', 'ASC')->get() as $sub)
                        <div class="grid">
                            <div class="grid_span_5">
                                <div class="mobile-header">Forum</div>
                                <p class="sub-forum-title"><a href="/forum/{{$sub->slug}}/">{{$sub->name}}</a></p>
                            </div>
                            <div class="grid_span_1 grid_start_6">
                                <div class="mobile-header">Threads</div>
                                <p class="sub-forum-number">{{number_format(count($sub->posts), 0, '.', ',')}}</p>
                            </div>
                            <div class="grid_span_1 grid_start_7">
                                <div class="mobile-header">Posts</div>
                                <p class="sub-forum-number">{{number_format(count($sub->comments), 0, '.', ',')}}</p>
                            </div>
                            <div class="grid_span_5 grid_start_8">
                                <div class="mobile-header">Last Post</div>
                                <div>
                                    <?php $lastThread = $sub->posts()->with('comments','user')->orderBy('updated_at','DESC')->first(); ?>
                                    @if(!empty($lastThread))
                                        <p class="sub-forum-title"><a href="/forum/{{$sub->slug}}/{{$lastThread->id}}-{{$lastThread->slug}}">{{$lastThread->title}}</a></p>
                                        <?php $comment = $lastThread->comments()->with('user')->first(); ?>
                                        @if(!empty($comment))
                                            <p class="sub-forum-subtitle">by <a href="/users/{{$comment->user->id}}">{{$comment->user->username}}</a></p>
                                        @else
                                            <p class="sub-forum-subtitle"><a href="/users/{{$lastThread->user->id}}">{{$lastThread->user->username}}</a> started thread</p>
                                        @endif
                                        <p class="sub-forum-date">{{ date( 'd-m-y H:iA', strtotime($lastThread->updated_at)) }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
@endsection