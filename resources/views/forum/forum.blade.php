@extends('templates.tool')

@section('title')
{{$forum->name}} - HOI4 Modding
@endsection


@section('description')
{{substr(strip_tags($forum->description), 0, 140)}}...
@endsection

@section('menu')
    @include('menus/generic')
@endsection

@section('content')
    <div class="forum">
        <div class="breadcrumb">
            <a href="/forum">Forum</a> -> <a href="/forum/{{$forum->slug}}">{{$forum->name}}</a>
        </div>
        <div class="forum-header">
            <a href="/forum/{{$forum->slug}}/create">Create New Post</a>
        </div>
        <?php 
            if(Auth::user()){
                $pagination = $forum->posts()->where('open','=', 1)->where('public', '=', 1)->orderBy('sticky', 'DESC')->orderBy('updated_at', 'DESC')->paginate(10); 
            }else{
                $pagination = $forum->posts()->where('open','=', 1)->orderBy('sticky', 'DESC')->orderBy('updated_at', 'DESC')->paginate(10);
            }
        ?>
        @foreach($pagination as $post)
        <div class="forum-block">
            <div class="header">
                <a href="/forum/{{$forum->slug}}/{{$post->id}}-{{$post->slug}}">{{$post->title}}</a>
            </div>
            <div class="description post">
                <div class="left">{{substr(strip_tags($post->content), 0, 140)}}...</div>
                <div class="right">
                    Last post:<br>
                    {{ date('d/M/Y', strtotime($post->updated_at)) }}
                </div>
            </div>
        </div>
        @endforeach
        <div class="post-pagination">
            @if($pagination->lastPage() >= 2)
                <a href="?page=1" class="text">First Page</a>
                    <a href="?page=1" class="number first-page-num @if($pagination->currentPage() == 1) active @endif">1</a>
                    @if($pagination->currentPage() == 1 || $pagination->currentPage() == 2)
                        <a href="?page=2" class="number second-page-num @if($pagination->currentPage() == 2) active @endif">2</a>
                    @endif
                    @if($pagination->currentPage() == 2 && $pagination->lastPage() >= 3)
                        <a href="?page=3" class="number @if($pagination->currentPage() == 3) active @endif">3</a>          
                    @endif
                    @if($pagination->lastPage() > 2 && $pagination->currentPage() > 2)
                    ...
                    @endif
                    @if(($pagination->currentPage() - 1) >= 3 && $pagination->currentPage() !== 3)
                        <a href="?page={{($pagination->currentPage() - 1)}}" class="number">{{($pagination->currentPage() - 1)}}</a>
                    @endif
                    @if($pagination->currentPage() > 2)
                        <a href="?page={{$pagination->currentPage()}}" class="number active">{{$pagination->currentPage()}}</a>
                    @endif
                    @if($pagination->lastPage() >= ($pagination->currentPage() + 1) && $pagination->currentPage() >= 3 && $pagination->lastPage() > 3)
                        <a href="?page={{($pagination->currentPage() + 1)}}" class="number">{{($pagination->currentPage() + 1)}}</a>
                    @endif
                    @if($pagination->lastPage() > ($pagination->currentPage() + 1) && ($pagination->currentPage() !== 2 || $pagination->lastPage() > 4) && $pagination->lastPage() > ($pagination->currentPage() + 2))
                        ...
                    @elseif($pagination->lastPage() > 3 && ($pagination->lastPage() - 1) > ($pagination->currentPage() + 1))
                        <a href="?page={{($pagination->lastPage() - 1)}}" class="second-last-page-num number">{{($pagination->lastPage() - 1)}}</a>
                    @endif
                    @if($pagination->lastPage() > 2 && $pagination->lastPage() > ($pagination->currentPage() + 1))
                        <a href="?page={{$pagination->lastPage()}}" class="last-page-num number">{{$pagination->lastPage()}}</a>
                    @endif
                <a href="?page={{$pagination->lastPage()}}" class="text">Last Page</a>
            @endif
        </div>
    </div>
@endsection