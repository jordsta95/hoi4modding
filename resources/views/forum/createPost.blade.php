@extends('templates.tool')

@section('title')
Create new post for {{$parent->name}} - HOI4 Modding
@endsection

@section('extra-meta')
<link href="/css/trumbowyg.min.css" rel='stylesheet' type='text/css'>
<script src="/js/trumbowyg.min.js"></script>
<script>$(window).load(function(){$('.wysiwyg').trumbowyg({
    btns: [
        ['viewHTML'],
        ['undo', 'redo'],
        ['formatting'],
        ['strong', 'em', 'del'],
        ['link'],
        ['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['fullscreen']
    ],
    autogrow: true
});
$('.trumbowyg-textarea').attr('name', 'wysiwyg');
});</script>
@endsection

@section('description')
Create a new forum post in {{$parent->name}}'s forum, on the HOI4 Modding forum
@endsection

@section('menu')
    @include('menus/generic')
@endsection

@section('content')
    <div class="forum"> 
        <div class="breadcrumb">
            <a href="/forum">Forum</a> -> <a href="/forum/{{$parent->slug}}">{{$parent->name}}</a> -> New Post
        </div> 
        <div class="post-comment">
            <form method="POST">
                {{ csrf_field() }}
                <div class="post-title">
                    <p>Title:</p>
                    <input name="title" required="required" placeholder="Post title...">
                </div>
                <div class="post-content">
                    <p>Content</p>
                    <div class="wysiwyg" placeholder="Post content..." name="wysiwyg"></div>
                </div>
                <button class="submit-post">Create New Post</button>
            </form>
        </div>
    </div>
    
@endsection