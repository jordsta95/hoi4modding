@extends('templates.tool')

@section('title')
Create new post for {{$parent->name}} - HOI4 Modding
@endsection

@section('extra-meta')
<link href="/css/trumbowyg.min.css" rel='stylesheet' type='text/css'>
<script src="/js/trumbowyg.min.js"></script>
<script src="https://unpkg.com/bowser@2.7.0/es5.js"></script>
<script>$(window).load(function(){$('.wysiwyg').trumbowyg({
	btns: [
		['viewHTML'],
		['undo', 'redo'],
		['formatting'],
		['strong', 'em', 'del'],
		['link'],
		['insertImage'],
		['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
		['unorderedList', 'orderedList'],
		['horizontalRule'],
		['removeformat'],
		['fullscreen']
	],
	autogrow: true
});
$('.trumbowyg-textarea').attr('name', 'wysiwyg');

var result = bowser.getParser(window.navigator.userAgent);
$('.browser').val(result.parsedResult.browser.name +" v" + result.parsedResult.browser.version + " on " + result.parsedResult.os.name);
});</script>
@endsection

@section('description')
Create a new forum post in {{$parent->name}}'s forum, on the HOI4 Modding forum
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<div class="forum"> 
		<div class="breadcrumb">
			<a href="/forum">Forum</a> -> <a href="/forum/{{$parent->slug}}">{{$parent->name}}</a> -> New Post
		</div> 
		<div class="post-comment">
			<form method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="post-title">
					<p>Short decription of error:</p>
					<input name="title" required="required" placeholder="E.g. Unable to edit focus">
					<small>Short title, don't go in-depth</small>
				</div>
				<hr>
				<div class="post-title">
					<p>Link to where error occured:</p>
					<input name="link" required="required" placeholder="E.g. https://hoi4modding.com/focus-tree/edit/1" value="{{ (isset($data['bug'])) ? urldecode($data['bug']) : '' }}">
					<small>This field will auto-fill if you press the "Bug Report" button in a tool</small>
				</div>
				<hr>
				<div class="post-title">
					<p>Time error occured:</p>
					<input name="time" required="required" placeholder="Time is UTC" value="{{ date('H:i d/M/Y') }}">
					<small>Time in UTC, current time is: {{ date('H:i d/M/Y') }}</small>
				</div>
				<hr>
				<div class="post-title">
					<p>Long description of error:</p>
					<textarea style="display: block;width: 100%; min-height: 100px;" name="content" required="required" placeholder="E.g. When trying to edit focus 'Example focus' nothing happens when I press submit."></textarea>
					<small>Add a bit more detail of the error encountered</small>
				</div>
				<div class="post-title">
					<p>(Optional) Images:</p>
					<input type="file" name="images[]" multiple="multiple">
					<small>Any images associated with your bug, Max file size (2MB)</small>
				</div>
				<div style="display: none">
					<input type="text" class="browser" name="browser" style="display: none;">
				</div>
				
				<button class="submit-post">Create New Post</button>
			</form>
		</div>
	</div>
	
@endsection