@extends('templates.tool')

@section('title')
HOI4 Modding - Tools for modding Hearts of Iron IV
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<div class="header-main">
		<h1>HOI4 Modding</h1>
		<h2>Modding Hearts of Iron IV could never be easier</h2>
	</div>
	<div class="body-main content-left">
		<div class="left">
			<h3>What is Hearts of Iron IV?</h3>
			<p>Hearts of Iron 4 is a video game created by Paradox Interative, which focuses on World War 2, and allows you to command a nation of your choosing to try and win the WW2, or rule the world.</p>
			<p>Hearts of Iron 4 (HOI4) has had great modding support from day 1 of release with many mods, small and large alike, being released. This site is for those people. The ones who wish to create mods.</p>
		</div>
		<div class="right">
			<h5>Planned Features</h5>
			<ul>
				<li>Idea/National Spirit Creator Tool</li>
				<li>State Editor Tool</li>
				<li>Research Creator Tool</li>
			</ul>
		</div>
	</div>
	<div class="body-main content-right">
		<div class="left">
			<h5>Currently Available Tools</h5>
			<ul>
				<li><a href="/focus-tree/create">National Focus Tree Creator</a></li>
				<li><a href="/flag/">Flag Creator</a></li>
				<li><a href="/events/">Event Creator</a></li>
				<li><a href="/country/">Country Creator</a></li>
				<li><a href="/decision">Decision Creator</a><sup>*</sup></li>
			</ul>
			<small>Tools marked with a * are not integrated with this site's account system, meaning progress is only saved locally</small>
		</div>
		<div class="right">
			<h3>What is this site?</h3>
			<p>This site is an ever growing tool that allows users to create and collaborate in creating mods for Hearts of Iron IV, whilst also serving as a hub for modding help, discussion, and general HOI4 conversations.</p>
		</div>
	</div>
	<div class="body-main content-left">
		<div class="left">
			<h3>Patreon Patrons?</h3>
			<p>These awesome people help keep this site alive</p>
			<ul>
				@foreach(patreons() as $user)
					<li><a href="/users/{{ $user->id }}">{{ $user->username }}</a></li>
				@endforeach
			</ul>
		</div>
		<div class="right">
			
		</div>
	</div>
@endsection

