@extends('templates.tool-focustree')

@section('title')
{{$tree->mod->mod_name}}'s Focus Tree for {{$tree->country_tag}} - HOI4 Modding
@endsection


@section('description')
{{$tree->mod->mod_description}}
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@foreach($tree->focuses as $focus)
		<div class="focus-content" data-focus-id="{{$focus->id}}" data-x="{{$focus->focus_x}}" data-y="{{$focus->focus_y}}">
			<img src="{{ storage( $focus->focus_gfx, 'public/storage') }}">
			<p class="focus-title">{{$focus->focus_title}}</p>
			<div class="focus-description">{{$focus->focus_description}}</div>
			<div class="focus-json">
				<?php
					$data = $focus;
					unset($data['id']);
					unset($data['focus_tree_id']);
					unset($data['focus_title']);
					unset($data['focus_description']);
					unset($data['focus_gfx']);
					unset($data['created_at']);
					unset($data['updated_at']);
					echo $data;
				?>
			</div>
		</div>
	@endforeach
	@if($can_edit)
		<a href="/focus-tree/edit/{{$tree->id}}" class="focus-user-edit">Edit This Tree</a>
	@endif
@endsection

