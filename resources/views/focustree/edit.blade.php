@extends('templates.tool-focustree')

@section('title')
{{$tree->mod->mod_name}}'s Focus Tree for {{$tree->country_tag}} - HOI4 Modding
@endsection


@section('description')
{{$tree->mod->mod_description}}
@endsection

@section('menu')
	@include('menus/tool-focus')
@endsection

@section('content')
	@foreach($tree->focuses as $focus)
		<div class="focus-content" data-x="{{$focus->focus_x}}" data-y="{{$focus->focus_y}}" data-focus-id="{{$focus->id}}">
			<img src="{{ storage( $focus->focus_gfx, 'public/storage') }}" class="focus-image">
			<p class="focus-title">{{$focus->focus_title}}</p>
			<div class="focus-description">
				<p class="edit-focus">Edit</p>
				<div>{{$focus->focus_description}}</div>
			</div>
			<div class="focus-json">
				<?php
					$data = $focus;
					unset($data['id']);
					unset($data['focus_tree_id']);
					unset($data['focus_title']);
					unset($data['focus_description']);
					unset($data['focus_gfx']);
					unset($data['created_at']);
					unset($data['updated_at']);
					echo $data;
				?>
			</div>
		</div>
	@endforeach
	<div class="focus-tree-raw-wrapper">
		<div class="focus-tree-raw closed"><div><button class="generic_button showFocusError">Only show errors</button></div>{!! $rawtext !!}</div>
		<div class="focus-tree-raw-toggle"><i class="fa fa-chevron-up"></i></div>
	</div>
@endsection

