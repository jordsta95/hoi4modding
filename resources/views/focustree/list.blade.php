@extends('templates.tool')

@section('title')
Focus Tree List - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<p style="text-align: right"><a href="/focus-tree/create" class="generic_button">Create Focus Tree</a></p>
	@if($myMods)
		<h3>My Mods With Focus Trees</h3>
		<div class="modlist">
			@foreach($myMods as $mod)
				<div class="mod">
					<div class="image">
						<img src="{{ storage($mod->mod_logo, '/public/storage/') }}">
					</div>
					<div class="info">
						<a href="/mod/{{$mod->id}}"><h3>{{$mod->mod_name}}</h3></a>
						<div class="focuses">
							<p>Focus Trees</p>
							@foreach($mod->focusTrees as $tree)
								<div class="focustree">
									<a href="/focus-tree/view/{{$tree->id}}">{{$tree->tree_id}} ({{$tree->country_tag}}) - {{count($tree->focuses)}} focuses</a>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@endif
@endsection

