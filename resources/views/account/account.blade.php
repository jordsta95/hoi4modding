@extends('templates.tool')

@section('title')
{{ $user->username }}'s profile - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<form method="POST" enctype="multipart/form-data"> 
		{{ csrf_field() }}
		<div class="user-info">
			<div class="image"><img src="{{ storage($user->avatar, '/public/storage/') }}" id="display-gfx">
				<br>Avatar:<br>
				<input type="file" accept="image/x-png" class="to_base_64" data-alters="display-gfx" name="avatar">
			</div>
			<div class="info">
				<h1>{{$user->username}}</h1>
				<p>Name:<br><input name="name" placeholder="Your Name" value="{{$user->name}}"></p>
				<p>Description:<br><textarea name="description" placeholder="Short description about yourself">{{ $user->description }}</textarea> 
				<p>Youtube link:<br><input name="youtube" placeholder="Youtube URL" value="{{$user->youtube_account}}"></p> 
				{{count($user->mods)}} Mod<?php if(count($user->mods) !== 1): ?>s<?php endif;?>
			</div>
		</div>
		<button class="generic_button">Update Account Info</button>
	</form>
@endsection

