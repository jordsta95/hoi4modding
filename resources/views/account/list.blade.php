@extends('templates.tool')

@section('title')
User List - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@foreach($users as $user)
		<div class="user">
			<a href="/users/{{$user->id}}"><h3>{{$user->username}}</h3></a>
		</div>
	@endforeach
@endsection

