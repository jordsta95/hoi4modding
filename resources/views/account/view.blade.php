@extends('templates.tool')

@section('title')
{{ $user->username }}'s profile - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<div class="user-info">
		<div class="image"><img src="{{ storage($user->avatar, '/public/storage/') }}"></div>
		<div class="info">
			@if(Auth::user())
				@if(Auth::user()->id == $user->id)
					<a href="/account" class="generic_button">Edit Account Details</a>
				@endif
			@endif
			<h1>{{$user->username}}</h1>
			@if(!empty($user->description))
				<div>
					{{$user->description}}
				</div>
			@endif
			@if(!empty($user->youtube_account))
				<p><i class="fa fa-youtube-play"></i> <a href="{{$user->youtube_account}}">{{$user->youtube_account}}</a></p> 
			@endif
			{{count($user->mods)}} Mod<?php if(count($user->mods) !== 1): ?>s<?php endif;?>
		</div>
	</div>
	@include('account.partials.tabs')
@endsection

