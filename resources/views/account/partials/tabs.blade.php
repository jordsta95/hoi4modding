	<div class="tabs">
	@if(count($user->mods) > 0)
		<div class="tab user-mods active">Mods</div>
	@endif
	@if($user->post_count > 0)
		<div class="tab user-forum @if(count($user->mods) == 0) active @endif">Forum</div>
	@endif
	@if(!empty($user->youtube_account))
		<div class="tab user-videos @if(count($user->mods) == 0 && $user->post_count == 0) active @endif">Videos</div>
	@endif
	</div>
	@if(count($user->mods) > 0)
		<div class="user-mods tabbed active">
			<h2>Mods</h2>
			@foreach($user->mods as $mod)
				<div class="mod">
					<a href="/mod/{{$mod->id}}">
						<div class="image"><img src="{{storage($mod->mod_logo, '/public/storage/') }}"></div>
						<div class="content">
							<h3>{{$mod->mod_name}}</h3>
							<h5>{{$mod->pivot->role}}</h5>
							<div>{{$mod->mod_description}}</div>
						</div>
					</a>
				</div>
			@endforeach	
		</div>
	@endif
	@if($user->post_count > 0)
		<div class="user-forum tabbed forum @if(count($user->mods) == 0) active @endif">
			<h2>Forum</h2>
			@if($user->forumPosts)
				<h3>Threads started ({{count($user->forumPosts)}})</h3>
				@foreach($user->forumPosts()->limit(3)->get() as $post)
					<div class="forum-block">
			            <div class="header">
			                <a href="/forum/{{$post->forum->slug}}/{{$post->id}}-{{$post->slug}}">{{$post->title}}</a>
			            </div>
			            <div class="description post">
			                <div class="left">{{substr(strip_tags($post->content), 0, 140)}}...</div>
			                <div class="right">
			                    Last post:<br>
			                    {{ date('d/M/Y', strtotime($post->updated_at)) }}
			                </div>
			            </div>
			        </div>
				@endforeach
			@endif
			@if($user->forumComments)
				<h3>Comments posted ({{count($user->forumComments)}})</h3>
				@foreach($user->forumComments()->limit(3)->get() as $comment)
					<a href="/forum/{{$comment->post->forum->slug}}/{{$comment->post->id}}-{{$comment->post->slug}}">
						<div class="forum-post" id="{{$comment->id}}">
				            <div class="post-info"> 
				                <div class="post-content">
				                	<h4>{{$comment->post->title}}:</h4>
				                	{!! strip_tags($comment->content, '<p><br><a><i><em><b><strong><h1><h2><h3><h4><h5><h6><img><font><span><hr><ul><li><ol><table><thead><tbody><tr><td><th>') !!}
				                </div>
				            </div>
				        </div>
			    	</a>
				@endforeach
			@endif
		</div>
	@endif
	@if(!empty($user->youtube_account))
		<div class="user-videos tabbed @if(count($user->mods) == 0 && $user->post_count == 0) active @endif">
			@foreach($videos as $video)
				<iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $video }}?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			@endforeach
		</div>
	@endif