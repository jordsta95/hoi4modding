@extends('templates.tool')

@section('title')
{{ $user->username }}'s media - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<form method="POST">
		{{ csrf_field() }}
		<div class="grid">
			<div class="grid_start_1 grid_span_4">Image</div>
			<div class="grid_start_5 grid_span_3">Delete?</div>
		</div>
		@foreach($user->media as $media)
			<div class="grid">
				<div class="grid_start_1 grid_span_4">
					<label for="media-{{ $media->id }}">
						<img src="{{ route('media.findSized', ['id' => $media->id, 'size' => 'event']) }}">
					</label>
				</div>
				<div class="grid_start_5 grid_span_3"><input type="checkbox" name="deleteMedia[]" value="{{ $media->id }}" id="media-{{ $media->id }}"></div>
			</div>
		@endforeach
		<p><button class="generic_button">Submit</button></p>
	</form>
@endsection

