@extends('templates.tool')

@section('title')
Your Notifications - HOI4 Modding
@endsection


@section('description')
Notifications on your account
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@if(empty($notifications))
		<h3>You have no notifications</h3>
	@else
		<p style="text-align: right;"><a href="?markAllAsRead=true">Mark all as read</a></p>
		<h3>Unread forum posts</h3>
		<ul>
			@foreach($notifications as $note)
				<li><a href="/forum/{{$note->fslug}}/{{$note->id}}-{{$note->pslug}}">{{$note->title}}</a></li>
			@endforeach
		</ul>
	@endif
@endsection

