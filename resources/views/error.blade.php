@extends('templates.tool')

@section('title')
Error - Tools for modding Hearts of Iron IV
@endsection


@section('description')
Error page
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<div class="header-main">
		<h1>Whoops, looks like you have encountered an error</h1>
	</div>
	<div class="body-main">
		<p>You are seeing this page, because you have done one of the following:</p>
		<ol>
			<li>Tried to navigate to a page which does not exist</li>
			<li>Tried to navigate to a page which you do not have sufficient permissions to access</li>
			<li>Encountered a bug in the website, which has caused it to return this error</li>
		</ol>
		<p>If you believe you have encoutered this page because of point 3, please leave a bug report on the <a href="/forum/bug-reports">bug report section of the forum</a></p>
	</div>
@endsection

