@extends('templates.tool-country')

@section('title')
{{$mod->mod_name}}'s Country - HOI4 Modding
@endsection


@section('description')
{{$mod->mod_description}}
@endsection

@section('menu')
	@include('menus/tool-country')
@endsection

@section('popups')
@endsection

@section('content')
<div id="country-id" data-country-id="{{$country->id}}"></div>
	<div class="country-tabs" data-tab-type="date">
		<?php $n = 0; $d = null; $i = null; ?>
		@foreach($country->dates as $date)
			<div class="country-tab @if($n == 0) active<?php $n++;$d = $date; ?> @endif" id="{{$date->id}}">{{$date->year}}.{{$date->month}}.{{$date->day}}</div>
		@endforeach
	</div>
	<div class="country-tabs" data-tab-type="ideology">
		@include('countries.partials.ideology-tabs', ['ideologies' => $country->dates[0]->ideologies, 'active' => $country->dates[0]->ideologies[0]->id])
	</div>
	<div class="country-header">
		<div class="country-flag">
		</div>
	</div>
	<div class="country-popup">
		<div class="close-bar">
			<i class="fa fa-times"></i>
		</div>

		<div class="country-leader-container readOnly">
			@include('countries.partials.leader', ['ideology' => $country->dates[0]->ideologies[0]])
		</div>

		<div class="country-ideas">
			<span class="row-label">
				Government
			</span>
			<div class="row">
				<span class="country-idea mobilization" data-idea="mobilization-law"></span>
				<span class="country-idea trade" data-idea="trade-law"></span>
				<span class="country-idea economy" data-idea="economy-law"></span>
				<span class="country-idea cabinet open" for="choose-person-idea" data-needs-additional="yes" data-idea="cabinet">@if(count($country->cabinet)) {{count($country->cabinet)}} @else 0 @endif</span>
				<span class="country-idea cabinet open" for="choose-person-idea" data-needs-additional="yes" data-idea="cabinet">@if(count($country->cabinet)) {{count($country->cabinet)}} @else 0 @endif</span>
				<span class="country-idea cabinet open" for="choose-person-idea" data-needs-additional="yes" data-idea="cabinet">@if(count($country->cabinet)) {{count($country->cabinet)}} @else 0 @endif</span>
			</div>
			<span class="row-label">
				Research & Production
			</span>
			<div class="row">
				<span class="country-idea tank open" for="choose-non-person-idea" data-needs-additional="yes" data-idea="tank-designer">@if(count($country->tank)) {{count($country->tank)}} @else 0 @endif</span>
				<span class="country-idea naval open" for="choose-non-person-idea" data-needs-additional="yes" data-idea="ship-designer">@if(count($country->ship)) {{count($country->ship)}} @else 0 @endif</span>
				<span class="country-idea aviation open" for="choose-non-person-idea" data-needs-additional="yes" data-idea="air-designer">@if(count($country->air)) {{count($country->air)}} @else 0 @endif</span>
				<span class="country-idea arms open" for="choose-non-person-idea" data-needs-additional="yes" data-idea="gun-designer">@if(count($country->gun)) {{count($country->gun)}} @else 0 @endif</span>
				<span class="country-idea technology open" for="choose-non-person-idea" data-needs-additional="yes" data-idea="industrial-designer">@if(count($country->industrial)) {{count($country->industrial)}} @else 0 @endif</span>
				<span class="country-idea theorist open" for="choose-person-idea" data-needs-additional="yes" data-idea="doctrine-advisor">@if(count($country->doctrine)) {{count($country->doctrine)}} @else 0 @endif</span>
			</div>
			<span class="row-label">
				Military Staff
			</span>
			<div class="row">
				<span class="country-idea army open" for="choose-person-idea" data-needs-additional="yes" data-idea="army-general-staff">@if(count($country->armygs)) {{count($country->armygs)}} @else 0 @endif</span>
				<span class="country-idea naval open" for="choose-person-idea" data-needs-additional="yes" data-idea="navy-general-staff">@if(count($country->navygs)) {{count($country->navygs)}} @else 0 @endif</span>
				<span class="country-idea aviation open" for="choose-person-idea" data-needs-additional="yes" data-idea="air-general-staff">@if(count($country->airgs)) {{count($country->airgs)}} @else 0 @endif</span>
				<span class="country-idea high-command open" for="choose-person-idea" data-needs-additional="yes" data-idea="military-advisor">@if(count($country->military)) {{count($country->military)}} @else 0 @endif</span>
				<span class="country-idea high-command open" for="choose-person-idea" data-needs-additional="yes" data-idea="military-advisor">@if(count($country->military)) {{count($country->military)}} @else 0 @endif</span>
				<span class="country-idea high-command open" for="choose-person-idea" data-needs-additional="yes" data-idea="military-advisor">@if(count($country->military)) {{count($country->military)}} @else 0 @endif</span>
			</div>
		</div>
	</div>

	<div class="country-extra-stuff">
		<div class="country-box state-box readOnly">
			@include('countries.partials.states', ['date' => $country->dates[0]])
		</div>

		<div class="country-box">
			<h3>Generals/Field Marshals</h3>
			<div class="country-ground country-general-wrapper" data-type="ground">
				<div class="grid with-gap">
					<div class="grid_start_2 grid_span_5">Name</div>
					<div class="grid_start_7 grid_span_3">Image</div>
					<div class="grid_start_10 grid_span_3">Level</div>
				</div>
				@foreach($country->army as $general)
					<div class="grid with-gap" data-id="{{$general->id}}">
						<div class="grid_start_1 grid_span_1 removeState"></div>
						<div class="grid_start_2 grid_span_5 editGeneral">
							<span class="name" data-id="{{$general->id}}">{{$general->name}}</span>
							<?php $gid = $general->id; unset($general->id); unset($general->created_at); unset($general->updated_at); $general->type = 'ground'; $general->field_marshal = $general->is_field_marshal; ?>
							<textarea class="generalJSON" data-id="{{$gid}}" name="general[{{$gid}}][json]" style="display:none;">{{$general}}</textarea>
						</div>
						<div class="grid_start_7 grid_span_3"><img src="{{ storage( $general->gfx, 'public/storage') }}" class="general-icon" data-id="{{$gid}}"></div>
						<div class="grid_start_10 grid_span_3 generalLevel" data-id="{{$gid}}">{{$general->level}}</div>
					</div>
				@endforeach
				
			</div>
		</div>

		<div class="country-box">
			<h3>Admirals</h3>
			<div class="country-navy country-general-wrapper" data-type="navy">
				<div class="grid with-gap">
					<div class="grid_start_2 grid_span_5">Name</div>
					<div class="grid_start_7 grid_span_3">Image</div>
					<div class="grid_start_10 grid_span_3">Level</div>
				</div>
				@foreach($country->navy as $general)
					<div class="grid with-gap" data-id="{{$general->id}}">
						<div class="grid_start_1 grid_span_1"></div>
						<div class="grid_start_2 grid_span_5 editGeneral">
							<span class="name" data-id="{{$general->id}}">{{$general->name}}</span>
							<?php $gid = $general->id; unset($general->id); unset($general->created_at); unset($general->updated_at); $general->type = 'navy'; $general->field_marshal = $general->is_field_marshal; ?>
							<textarea class="generalJSON" data-id="{{$gid}}" name="general[{{$gid}}][json]" style="display:none;">{{$general}}</textarea>
						</div>
						<div class="grid_start_7 grid_span_3"><img src="{{ storage( $general->gfx, 'public/storage') }}" class="general-icon" data-id="{{$gid}}"></div>
						<div class="grid_start_10 grid_span_3 generalLevel" data-id="{{$gid}}">{{$general->level}}</div>
					</div>
				@endforeach
			</div>
		</div>

		<div class="country-box">
			<h3>Research</h3>
			<div class="country-research-items">
				<div class="grid with-gap">
					<div class="grid_start_2 grid_span_11">Name</div>
				</div>
				@if(!empty($country->dates[0]->parsedResearch))
					@foreach($country->dates[0]->parsedResearch as $r)
						@if(!empty($r))
							<div class="grid with-gap" data-id="new">
								<div class="grid_start_1 grid_span_1"></div>
								<div class="grid_start_2 grid_span_11 research-item" data-id="{{$r}}">{{$research->lang[$r]}}</div>
							</div>
						@endif
					@endforeach
				@endif
			</div>
		</div>
	</div>
@endsection

