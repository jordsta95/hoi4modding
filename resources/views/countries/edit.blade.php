@extends('templates.tool-country')

@section('title')
{{$mod->mod_name}}'s Country - HOI4 Modding
@endsection


@section('description')
{{$mod->mod_description}}
@endsection

@section('menu')
	@include('menus/tool-country')
@endsection

@section('popups')
	<div class="focus-panel" id="country-traits">
		<div class="panel-head">
			<i class="fa fa-times close" for="country-traits"></i>
			Choose Traits
		</div>
		<div class="panel-body">
			<div class="search-traits">
				Search<br>
				<input placeholder="Search traits by name or effect" id="search-traits">
			</div> 
			@foreach($traits->traits as $id => $effect)
				<div class="country-trait" data-id="{{$id}}">
					@if(isset($traits->lang[$id]))
						<div class="name">
							{{$traits->lang[$id]}}
						</div>
						<div class="effect">
							<textarea disabled >{{$effect}}</textarea>
						</div>
					@endif
				</div>
			@endforeach
		</div>
	</div>
	<div class="focus-panel" id="country-research">
		<div class="panel-head">
			<i class="fa fa-times close" for="country-research"></i>
			Choose Research
		</div>
		<div class="panel-body">
			<div class="search-traits">
				Search<br>
				<input placeholder="Search research by name or effect" id="search-research">
			</div> 
			@foreach($research->tech as $id => $effect)
				<div class="country-research" data-id="{{$id}}">
					@if(isset($research->lang[$id]))
						<div class="name">
							{{$research->lang[$id]}}
						</div>
						<div class="effect">
							<textarea disabled >{{$effect}}</textarea>
						</div>
					@endif
				</div>
			@endforeach
		</div>
	</div>
	<div class="focus-panel" id="country-leader-traits">
		<div class="panel-head">
			<i class="fa fa-times close" for="country-leader-traits"></i>
			Choose Traits
		</div>
		<div class="panel-body">
			<div class="search-traits">
				Search<br>
				<input placeholder="Search traits by name or effect" id="search-leader-traits">
			</div> 
			@foreach($leaderTraits->traits as $id => $effect)
				<div class="country-trait leader-trait" data-id="{{$id}}">
					@if(isset($traits->lang[$id]))
						<div class="name">
							{{$traits->lang[$id]}}
						</div>
						<div class="effect">
							<textarea disabled >{{$effect}}</textarea>
						</div>
					@endif
				</div>
			@endforeach
		</div>
	</div>
@endsection

@section('content')
<div id="country-id" data-country-id="{{$country->id}}"></div>
	<div class="country-tabs" data-tab-type="date">
		<?php $n = 0; $d = null; $i = null; ?>
		@foreach($country->dates as $date)
			<div class="country-tab @if($n == 0) active<?php $n++;$d = $date; ?> @endif" id="{{$date->id}}">{{$date->year}}.{{$date->month}}.{{$date->day}}</div>
		@endforeach
	</div>
	<div class="country-tabs" data-tab-type="ideology">
		@include('countries.partials.ideology-tabs', ['ideologies' => $country->dates[0]->ideologies, 'active' => $country->dates[0]->ideologies[0]->id])
	</div>
	<div class="country-header">
		<div class="country-flag">
		</div>
	</div>
	<div class="country-popup">
		<div class="close-bar">
			<i class="fa fa-times"></i>
		</div>

		<div class="country-leader-container">
			@include('countries.partials.leader', ['ideology' => $country->dates[0]->ideologies[0]])
		</div>

		<div class="country-ideas">
			<span class="row-label">
				Government
			</span>
			<div class="row">
				<span class="country-idea mobilization" data-idea="mobilization-law"></span>
				<span class="country-idea trade" data-idea="trade-law"></span>
				<span class="country-idea economy" data-idea="economy-law"></span>
				<span class="country-idea cabinet open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="cabinet">@if(count($country->cabinet)) {{count($country->cabinet)}} @else 0 @endif</span>
				<span class="country-idea cabinet open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="cabinet">@if(count($country->cabinet)) {{count($country->cabinet)}} @else 0 @endif</span>
				<span class="country-idea cabinet open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="cabinet">@if(count($country->cabinet)) {{count($country->cabinet)}} @else 0 @endif</span>
			</div>
			<span class="row-label">
				Research & Production
			</span>
			<div class="row">
				<span class="country-idea tank open" for="edit-ideas" data-for="choose-non-person-idea" data-needs-additional="yes" data-idea="tank-designer">@if(count($country->tank)) {{count($country->tank)}} @else 0 @endif</span>
				<span class="country-idea naval open" for="edit-ideas" data-for="choose-non-person-idea" data-needs-additional="yes" data-idea="ship-designer">@if(count($country->ship)) {{count($country->ship)}} @else 0 @endif</span>
				<span class="country-idea aviation open" for="edit-ideas" data-for="choose-non-person-idea" data-needs-additional="yes" data-idea="air-designer">@if(count($country->air)) {{count($country->air)}} @else 0 @endif</span>
				<span class="country-idea arms open" for="edit-ideas" data-for="choose-non-person-idea" data-needs-additional="yes" data-idea="gun-designer">@if(count($country->gun)) {{count($country->gun)}} @else 0 @endif</span>
				<span class="country-idea technology open" for="edit-ideas" data-for="choose-non-person-idea" data-needs-additional="yes" data-idea="industrial-designer">@if(count($country->industrial)) {{count($country->industrial)}} @else 0 @endif</span>
				<span class="country-idea theorist open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="doctrine-advisor">@if(count($country->doctrine)) {{count($country->doctrine)}} @else 0 @endif</span>
			</div>
			<span class="row-label">
				Military Staff
			</span>
			<div class="row">
				<span class="country-idea army open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="army-general-staff">@if(count($country->armygs)) {{count($country->armygs)}} @else 0 @endif</span>
				<span class="country-idea naval open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="navy-general-staff">@if(count($country->navygs)) {{count($country->navygs)}} @else 0 @endif</span>
				<span class="country-idea aviation open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="air-general-staff">@if(count($country->airgs)) {{count($country->airgs)}} @else 0 @endif</span>
				<span class="country-idea high-command open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="military-advisor">@if(count($country->military)) {{count($country->military)}} @else 0 @endif</span>
				<span class="country-idea high-command open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="military-advisor">@if(count($country->military)) {{count($country->military)}} @else 0 @endif</span>
				<span class="country-idea high-command open" for="edit-ideas" data-for="choose-person-idea" data-needs-additional="yes" data-idea="military-advisor">@if(count($country->military)) {{count($country->military)}} @else 0 @endif</span>
			</div>
		</div>
	</div>

	<div class="country-extra-stuff">
		<div class="country-box state-box">
			@include('countries.partials.states', ['date' => $country->dates[0]])
		</div>

		<div class="country-box">
			<h3>Generals/Field Marshals</h3>
			<div class="country-ground country-general-wrapper" data-type="ground">
				<div class="grid with-gap">
					<div class="grid_start_2 grid_span_5">Name</div>
					<div class="grid_start_7 grid_span_3">Image</div>
					<div class="grid_start_10 grid_span_3">Level</div>
				</div>
				@foreach($country->army as $general)
					<div class="grid with-gap" data-id="{{$general->id}}">
						<div class="grid_start_1 grid_span_1 removeState">x<input style="display: none;" type="checkbox" value="1" name="general[{{$general->id}}][remove]" class="generalJSON removeStateOption"></div>
						<div class="grid_start_2 grid_span_5 editGeneral">
							<span class="name" data-id="{{$general->id}}">{{$general->name}}</span>
							<?php $gid = $general->id; unset($general->id); unset($general->created_at); unset($general->updated_at); $general->type = 'ground'; $general->field_marshal = $general->is_field_marshal; ?>
							<textarea class="generalJSON" data-id="{{$gid}}" name="general[{{$gid}}][json]" style="display:none;">{{$general}}</textarea>
						</div>
						<div class="grid_start_7 grid_span_3"><img src="{{ storage( $general->gfx, 'public/storage') }}" class="general-icon" data-id="{{$gid}}"></div>
						<div class="grid_start_10 grid_span_3 generalLevel" data-id="{{$gid}}">{{$general->level}}</div>
					</div>
				@endforeach
				
			</div>
			<div class="generic_button open" for="country-military" data-type="ground">Add general/field marshal</div>
		</div>

		<div class="country-box">
			<h3>Admirals</h3>
			<div class="country-navy country-general-wrapper" data-type="navy">
				<div class="grid with-gap">
					<div class="grid_start_2 grid_span_5">Name</div>
					<div class="grid_start_7 grid_span_3">Image</div>
					<div class="grid_start_10 grid_span_3">Level</div>
				</div>
				@foreach($country->navy as $general)
					<div class="grid with-gap" data-id="{{$general->id}}">
						<div class="grid_start_1 grid_span_1 removeState">x<input style="display: none;" type="checkbox" value="1" name="general[{{$general->id}}][remove]" class="generalJSON removeStateOption"></div>
						<div class="grid_start_2 grid_span_5 editGeneral">
							<span class="name" data-id="{{$general->id}}">{{$general->name}}</span>
							<?php $gid = $general->id; unset($general->id); unset($general->created_at); unset($general->updated_at); $general->type = 'navy'; $general->field_marshal = $general->is_field_marshal; ?>
							<textarea class="generalJSON" data-id="{{$gid}}" name="general[{{$gid}}][json]" style="display:none;">{{$general}}</textarea>
						</div>
						<div class="grid_start_7 grid_span_3"><img src="{{ storage( $general->gfx, 'public/storage') }}" class="general-icon" data-id="{{$gid}}"></div>
						<div class="grid_start_10 grid_span_3 generalLevel" data-id="{{$gid}}">{{$general->level}}</div>
					</div>
				@endforeach
			</div>
			<div class="generic_button open" for="country-military" data-type="navy">Add admiral</div>
		</div>

		<div class="country-box">
			<h3>Research</h3>
			<div class="country-research-items">
				<div class="grid with-gap">
					<div class="grid_start_2 grid_span_11">Name</div>
				</div>
				@if(!empty($country->dates[0]->parsedResearch))
					@foreach($country->dates[0]->parsedResearch as $r)
						@if(!empty($r))
							<div class="grid with-gap" data-id="new">
								<div class="grid_start_1 grid_span_1 removeState">x</div>
								<div class="grid_start_2 grid_span_11 research-item" data-id="{{$r}}">{{$research->lang[$r]}}</div>
							</div>
						@endif
					@endforeach
				@endif
			</div>
			<div class="generic_button open" for="country-research">Add researched tech</div>
		</div>
		{{--
		<div class="country-box">
			<h3>Divisions</h3>
			@include('countries.partials.divisions')
		</div>
		--}}
		
	</div>

	<div class="all-states" style="display: none;">
		{{mb_convert_encoding(file_get_contents(public_path().'/../states.json'),'HTML-ENTITIES', 'UTF-8')}}
	</div>
	<div class="map-wrapper" id="mapWrapper" data-zoom="1.0">
		<div class="close" for="mapWrapper"><i class="fa fa-times"></i></div>
		<input type="text" class="map-hide" placeholder="Hide nations by tag - TAG1|TAG2">
		<button class="generic_button" id="map-reshow">Reshow All Nations</button>
		<div class="state-overview" id="state-overview">
			<div class="close" for="state-overview"><i class="fa fa-times"></i></div>
			<div class="state-name">
				STATE NAME
			</div>
			<div class="state-info">
				<div class="wrapper core">
					<div class="checkbox">
						
					</div>
					<div class="label">
						Core State
					</div>
				</div>
				<div class="wrapper controls">
					<div class="checkbox">
						
					</div>
					<div class="label">
						Owned State
					</div>
				</div>
				<button class="generic_button update">Update</button>
				<button class="generic_button applyToAllStates">Apply to all states from nation</button>
			</div>
			<div class="state-buttons">
				<button id="hide-state">Hide State</button>
				<button id="hide-tag">Hide All States By Nation</button>
			</div>
		</div>
		<div class="zoom-options">
			<div class="increase">
				<i class="fa fa-plus"></i>
			</div>
			<div class="decrease">
				<i class="fa fa-minus"></i>
			</div>
		</div>
		<div class="map-inner dragscroll">
			@include('components/state-svg')
		</div>
	</div>
	
	
	<div class="map-wrapper" id="divisionMap" data-state="0">
		<div class="close" for="divisionMap"><i class="fa fa-times"></i></div>
		<div class="state-overview" id="state-overview">
			<div class="close" for="state-overview"><i class="fa fa-times"></i></div>
			<div class="state-name">
				STATE NAME
			</div>
			<div class="state-buttons">
				<button id="hide-state">Hide State</button>
				<button id="hide-tag">Hide All States By Nation</button>
			</div>
		</div>
		<div class="zoom-options">
			<div class="increase">
				<i class="fa fa-plus"></i>
			</div>
			<div class="decrease">
				<i class="fa fa-minus"></i>
			</div>
		</div>
		<div class="map-inner dragscroll">
			@include('components/state-svg')
		</div>
	</div>

	

@endsection

