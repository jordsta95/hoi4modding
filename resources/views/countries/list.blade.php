@extends('templates.tool')

@section('title')
Countries List - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<p style="text-align: right"><a href="/country/create" class="generic_button">Create Country</a></p>
	@if($myMods)
		<h3>My Mods With Countries</h3>
		<div class="modlist">
			@foreach($myMods as $mod)
				@if(isset($mod->countries[0]))
					<div class="mod">
						<div class="image">
							<img src="{{ storage($mod->mod_logo, '/public/storage/') }}">
						</div>
						<div class="info">
							<a href="/mod/{{$mod->id}}"><h3>{{$mod->mod_name}}</h3></a>
							<div class="focuses">
								<p>Countries</p>
								@foreach($mod->countries as $c)
									<div class="focustree">
										<a href="/country/view/{{$c->id}}">{{$c->name}} ({{$c->tag}})</a>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				@endif
			@endforeach
		</div>
	@endif
@endsection

