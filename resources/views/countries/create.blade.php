@extends('templates.tool-focustree')

@section('title')
Create country - HOI4 Modding
@endsection


@section('description')
Create countries for HOI IV
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@if(Auth::user())
		<div class="add_user_popup active create-focus" id="addUser">
			<div class="add_user_box stepped" data-step="1" data-last="3" data-is-last="0">
				<form method="POST" action="" enctype="multipart/form-data">
					<h3>Create New Country</h3>
					<div class="step step_1">
						{{ csrf_field() }}
						<h3>Select Mod Or Create A New One</h3>
						<p>
							<select name="mod_id">
								<option value="createNewMod">Create New Mod</option>
								@if(!empty($mods))
									@foreach($mods as $mod)
										<option value="{{$mod->id}}">{{$mod->mod_name}}</option>
									@endforeach
								@endif
							</select>
						</p>
					</div>
					<div class="step step_2 toggleNewModStep" data-new-mod-class="step step_2 toggleNewModStep" data-existing-mod-class="hidden_step toggleNewModStep">
						<div class="new-mod">
							<h3>New Mod Details</h3>
							<p><label>Mod Name</label></p>
							<p><input name="mod_name"></p>
							<p><label>Mod Description</label></p>
							<p><textarea name="mod_description"></textarea></p>
							<p><label>Mod Logo</label></p>
							<p><input name="mod_logo" type="file" accept="image/x-png"></p>
						</div>
						
					</div>
					<div class="step step_3 toggleNewModStep" data-new-mod-class="step step_3 toggleNewModStep" data-existing-mod-class="step step_2 toggleNewModStep">
						<div class="new-focus-tree">
							<p><label>Localisation Language</label></p>
							<select name="language">
								<option value="english">English</option>
								<option value="french">French</option>
								<option value="german">German</option>
								<option value="polish">Polish</option>
								<option value="braz_por">Portuguese</option>
								<option value="russian">Russian</option>
								<option value="spanish">Spanish</option>
							</select>

							<p><label>Country Name</label></p>
							<input name="name" placeholder="Germany" autocomplete="disabled">

							<p><label>Country Tag</label></p>
							<input name="tag" placeholder="GER" autocomplete="disabled">

							<p><label>Country Culture</label></p>
							<select name="culture">
								<option value="Europe">European</option>
								<option value="Asia">Asian</option>
								<option value="Africa">African</option>
								<option value="SouthAmerica">South American</option>
							</select>

							<p><label>Country Capital Region</label></p>
							<input name="capital" class="choose-state" id="capital" autocomplete="disabled">

							<p><label>Country Colour</label></p>
							<div class="colour-picker">
								<div class="sliders">
									<div id="red"></div>
									<div id="green"></div>
									<div id="blue"></div>
								 </div>
								 <div class="swatch">
									<div id="swatch" class="ui-widget-content ui-corner-all" data-name="colour"></div>
								</div>
							</div>
							<input name="colour" placeholder="0,0,0" autocomplete="disabled">

							<p><label>War Support</label></p>
							<input name="war_support" placeholder="10" type="number" step="1" min="0" max="100">

							<p><label>Stability</label></p>
							<input name="stability" placeholder="10" type="number" step="1" min="0" max="100">

							<p><label>Mod Is For? - This will determine start dates/ideologies</label></p>
							<select name="mod_type">
								<option value="default">Base Game</option>
								<option value="kaiserreich">Kaiserreich</option>
								<option value="endsieg">Endsieg</option>
							</select>
							<p><button class="generic_button">Submit</button></p>
						</div>
					</div>
					<div class="stepper">
						<div class="prev">Back</div>
						<div class="next">Next</div>
					</div>
				</form>
			</div>
		</div>
	@endif
@endsection

