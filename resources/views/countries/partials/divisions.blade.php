<div class="division-editor" id="divisionEditor">
	<div class="top">
		<input type="text" id="divisionName" placeholder="Divsion name" name="name">
		<div class="priority">
			<input type="radio" name="priority" value="0">
			<input type="radio" name="priority" value="1">
			<input type="radio" name="priority" value="2">
		</div>
	</div>
	<div class="support">
		<table class="divisionSupport">
			<tr>
				<td><input name="divisionSupport[00]"></td>
				<td><input name="divisionSupport[01]"></td>
				<td><input name="divisionSupport[02]"></td>
				<td><input name="divisionSupport[03]"></td>
				<td><input name="divisionSupport[04]"></td>
			</tr>
		</table>
	</div>
	<div class="units">
		<table class="divisionUnits">
			<tr>
				<td><input name="divisionUnit[00]"></td>
				<td><input name="divisionUnit[01]"></td>
				<td><input name="divisionUnit[02]"></td>
				<td><input name="divisionUnit[03]"></td>
				<td><input name="divisionUnit[04]"></td>
			</tr>
			<tr>
				<td><input name="divisionUnit[10]"></td>
				<td><input name="divisionUnit[11]"></td>
				<td><input name="divisionUnit[12]"></td>
				<td><input name="divisionUnit[13]"></td>
				<td><input name="divisionUnit[14]"></td>
			</tr>
			<tr>
				<td><input name="divisionUnit[20]"></td>
				<td><input name="divisionUnit[21]"></td>
				<td><input name="divisionUnit[22]"></td>
				<td><input name="divisionUnit[23]"></td>
				<td><input name="divisionUnit[24]"></td>
			</tr>
			<tr>
				<td><input name="divisionUnit[30]"></td>
				<td><input name="divisionUnit[31]"></td>
				<td><input name="divisionUnit[32]"></td>
				<td><input name="divisionUnit[33]"></td>
				<td><input name="divisionUnit[34]"></td>
			</tr>
			<tr>
				<td><input name="divisionUnit[40]"></td>
				<td><input name="divisionUnit[41]"></td>
				<td><input name="divisionUnit[42]"></td>
				<td><input name="divisionUnit[43]"></td>
				<td><input name="divisionUnit[44]"></td>
			</tr>
		</table>
	</div>
	<div class="name-options">
		<h4>Historical division names</h4>
		<p><small>Custom names for the nth division type. Put "%d" where the number should go. E.g. "Fantastic %dth" would display "Fantastic 5th" for the 5th division of this type</small></p>
		<div class="division-names">
			<p id="nameOption"><input type="text" name="nameOption[]" placeholder="%st Division"></p>
		</div>
		<p><span class="generic_button addDivisionName">Add division name</span></p>
	</div>
</div>