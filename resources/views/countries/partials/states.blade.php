<h3>State Selector</h3>
<p>States for start date: {{$date->year }}.{{$date->month}}.{{$date->day}}</p>
<div class="country-states">
	<div class="grid with-gap">
		<div class="grid_start_2 grid_span_5">Name</div>
		<div class="grid_start_7 grid_span_3">Core?</div>
		<div class="grid_start_10 grid_span_3">Controls?</div>
	</div>
	@foreach($date->states as $state)

	<div class="grid with-gap" data-id="{{$state->id}}" data-state-id="{{$state->state_id}}">
		<div class="grid_start_1 grid_span_1 removeState">x<input style="display: none;" type="checkbox" value="1" name="states[{{$state->id}}][remove]" class="stateValue removeStateOption"></div>
		<div class="grid_start_2 grid_span_5">
			{{$state->state_name}}
			<input class="stateValue" name="states[{{$state->id}}][state_name]" style="display:none;" value="{{$state->state_name}}">
			<input class="stateValue" name="states[{{$state->id}}][state_id]" style="display:none;" value="{{$state->state_id}}">
		</div>
		<div class="grid_start_7 grid_span_3"><input class="core stateValue" value="1" name="states[{{$state->id}}][core]" @if($state->core) checked="checked" @endif type="checkbox"></div>
		<div class="grid_start_10 grid_span_3"><input class="controls stateValue" value="1" name="states[{{$state->id}}][controls]" @if($state->controls) checked="checked" @endif type="checkbox"></div>
	</div>
	@endforeach
</div>
<div class="generic_button open" for="mapWrapper">Add state</div>