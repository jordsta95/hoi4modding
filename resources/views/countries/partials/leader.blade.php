<div class="country-leader">
	<div class="picture open" for="choose-leader" style="background-image: url('{{ storage( $ideology->leader_gfx, 'public/storage') }}');" data-image="{{ $ideology->leader_gfx }}"></div>
	<input class="name" value="{{ $ideology->leader_name }}" name="country_leader">
</div>
<div class="country-leader_side">
	{{--<div class="country-focus">Focuses</div>
	<div class="country-spirits">National Spirits</div>--}}
	<div class="support">
		{{ ucwords(str_replace('_', ' ', $ideology->ideology_name)) }} support:<br>
		<input name="ideology_support" type="number" min="0" max="100" step="0.01" value="{{$ideology->popularity}}">%
	</div>

	<div class="support">
		Country name:<br>
		<input name="country_name" value="{{ !empty($ideology->country_name) ? $ideology->country_name : $ideology->date->country->name }}" autocomplete="disabled">
	</div>
</div>