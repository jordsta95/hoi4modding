@foreach($ideologies as $ideology)
	<div class="country-tab @if($ideology->id == $active) active @endif" id="{{$ideology->id}}">{{ ucwords(str_replace('_', ' ', $ideology->ideology_name)) }}</div>
@endforeach