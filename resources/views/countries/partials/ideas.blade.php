@foreach($ideas as $idea)
	<div class="existing-idea" data-id="{{$idea->id}}" style="background-image: url({{ storage( $idea->gfx, 'public/storage') }})">
		<i class="fa fa-times"></i>
	</div>
@endforeach