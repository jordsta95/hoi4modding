@extends('templates.tool')

@section('title')
Ideas List - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<p style="text-align: right"><a href="/ideas/create" class="generic_button">Create Ideas</a></p>
	@if($myMods)
		<h3>My Mods With Ideas</h3>
		<div class="modlist">
			@foreach($myMods as $mod)
				@if(isset($mod->ideas[0]))
					<div class="mod">
						<div class="image">
							<img src="{{ storage($mod->mod_logo, '/public/storage/') }}">
						</div>
						<div class="info">
							<a href="/mod/{{$mod->id}}"><h3>{{$mod->mod_name}}</h3></a>
							<div class="focuses">
								<p>Ideas</p>
								@foreach($mod->ideas as $c)
									<div class="focustree">
										<a href="/ideas/view/{{$c->id}}">{{$c->name}} ({{$c->ideas->count()}} ideas)</a>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				@endif
			@endforeach
		</div>
	@endif
@endsection

