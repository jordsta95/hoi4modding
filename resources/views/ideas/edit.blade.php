@extends('templates.tool-ideas')

@section('title')
Edit ideas - HOI4 Modding
@endsection


@section('description')
Edit ideas for HOI IV
@endsection

@section('menu')
	@include('menus/tool-ideas')
@endsection

@section('content')
	<?php $group = ''; ?>
	@foreach($ideas->ideasOrdered as $idea)
		<?php 
		if($idea->group != $group){
			$group = $idea->group;
			echo '<h3 class="getIdeaValue">'.$group.'</h3>';
		}
		?>
		<div class="idea-wrapper" data-group="{{ $idea->group }}" data-id="{{ $idea->id }}">
			<span class="name">{{ $idea->name }}</span>
			<span class="gfx"><img src="{{ json_decode($idea->options)->gfx }}"></span>
			<span class="group">{{ $idea->group }}</span>
			<span class="json">
				{{ $idea->options }}
			</span>
			<span class="delete"><i class="fa fa-times"></i></span>
		</div>
	@endforeach
@endsection

