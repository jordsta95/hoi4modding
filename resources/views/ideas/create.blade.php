@extends('templates.tool-focustree')

@section('title')
Create ideas - HOI4 Modding
@endsection


@section('description')
Create ideas for HOI IV
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@if(Auth::user())
		<div class="add_user_popup active create-focus" id="addUser">
			<div class="add_user_box stepped" data-step="1" data-last="3" data-is-last="0">
				<form method="POST" action="" enctype="multipart/form-data">
					<h3>Create New Ideas</h3>
					<div class="step step_1">
						{{ csrf_field() }}
						<h3>Select Mod Or Create A New One</h3>
						<p>
							<select name="mod_id">
								<option value="createNewMod">Create New Mod</option>
								@if(!empty($mods))
									@foreach($mods as $mod)
										<option value="{{$mod->id}}">{{$mod->mod_name}}</option>
									@endforeach
								@endif
							</select>
						</p>
					</div>
					<div class="step step_2 toggleNewModStep" data-new-mod-class="step step_2 toggleNewModStep" data-existing-mod-class="hidden_step toggleNewModStep">
						<div class="new-mod">
							<h3>New Mod Details</h3>
							<p><label>Mod Name</label></p>
							<p><input name="mod_name"></p>
							<p><label>Mod Description</label></p>
							<p><textarea name="mod_description"></textarea></p>
							<p><label>Mod Logo</label></p>
							<p><input name="mod_logo" type="file" accept="image/x-png"></p>
						</div>
						
					</div>
					<div class="step step_3 toggleNewModStep" data-new-mod-class="step step_3 toggleNewModStep" data-existing-mod-class="step step_2 toggleNewModStep">
						<div class="new-focus-tree">
							<p><label>Localisation Language</label></p>
							<select name="language">
								<option value="english">English</option>
								<option value="french">French</option>
								<option value="german">German</option>
								<option value="polish">Polish</option>
								<option value="braz_por">Portuguese</option>
								<option value="russian">Russian</option>
								<option value="spanish">Spanish</option>
							</select>

							<p><label>Name</label></p>
							<input name="name" placeholder="Germany Ideas" autocomplete="disabled">

							<p><label>Idea prefix</label><br>
								<small>Prefix for all ideas in this file (Not required unless you want to have ideas with the same name for different countries, for example)</small></p>
							<input name="prefix" placeholder="GER" autocomplete="disabled">

							<p><button class="generic_button">Submit</button></p>
						</div>
					</div>
					<div class="stepper">
						<div class="prev">Back</div>
						<div class="next">Next</div>
					</div>
				</form>
			</div>
		</div>
	@else
		<div class="add_user_popup active create-focus" id="addUser">
			<h1>Log in to create ideas</h1>
		</div>
	@endif
@endsection

