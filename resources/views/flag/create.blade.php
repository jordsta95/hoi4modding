@extends('templates.tool')

@section('title')
HOI4 Modding - Flag Creator
@endsection


@section('description')
Create simple flags with ease
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	<div class="header-main">
		<h1>Flag Creator</h1>
		<h2>Create your own flag, or generate a random one</h2>
		<p>This tool has no save functionality, apart from the exporting of files. And is meant to act as an easy way to generate flags for your custom nation(s) - ensuring files are exported in the correct format and sizes.</p>
	</div>
	<div class="body-main">
		<div class="flag-steps">
			<form action="{{ route('flag.create') }}" method="POST" enctype="multipart/form-data" target="preview-frame" class="generic_form" data-number="1">
				{{csrf_field()}}
				<div class="flag-start">
					<div>
						<label>Flag Background</label><br>
						<input class="colour-picker" type="color" name="flag_background" value="#FFFFFF"><span class="colour-viewer"></span>
					</div>
					<div>
						<label>Ideology</label><br>
						<select name="ideology">
							<option value="democratic">Democratic - Vanilla</option>
							<option value="communism">Communism - Vanilla</option>
							<option value="fascism">Fascism - Vanilla</option>
							<option value="neutrality">Non-Aligned - Vanilla</option>
							<option value="authoritarian_democrat">Authoritarian Democrat - Kaiserreich</option>
							<option value="national_populist">National Populist - Kaiserreich</option>
							<option value="paternal_autocrat">Paternal Autocrat - Kaiserreich</option>
							<option value="radical_socialist">Radical Socialist - Kaiserreich</option>
							<option value="syndicalist">Syndicalist - Kaiserreich</option>
							<option value="totalist">Totalist - Kaiserreich</option>
							<option value="communist">Communist - Millennium Dawn</option>
							<option value="conservative">Conservative - Millennium Dawn</option>
							<option value="democratic_socialist">Democratic Socialist - Millennium Dawn</option>
							<option value="fascist">Fascist - Millennium Dawn</option>
							<option value="islamist">Islamist - Millennium Dawn</option>
							<option value="market_liberal">Market Liberal - Millennium Dawn</option>
							<option value="monarchist">Monarchist - Millennium Dawn</option>
							<option value="nationalist">Nationalist - Millennium Dawn</option>
							<option value="progressive">Progressive - Millennium Dawn</option>
							<option value="reactionary">Reactionary - Millennium Dawn</option>
							<option value="social_democrat">Social Democrat - Millennium Dawn</option>
							<option value="social_liberal">Social Liberal - Millennium Dawn</option>
							<option value="anarchist">Anarchist - Endsieg</option>
							<option value="fascism">Fascism - Endsieg</option>
							<option value="left_wing_radical">Left Wing Radical - Endsieg</option>
							<option value="leninist">Leninist - Endsieg</option>
							<option value="market_liberal">Market Liberal - Endsieg</option>
							<option value="national_socialist">National Socialist - Endsieg</option>
							<option value="paternal_autocrat">Paternal Autocrat - Endsieg</option>
							<option value="populist">Populist - Endsieg</option>
							<option value="social_conservative">Social Conservative - Endsieg</option>
							<option value="social_democrat">Social Democrat - Endsieg</option>
							<option value="social_liberal">Social Liberal - Endsieg</option>
							<option value="stalinist">Stalinist - Endsieg</option>
							<option value="absolutist">Absolutist - Victoria 2</option>
							<option value="radical">Radical - Victoria 2</option>
						</select>
					</div>
					<div>
						<label>Country Tag</label><br>
						<input name="tag" required="required"><br>
						<small>Freetype-only - country chooser popup disabled</small>
					</div>
					<div>
						<span class="generic_button flag-start_button">Continue</span>
					</div>
				</div>
				<div class="flag-choice">
					<span class="generic_button" id="flag-file">Convert flag from PNG</span><span class="generic_button" id="flag-build">Build Flag</span><span class="generic_button" id="flag-randomize">Randomize Flag</span>
				</div>
				<div class="flag-file">
					<h4>Convert Flag From PNG; largest flag size is 82x52px - keep to this ratio</h4>
					<input name="file" type="file" accept="image/x-png">
					<button class="generic_button previewFlag">Preview Flag</button>
				</div>
				<div class="flag-preview">
					<iframe name="preview-frame" id="preview-frame" style="width: 100%;min-height: 200px;" frameborder="0"></iframe>
				</div>
				<div class="flag-build">
					<h4>Build Your Own Flag</h4>
					<div class="copyable">
						<p class="flag-options active"><label>Layer Style <small>at default settings</small></label><br>
							<?php 
								$layers = ['circle', 'swastika', 'star', 'hammer-sickle','fist', 'cross', 'cross-thick', 'cross-celtic', 'cross-thick-celtic', 'cross-scandinavia', 'cross-thick-scandinavia', 'triangle-left', 'triangle-right', 'h-half-1', 'h-half-2', 'v-half-1', 'v-half-2', 'h-bar-1', 'h-bar-2', 'h-bar-3', 'v-bar-1', 'v-bar-2', 'v-bar-3'];
								$controller = new \App\Http\Controllers\FlagController;
								$request = new \Illuminate\Http\Request;
							?>
							@foreach($layers as $layer)
								<label class="flag-selector">
									<input type="radio" name="element[1][shape]" data-name="element" data-type="shape" value="{{$layer}}">
									<img src="data:image/png;base64,{{ base64_encode($controller->create($request, $layer)) }}">
								</label>
							@endforeach
							</p>

						<p class="flag-50">
							<label>Horizontal Offset</label><br>
							<input type="number" value="0" step="1" min="0" max="100" name="element[1][offsety]" data-name="element" data-type="offsety"><br>
							<small>Not all elements support this value</small>
						</p>
						<p class="flag-50">
							<label>Vertical Offset</label><br>
							<input type="number" value="0" step="1" min="0" max="100" name="element[1][offsetx]" data-name="element" data-type="offsetx"><br>
							<small>Not all elements support this value</small>
						</p>
						<p class="flag-50">
							<label>Size</label><br>
							<input type="number" value="100" step="1" min="0" max="100" name="element[1][size]" data-name="element" data-type="size"><br>
							<small>Size, as a percentage of the flag. Not all elements support this value</small>
						</p>
						<p class="flag-50">
							<label>Layer Colour</label><br>
							<input type="color" class="colour-picker" value="#FFFFFF" name="element[1][colour]" data-name="element" data-type="colour"><span class="colour-viewer"></span>
						</p>
					</div>
					<div class="newLayers">

					</div>
					<button type="button" class="generic_button addFlagLayer">Add More</button>
					<button class="generic_button previewFlag">Preview</button>
				</div>
				<div class="flag-randomize">
					<label>Layers</label><br>
					<input type="checkbox" name="randomize" value="1" style="display: none">
					<input name="layers" type="number" max="10" min="1" value="1"><br>
					<small>Lower numbers generate less crazy flags</small>
					<button class="generic_button previewFlag">Generate Random Flag</button>
				</div>
			</form>
		</div>
	</div>

@endsection

