@extends('templates.tool')

@section('title')
Mod List - HOI4 Modding
@endsection


@section('description')
Tools designed to make modding hearts of iron iv easy, and require no modding experience
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
	@foreach($mods as $mod)
		<div class="mod">
			<a href="/mod/{{$mod->id}}"><h3>{{$mod->mod_name}}</h3></a>
		</div>
	@endforeach
@endsection

