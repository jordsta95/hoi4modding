@extends('templates.tool')

@section('title')
{{ $mod->mod_name }} - HOI4 Modding
@endsection


@section('description')
{{ $mod->mod_description }}
@endsection

@section('menu')
	@include('menus/generic')
@endsection

@section('content')
<div class="add_user_popup" id="addUser">
	<div class="add_user_box stepped" data-step="1" data-last="2" data-is-last="0">
		<p class="popupClose toggleActive" for="addUser"><i class="fa fa-times"></i></p>
		<h3>Add User</h3>
		<div class="step step_1">
			<h3>User Role:</h3>
			<small>e.g. German tree designer</small>
			<p><input class="role"></p>
		</div>
		<div class="step step_2">
			<h3>Search user to add</h3>
			<p><input class="search_users" data-mod-id="{{$mod->id}}"></p>
			<div class="users_output">

			</div>
		</div>
		<div class="stepper">
			<div class="prev">Back</div>
			<div class="next">Next</div>
		</div>
	</div>
</div>
<div class="add_user_popup" id="editFocusTree">
	<div class="add_user_box stepped" data-step="1" data-last="3" data-is-last="0">
		<form action="/focus-tree/update/" id="editFocusTreeDetails" data-action="/focus-tree/update/" method="POST">
			{{csrf_field()}}
			<p class="popupClose toggleActive" for="editFocusTree"><i class="fa fa-times"></i></p>
			<h3>Edit Focus Tree Details</h3>
			<div class="step step_1">
				<h3>Focus Tree ID:</h3>
				<p><input name="tree_id"></p>
			</div>
			<div class="step step_2">
				<h3>Tag</h3>
				<p><input name="country_tag"></p>
			</div>
			<div class="step step_3">
				<h3>Language</h3>
				<p><select name="lang">
					<option value="english">English</option>
					<option value="french">French</option>
					<option value="german">German</option>
					<option value="polish">Polish</option>
					<option value="braz_por">Portuguese</option>
					<option value="russian">Russian</option>
					<option value="spanish">Spanish</option>
				</select></p>
				<button class="generic_button">Submit</button>
			</div>
			<div class="stepper">
				<div class="prev">Back</div>
				<div class="next">Next</div>
			</div>
		</form>
	</div>
</div>
	<div class="mod-info">
		<div class="image"><img src="{{ storage($mod->mod_logo, '/public/storage/') }}"></div>
		<div class="info">
			<form action="" method="POST" class="generic_form" enctype="multipart/form-data">
				{{ csrf_field() }}
				<h1 class="mod-edit">{{$mod->mod_name}}</h1>
				<div class="wrapper mod-edit">
					<h4>Title</h4>
					<input class="mod-edit" name="mod_name" value="{{ $mod->mod_name }}">
				</div>
				<p class="mod-edit">{{ $mod->mod_description }}</p>
				<div class="wrapper mod-edit">
					<h4>Description</h4>
					<textarea name="mod_description" class="mod-edit">{{ $mod->mod_description }}</textarea>
					<h4>Image</h4>
					<input type="file" name="mod_logo" class="mod-edit" accept="image/x-png">
				</div>
				<button class="generic_button mod-edit">Submit</button>
			</form>
			<h4>Team</h4>
			<ul class="mod-authors">
				<?php $viewer = null; 
				if(!empty(Auth::user())){
					$viewer = \App\UserMod::where('mod_id', '=', $mod->id)->where('user_id', '=', Auth::user()->id)->first();
				}
				?>
				@foreach($mod->users as $user)
					<li>{{$user->pivot->role}}: {{$user->username}}
						@if(!empty($viewer))
							@if($viewer->role == 'Creator' && $user->id !== Auth::user()->id)
								<a href="/api/v1/mod/remove_role/{{$mod->id}}/{{$user->id}}">X</a>
							@endif
						@endif
					</li>
				@endforeach
				@if($can_edit)
					<button class="addUserToMod toggleActive" for="addUser">Add User To Mod</button>
					<button class="generic_button editMod">Edit Mod</button>
					<div id="delete">
						<button class="deleteMod" onclick="$('#deletemod').toggle();">Press to delete mod, and all items attached to it</button>
						<div id="deletemod" style="display: none;">
							<form method="POST" action="/mod/{{$mod->id}}/delete">
								{{ csrf_field() }}
								<p>Type DELETE in the box below, to confirm you want to delete this mod<br>
									<input name="delete"></p>
								<p><button>Delete</button></p>
							</form>
						</div>
					</div>
								
					
				@endif
				<a href="/mod/{{$mod->id}}/export" class="generic_button" id="downloadMod">Export Mod</a>
			</ul>
		</div>
	</div>
	<div class="mod-data">
		@if($mod->focusTrees->count() > 0)
			<div class="mod-focus_tree">
				<h2>Focus Trees</h2>
				@foreach($mod->focusTrees as $tree)
					<div>
						<a href="/focus-tree/view/{{$tree->id}}" class="generic_button">{{count($tree->focuses)}} focuses for {{$tree->country_tag}}</a>
						@if($can_edit)
							<span style="width: auto" data-id="{{$tree->id}}" data-tag="{{$tree->country_tag}}" data-tree-id="{{$tree->tree_id}}" data-lang="{{$tree->lang}}" class="toggleActive generic_button editFocusTreeDetails" for="editFocusTree"><i class="fa fa-pencil-square-o"></i></span>
							<a href="/focus-tree/delete/{{$tree->id}}" class="generic_button red delete genericMessage"> X </a>
						@endif
					</div>
				@endforeach
			</div>
		@endif
		@if($mod->events->count() > 0)
			<div class="mod-events">
				<h2>Events</h2>
				<a href="/events/view/{{$mod->id}}" class="generic_button">{{count($mod->events)}} events</a>
			</div>
		@endif
		@if(!empty($mod->ideas))
			<div class="mod-ideas">
				<h2>Ideas</h2>
				@foreach($mod->ideas as $ideas)
					<div>
						<a href="/ideas/view/{{$ideas->id}}" class="generic_button">{{$ideas->name}}</a>
						{{--@if($can_edit)
							<a href="/country/delete/{{$country->id}}" class="generic_button red delete genericMessage"> X </a>
						@endif--}}
					</div>
				@endforeach
			</div>
		@endif
		@if($mod->countries->count() > 0)
			<div class="mod-country">
				<h2>Countries</h2>
				@foreach($mod->countries as $country)
					<div>
						<a href="/country/view/{{$country->id}}" class="generic_button">{{$country->name}}</a>
						@if($can_edit)
							<a href="/country/delete/{{$country->id}}" class="generic_button red delete genericMessage"> X </a>
						@endif
					</div>
				@endforeach
			</div>
		@endif
	</div>
@endsection

