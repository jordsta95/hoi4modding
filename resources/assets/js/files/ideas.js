$(document).ready(function(){
	$('#ideaGroup').on('change', function(){
		setIdeaOption($(this).val());
	});


	function setIdeaOption(val){
		$('.option-wrapper').hide();
		$('.option-wrapper *').addClass('inactive');
		$('.option-wrapper[data-type*="'+val+'|"]').show();
		$('.option-wrapper[data-type*="'+val+'|"] *').removeClass('inactive');
	}

	$('#idea-popup form').on('submit', function(e){
		e.preventDefault();
		var data = {};
		data["_token"] = $('[name="_token"]').val();
		data["prefix"] = $('#idea-popup form').attr('data-prefix');
		data["name"] = $('#idea-popup [name="name"]').val();
		data["group"] = $('#ideaGroup').val();
		data["options"] = {};
		$('#idea-popup [name*="option["]').each(function(){
			var spl = $(this).attr('name').split('[');
			var spli = spl[1].split(']')
			data["options"][spli[0]] = $(this).val();
		});
		data["options"] = data["options"];

		var id = $('#idea-popup form').attr('data-ideaid');
		var url = $('#idea-popup form').attr('action');


		$.post(url+id, data, function(returnData){
			appendIdeaData(returnData);
			$('#idea-popup').slideUp();
		});
		
	});

	$('.content.ideas').on('click', '.idea-wrapper .delete', function(e){
		e.stopPropagation();
		var deleteID = $(this).parent().attr('data-id');
		var confirm = window.confirm('Are you sure you want to delete '+$(this).parent().find('.name').text()+'? The action is irreversable!');
		if(confirm == true){
			$(this).parent().remove();
			$.post('/ideas/edit/'+$('.content.ideas').attr('data-id')+'/'+deleteID+'/delete', {
				_token: $('[name="_token"]').val()
			});
		}
	});

	$('.content.ideas').on('click', '.idea-wrapper', function(){
		var json = JSON.parse($(this).find('.json').text());

		$('#idea-popup [name="name"]').val($(this).find('.name').text());
		$('#idea-popup [name="group"]').val($(this).find('.group').text());
		$.each(json, function(key, value){
			$('#idea-popup [name="option['+key+']"').val(value);
			if(key == 'gfx'){
				$('#idea-popup #display-gfx').attr('src', value);
			}
		});
		$('#idea-popup .panel-head .action').text('Edit');
		$('#idea-popup form').attr('data-ideaid', $(this).attr('data-id'));
		$('#idea-popup').slideDown();
	});

	$('li[for="idea-popup"]').on('click', function(){
		$('[name="_token"]').attr('data-default', $('[name="_token"]').val());
		$('#idea-popup input,#idea-popup textarea, #idea-popup select').each(function(){
			$(this).val($(this).attr('data-default'));
		});
		$('#idea-popup form').attr('data-ideaid', 'new');
		$('#idea-popup #display-gfx').attr('src', '/media/get/hoi4/focus_icons/goal_unknown.png');
		$('#idea-popup .panel-head .action').text('Add');
	});

	$('.getIdeaValue').each(function(){
		$(this).text(getIdeaValue($(this).text()));
	});

	function getIdeaValue(text){
		return $('#ideaGroup option[value="'+text+'"]').text()
	}

	function appendIdeaData(data){
		var dataJson = JSON.parse(data.options);
		var text = '<div class="idea-wrapper" data-group="'+data.group+'" data-id="'+data.id+'">';
		text += '<span class="name">'+data.name+'</span>';
		text += '<span class="gfx"><img src="'+dataJson.gfx+'"></span>';
		text += '<span class="group">'+data.group+'</span>';
		text += '<span class="json">'+data.options+'</span>';
		text += '<span class="delete"><i class="fa fa-times"></i>';
		text += '</div>';

		var idExists = $('.content.ideas').find('[data-id="'+data.id+'"]');
		if(idExists.length){
			$(idExists).replaceWith(text);
		}else{
			var groupCheck = $('.content.ideas').find('[data-group="'+data.group+'"]');
			if(groupCheck.length){
				$(text).insertAfter(groupCheck);
			}else{
				$('.content.ideas').append('<h3>'+getIdeaValue(data.group)+'</h3>');
				$('.content.ideas').append(text);
			}
		}
	}
});