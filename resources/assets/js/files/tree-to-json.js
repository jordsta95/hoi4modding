function preprocess(s) {
	lines = s.split(/\r?\n/);
	result = "";
	for(var i = 0; i < lines.length; ++i) {
		var line = lines[i];
		var index = line.indexOf("#");
		if(index != -1) {
			line = line.substring(0, index);
		}
		result += line + "\n";
	}
	return result;
}

window.toJSON = function(s, level) {
	let maxLevel = 2;
	s = preprocess(s);
	var key = "";
	var value = "";
	var buildingKey = true;
	var braceCount = 0; 
	var hadBraces = false;
	var json = {};
	for(var i = 0; i < s.length; ++i) {
		let c = s.charAt(i);
		
		//If the parser is currently expecting a key
		if(buildingKey) {
			//Ignore these characters because they won't be part of a key
			if(c === "{" || c === "}") {
				continue;
			}
			
			//As long as we don't hit the = character, we are still building a key.
			//The assumption here is that keys can contain space characters
			if(c !== '=') {
				key += c;
			}
			else {
				buildingKey = false;
				key = key.trim();
			}
		}
		//The parser is expecting a value
		else {
			value += c;
			//if only whitespace
			//  continue
			if(c === "{") {
				++braceCount;
				hadBraces = true;
			}
			else if(c === "}") {
				--braceCount;
			}
			
			//If the braces are evenly matched,
			//and our value string is not just whitespace characters
			//and the next character to add is whitespace,
			//then we are done building the value string
			if(braceCount === 0 && /\S/.test(value) && /\s/.test(c)) {
				value = value.trim();
				
				//In the stupid format, the same key can appear multiple times.
				//If this happens, then what we really want is to treat that key
				//as an array
				if(key in json) {
					
					//Convert value stored at that key to
					//an array if it isn't already one
					if(!Array.isArray(json[key])) {
						var obj = json[key];
						json[key] = [obj];
					}

					//If the value had {} characters, then
					//it will consist of other key/value pairs.
					if(hadBraces && level < maxLevel) {                    
						json[key] = json[key].concat(toJSON(value, level+1));
						hadBraces = false;
					}
					else {
						json[key] = json[key].concat(value);
					} 
				}
				
				else {
					if(hadBraces && level < maxLevel) {                    
						json[key] = toJSON(value, level+1);
						hadBraces = false;
					}
					else {
						json[key] = value;
					} 
				}
				
				
				buildingKey = true;
				key = "";
				value = "";
			}
		}
	}

	return json;
	//$("#existing-focus-tree-output").val(JSON.stringify(json));
	//console.log(json);
	//$("#show-output").append(JSON.stringify(json));
	var obj = json; //JSON.parse($("#existing-focus-tree-output").val());

	var focus = obj.focus;
	for(var i in focus){

		if(focus[i].hasOwnProperty('id')){
			var id = focus[i].id;
			var name = "undefined";
			var desc = "undefined";
			var text = focus[i].id;
			if(focus[i].hasOwnProperty('text')){
				text = focus[i].text;
			}
			var localisation = $("#existing-localisation").val().split(/\n/);
			$.each(localisation,function(u, i) {
				if(i.indexOf(":") !== -1){
					var splitid = i.split(/:(.+)/);
					if(splitid[0].replace(/\s+/g, "") == text && name == 'undefined'){
						name = splitid[1].replace("0 ","").slice(1, -1);
					}
					if(splitid[0].replace(/\s+/g, "") == text+"_desc" || name !== 'undefined' && splitid[0].replace(/\s+/g, "") == text && splitid[0].replace(/\s+/g, "") !== name){
						desc = splitid[1].replace("0 ","").slice(1, -1);
					}
				}
			});
			if(focus[i].hasOwnProperty('mutually_exclusive')){
				var me = focus[i].mutually_exclusive;

				if(Array.isArray(me)){
					//It's an AND pr
					var mutually_exclusive = '';
					$.each(me, function(key, value){
						if(value.hasOwnProperty('focus')) {
							mutually_exclusive += value.focus+'&&';
						} else {
							var meVal = value.toLowerCase().replace(/\s/g, "");
							meVal = meVal.replace('{focus=', '');
							meVal = prVal.replace('}', '');
							//If it's an OR focus this'll replace those pesky ORs
							meVal = meVal.replace('focus=', '||');
							mutually_exclusive += meVal+'&&';
						}
					});
					mutually_exclusive = mutually_exclusive.toLowerCase();
				}else{
					if(me.hasOwnProperty('focus')){
						var mutually_exclusive = me.focus;
					}else{
						//It's an OR pr/solo
						var meVal = me.toLowerCase().replace(/\s/g, "");
						meVal = meVal.replace('{focus=', '');
						meVal = meVal.replace('}', '');
						//If it's an OR focus this'll replace those pesky ORs
						meVal = meVal.replace('focus=', '||');
						var mutually_exclusive = meVal.toLowerCase();
					}
					
				}

			}else{
				var mutually_exclusive = "";	
			}
			focus[i].mutually_exclusive = mutually_exclusive;
			if(focus[i].hasOwnProperty('prerequisite')){
				var pr = focus[i].prerequisite;
				
				if(Array.isArray(pr)){
					//It's an AND pr
					var prerequisite = '';
					$.each(pr, function(key, value){
						if(value.hasOwnProperty('focus')) {
							prerequisite += value.focus+'&&';
						} else {
							var prVal = value.toLowerCase().replace(/\s/g, "");
							prVal = prVal.replace('{focus=', '');
							prVal = prVal.replace('}', '');
							//If it's an OR focus this'll replace those pesky ORs
							prVal = prVal.replace('focus=', '||');
							prerequisite += prVal+'&&';
						}
							
						
					});
					prerequisite = prerequisite.toLowerCase();
				}else{
					if(pr.hasOwnProperty('focus')) {
						prerequisite = pr.focus;
					} else {
						//It's an OR pr/solo
						var prVal = pr.toLowerCase().replace(/\s/g, "");
						prVal = prVal.replace('{focus=', '');
						prVal = prVal.replace('}', '');
						//If it's an OR focus this'll replace those pesky ORs
						prVal = prVal.replace('focus=', '||');
						prerequisite = prVal.toLowerCase();
					}
				}
			}else{
				var prerequisite = "";	
			}
			focus[i].prerequisite = prerequisite;
			if(focus[i].hasOwnProperty('ai_will_do')){
				var ai = focus[i].ai_will_do;
				if(ai.hasOwnProperty('factor')){
					ai = ai.factor;
				}else{
					ai = ai.replace('factor', '').replace('=', '').replace(/ /g, '').replace('{', '').replace('}', '');
				}
				
			}else{
				var ai = "";	
			}
			focus[i].ai_will_do = ai;
			if(focus[i].hasOwnProperty('relative_position_id')){
				var relative_pos = focus[i].relative_position_id;
	
			}else{
				var relative_pos = '';
			}
			focus[i].relative_position_id = relative_pos;
			if(focus[i].hasOwnProperty('completion_reward')){
				var completion_reward = focus[i].completion_reward;
			}else{
				var completion_reward = "";	
			}
			focus[i].completion_reward = completion_reward;
			if(focus[i].hasOwnProperty('available')){
				var available = focus[i].available;
			}else{
				var available = "";	
			}
			focus[i].available = available;
			if(focus[i].hasOwnProperty('bypass')){
				var bypass = focus[i].bypass;
			}else{
				var bypass = "";	
			}
			focus[i].bypass = bypass;
			if(focus[i].hasOwnProperty('completion_tooltip')){
				var completion_tooltip = focus[i].completion_tooltip;
			}else{
				var completion_tooltip = "";	
			}
			focus[i].completion_tooltip = completion_tooltip;
			if(focus[i].hasOwnProperty('cost')){
				var cost = focus[i].cost;
			}else{
				var cost = "10";	
			}
			focus[i].cost = cost;

			if(focus[i].hasOwnProperty('icon')){
				var icon = focus[i].icon;
				icon = icon.replace('GFX_', '');
				icon = '/media/get/hoi4/focus_icons/'+icon+'.png';
			}else{
				var icon = "";	
			}

			focus[i].title = name;
			focus[i].description = desc;
			focus[i].icon = icon;
		}
	}
	return json;
	
}