$(document).ready(function(){
	if(!getCookie('CountryTags')){
		resetCountryTags();
	}

	if(!getCookie('BuilderResults')){
		resetBuilderResults();
	}


	if(!getCookie('StateResults')){
		resetStateResults();
	}

	function resetCountryTags(){
		$.getJSON("/tags.json", function(data) {
			window.localStorage.setItem('CountryTags', JSON.stringify(data));
			setCookie('CountryTags', 'Set', 5);
		});
	}

	function resetBuilderResults(){
		if($('[name="_token"]').length){
			$.post('/builder', {"_token":$('[name="_token"]').val(), "value":''}, function(data) {
				window.localStorage.setItem('BuilderResults', JSON.stringify(data));
				setCookie('BuilderResults', 'Set', 5);
			});
		}
	}

	function resetStateResults(){
		$.getJSON("/states.json", function(data) {
			window.localStorage.setItem('StateResults', JSON.stringify(data));
			setCookie('StateResults', 'Set', 5);
		});
	}

	$('.editFocusTreeDetails').on('click', function(){
		$('#editFocusTreeDetails').attr('action', $('#editFocusTreeDetails').attr('data-action')+$(this).attr('data-id'));
		$('#editFocusTreeDetails [name="country_tag"]').val($(this).attr('data-tag'));
		$('#editFocusTreeDetails [name="tree_id"]').val($(this).attr('data-tree-id'));
		$('#editFocusTreeDetails [name="lang"]').val($(this).attr('data-lang'));
	});

	$('.search_users').on('keyup', function(){
		if($(this).val().length > 0){
			var searchField = $(this).val();
			var regex = new RegExp(searchField, "i");
			var count = 1;
			var output = "";
			$.getJSON("/api/v1/users", function(data) {
				$.each(data, function(key, val){
					if ((val.username.search(regex) != -1)) {
						output += '<p>';
							output += '<a href="/api/v1/mod/add_role/'+$('.search_users').attr('data-mod-id')+'/'+val.id+'/'+$('.role').val()+'">'+val.username+'</a>';
						output += '</p>';
					}
				});
				$('.users_output').html(output);
			});
		}else{
			$('.users_output').html('');
		}
	});
	$('.toggle_menu').on('click', function(){
		$('.page').toggleClass('menu_toggled'); 
		if($('.page').hasClass('menu_toggled')){
			document.cookie = "menu=toggled; path=/";
		}else{
			document.cookie = "menu=default; path=/";
		}
	});

	$('.stepped .next').on('click', function(){
		var $stepped = $(this).closest('.stepped');
		var current = parseInt($stepped.attr('data-step'));
		var last = parseInt($stepped.attr('data-last'));
		var next = (current + 1)
		if(next == last){
			$stepped.attr('data-is-last', 1);
		}
		$stepped.find('.step_'+current).slideUp();
		$stepped.find('.step_'+next).slideDown();
		$stepped.attr('data-step', next);
	});
	$('.stepped .prev').on('click', function(){
		var $stepped = $(this).closest('.stepped');
		var current = parseInt($stepped.attr('data-step'));
		var prev = (current - 1)
		$stepped.attr('data-is-last', 0);
		$stepped.find('.step_'+current).slideUp();
		$stepped.find('.step_'+prev).slideDown();
		$stepped.attr('data-step', prev);
	});

	$(document).on('click', '.open', function(){
		var id = '#'+$(this).attr('for');
		$(id).slideDown();
	});

	$(document).on('click', '.clear-import', function(){
		$(this).parent().find('.can-be-cleared').html('');
	});

	$(document).on('click', '.close', function(){
		var id = '#'+$(this).attr('for');
		$(id).slideUp();
	});

	$(document).on('click', '.toggleActive', function(){
		var id = '#'+$(this).attr('for');
		$(id).toggleClass('active');
	});

	$(document).on('click', '.toggle', function(){
		var id = '#'+$(this).attr('for');
		$(id).slideToggle();
	});

	$(document).on('click', '.choose-country', function(e){
		e.stopPropagation();
		var id = $(this).attr('id');
		$('#focus-tag').attr('data-editing', id);
		$('#focus-tag').slideDown();
	});

	$(document).on('click', '.choose-state', function(e){
		e.stopPropagation();
		var id = $(this).attr('id');
		$('#focus-state').attr('data-editing', id);
		$('#focus-state').slideDown();
	});

	$(document).on('click', '.quickedit.textarea', function(){
		var content = $(this).text();
		$(this).replaceWith('<textarea>'+content+'</textarea><br><button class="submit_quickedit textarea">Submit</button>')
	});
	$(document).on('click', '.submit_quickedit.textarea', function(){
		$(this).prev().remove();
		var content = $(this).prev().val();
		$(this).prev().replaceWith('<div class="quickedit textarea">'+content+'</div>');
		$(this).remove();
	});

	$('body').on('click', '.tab', function(){
		$('.tabbed.active').removeClass('active');
		$('.tab.active').removeClass('active');
		var getClass = $(this).attr('class').replace('tab', '').trim();
		$(this).addClass('active');
		$('.tabbed.'+getClass).addClass('active');
	});

	$(document).on('click', '.loader-switch', function(){
		$(this).hide();
		$(this).parent().find('.loader').show();
	});

	$('body').on('click', '[for="focus-popup"],.edit-focus,.event-edit,[for="event-popup"],.idea-wrapper,[for="idea-popup"]', function(){
		resetLoader();
	});

	function resetLoader(){
		$('.loader').hide();
		$('.loader-switch').show();
	}

	$('.limit-children').on('keyup', function(){
		var search = $(this).val();
		search = search.toLowerCase();
		var limit = $(this).attr('data-limits');
		if(search.length !== 0){
			$(limit).children().each(function(){
				var text = $(this).text();
				text = text.toLowerCase();
				if(text.indexOf(search) !== -1){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
		}else{
			$(limit).children().show();
		}
	});

	setTimeout(function(){
		$('.popup_message').not('.clickToDismiss').remove();
	}, 15000);

	$('.clickToDismiss').on('click', function(){
		$(this).remove();
	});

	$('body').on('click', '.delete.genericMessage', function(e){
		e.preventDefault();
		var confirm = window.confirm('Are you sure you want to delete this? This action is irreversable.');
		if(confirm == true){
			window.location.href = $(this).attr('href');
		}
	});

	//Images to Base64
	File.prototype.convertToBase64 = function(callback){
		var reader = new FileReader();
		reader.onload = function(e) {
			 callback(e.target.result)
		};
		reader.onerror = function(e) {
			 callback(null, e);
		};        
		reader.readAsDataURL(this);
	};

	$(".to_base_64").on('change',function(){
		var alters = '#'+$(this).attr('data-alters');
		var customfield = '#'+$(this).attr('data-field');
		var selectedFile = this.files[0];
		selectedFile.convertToBase64(function(base64){
			$(alters).attr('src', base64);
			$(customfield).val('custom');
		});
	});

	$('form').on('change', '.colour-picker', function(){
		$(this).parent().find('.colour-viewer').css('background', $(this).val());
	});

	$('.addCookie').on('click', function(){
		var date = new Date();
		date.setTime(date.getTime() + (24*60*60*1000));
		var expires = "; expires=" + date.toUTCString();
		var name = $(this).attr('data-cake');
		var value = $(this).attr('data-cake-value');
		document.cookie = name + "=" + value  + expires + "; path=/";
	});

	$('#downloadMod').on('click', function(){
		var cookieCheck = setInterval(function(){
			if(document.cookie.indexOf("downloadedMod") >= 0){
				document.cookie = "downloadedMod=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
				clearInterval(cookieCheck);
				window.location.href = window.location.href;
			}
		}, 1000);
	});

	if($('.colour-picker').length){
		function refreshSwatch() {
			var red = $( "#red" ).slider( "value" );
			var green = $( "#green" ).slider( "value" );
			var blue = $( "#blue" ).slider( "value" );
			$( "#swatch" ).css( "background-color", "rgb(" + red + ',' + green + ',' + blue + ')' );
			$('body').find('[name="'+$( "#swatch" ).attr('data-name')+'"]').val(red + ',' + green + ',' + blue);
		}
	 
		$( "#red, #green, #blue" ).slider({
		  orientation: "horizontal",
		  range: "min",
		  max: 255,
		  value: 127,
		  slide: refreshSwatch,
		  change: refreshSwatch
		});
		$( "#red" ).slider( "value", 0 );
		$( "#green" ).slider( "value", 0 );
		$( "#blue" ).slider( "value", 0 );
	}

	$('.editMod').on('click', function(){
		$('.mod-edit').hide();
		$('button.mod-edit,input.mod-edit,textarea.mod-edit,.wrapper.mod-edit').show();
	});
});

function uploadImage(file, appendTo, type){
	var formdata = new FormData();
	formdata.append('_token', $('[name="_token"]').val());
	if(file.prop('files').length > 0){
        data = file.prop('files')[0];
        formdata.append("image", data);
    }else{
    	alert('Please select an image');
    	return;
    }
	$.ajax({
	    url: '/image/upload',
	    type: "POST",
	    data: formdata,
	    processData: false,
	    contentType: false,
	    success: function (result) {
	    	if(type){
	    		if(type == 'focus'){
					appendTo.append('<img src="/image/find/'+result.id+'/focus" id="custom-image-'+result.id+'" class="nficon">');
					appendTo.find('#custom-image-'+result.id).trigger('click');
					nfGFX('/image/find/'+result.id+'/focus');
	    		}
	    		if(type == 'event'){
					appendTo.append('<img src="/image/find/'+result.id+'/idea" id="custom-image-'+result.id+'" class="nficon">');
					appendTo.find('#custom-image-'+result.id).trigger('click');
					nfGFX('/image/find/'+result.id);
	    		}
	    		if(type == 'idea'){
					appendTo.append('<img src="/image/find/'+result.id+'/idea" id="custom-image-'+result.id+'" class="nficon">');
					appendTo.find('#custom-image-'+result.id).trigger('click');
					nfGFX('/image/find/'+result.id);
	    		}
	    	}else{
	    		appendTo.append('<img src="/image/find/'+result.id+'" id="custom-image-'+result.id+'">');
	    	}
	    	return result;
	    }
	});
}

function generateId(name){
	return name.replace(/\s+/g, '').replace(/[^a-zA-Z_0-9]/g, '').toLowerCase();
}

window.GENERICreload = function(){
	window.location.href = window.location.href;
}

window.GENERICdeleteNotFound = function(){
	alert('You have tried to delete an item which no longer exists');
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return false;
}

//Not too sure why, but the last window variable keeps getting called.
window.fix = '';