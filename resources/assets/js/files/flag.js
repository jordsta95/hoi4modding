$('.addFlagLayer').on('click', function(){
	var $form = $(this).closest('form');
	var num = parseInt($form.attr('data-number'));
	num++;
	var html = $('.copyable').html();
	$('.newLayers').append('<div id="addedlayer">'+html+'</div>');
	$('#addedlayer input,#addedlayer select').each(function(){
		$(this).attr('name', $(this).attr('data-name')+'['+num+']['+$(this).attr('data-type')+']');
	});
	$('#addedlayer').attr('id', '');
	$form.attr('data-number', num);	

});

$('.previewFlag').on('click', function(){
	$('.content').animate({
		scrollTop: ($("#preview-frame").offset().top + 200)
	}, 1000);
});

$('.flag-start_button').on('click', function(){
	if($('[name="tag"]').val().length){
		$('.flag-start').hide();
		$('.flag-choice').attr('style', 'display:flex;');
	}else{
		$('[name="tag"]').attr('style', 'border: 1px solid red;');
	}
});

$('.flag-choice .generic_button').on('click', function(){
	$('.'+$(this).attr('id')).show();
	$('.flag-preview').show();
	$('.flag-choice').hide();
});

$('#flag-randomize').on('click', function(){
	$('[name="randomize"]').prop('checked', true);
});

$('.flag-build').on('click', '.flag-options', function(){
	$(this).toggleClass('active');
});

$('.flag-build').on('click', '.flag-selector', function(e){
	e.stopPropagation();
	$(this).closest('.flag-options').removeClass('active');
});