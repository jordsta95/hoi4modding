$('body').on('click', '.country-tab', function(){
	$(this).parent().find('.active').removeClass('active');
	$(this).addClass('active');
});

$('.country-leader-icon').on('click', function(){
	var image = $(this).attr('src');
	if($(this).closest('#choose-leader').attr('data-for') == 'leader'){
		$('.country-leader .picture').css('backgroundImage', 'url('+image+')');
		$('.country-leader .picture').attr('data-image', image);	
	}
	if($(this).closest('#choose-leader').attr('data-for') == 'military'){
		$('#country-military .image').attr('src', image);
		$('#choose-leader').attr('data-for', 'leader');
	}
	$(this).closest('.focus-panel').slideUp();
});

$('.country-idea').on('click', function(){
	$('.country-idea-add').attr('data-idea', $(this).attr('data-idea'));
	$('.country-idea-add').attr('data-needs-additional', $(this).attr('data-needs-additional'));
	$('.country-idea-add').attr('data-for', $(this).attr('data-for'));

	$('#existing-ideas').html('<i class="fa fa-spinner fa-spin"></i>');

	$.get('/country/edit/'+$('#country-id').attr('data-country-id')+'/get-ideas?type='+$(this).attr('data-idea'), function(data){
		$('#existing-ideas').html(data);
	});
});

$('#existing-ideas').on('click', '.existing-idea .fa-times', function(){
	var idea = $(this).closest('.existing-idea');
	idea.hide();
	$.post('/country/delete-idea/', {
		_token:$('[name="_token"]').val(),
		id:idea.attr('data-id')
	}, function(data){
		$('.country-idea[data-idea="'+$('.country-idea-add').attr('data-idea')+'"]').text((parseInt($('.country-idea[data-idea="'+$('.country-idea-add').attr('data-idea')+'"]').text()) - 1));
	});
});

$('.country-idea-add').on('click', function(){
	$('#choose-person-idea,#choose-non-person-idea,#choose-any-idea').attr('data-idea-to-add',$(this).attr('data-idea'));
	$('#choose-person-idea,#choose-non-person-idea,#choose-any-idea').attr('data-needs-additional',$(this).attr('data-needs-additional'));
	$('#'+$(this).attr('data-for')).slideDown();
	$('#edit-ideas').slideUp();

});

$('.country-idea-icon').on('click', function(){
	var parent = $(this).closest('.focus-panel');
	if(parent.attr('data-needs-additional') == 'yes'){
		$('#idea-additional-info').slideDown();
		$('#idea-additional-info').attr('data-idea-to-add',parent.attr('data-idea-to-add'));
		$('#idea-additional-info .left .image').css('backgroundImage', 'url('+$(this).attr('src')+')');
		$('#idea-additional-info .left [name="gfx"]').val($(this).attr('src'));
		$('#choose-person-idea,#choose-non-person-idea').slideUp();
	}else{
		$('[data-idea="'+parent.attr('data-idea-to-add')+'"]').css('backgroundImage', 'url('+$(this).attr('src')+')');
		$(this).closest('.focus-panel').slideUp();
	}
});

$('.country-ideas-info .right button').on('click', function(){
	var parent = $(this).closest('.focus-panel');

	$.post('/country/edit/'+$('#country-id').attr('data-country-id')+'/add-idea', {
		_token:$('[name="_token"]').val(),
		country_id:$('#country-id').attr('data-country-id'),
		name: value('name','.country-ideas-info'),
		short_desc: value('short_desc','.country-ideas-info'),
		long_desc: value('long_desc','.country-ideas-info'),
		effect: value('effect','.country-ideas-info'),
		gfx: value('gfx','.country-ideas-info'),
		national_spirit_type: parent.attr('data-idea-to-add'),
	}, function(data){
		var total = parseInt($('.country-idea[data-idea="'+parent.attr('data-idea-to-add')+'"]').eq(0).text()) + 1;
		$('.country-idea[data-idea="'+parent.attr('data-idea-to-add')+'"]').text(total);
	});
});

$('#country-state').on('click', '.searched_states' , function(){
	addState($(this).attr('id').replace('state_',''), $(this).text());
});

function addState(stateId, name, core, controls){
	var hasCore = false;
	var hasControl = false;
	if(core != null){
		hasCore = core;
	}
	if(controls != null){
		hasControl = controls;
	}
	var id = Math.round((new Date()).getTime() / 1000)+(Math.floor(Math.random() * 100) + 1);
	var countryBox = '<div class="grid with-gap" data-id="new" data-state-id="'+stateId+'">';
		countryBox += '<div class="grid_start_1 grid_span_1 removeState">x</div>';
		countryBox += '<div class="grid_start_2 grid_span_5">';
			countryBox += name;
			countryBox += '<input class="stateValue" name="states[new]['+id+'][state_name]" style="display:none;" value="'+name+'">';
			countryBox += '<input class="stateValue" name="states[new]['+id+'][state_id]" style="display:none;" value="'+stateId+'">';
		countryBox += '</div>';
		countryBox += '<div class="grid_start_7 grid_span_3"><input class="core stateValue" value="1" name="states[new]['+id+'][core]" '+(hasCore ? ' checked ' : '')+' type="checkbox"></div>';
		countryBox += '<div class="grid_start_10 grid_span_3"><input class="controls stateValue" value="1" name="states[new]['+id+'][controls]" '+(hasControl ? ' checked ' : '')+'type="checkbox"></div>';
	countryBox += '</div>';
	$('.country-states').append(countryBox);
}

function removeState(stateId){
	var row = $('.state-box').find('[data-state-id="'+stateId+'"]');
	if(row.length){
		if(row.attr('data-id') == 'new'){
			row.remove();
		}else{
			row.find('.removeState').trigger('click');
		}
	}
}

$('.country-extra-stuff').on('click', '.removeState', function(){
	if($(this).parent().attr('data-id') != 'new'){
		$(this).parent().find('.removeStateOption').prop('checked', true);
		$(this).parent().hide();
	}else{
		$(this).parent().remove();
	}
});

$('.leader-trait').on('click', function(){
	$('#idea-additional-info [name="effect"]').val($(this).attr('data-id'));
	$('#country-leader-traits').slideUp();
});

$('.country-save').on('click', function(){

	if($('.stateValue').length){
		var states = $('.stateValue').serialize();
	}else{
		var states = '';
	}

	if($('.generalJSON').length){
		var generals = $('.generalJSON').serialize();
	}else{
		var generals = '';
	}

	research = '';

	$('body').find('.research-item').each(function(){
		research += $(this).attr('data-id')+',';
	});


	$.post('/country/edit/'+$('#country-id').attr('data-country-id'), {
		_token: value('_token'),
		date_id: $('body').find('[data-tab-type="date"] .active').attr('id'),
		ideology_id: $('body').find('[data-tab-type="ideology"] .active').attr('id'),
		ideology_support: value('ideology_support'),
		country_name: value('country_name'),
		leader_name: value('country_leader'),
		leader_gfx: $('body').find('.country-leader .picture').attr('data-image'),
		states: states,
		generals: generals,
		research: research
	},
	function(data){
		GENERICreload();
	});
});

$('.country').on('click', '[data-tab-type="date"] .country-tab', function(){
	$('.country-leader-container').html('<div class="loader">Select ideology for selected date</div>');
	$('[data-tab-type="ideology"]').html('<div class="loader"><i class="fa fa-spinner fa-spin"></i></div>');
	$.post('/country/get-tabs', {
		_token: value('_token'),
		date_id: $(this).attr('id')
	}, function(data){
		$('[data-tab-type="ideology"]').html(data);
	});
	$('.state-box').html('<div class="loader"><i class="fa fa-spinner fa-spin"></i></div>');
	$.post('/country/get-states', {
		_token: value('_token'),
		date_id: $(this).attr('id')
	}, function(data){
		$('.state-box').html(data);
		setReadOnly();
		regenMapStates();
	});
});

$('.country').on('click', '[data-tab-type="ideology"] .country-tab', function(){
	$('.country-leader-container').html('<div class="loader"><i class="fa fa-spinner fa-spin"></i></div>');
	$.post('/country/get-leader', {
		_token: value('_token'),
		ideology_id: $(this).attr('id')
	}, function(data){
		$('.country-leader-container').html(data);
		setReadOnly();
	});
});

$('#country-military .submit').on('click', function(){
	var id = Math.round((new Date()).getTime() / 1000)+(Math.floor(Math.random() * 100) + 1);
	var json = {};
	$('#country-military input,#country-military textarea').each(function(){
		json[$(this).attr('name')] = $(this).val();
	});
	json['field_marshal'] = $('#country-military [name="field_marshal"]').prop('checked');
	json['gfx'] = $('#country-military .image').attr('src');
	json['type'] = $('#country-military').attr('data-type');
	if($('#country-military').attr('data-id') == 'new'){
		var countryBox = '<div class="grid with-gap" data-id="new">';
			countryBox += '<div class="grid_start_1 grid_span_1 removeState">x</div>';
			countryBox += '<div class="grid_start_2 grid_span_5">';
				countryBox += $('#country-military .name').val();
				countryBox += '<textarea class="generalJSON" name="general[new]['+id+']" style="display:none;">'+JSON.stringify(json)+'</textarea>';
			countryBox += '</div>';
			countryBox += '<div class="grid_start_7 grid_span_3"><img src="'+$('#country-military .image').attr('src')+'" class="general-icon"></div>';
			countryBox += '<div class="grid_start_10 grid_span_3">'+$('#country-military [name="level"]').val()+'</div>';
		countryBox += '</div>';
		$('.country-'+$('#country-military').attr('data-type')).append(countryBox);
	}else{
		var dataId = '[data-id="'+$('#country-military').attr('data-id')+'"]';
		$('.generalJSON'+dataId).val(JSON.stringify(json));
		$('.editGeneral .name'+dataId).text($('#country-military .name').val());
		$('.general-icon'+dataId).attr('src', $('#country-military .image').attr('src'));
		$('.generalLevel'+dataId).text($('#country-military [name="level"]').val());
	}
});

$('#search-traits').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.country-trait').show();
	$('.country-trait').each(function(){
		var hide = true
		if($(this).attr('data-id').toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('.name').text().toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('textarea').val() != null){
			if($(this).find('textarea').val().toLowerCase().includes(val)){
				hide = false;
			}
		}


		if(hide){
			$(this).hide();
		}
	});
});

$('#search-leader-traits').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.leader-trait').show();
	$('.leader-trait').each(function(){
		var hide = true
		if($(this).attr('data-id').toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('.name').text().toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('textarea').val() != null){
			if($(this).find('textarea').val().toLowerCase().includes(val)){
				hide = false;
			}
		}


		if(hide){
			$(this).hide();
		}
	});
});

$('#search-research').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.country-research').show();
	$('.country-research').each(function(){
		var hide = true
		if($(this).attr('data-id').toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('.name').text().toLowerCase().includes(val)){
			hide = false;
		}
		if($(this).find('textarea').val() != null){
			if($(this).find('textarea').val().toLowerCase().includes(val)){
				hide = false;
			}
		}


		if(hide){
			$(this).hide();
		}
	});
});

$('.editGeneral').on('click', function(){
	var json = JSON.parse($(this).find('.generalJSON').val());
	var type = $(this).closest('.country-general-wrapper').attr('data-type');
	$('#country-military [name="field_marshal"]').prop('checked', false)

	$('#country-military').attr('data-id', $(this).closest('.grid').attr('data-id'));
	$('#country-military .image').attr('src', json.gfx);
	$('#country-military [name="name"]').val(json.name);
	$('#country-military [name="traits"]').val(json.traits);
	$('#country-military [name="level"]').val(json.level);
	$('#country-military [name="attack_stat"]').val(json.attack_stat);
	$('#country-military [name="defence_stat"]').val(json.defence_stat);
	$('#country-military [name="supply_stat"]').val(json.supply_stat);
	$('#country-military [name="planning_stat"]').val(json.planning_stat);
	if(json.is_field_marshal){
		$('#country-military [name="field_marshal"]').prop('checked', true);
	}

	$('#country-military').slideDown();
});

$('.country-trait').on('click', function(){
	$('[name="traits"]').val($('[name="traits"]').val()+' '+$(this).attr('data-id'));
	$(this).hide();
});

$('.country [for="country-military"]').on('click', function(){
	$('.country-trait').show();
	$('#country-military').attr('data-type', $(this).attr('data-type'));
	$('#country-military .image').attr('src', $('#country-military .image').attr('data-default'));
	$('#country-military input,#country-military textarea').each(function(){
		$(this).val('');
	});
	$('#country-military').attr('data-id', 'new');
	$('#country-military [name="field_marshal"]').val('1');
	$('#country-military [name="field_marshal"]').prop('checked', false);
});

$('#country-military .image').on('click', function(){
	$('#choose-leader').attr('data-for', 'military');
});

$('#country-military .close').on('click', function(){
	$('#choose-leader').attr('data-for', 'leader');
});

$('#search-country-leaders').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.country-leader-icon').show();
	$('.country-leader-icon').each(function(){
		if($(this).attr('id') !== null){
			if(!$(this).attr('id').toLowerCase().includes(val)){
				$(this).hide();
			}
		}
	});
});

$('.country-research').on('click', function(){
	var countryBox = '<div class="grid with-gap" data-id="new">';
		countryBox += '<div class="grid_start_1 grid_span_1 removeState">x</div>';
		countryBox += '<div class="grid_start_2 grid_span_11 research-item" data-id="'+$(this).attr('data-id')+'">'+$(this).find('.name').text()+'</div>';
	countryBox += '</div>';
	$('body').find('.country-research-items').append(countryBox);
	$(this).hide();
});

$('[for="country-research"]').on('click', function(){
	$('.country-research').show();
});
setReadOnly();
function setReadOnly(){
	$('.readOnly').find('input,textarea').each(function(){
		$(this).attr('disabled', 'disabled');
	});
}

function value(name, parent){
	if(parent){
		return $('body').find(parent).find('[name="'+name+'"]').val();
	}else{
		return $('body').find('[name="'+name+'"]').val();
	}
}

$('.map-wrapper .increase').on('click', function(){
	zoomInMap();
});

$('.map-wrapper .decrease').on('click', function(){
	zoomOutMap();
});

$('.map-wrapper').bind('mousewheel DOMMouseScroll', function(event){
	event.preventDefault();
    if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
        zoomInMap();
    }
    else {
    	zoomOutMap();
    }
});

function regenMapStates(){
	if($('.map-wrapper').length){
		$('.map-wrapper g').removeAttr('data-core-state');
		$('.map-wrapper g').removeAttr('data-control-state');

		$('body').find('.country-states .grid.with-gap').each(function(){
			var stateId = $(this).attr('data-state-id');
			if($(this).find('.core').prop('checked') && !$(this).find('.removeStateOption').prop('checked')){
				showCoreState(stateId);
			}
			if($(this).find('.controls').prop('checked') && !$(this).find('.removeStateOption').prop('checked')){
				showControlState(stateId);
			}
		});
	}
}

regenMapStates();
$('.state-box').on('click', '[for="mapWrapper"]', function(){
	regenMapStates();
});

var mX, mY,
    $element  = $('.map-wrapper svg').parent();

function calculateDistance(elem, mouseX, mouseY) {
    return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left+(elem.width()/2)), 2) + Math.pow(mouseY - (elem.offset().top+(elem.height()/2)), 2)));
}

$('.map-wrapper svg').parent().mousemove(function(e) {  
    mX = e.pageX;
    mY = e.pageY;
    distance = calculateDistance($element, mX, mY);        
});

function zoomInMap(){
	var f = parseFloat($('.map-wrapper').attr('data-zoom'));
	var zoomable = false;
	if(f < 16){
		zoomable = true;
		f = f * 2;
	}
	var sl = $('.map-wrapper svg').parent().scrollLeft();
	var st = $('.map-wrapper svg').parent().scrollTop();
	$('.map-wrapper').attr('data-zoom', f);
	$('.map-wrapper svg').css('height', (f * 100) + '%');

	if(zoomable){
		$('.map-wrapper svg').parent().scrollLeft((sl * 2) + mX);
		$('.map-wrapper svg').parent().scrollTop((st * 2) + mY);
	}

}

function zoomOutMap(){
	var f = parseFloat($('.map-wrapper').attr('data-zoom'));
	if(f > 0.5){
		f = f / 2;
	}
	var sl = $('.map-wrapper svg').parent().scrollLeft();
	var st = $('.map-wrapper svg').parent().scrollTop();
	$('.map-wrapper').attr('data-zoom', f);
	$('.map-wrapper svg').css('height', (f * 100) + '%');
	if(f > 0.5){
		$('.map-wrapper svg').parent().scrollLeft((sl / 2) - mX);
		$('.map-wrapper svg').parent().scrollTop((st / 2) - mY);
	}
}

var stateJson = '';
if($('.all-states').length){
	stateJson = JSON.parse($('.all-states').text());
}

$('.map-wrapper g[id*="state-"]').on('click', function(){
	$('.state-overview').hide();
	$('.state-overview .core .checkbox').removeClass('active');
	$('.state-overview .controls .checkbox').removeClass('active');
	if($(this).attr('data-state-id')){
		var tag = $(this).attr('data-controls');
		var stateId = $(this).attr('data-state-id');
		var state = '';
		$.each(stateJson, function(){
			if(this.id == stateId){
				state = this;
			}
		});
		$('.state-overview .state-name').text(state.name);
		$('.state-overview').attr('data-state-id', state.id);
		$('.state-overview #hide-tag').attr('data-tag', tag);

		if($('.state-box').find('[data-state-id="'+state.id+'"]').length){
			if($('.state-box').find('[data-state-id="'+state.id+'"] .core').prop('checked')){
				$('.state-overview .core .checkbox').addClass('active');
			}
			if($('.state-box').find('[data-state-id="'+state.id+'"] .controls').prop('checked')){
				$('.state-overview .controls .checkbox').addClass('active');
			}
		}
		$('.state-overview').slideDown();
	}
});

$('.state-overview .state-info .wrapper').on('click', function(){
	$(this).find('.checkbox').toggleClass('active');
});


$('.state-overview #hide-tag').on('click', function(){
	if($(this).attr('data-tag') != ''){
		$('.map-wrapper g[data-controls="'+$(this).attr('data-tag')+'"]').hide();
	}
});

$('.state-overview .update').on('click', function(){
	if($('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').length){
		var stateBox = $('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]');
		if($('.state-overview .core').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')){
			stateBox.find('removeStateOption').prop('checked', false);
			stateBox.find('core').prop('checked', $('.state-overview .core .checkbox').hasClass('active'));
			stateBox.find('controls').prop('checked', $('.state-overview .controls').hasClass('active'));
		}else{
			stateBox.find('removeStateOption').prop('checked', true);
		}
	}
	if(($('.state-overview .core .checkbox').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')) && !$('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').length){
		addState($('.state-overview').attr('data-state-id'), $('.state-overview .state-name').text(), $('.state-overview .core .checkbox').hasClass('active'), $('.state-overview .controls .checkbox').hasClass('active'));
	}

	$('.state-overview').slideUp();

	if($('.state-overview .core .checkbox').hasClass('active')){
		showCoreState($('.state-overview').attr('data-state-id'));
	}else{
		removeCoreState($('.state-overview').attr('data-state-id'));
	}

	if($('.state-overview .controls .checkbox').hasClass('active')){
		showControlState($('.state-overview').attr('data-state-id'));
	}else{
		removeControlState($('.state-overview').attr('data-state-id'));
	}
});

$('.state-overview .applyToAllStates').on('click', function(){
	var stateImage = $('g[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]');

	if($('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').length){
		var stateBox = $('.state-box').find('[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]');
		if($('.state-overview .core').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')){
			stateBox.find('removeStateOption').prop('checked', false);
			stateBox.find('core').prop('checked', $('.state-overview .core .checkbox').hasClass('active'));
			stateBox.find('controls').prop('checked', $('.state-overview .controls').hasClass('active'));
		}else{
			stateBox.find('removeStateOption').prop('checked', true);
		}
	}

	$('[data-controls="'+stateImage.attr('data-controls')+'"]').each(function(){
		var state = $(this).attr('data-state-id');

		var name = '';
		$.each(stateJson, function(){
			if(this.id == state){
				name = this.name;
			}
		});

		if(($('.state-overview .core .checkbox').hasClass('active') || $('.state-overview .controls .checkbox').hasClass('active')) && !$('.state-box').find('[data-state-id="'+state+'"]').length){
			addState(state, name, $('.state-overview .core .checkbox').hasClass('active'), $('.state-overview .controls .checkbox').hasClass('active'));
		}

		if(!$('.state-overview .core .checkbox').hasClass('active') || !$('.state-overview .controls .checkbox').hasClass('active')){
			removeState(state);
		}

		$('.state-overview').slideUp();

		if($('.state-overview .core .checkbox').hasClass('active')){
			showCoreState(state);
		}else{
			removeCoreState(state);
		}

		if($('.state-overview .controls .checkbox').hasClass('active')){
			showControlState(state);
		}else{
			removeControlState(state);
		}
	});
});

$('.uploadCustomLeader').on('click', function(){
	var file = $(this).parent().find('input[type="file"]');
	var container = $(this).closest('.panel-body').find('.custom-images');

	var result = uploadImage(file, container);
	setTimeout(function(){
		if(result){
			$('body').find('#custom-image-'+result.id).remove();
			container.append('<img src="/images/find/'+result.id+'" id="custom-image-'+result.id+'" class="country-leader-icon">');
			container.find('#custom-image-'+result.id).trigger('click');
		}
	}, 5000);
});

function showCoreState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').attr('data-core-state', true);
}

function showControlState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').attr('data-control-state',true);
}

function removeCoreState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').removeAttr('data-core-state');
}

function removeControlState(stateId){
	$('.map-wrapper g[data-state-id="'+stateId+'"]').removeAttr('data-control-state');
}

$('.state-overview #hide-state').on('click', function(){
	$('.map-wrapper g[data-state-id="'+$('.state-overview').attr('data-state-id')+'"]').hide();
});

$('.map-wrapper #map-reshow').on('click', function(){
	$('.map-wrapper g').show();
});

$('.map-wrapper .map-hide').on('keyup', function(){
	$('.map-wrapper svg g').show();

	var vals = $(this).val().split('|');
	$.each(vals, function(){
		if(this.length > 0){
			$('.map-wrapper svg g[data-controls*="'+this.toUpperCase()+'|"]').hide();
		}
	});
});