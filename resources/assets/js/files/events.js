$(document).ready(function(){
	$('[name="_token"]').attr('data-default', $('[name="_token"]').val());
	$('.chooseModForEvent').on('change', function(){
		var option = $(this).find('option[value="'+$(this).val()+'"]');
		if(option.attr('data-has-events-language') == 'true'){
			$('.goToEvents').show();
			$('.goToEvents a').attr('href', $('.goToEvents').attr('data-url')+$(this).val());
			$('.stepper').hide();
		}else{
			$('.goToEvents').hide();
			$('.stepper').show();
		}
	});

	$('[for="event-popup"]').on('click', function(){
		resetEventData();
		$('#display-gfx').attr('src', $('#chosen-gfx').val());
	});

	$('.event-outcome-add').on('click', function(){
		resetOutcomeData();
		resetEventData();
		$('#event-outcome form').attr('data-task', 'add');
		$('#event-outcome form').attr('action', $('#event-outcome form').attr('data-action')+'/'+$(this).closest('.event-wrapper').attr('data-event-id'));
		$('#event-outcome').slideDown();
	});

	$('body').on('click', '.event-outcome .event-edit', function(){
		resetOutcomeData();
		resetEventData();
		$('#event-outcome form').attr('data-task', 'edit');
		var json = JSON.parse($(this).parent().find('.outcome_json').text());
		$.each(json, function(key, value){
			$('#event-outcome form [name="'+key+'"]').val(value);
		});
		var eventUrl = $('#event-outcome form').attr('data-action');
		eventUrl = eventUrl.replace('/add', '/edit/'+$(this).parent().attr('data-outcome-id'));
		$('#event-outcome form').attr('action', eventUrl);
		$('#event-outcome').slideDown();

	});

	$('body').on('click', '.event-wrapper .event-edit.event', function(){
		resetOutcomeData();
		resetEventData();
		$('#event-popup form').attr('data-task', 'edit');
		var json = JSON.parse($(this).parent().find('.event-json').text());
		$.each(json, function(key, value){
			$('#event-popup form [name="'+key+'"]').val(value);
		});
		$('#event-popup form [name="chosen_gfx"]').val(json.event_gfx);
		$('#display-gfx').attr('src', json.event_gfx);
		var eventUrl = $('#event-popup form').attr('data-action');
		eventUrl = eventUrl.replace('/add', '/edit/'+$(this).parent().attr('data-event-id'));
		$('#event-popup form').attr('action', eventUrl);
		$('#event-popup').slideDown();

	});

	//Delete Outcome
	$('body').on('click', '.event-outcome .event-delete', function(){
		var confirm = window.confirm('Are you sure you want to delete '+$(this).parent().find('span').text()+'?');
		if(confirm == true){
			var eventid = $(this).closest('.event-wrapper').attr('data-event-id');
			var outcomeid = $(this).parent().attr('data-outcome-id');
			$(this).closest('.event-outcome').remove();
			$.get('/events/delete/'+eventid+'/'+outcomeid+'/');
		}
	});

	//Delete Event
	$('body').on('click', '.event-wrapper .event-delete.event', function(){
		var confirm = window.confirm('Are you sure you want to delete '+$(this).closest('.event-wrapper').find('.event-title').text()+' and all outcomes attached to it? The action is irreversable!');
		if(confirm == true){
			var eventid = $(this).closest('.event-wrapper').attr('data-event-id');
			$(this).closest('.event-wrapper').remove();
			$.get('/events/delete/'+eventid+'/');
		}
	});



	$('#event-outcome form button').on('click', function(){
		if($('#event-outcome form').attr('data-task') == 'edit'){
			

		}
		var inputs = {};
		$('#event-outcome').slideUp();
	});

	function resetOutcomeData(){
		$('#event-outcome form input, #event-outcome form textarea').each(function(){
			$(this).val($(this).attr('data-default'));
		});
	}

	function resetEventData(){
		$('#event-popup form').attr('action', $('#event-popup form').attr('data-action'));
		$('#event-popup form input, #event-popup form textarea').each(function(){
			$(this).val($(this).attr('data-default'));
		});
	}

});

window.EVENTuploadEvent = function(outcome, result, id){
	if(outcome == 'success'){
		var image = result;
		var title = $('input[name="event_title"]').val();
		var description = $('textarea[name="event_description"]').val();
		var eventType = $('select[name="event_type"]').val();
		var json = {};
		$('.can_json input, .can_json select, .can_json textarea').each(function(){
			var name = $(this).attr('name');
			var value = $(this).val();
			if(name.length > 1){
				json[name] = value;
			}
		});
		json['event_gfx'] = $('input[name="chosen_gfx"]').val();
		var event = '<div class="event-'+eventType+' event-wrapper">';
			event += '<div class="event-content">';
				event += '<div class="event-gfx"><img src="'+image+'"></div>';
				event += '<div class="event-title">'+title+'</div>';
				event += '<div class="event-description">'+description+'</div>';
				event += '<div class="event-json">'+JSON.stringify(json)+'</div>';
				event += '<div class="event-outcome-add">Add New Option</div>';
			event += '</div>';
		event += '</div>';

		$('.content.events').append(event);
		$('#focus-popup').slideUp();
	}else{
		alert('Error(s) occured: '+result);
	}


}

window.EVENTuploadOutcome = function(outcome, eventid, id){
	window.location.href = window.location.href;
}

