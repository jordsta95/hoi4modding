if(document.querySelector('.avatar')){	
	var consoleMsg1 = '%cGot experience with Javascript and/or Laravel/PHP?';
	var consoleMsg2 = '%cLooking for a project to help out on, and can add to your portfolio?';
	var consoleMsg3 = '%cWe\'re looking for someone like you!';
	var consoleMsg4 = '%cWhether is jQuery, Vue.js, or just plain old PHP, you could help speed up the HOI4 Modding development process, keeping the site up to date with the latest game updates, whilst also creating innovative tools to help the community create great mods.';
	var consoleMsg5 = 'Want to join? Email: development@hoi4modding.com along with your name/any relevant experience/language(s) you understand, code and communication for more info.';
	var lineSplit = '%c                                                      ';
	console.log(lineSplit, 'background: #1b93e1');
	console.log(consoleMsg1, 'font-size: 18px; color: #1b93e1; font-weight: 800; text-align: center; font-family: Arial, sans-serif;');
	console.log(consoleMsg2, 'font-size: 17px; color: #1675b3; text-align: center; font-family: Arial, sans-serif;');
	console.log(consoleMsg3, 'font-size: 16px; color: #333333; text-align: center; font-weight: 600; font-family: Arial, sans-serif;');
	console.log(consoleMsg4, 'font-family: Arial, sans-serif;');
	console.log(consoleMsg5);
	console.log(lineSplit, 'background: #1b93e1');
}