var nfGlobalWidth = 80;
var nfGlobalHeight = 170;

function calculateLocations(){
	$('.focus-content').not(".importing").each(function(){
		var x = $(this).attr('data-x');
		var y = $(this).attr('data-y');
		var location = {
			left: (parseInt(x) * nfGlobalWidth)+'px',
			top: (parseInt(y) * nfGlobalHeight)+'px'
		}
		$(this).css(location);
	});
	if($('.focus-content').length > 1){
		calculatePrerequisites();
	}
}

function calculatePrerequisites(){
	$('.focus-prerequisite').remove();
	$('.focus-content').each(function(){
		var thisid = $(this).attr('data-focus-id');
		var json = JSON.parse($(this).find('.focus-json').text());
		if(json.hasOwnProperty('focus_prerequisite') && json.focus_prerequisite !== null && json.focus_prerequisite !== ''){

			var preReq = json.focus_prerequisite;
			var hasAnd = false;
			var hasOr = false;

			if(preReq.indexOf('&&') > -1){
				hasAnd = true;
			}
			if(preReq.indexOf('||') > -1){
				hasOr = true;
			}

			if(hasAnd && hasOr){
				var orSplit = preReq.split('||');
				$.each(orSplit, function(){
					if(this.indexOf('&&') > -1){
						var andSplit = this.split('&&');
						$.each(andSplit, function(){
							doConnectors(thisid, this, null);
						});
					}else{
						doConnectors(thisid, this, 'or');
					}
				});		
			}else{
				if(hasAnd){
					var andSplit = preReq.split('&&');
					$.each(andSplit, function(){
						doConnectors(thisid, this, null);
					});
				}else{
					if(hasOr){
						var orSplit = preReq.split('||');
						$.each(orSplit, function(){
							doConnectors(thisid, this, 'or');
						});
					}else{
						doConnectors(thisid, preReq, null);
					}
				}
			}
		}
		if(json.hasOwnProperty('focus_mutually_exclusive') && json.focus_mutually_exclusive !== null && json.focus_mutually_exclusive !== ''){

			var preReq = json.focus_mutually_exclusive;
			var hasAnd = false;
			var hasOr = false;

			if(preReq.indexOf('&&') > -1){
				hasAnd = true;
			}
			if(preReq.indexOf('||') > -1){
				hasOr = true;
			}

			if(hasAnd && hasOr){
				var orSplit = preReq.split('||');
				$.each(orSplit, function(){
					if(this.indexOf('&&') > -1){
						var andSplit = this.split('&&');
						$.each(andSplit, function(){
							doConnectors(thisid, this, 'mutual');
						});
					}else{
						doConnectors(thisid, this, 'mutual');
					}
				});		
			}else{
				if(hasAnd){
					var andSplit = preReq.split('&&');
					$.each(andSplit, function(){
						doConnectors(thisid, this, 'mutual');
					});
				}else{
					if(hasOr){
						var orSplit = preReq.split('||');
						$.each(orSplit, function(){
							doConnectors(thisid, this, 'mutual');
						});
					}else{
						doConnectors(thisid, preReq, 'mutual');
					}
				}
			}
		}
	});
}

var hasBeenDeleting = false;
function doConnectors(first, second, extraclasses){
	var className = 'focus-prerequisite';
	if(extraclasses !== null){
		className += ' '+extraclasses;
	}
	
	var $this = $('[data-focus-id="'+first+'"]');
	var $prequisite = $('[data-focus-id="'+second+'"]');
	if($this.length && $prequisite.length){
		if(parseInt($this.css('top').replace('px', '')) >= parseInt($prequisite.css('top').replace('px', ''))){
			var height = (parseInt($this.css('top').replace('px', '')) - parseInt($prequisite.css('top').replace('px', ''))) - (10 + $prequisite.height());
			var top = parseInt($prequisite.css('top').replace('px', '')) + nfGlobalHeight;

		}else{
			var height = (parseInt($prequisite.css('top').replace('px', '')) - parseInt($this.css('top').replace('px', ''))) - (10 + $prequisite.height());
			var top = parseInt($this.css('top').replace('px', '')) + nfGlobalHeight;

		}


		if(parseInt($this.css('left').replace('px', '')) >= parseInt($prequisite.css('left').replace('px', ''))){
			var width = parseInt($this.css('left').replace('px', '')) - parseInt($prequisite.css('left').replace('px', ''));
			var left = (parseInt($this.css('left').replace('px', '')) + ($this.width() / 2 )) - 1;
			var styleFirst = 'top:'+(top - 100)+'px;left:'+(left - width)+'px;height:100px;';
			var styleSecond = 'top:'+top+'px;left:'+(left - width)+'px;width:'+width+'px;';
			var styleThird = 'top:'+top+'px;left:'+left+'px;height:'+height+'px;';
		}else{
			var width = parseInt($prequisite.css('left').replace('px', '')) - parseInt($this.css('left').replace('px', ''));
			var left = (parseInt($prequisite.css('left').replace('px', '')) + ($prequisite.width() / 2 )) - 1;
			var styleFirst = 'top:'+(top - 100)+'px;left:'+left+'px;height:100px;';
			var styleSecond = 'top:'+top+'px;left:'+(left - width)+'px;width:'+width+'px;';
			var styleThird = 'top:'+top+'px;left:'+(left - width)+'px;height:'+height+'px;';
		}
		if(extraclasses == 'mutual'){
			var styleFirst = 'top:'+(top - 0)+'px;left:'+left+'px;height:0;';
		}
	}else{
		if($('#delete-mode').prop('checked') == true){
			hasBeenDeleting = true;
		}else{
			if(hasBeenDeleting){
				//If the user has been deleting focuses, let's reload the page - save the hassle of pissing around with trying to delete stuff in jsons, and it cause errors with mutually exclusives.
				window.location.href = window.location.href;
			}else{
				if(second.length > 0){
					alert('An error has been found in your focus tree. '+$this.find('.focus-title').text()+' is referencing a focus which does not exist ('+second+') as a prerequisite or mutually exclusive focus.');
				}
			}
		}
	}

	


	var connector = '<div class="'+className+'" style="'+styleFirst+'"></div>';
		connector += '<div class="'+className+' horizontal" style="'+styleSecond+'"></div>';
		connector += '<div class="'+className+'" style="'+styleThird+'"></div>';
	$('.content.focus-tree').append(connector);
}
$(document).ready(function(){

	$('[name="mod_id"]').on('change', function(){
		if($(this).val() == 'createNewMod'){
			$('.new-mod').show();
			$('.toggleNewModStep').each(function(){
				$(this).attr('class', $(this).attr('data-new-mod-class'));
			});
			$('.create-focus .stepped').attr('data-last', '3');
		}else{
			$('.new-mod').hide();
			$('.country-wrapper').hide();
			$('.country-wrapper[data-id="'+$(this).val()+'"]').show();
			$('.toggleNewModStep').each(function(){
				$(this).attr('class', $(this).attr('data-existing-mod-class'));
			});
			$('.create-focus .stepped').attr('data-last', '2');
		}
	});
	
	$('li[for="focus-popup"]').on('click', function(){
		$('[name="_token"]').attr('data-default', $('[name="_token"]').val());
		$('#focus-popup input,#focus-popup textarea, #focus-popup select').each(function(){
			$(this).val($(this).attr('data-default'));
		});
		$('#focus-popup #display-gfx').attr('src', '/media/get/hoi4/focus_icons/goal_unknown.png');
		$('#focus-popup form').attr('action', '/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/add');
	});

	$('.content.focus-tree.edit').on('click', '.edit-focus', function(){
		$parent = $(this).parent().parent();
		var id = $parent.attr('data-focus-id');
		var treeid = $('#focus-popup form').attr('data-treeid');
		var newAction = '/focus-tree/edit/'+treeid+'/edit/'+id;
		$('#focus-popup form').attr('action', newAction);

		var img = $parent.find('.focus-image').attr('src');
		var title = $parent.find('.focus-title').text();
		var desc = $parent.find('.focus-description div').text();
		var json = JSON.parse($parent.find('.focus-json').text());

		$('#focus-popup [name="focus_title"]').val(title);
		$('#focus-popup [name="focus_description"]').val(desc);
		$('#focus-popup #chosen-gfx').val(img);
		$('#focus-popup #display-gfx').attr('src', img);

		$.each(json, function(name, value){
			$('#focus-popup [name="'+name+'"]').val(value);
		});

		$('#focus-popup').slideDown();
	});

	$('.nficon').on('click', function(){
		var src = $(this).attr('src');
		nfGFX(src);
	});

	$('.uses-builder').on('focus', function(){
		$('#focus-builder').attr('data-building', $(this).attr('name'));
		$('#focus-builder').slideDown();
	});

	$('#submit-build').on('click', function(){
		var input = $('#focus-builder').attr('data-building');
		$('[name="'+input+'"]').val($('.dragged.main').text().replace(/	/g, '').replace(/\n/g, '\r\n').replace(/}/g, '}\r\n'));
		$('.dragged.main').html('');
		$('#focus-builder').slideUp();
	});

	if($('.content.focus-tree').length){
		$( ".content.focus-tree.edit .focus-content" ).draggable({ 
			grid: [ nfGlobalWidth, nfGlobalHeight ],
			snap: ".content.focus-tree",
			stop: function(){
				var left = $(this).css('left');
				var top = $(this).css('top');
				left = parseInt(left.replace('px', ''));
				top = parseInt(top.replace('px', ''));
				if(0 > left){
					left = 0;
				}
				if(0 > top){
					top = 0;
				}
				$(this).attr('data-y', (top/nfGlobalHeight));
				$(this).attr('data-x', (left/nfGlobalWidth));
				$(this).css({
					left:left+'px',
					top:top+'px'
				})
				var json = $(this).find('.focus-json').text();
				json = JSON.parse(json);
				json.focus_x = (left/nfGlobalWidth);
				json.focus_y = (top/nfGlobalHeight);
				json = JSON.stringify(json);
				$(this).find('.focus-json').text(json);
				calculatePrerequisites();
				$.post('/focus-tree/quickedit/'+$('#focus-popup form').attr('data-treeid')+'/edit/'+$(this).attr('data-focus-id'),
					{
						_token: $('[name="_token"]').val(),
						action: "updateXY",
						x: (left/nfGlobalWidth),
						y: (top/nfGlobalHeight)
					}
				);
			}
		});
	}
	$('#old-tool-output-button').on('click', function(){
		

		$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/importFromOldTool', {
			_token: $('[name="_token"]').val(),
			password: $('#old-tool-output-form input[name="pw"]').val()
		}, function(data){
			$('#old-tool-output').html(data);
			$('#focus-import-old-popup .loader-switch').show();
			$('#focus-import-old-popup .loader').hide();
		});
	});

	$('body').on('click', '#importviajson', function(){
		$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/importViaJSON', {
			_token: $('[name="_token"]').val(),
			tree: $('#existing-focus-tree-output').val()
		}, function(data){
			window.location.href = window.location.href;
		});
	});

	$(document).on('click', 'button.imported-focus', function(){
		$(this).parent().parent().hide();
		$(this).parent().parent().next().hide();
	});

	var draggedNum = 0;
	var droppableOptions = {
        accept: '.builder-example',
        greedy: true, 
        hoverClass: "dragged-hover", /* highlight the current drop zone */
        drop: function (event, ui) {
        	$d = $(ui.draggable).clone();
        	var id = $d.attr('id').replace('_drag','');
        	var game_id = $('#'+$d.attr('id').replace('_drag','_id')).text()
	 		var outcome = $('#'+id+'_defaultoutcome').text();
            if(outcome == 'new-level'){
				var toAdd = '<div class="block">';
					toAdd += game_id.replace('TAG', '<span class="choose-country" id="country_'+id+draggedNum+'">CHOOSE COUNTRY</span>').replace('state_id', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>').replace('STATEID', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>')+' = {'
						toAdd += '<div class="new-level dragged">';
						toAdd += '</div> ';
					toAdd += '}'
				toAdd += '</div>';
			}else{
				var toAdd = '<div class="block">';
					toAdd += game_id.replace('TAG', '<span class="choose-country" id="country_'+id+draggedNum+'">CHOOSE COUNTRY</span>').replace('state_id', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>').replace('STATEID', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>');
						toAdd += '<div class="quickedit textarea">';
						toAdd += outcome.replace('TAG', '<span class="choose-country" id="country_'+id+draggedNum+'">CHOOSE COUNTRY</span>').replace('state_id', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>').replace('STATEID', '<span class="choose-state" id="state_'+id+draggedNum+'">CHOOSE STATE</span>');
						toAdd += '</div> '
					toAdd+= '</div> '
			}
			$d.html(toAdd);
			$d.removeClass('builder-example');
            $d.find('.dragged').droppable(droppableOptions);
            $(this).append($d);
            draggedNum++;
        }
    };

    if($('.content.focus-tree,.content.events,.content.ideas').length){
    	$(".dragged.main").droppable(droppableOptions);
    }

    $('#clear-build').on('click', function(){
    	$(".dragged.main").html('');
    });

    var builderTimer; 
    $('#search-builder').on('keydown', function () {
	  clearTimeout(builderTimer);
	});
	$('#search-builder').on('keyup', function(){
		searchBuilder();
	});
	function searchBuilder(){
		var searchField = $('#search-builder').val();
		var count = 1;
		var output = "";
		var data = JSON.parse(window.localStorage.getItem('BuilderResults'));
		$.each(data, function(key, val){
			if(val.game_id.indexOf(searchField) !== -1 || val.description.indexOf(searchField) !== -1){
				if(val.uses_tag){
					var tag = "yes";
				}else{
					var tag = "no";
				}
				if(val.uses_state){
					var state = "yes";
				}else{
					var state = "no";
				}
				output += '<div class="search-result">';
					output += '<p class="build-description" id="builder-'+val.id+'" tag="'+tag+'" state="'+state+'" >'+val.description+'</p>';
					output += '<div style="display:none;" id="builder-'+val.id+'_id">'+val.game_id+'</div>';
					output += '<div class="default-outcome" id="builder-'+val.id+'_defaultoutcome">'+val.default_outcome+'</div>';
					output += '<div class="builder-example" id="builder-'+val.id+'_drag">'+val.example+'</div>';
				output += '</div>';
			}
		});
		$('.builder-search-output').html(output);
		$('.builder-example').draggable({
	        helper: 'clone'
	    });
		
	}

	$('.builder-search-output').on('click', '.build-description', function(){
		$(this).parent().find('.builder-example').slideToggle();
	});

	if($(".tag-search-output").length){
		var data = JSON.parse(window.localStorage.getItem('CountryTags'));
		var output = '';
		$.each(data, function(key, val){
		 	output += '<p id="'+val.tag+'" class="focus_tree_tags">'+val.country+'</p>';
		});
		$(".tag-search-output").prepend(output);
	}

	$("#focus-tag input").keyup(function(){
		var searchField = $(this).val();
		var regex = new RegExp(searchField, "i");
		var count = 1;
		var output = "";
		if(searchField.length > 0){
			$('.focus_tree_tags').each(function(){
				if (($(this).text().search(regex) != -1) || ($(this).attr('id').search(regex) != -1)) {
			 		$(this).show();
				}else{
					$(this).hide();
				}
			});
		}else{
			$('.focus_tree_tags').show();
		}
	});
	$(document).on('click', ".focus_tree_tags", function() {
		var tagid = $(this).attr("id");
		var toedit = $('#focus-tag').attr('data-editing');
		$('#'+toedit).text(tagid);
		$('#'+toedit).val(tagid);
		$('#focus-tag').slideUp();
	});

	$('#search-states').keyup(function(){
		var searchField = $(this).val();
		var regex = new RegExp(searchField, "i");
		var count = 1;
		var output = "";
		var data = JSON.parse(window.localStorage.getItem('StateResults'));
		$.each(data, function(key, val){
			if ((val.id.search(regex) != -1) || (val.name.search(regex) != -1)) {
		 		output += '<p id="state_'+val.id+'" class="searched_states">'+val.name+'</p>';
			}
		});
		$('.state-search-output').html(output);
 
	});
	
	$(document).on('click', ".searched_states", function() {
		var tagid = $(this).attr("id").replace("state_","");
		var toedit = $('#focus-state').attr('data-editing');
		$('#'+toedit).text(tagid);
		$('#'+toedit).val(tagid);
		$("#focus-state").slideUp();
	});

	$(document).on('click', '.focus-content', function(){
		if($('#delete-mode').prop('checked') == true){
			var confirm = window.confirm('Are you sure you want to delete '+$(this).find('.focus-title').text()+'? The action is irreversable!');
			if(confirm == true){
				var id = $(this).attr('data-focus-id');
				$(this).remove();
				$('.focus-prerequisite').remove();
				calculateLocations();
				$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/delete/'+id+'/', {
					_token: $('[name="_token"]').val()
				});
			}
		}
	});
	
	$('#delete-all').on('click', function(){
		//Delete all!
		var confirm = window.confirm('Are you sure you want to delete everything? The action is irreversable!');
		if(confirm == true){
			$('.focus-content').remove();
			$('.focus-prerequisite').remove();
			$.post('/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/delete/all/', {
				_token: $('[name="_token"]').val()
			});
		}
	});

	$('[name="focus_mutually_exclusive"], [name="focus_prerequisite"]').on('focus', function(){
		$('#focus-chooser').attr('data-editing', $(this).attr('name'));
		$("#select-focus").html("");
		$('.focus-content').each(function () {
			var focusname = $(this).find('.focus-title').text();
			var focusimg = $(this).find('.focus-image').attr('src');
			var focusid = $(this).attr('data-focus-id');//generateId(focusname);
			var focus = '<p class="select-focus" id="'+focusid+'_sel">';
				focus += '<img src="'+focusimg+'" align="left" width="25px;">'+focusname+'</p>'
			$("#select-focus").append(focus);
		});
		$('#focus-chooser input').val('');
		$("#focus-chooser").slideDown();
	});

	$(document).on('click', ".select-focus", function() {
		var toedit = $('#focus-chooser').attr('data-editing');
		if($("#select-and").prop('checked') == true || $("#select-or").prop('checked') == true){
			if($("#select-and").prop('checked') == true){
				var connection = "&&";
			}
			if($("#select-or").prop('checked') == true){
				var connection = "||";
			}
			var msid = (this.id).replace("_sel","");
			
			$('[name="'+toedit+'"]').val($('[name="'+toedit+'"]').val() +  msid + connection);
			$(this).remove();
		}else{
			var msid = (this.id).replace("_sel","");

			$('[name="'+toedit+'"]').val($('[name="'+toedit+'"]').val() + msid);
			$("#select-focus").html("");
			$("#focus-chooser").slideUp();
		}
	});

	$('#multiMove').on('change', function(){
		if($(this).prop('checked')){
			$( ".focus-content" ).draggable( 'destroy' );
			$( ".content.focus-tree" ).selectable({
				stop: function( event, ui ) {
					$('.focus-content.ui-selected').each(function(){
						var elem = '<div class="addedFocus">'
							elem += '<input name="focus[]" value="'+$(this).attr('data-focus-id')+'">';
							elem += '<span>'+$(this).find('.focus-title').text()+' <i class="fa fa-times"></i></span>';
						elem += '</div>';
						$('#selectedFocuses').append(elem);
					});
					$('#focus-multi-move').slideDown();
				}
			});
		}else{
			window.location.href = window.location.href;
		}
	});

	$('body').on('click', '.addedFocus .fa-times', function(){
		$(this).closest('.addedFocus').remove();
	});

	$('.focus-tree-raw-toggle').on('click', function(){
		$('.focus-tree-raw').toggleClass('closed');
	});

	var focusTreeErrors = $('.focus-tree-raw .focus-error').length;
	if(focusTreeErrors > 0){
		$('.focus-tree-raw-toggle').append('<span class="errors">'+focusTreeErrors+'</span>');
	}

	$('.showFocusError').on('click', function(){
		$('.focus-tree-raw-preview').each(function(){
			if($(this).find('.focus-error').length){
				$(this).show();
			}else{
				$(this).hide();
			}
		});
	});

	$('#focusTreeFile').on('change', function(){
		var reader = new FileReader();
    	reader.readAsText(document.getElementById('focusTreeFile').files[0], "UTF-8");
    	reader.onload = function (evt) {
	        $('#existing-focus-tree-output').val(JSON.stringify(toJSON(evt.target.result, 0)));
	    }
	});

	$("#treetojson").click(function(){

		formdata = new FormData();
		formdata.append('_token', $('[name="_token"]').val());
	    if($('#focusTreeFile').prop('files').length > 0)
	    {
	        file =$('#focusTreeFile').prop('files')[0];
	        formdata.append("focusTreeFile", file);
	    }
	    if($('#localisationFile').prop('files').length > 0)
	    {
	        file =$('#localisationFile').prop('files')[0];
	        formdata.append("localisationFile", file);
	    }
	    if($('#existing-focus-tree-output').val().length > 0){
	    	formdata.append('JSON', $('#existing-focus-tree-output').val());
	    }
		$.ajax({
		    url: '/focus-tree/edit/'+$('#focus-popup form').attr('data-treeid')+'/importFromExistingFile',
		    type: "POST",
		    data: formdata,
		    processData: false,
		    contentType: false,
		    success: function (result) {
		  //   	$('#focus-import-existing-tree .loader').hide();
				// $('#focus-import-existing-tree .loader-switch').show();
				$('#focus-import-existing-tree .imported-textboxes').html(result);
		  //       var result = toJSON($("#existing-focus-tree").val().replace(/\t/g,""),0);
		  //       $('#import-result').html('Found '+result.focus_tree.focus.length+' focuses.<br>Press the button below to import them.<p><button class="generic_button" id="importviajson">Import Focus Tree</button></p>');
				// $("#existing-focus-tree-output").val(JSON.stringify(result));
		
		    }
		});

		
	});

});

$(window).load(function(){
	calculateLocations();
});

$('#customgfx').on('change', function(){
	var file = $(this);
	var container = $('#choosegfx .custom-images');
	var type = 'focus';
	if($('#event-popup').length){
		type = 'event';
	}
	if($('#idea-popup').length){
		type = 'idea';
	}
	var result = uploadImage(file, container, type);
});

window.NFunauthorisedUser = function(){
	alert('You are not authorised to edit this.')
}

window.NFsessionExpired = function(){
	alert('Your session has expired. Please relogin to continue using this tool.');
}

window.NFuploadOutcome = function(outcome, result, id){
	if(outcome == 'success'){
		var image = result;
		var title = $('input[name="focus_title"]').val();
		var description = $('textarea[name="focus_description"]').val();
		var json = {};
		$('.can_json input, .can_json select, .can_json textarea').each(function(){
			var name = $(this).attr('name');
			var value = $(this).val();
			if(name.length > 1){
				json[name] = value;
			}
		});

		var focus = '<div class="focus-content" data-x="'+$('input[name="focus_x"]').val()+'" data-y="'+$('input[name="focus_y"]').val()+'" data-focus-id="'+id+'">';
			focus += '<img src="'+image+'" class="focus-image">';
			focus += '<p class="focus-title">'+title+'</p>';
			focus += '<div class="focus-description"><p class="edit-focus">Edit</p><div>'+description+'</div></div>';
			focus += '<div class="focus-json">'+JSON.stringify(json)+'</div>';
		focus += '</div>';

		$('.content.focus-tree').append(focus);
		
		$( ".content.focus-tree.edit .focus-content" ).draggable({ 
			grid: [ nfGlobalWidth, nfGlobalHeight ],
			snap: ".content.focus-tree",
			stop: function(){
				var left = $(this).css('left');
				var top = $(this).css('top');
				left = parseInt(left.replace('px', ''));
				top = parseInt(top.replace('px', ''));
				$(this).attr('data-y', (top/nfGlobalHeight));
				$(this).attr('data-x', (left/nfGlobalWidth));
				calculatePrerequisites();
				$.post('/focus-tree/quickedit/'+$('#focus-popup form').attr('data-treeid')+'/edit/'+$(this).attr('data-focus-id'),
					{
						_token: $('[name="_token"]').val(),
						action: "updateXY",
						x: (left/nfGlobalWidth),
						y: (top/nfGlobalHeight)
					}
				);
			}
		});
		$('#focus-popup').slideUp();
		var sync = syncFocusConnections(id);
		if(sync){
			window.location.href = window.location.href;
		}
		calculateLocations();
	}else{
		alert('Error(s) occured: '+result);
	}
}

window.NFupdateOutcome = function(outcome, result, id){
	if(outcome == 'success'){
		$('[data-focus-id="'+id+'"]').remove();
		var image = result;
		var title = $('input[name="focus_title"]').val();
		var description = $('textarea[name="focus_description"]').val();
		var json = {};
		$('.can_json input, .can_json select, .can_json textarea').each(function(){
			var name = $(this).attr('name');
			var value = $(this).val();
			json[name] = value;
		});

		var focus = '<div class="focus-content" data-x="'+$('input[name="focus_x"]').val()+'" data-y="'+$('input[name="focus_y"]').val()+'" data-focus-id="'+id+'">';
			focus += '<div class="focus-description"><p class="edit-focus">Edit</p><div>'+description+'</div></div>';
			focus += '<img src="'+image+'" class="focus-image">';
			focus += '<p class="focus-title">'+title+'</p>';
			focus += '<div class="focus-json">'+JSON.stringify(json)+'</div>';
		focus += '</div>';

		$('.content.focus-tree').append(focus);
		
		$( ".content.focus-tree.edit .focus-content" ).draggable({ 
			grid: [ nfGlobalWidth, nfGlobalHeight ],
			snap: ".content.focus-tree",
			stop: function(){
				var left = $(this).css('left');
				var top = $(this).css('top');
				left = parseInt(left.replace('px', ''));
				top = parseInt(top.replace('px', ''));
				$(this).attr('data-y', (top/nfGlobalHeight));
				$(this).attr('data-x', (left/nfGlobalWidth));
				calculatePrerequisites();
				$.post('/focus-tree/quickedit/'+$('#focus-popup form').attr('data-treeid')+'/edit/'+$(this).attr('data-focus-id'),
					{
						_token: $('[name="_token"]').val(),
						action: "updateXY",
						x: (left/nfGlobalWidth),
						y: (top/nfGlobalHeight)
					}
				);
			}
		});
		$('#focus-popup').slideUp();
		var sync = syncFocusConnections(id);
		if(sync){
			window.location.href = window.location.href;
		}
		calculateLocations();
	}else{
		alert('Error(s) occured: '+result);
	}
}

function syncFocusConnections(focusToAdd){
	var focus = JSON.parse($('body').find('[data-focus-id="'+focusToAdd+'"] .focus-json').text());

	if(focus.hasOwnProperty('focus_mutually_exclusive')){
		//We only want to do this if it has one
		if(focus.focus_mutually_exclusive !== null && focus.focus_mutually_exclusive !== ''){
			var idsToSync = focus.focus_mutually_exclusive.replace(/&&/g, '||');
			idsToSync = idsToSync.split('||');
			var hasUpdated = false;
			$.each(idsToSync, function(key, value){
				var focusCheck = createFocusInfoToExistingJSONs(value, 'focus_mutually_exclusive', focusToAdd);
				if(focusCheck){
					hasUpdated = true;
				}
			});
			if(hasUpdated){
				return true;
			}
		}
	}

		
}

function createFocusInfoToExistingJSONs(id, property, focusToAdd){
	var json = JSON.parse($('[data-focus-id="'+id+'"]').find('.focus-json').text());
	if(json.hasOwnProperty(property)){
		//Check if we can check it's there or not.
		if(json[property] !== null && json[property] !== ''){
			if(json[property].indexOf(focusToAdd) > -1){
				//It's got it, so ignore it - kinda buggy with 1-10, but we don't care about them
				return false;
			}else{
				json[property] = json[property]+'&&'+focusToAdd;	
			}
		}else{
			json[property] = focusToAdd;
		}
	}else{
		if(json[property].length > 0){
			json[property] = json[property]+'&&'+focusToAdd;
		}else{
			json[property] = focusToAdd;
		}
	}
	$('[data-focus-id="'+id+'"]').find('.focus-json').text(JSON.stringify(json));
	return true;
}

function nfGFX(src){
	$('#display-gfx').attr('src', src);
	$('#chosen-gfx').val(src);
	$('#choosegfx').slideUp();
}


// $('#treetojson').on('click', function(){
// 	$('<p>This may take a minute to upload.</p><p><strong>Once upload is complete, your focus tree will be scheduled for import</strong>').insertAfter($(this));
// 	$(this).hide();
// });

$('.filterGfx').on('keyup', function(){
	var val = $(this).val().toLowerCase();
	$('.nficon').show();
	if(val.length === 0){
		return;
	}
	$('.nficon').each(function(){
		var id = $(this).attr('id');
		var regex = new RegExp(val, "i");
		if (id.search(regex) == -1) {
			$(this).hide();
		}
	});
});